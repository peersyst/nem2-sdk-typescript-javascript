import { Observable } from 'rxjs';
import { MerkleProofInfo } from '../model/blockchain/MerkleProofInfo';
import { NetworkType } from '../model/blockchain/NetworkType';
import { Statement } from '../model/receipt/Statement';
import { Http } from './Http';
import { ReceiptRepository } from './ReceiptRepository';
/**
 * Receipt http repository.
 *
 * @since 1.0
 */
export declare class ReceiptHttp extends Http implements ReceiptRepository {
    /**
     * Constructor
     * @param url
     * @param networkType
     */
    constructor(url: string, networkType?: NetworkType);
    /**
     * Get the merkle path for a given a receipt statement hash and block
     * Returns the merkle path for a [receipt statement or resolution](https://nemtech.github.io/concepts/receipt.html)
     * linked to a block. The path is the complementary data needed to calculate the merkle root.
     * A client can compare if the calculated root equals the one recorded in the block header,
     * verifying that the receipt was linked with the block.
     * @param height The height of the block.
     * @param hash The hash of the receipt statement or resolution.
     * @return Observable<MerkleProofInfo>
     */
    getMerkleReceipts(height: string, hash: string): Observable<MerkleProofInfo>;
    /**
     * Gets an array receipts for a block height.
     * @param height - Block height from which will be the first block in the array
     * @param queryParams - (Optional) Query params
     * @returns Observable<Statement>
     */
    getBlockReceipts(height: string): Observable<Statement>;
}
