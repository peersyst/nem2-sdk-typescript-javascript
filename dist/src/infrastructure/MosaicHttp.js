"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const rxjs_1 = require("rxjs");
const operators_1 = require("rxjs/operators");
const PublicAccount_1 = require("../model/account/PublicAccount");
const MosaicFlags_1 = require("../model/mosaic/MosaicFlags");
const MosaicId_1 = require("../model/mosaic/MosaicId");
const MosaicInfo_1 = require("../model/mosaic/MosaicInfo");
const UInt64_1 = require("../model/UInt64");
const api_1 = require("./api");
const Http_1 = require("./Http");
/**
 * Mosaic http repository.
 *
 * @since 1.0
 */
class MosaicHttp extends Http_1.Http {
    /**
     * Constructor
     * @param url
     * @param networkType
     */
    constructor(url, networkType) {
        super(url, networkType);
        this.mosaicRoutesApi = new api_1.MosaicRoutesApi(url);
    }
    /**
     * Gets the MosaicInfo for a given mosaicId
     * @param mosaicId - Mosaic id
     * @returns Observable<MosaicInfo>
     */
    getMosaic(mosaicId) {
        return this.getNetworkTypeObservable().pipe(operators_1.mergeMap((networkType) => rxjs_1.from(this.mosaicRoutesApi.getMosaic(mosaicId.toHex())).pipe(operators_1.map(({ body }) => new MosaicInfo_1.MosaicInfo(new MosaicId_1.MosaicId(body.mosaic.id), UInt64_1.UInt64.fromNumericString(body.mosaic.supply), UInt64_1.UInt64.fromNumericString(body.mosaic.startHeight), PublicAccount_1.PublicAccount.createFromPublicKey(body.mosaic.ownerPublicKey, networkType), body.mosaic.revision, new MosaicFlags_1.MosaicFlags(body.mosaic.flags), body.mosaic.divisibility, UInt64_1.UInt64.fromNumericString(body.mosaic.duration))), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))))));
    }
    /**
     * Gets MosaicInfo for different mosaicIds.
     * @param mosaicIds - Array of mosaic ids
     * @returns Observable<MosaicInfo[]>
     */
    getMosaics(mosaicIds) {
        const mosaicIdsBody = {
            mosaicIds: mosaicIds.map((id) => id.toHex()),
        };
        return this.getNetworkTypeObservable().pipe(operators_1.mergeMap((networkType) => rxjs_1.from(this.mosaicRoutesApi.getMosaics(mosaicIdsBody)).pipe(operators_1.map(({ body }) => body.map((mosaicInfoDTO) => {
            return new MosaicInfo_1.MosaicInfo(new MosaicId_1.MosaicId(mosaicInfoDTO.mosaic.id), UInt64_1.UInt64.fromNumericString(mosaicInfoDTO.mosaic.supply), UInt64_1.UInt64.fromNumericString(mosaicInfoDTO.mosaic.startHeight), PublicAccount_1.PublicAccount.createFromPublicKey(mosaicInfoDTO.mosaic.ownerPublicKey, networkType), mosaicInfoDTO.mosaic.revision, new MosaicFlags_1.MosaicFlags(mosaicInfoDTO.mosaic.flags), mosaicInfoDTO.mosaic.divisibility, UInt64_1.UInt64.fromNumericString(mosaicInfoDTO.mosaic.duration));
        })), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))))));
    }
    /**
     * Gets an array of mosaics created for a given account address.
     * @summary Get mosaics created by an account
     * @param address Account address.
     */
    getMosaicsFromAccount(address) {
        return this.getNetworkTypeObservable().pipe(operators_1.mergeMap((networkType) => rxjs_1.from(this.mosaicRoutesApi.getMosaicsFromAccount(address.plain())).pipe(operators_1.map(({ body }) => body.mosaics.map((mosaicInfo) => new MosaicInfo_1.MosaicInfo(new MosaicId_1.MosaicId(mosaicInfo.id), UInt64_1.UInt64.fromNumericString(mosaicInfo.supply), UInt64_1.UInt64.fromNumericString(mosaicInfo.startHeight), PublicAccount_1.PublicAccount.createFromPublicKey(mosaicInfo.ownerPublicKey, networkType), mosaicInfo.revision, new MosaicFlags_1.MosaicFlags(mosaicInfo.flags), mosaicInfo.divisibility, UInt64_1.UInt64.fromNumericString(mosaicInfo.duration)))), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))))));
    }
    /**
     * Gets mosaics created for a given array of addresses.
     * @summary Get mosaics created for given array of addresses
     * @param addresses Array of addresses
     */
    getMosaicsFromAccounts(addresses) {
        const accountIdsBody = {
            addresses: addresses.map((address) => address.plain()),
        };
        return this.getNetworkTypeObservable().pipe(operators_1.mergeMap((networkType) => rxjs_1.from(this.mosaicRoutesApi.getMosaicsFromAccounts(accountIdsBody)).pipe(operators_1.map(({ body }) => body.mosaics.map((mosaicInfoDTO) => {
            return new MosaicInfo_1.MosaicInfo(new MosaicId_1.MosaicId(mosaicInfoDTO.id), UInt64_1.UInt64.fromNumericString(mosaicInfoDTO.supply), UInt64_1.UInt64.fromNumericString(mosaicInfoDTO.startHeight), PublicAccount_1.PublicAccount.createFromPublicKey(mosaicInfoDTO.ownerPublicKey, networkType), mosaicInfoDTO.revision, new MosaicFlags_1.MosaicFlags(mosaicInfoDTO.flags), mosaicInfoDTO.divisibility, UInt64_1.UInt64.fromNumericString(mosaicInfoDTO.duration));
        })), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))))));
    }
}
exports.MosaicHttp = MosaicHttp;
//# sourceMappingURL=MosaicHttp.js.map