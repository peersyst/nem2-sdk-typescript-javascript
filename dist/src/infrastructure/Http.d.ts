import { Observable } from 'rxjs';
import { NetworkType } from '../model/blockchain/NetworkType';
import { QueryParams } from './QueryParams';
/**
 * Http extended by all http services
 */
export declare abstract class Http {
    protected readonly url: string;
    protected networkType: NetworkType;
    /**
     * Constructor
     * @param url Base catapult-rest url
     * @param networkType
     */
    constructor(url: string, networkType?: NetworkType);
    getNetworkTypeObservable(): Observable<NetworkType>;
    queryParams(queryParams?: QueryParams): any;
    errorHandling(error: any): Error;
}
