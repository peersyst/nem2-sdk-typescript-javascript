"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const rxjs_1 = require("rxjs");
const operators_1 = require("rxjs/operators");
const DtoMapping_1 = require("../core/utils/DtoMapping");
const restrictionAccountRoutesApi_1 = require("./api/restrictionAccountRoutesApi");
const Http_1 = require("./Http");
/**
 * RestrictionAccount http repository.
 *
 * @since 1.0
 */
class RestrictionAccountHttp extends Http_1.Http {
    /**
     * Constructor
     * @param url
     * @param networkType
     */
    constructor(url, networkType) {
        super(url, networkType);
        this.restrictionAccountRoutesApi = new restrictionAccountRoutesApi_1.RestrictionAccountRoutesApi(url);
    }
    /**
     * Get Account restrictions.
     * @param publicAccount public account
     * @returns Observable<AccountRestrictions[]>
     */
    getAccountRestrictions(address) {
        return rxjs_1.from(this.restrictionAccountRoutesApi.getAccountRestrictions(address.plain()))
            .pipe(operators_1.map(({ body }) => DtoMapping_1.DtoMapping.extractAccountRestrictionFromDto(body).accountRestrictions.restrictions), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
    /**
     * Get Account restrictions.
     * @param address list of addresses
     * @returns Observable<AccountRestrictionsInfo[]>
     */
    getAccountRestrictionsFromAccounts(addresses) {
        const accountIds = {
            addresses: addresses.map((address) => address.plain()),
        };
        return rxjs_1.from(this.restrictionAccountRoutesApi.getAccountRestrictionsFromAccounts(accountIds))
            .pipe(operators_1.map(({ body }) => body.map((restriction) => {
            return DtoMapping_1.DtoMapping.extractAccountRestrictionFromDto(restriction).accountRestrictions;
        })), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
}
exports.RestrictionAccountHttp = RestrictionAccountHttp;
//# sourceMappingURL=RestrictionAccountHttp.js.map