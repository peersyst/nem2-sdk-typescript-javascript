import { Observable } from 'rxjs';
import { Address } from '../model/account/Address';
import { MosaicId } from '../model/mosaic/MosaicId';
import { MosaicInfo } from '../model/mosaic/MosaicInfo';
/**
 * Mosaic interface repository.
 *
 * @since 1.0
 */
export interface MosaicRepository {
    /**
     * Gets a MosaicInfo for a given mosaicId
     * @param mosaicId - Mosaic id
     * @returns Observable<MosaicInfo>
     */
    getMosaic(mosaicId: MosaicId): Observable<MosaicInfo>;
    /**
     * Gets MosaicInfo for different mosaicIds.
     * @param mosaicIds - Array of mosaic ids
     * @returns Observable<MosaicInfo[]>
     */
    getMosaics(mosaicIds: MosaicId[]): Observable<MosaicInfo[]>;
    /**
     * Gets mosaics created for a given address.
     * @summary Get mosaics created for given address
     * @param address Address
     */
    getMosaicsFromAccount(address: Address): Observable<MosaicInfo[]>;
    /**
     * Gets mosaics created for a given array of addresses.
     * @summary Get mosaics created for given array of addresses
     * @param addresses Address
     */
    getMosaicsFromAccounts(addresses: Address[]): Observable<MosaicInfo[]>;
}
