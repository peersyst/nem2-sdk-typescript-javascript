"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const rxjs_1 = require("rxjs");
const operators_1 = require("rxjs/operators");
const AccountInfo_1 = require("../model/account/AccountInfo");
const ActivityBucket_1 = require("../model/account/ActivityBucket");
const Address_1 = require("../model/account/Address");
const Mosaic_1 = require("../model/mosaic/Mosaic");
const MosaicId_1 = require("../model/mosaic/MosaicId");
const UInt64_1 = require("../model/UInt64");
const api_1 = require("./api");
const Http_1 = require("./Http");
const CreateTransactionFromDTO_1 = require("./transaction/CreateTransactionFromDTO");
/**
 * Account http repository.
 *
 * @since 1.0
 */
class AccountHttp extends Http_1.Http {
    /**
     * Constructor
     * @param url
     * @param networkType
     */
    constructor(url, networkType) {
        super(url, networkType);
        this.accountRoutesApi = new api_1.AccountRoutesApi(url);
    }
    /**
     * Gets an AccountInfo for an account.
     * @param address Address
     * @returns Observable<AccountInfo>
     */
    getAccountInfo(address) {
        return rxjs_1.from(this.accountRoutesApi.getAccountInfo(address.plain())).pipe(operators_1.map(({ body }) => this.toAccountInfo(body)), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
    /**
     * Gets AccountsInfo for different accounts.
     * @param addresses List of Address
     * @returns Observable<AccountInfo[]>
     */
    getAccountsInfo(addresses) {
        const accountIdsBody = {
            addresses: addresses.map((address) => address.plain()),
        };
        return rxjs_1.from(this.accountRoutesApi.getAccountsInfo(accountIdsBody)).pipe(operators_1.map(({ body }) => body.map(this.toAccountInfo)), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
    /**
     * This method maps a AccountInfoDTO from rest to the SDK's AccountInfo model object.
     *
     * @internal
     * @param {AccountInfoDTO} dto AccountInfoDTO the dto object from rest.
     * @returns AccountInfo model
     */
    toAccountInfo(dto) {
        return new AccountInfo_1.AccountInfo(Address_1.Address.createFromEncoded(dto.account.address), UInt64_1.UInt64.fromNumericString(dto.account.addressHeight), dto.account.publicKey, UInt64_1.UInt64.fromNumericString(dto.account.publicKeyHeight), dto.account.accountType.valueOf(), dto.account.linkedAccountKey, dto.account.activityBuckets.map((bucket) => {
            return new ActivityBucket_1.ActivityBucket(bucket.startHeight, bucket.totalFeesPaid, bucket.beneficiaryCount, bucket.rawScore);
        }), dto.account.mosaics.map((mosaicDTO) => new Mosaic_1.Mosaic(new MosaicId_1.MosaicId(mosaicDTO.id), UInt64_1.UInt64.fromNumericString(mosaicDTO.amount))), UInt64_1.UInt64.fromNumericString(dto.account.importance), UInt64_1.UInt64.fromNumericString(dto.account.importanceHeight));
    }
    /**
     * Gets an array of confirmed transactions for which an account is signer or receiver.
     * @param address - * Address can be created rawAddress or publicKey
     * @param queryParams - (Optional) Query params
     * @returns Observable<Transaction[]>
     */
    getAccountTransactions(address, queryParams) {
        return rxjs_1.from(this.accountRoutesApi.getAccountTransactions(address.plain(), this.queryParams(queryParams).pageSize, this.queryParams(queryParams).id, this.queryParams(queryParams).order)).pipe(operators_1.map(({ body }) => body.map((transactionDTO) => {
            return CreateTransactionFromDTO_1.CreateTransactionFromDTO(transactionDTO);
        })), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
    /**
     * Gets an array of transactions for which an account is the recipient of a transaction.
     * A transaction is said to be incoming with respect to an account if the account is the recipient of a transaction.
     * @param address - * Address can be created rawAddress or publicKey
     * @param queryParams - (Optional) Query params
     * @returns Observable<Transaction[]>
     */
    getAccountIncomingTransactions(address, queryParams) {
        return rxjs_1.from(this.accountRoutesApi.getAccountIncomingTransactions(address.plain(), this.queryParams(queryParams).pageSize, this.queryParams(queryParams).id, this.queryParams(queryParams).order)).pipe(operators_1.map(({ body }) => body.map((transactionDTO) => {
            return CreateTransactionFromDTO_1.CreateTransactionFromDTO(transactionDTO);
        })), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
    /**
     * Gets an array of transactions for which an account is the sender a transaction.
     * A transaction is said to be outgoing with respect to an account if the account is the sender of a transaction.
     * @param address - * Address can be created rawAddress or publicKey
     * @param queryParams - (Optional) Query params
     * @returns Observable<Transaction[]>
     */
    getAccountOutgoingTransactions(address, queryParams) {
        return rxjs_1.from(this.accountRoutesApi.getAccountOutgoingTransactions(address.plain(), this.queryParams(queryParams).pageSize, this.queryParams(queryParams).id, this.queryParams(queryParams).order)).pipe(operators_1.map(({ body }) => body.map((transactionDTO) => {
            return CreateTransactionFromDTO_1.CreateTransactionFromDTO(transactionDTO);
        })), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
    /**
     * Gets the array of transactions for which an account is the sender or receiver and which have not yet been included in a block.
     * Unconfirmed transactions are those transactions that have not yet been included in a block.
     * Unconfirmed transactions are not guaranteed to be included in any block.
     * @param address - * Address can be created rawAddress or publicKey
     * @param queryParams - (Optional) Query params
     * @returns Observable<Transaction[]>
     */
    getAccountUnconfirmedTransactions(address, queryParams) {
        return rxjs_1.from(this.accountRoutesApi.getAccountUnconfirmedTransactions(address.plain(), this.queryParams(queryParams).pageSize, this.queryParams(queryParams).id, this.queryParams(queryParams).order)).pipe(operators_1.map(({ body }) => body.map((transactionDTO) => {
            return CreateTransactionFromDTO_1.CreateTransactionFromDTO(transactionDTO);
        })), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
    /**
     * Gets an array of transactions for which an account is the sender or has sign the transaction.
     * A transaction is said to be aggregate bonded with respect to an account if there are missing signatures.
     * @param address - * Address can be created rawAddress or publicKey
     * @param queryParams - (Optional) Query params
     * @returns Observable<AggregateTransaction[]>
     */
    getAccountPartialTransactions(address, queryParams) {
        return rxjs_1.from(this.accountRoutesApi.getAccountPartialTransactions(address.plain(), this.queryParams(queryParams).pageSize, this.queryParams(queryParams).id, this.queryParams(queryParams).order)).pipe(operators_1.map(({ body }) => body.map((transactionDTO) => {
            return CreateTransactionFromDTO_1.CreateTransactionFromDTO(transactionDTO);
        })), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
}
exports.AccountHttp = AccountHttp;
//# sourceMappingURL=AccountHttp.js.map