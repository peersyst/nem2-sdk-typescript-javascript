import { Observable } from 'rxjs';
import { NetworkName } from '../model/blockchain/NetworkName';
import { NetworkType } from '../model/blockchain/NetworkType';
/**
 * Network interface repository.
 *
 * @since 1.0
 */
export interface NetworkRepository {
    /**
     * Get current network type.
     * @return network type enum.
     */
    getNetworkType(): Observable<NetworkType>;
    /**
     * Get current network type name and description
     *
     * @return current network type name and description
     */
    getNetworkName(): Observable<NetworkName>;
}
