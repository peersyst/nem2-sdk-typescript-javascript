import { Observable } from 'rxjs';
import { AccountInfo } from '../model/account/AccountInfo';
import { Address } from '../model/account/Address';
import { AggregateTransaction } from '../model/transaction/AggregateTransaction';
import { Transaction } from '../model/transaction/Transaction';
import { QueryParams } from './QueryParams';
/**
 * Account interface repository.
 *
 * @since 1.0
 */
export interface AccountRepository {
    /**
     * Gets an AccountInfo for an account.
     * @param address Address
     * @returns Observable<AccountInfo>
     */
    getAccountInfo(address: Address): Observable<AccountInfo>;
    /**
     * Gets AccountsInfo for different accounts.
     * @param addresses List of Address
     * @returns Observable<AccountInfo[]>
     */
    getAccountsInfo(addresses: Address[]): Observable<AccountInfo[]>;
    /**
     * Gets an array of confirmed transactions for which an account is signer or receiver.
     * @param address - * Address can be created rawAddress or publicKey
     * @param queryParams - (Optional) Query params
     * @returns Observable<Transaction[]>
     */
    getAccountTransactions(address: Address, queryParams?: QueryParams): Observable<Transaction[]>;
    /**
     * Gets an array of transactions for which an account is the recipient of a transaction.
     * A transaction is said to be incoming with respect to an account if the account is the recipient of a transaction.
     * @param address - * Address can be created rawAddress or publicKey
     * @param queryParams - (Optional) Query params
     * @returns Observable<Transaction[]>
     */
    getAccountIncomingTransactions(address: Address, queryParams?: QueryParams): Observable<Transaction[]>;
    /**
     * Gets an array of transactions for which an account is the sender a transaction.
     * A transaction is said to be outgoing with respect to an account if the account is the sender of a transaction.
     * @param address - * Address can be created rawAddress or publicKey
     * @param queryParams - (Optional) Query params
     * @returns Observable<Transaction[]>
     */
    getAccountOutgoingTransactions(address: Address, queryParams?: QueryParams): Observable<Transaction[]>;
    /**
     * Gets the array of transactions for which an account is the sender or receiver and which have not yet been included in a block.
     * Unconfirmed transactions are those transactions that have not yet been included in a block.
     * Unconfirmed transactions are not guaranteed to be included in any block.
     * @param address - * Address can be created rawAddress or publicKey
     * @param queryParams - (Optional) Query params
     * @returns Observable<Transaction[]>
     */
    getAccountUnconfirmedTransactions(address: Address, queryParams?: QueryParams): Observable<Transaction[]>;
    /**
     * Gets an array of transactions for which an account is the sender or has sign the transaction.
     * A transaction is said to be aggregate bonded with respect to an account if there are missing signatures.
     * @param address - * Address can be created rawAddress or publicKey
     * @param queryParams - (Optional) Query params
     * @returns Observable<AggregateTransaction[]>
     */
    getAccountPartialTransactions(address: Address, queryParams?: QueryParams): Observable<AggregateTransaction[]>;
}
