"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const rxjs_1 = require("rxjs");
const operators_1 = require("rxjs/operators");
const NetworkName_1 = require("../model/blockchain/NetworkName");
const apis_1 = require("./api/apis");
const Http_1 = require("./Http");
const NodeHttp_1 = require("./NodeHttp");
/**
 * Network http repository.
 *
 * @since 1.0
 */
class NetworkHttp extends Http_1.Http {
    /**
     * Constructor
     * @param url
     */
    constructor(url) {
        super(url);
        this.nodeHttp = new NodeHttp_1.NodeHttp(url);
        this.networkRouteApi = new apis_1.NetworkRoutesApi(url);
    }
    /**
     * Get current network identifier.
     *
     * @return network identifier.
     */
    getNetworkType() {
        return rxjs_1.from(this.nodeHttp.getNodeInfo()).pipe(operators_1.map(((nodeInfo) => nodeInfo.networkIdentifier), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error)))));
    }
    /**
     * Get current network type name and description
     *
     * @return current network type name and description
     */
    getNetworkName() {
        return rxjs_1.from(this.networkRouteApi.getNetworkType()).pipe(operators_1.map((({ body }) => new NetworkName_1.NetworkName(body.name, body.description))), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
}
exports.NetworkHttp = NetworkHttp;
//# sourceMappingURL=NetworkHttp.js.map