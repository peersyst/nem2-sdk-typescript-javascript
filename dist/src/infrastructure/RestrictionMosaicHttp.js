"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const rxjs_1 = require("rxjs");
const operators_1 = require("rxjs/operators");
const Address_1 = require("../model/account/Address");
const MosaicId_1 = require("../model/mosaic/MosaicId");
const MosaicAddressRestriction_1 = require("../model/restriction/MosaicAddressRestriction");
const MosaicGlobalRestriction_1 = require("../model/restriction/MosaicGlobalRestriction");
const MosaicGlobalRestrictionItem_1 = require("../model/restriction/MosaicGlobalRestrictionItem");
const restrictionMosaicRoutesApi_1 = require("./api/restrictionMosaicRoutesApi");
const Http_1 = require("./Http");
/**
 * RestrictionMosaic http repository.
 *
 * @since 1.0
 */
class RestrictionMosaicHttp extends Http_1.Http {
    /**
     * Constructor
     * @param url
     * @param networkType
     */
    constructor(url, networkType) {
        super(url, networkType);
        this.restrictionMosaicRoutesApi = new restrictionMosaicRoutesApi_1.RestrictionMosaicRoutesApi(url);
    }
    /**
     * Get mosaic address restriction.
     * @summary Get mosaic address restrictions for a given mosaic and account identifier.
     * @param mosaicId Mosaic identifier.
     * @param address address
     * @returns Observable<MosaicAddressRestriction>
     */
    getMosaicAddressRestriction(mosaicId, address) {
        return rxjs_1.from(this.restrictionMosaicRoutesApi.getMosaicAddressRestriction(mosaicId.toHex(), address.plain())).pipe(operators_1.map(({ body }) => this.toMosaicAddressRestriction(body)), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
    /**
     * Get mosaic address restrictions.
     * @summary Get mosaic address restrictions for a given mosaic and account identifiers array
     * @param mosaicId Mosaic identifier.
     * @param addresses list of addresses
     * @returns Observable<MosaicAddressRestriction[]>
     */
    getMosaicAddressRestrictions(mosaicId, addresses) {
        const accountIds = {
            addresses: addresses.map((address) => address.plain()),
        };
        return rxjs_1.from(this.restrictionMosaicRoutesApi.getMosaicAddressRestrictions(mosaicId.toHex(), accountIds)).pipe(operators_1.map(({ body }) => body.map(this.toMosaicAddressRestriction)), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
    /**
     * This method maps a MosaicAddressRestrictionDTO from rest to the SDK's MosaicAddressRestriction model object.
     *
     * @internal
     * @param {MosaicAddressRestrictionDTO} dto the MosaicAddressRestrictionDTO object from rest.
     * @returns {MosaicAddressRestriction} a MosaicAddressRestriction model
     */
    toMosaicAddressRestriction(dto) {
        const restrictionItems = new Map();
        dto.mosaicRestrictionEntry.restrictions.forEach((restriction) => {
            restrictionItems.set(restriction.key, restriction.value);
        });
        return new MosaicAddressRestriction_1.MosaicAddressRestriction(dto.mosaicRestrictionEntry.compositeHash, dto.mosaicRestrictionEntry.entryType.valueOf(), new MosaicId_1.MosaicId(dto.mosaicRestrictionEntry.mosaicId), Address_1.Address.createFromEncoded(dto.mosaicRestrictionEntry.targetAddress), restrictionItems);
    }
    /**
     * Get mosaic global restriction.
     * @summary Get mosaic global restrictions for a given mosaic identifier.
     * @param mosaicId Mosaic identifier.
     * @returns Observable<MosaicGlobalRestriction>
     */
    getMosaicGlobalRestriction(mosaicId) {
        return rxjs_1.from(this.restrictionMosaicRoutesApi.getMosaicGlobalRestriction(mosaicId.toHex())).pipe(operators_1.map(({ body }) => this.toMosaicGlobalRestriction(body)), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
    /**
     * Get mosaic global restrictions.
     * @summary Get mosaic global restrictions for a given list of mosaics.
     * @param mosaicIds List of mosaic identifier.
     * @returns Observable<MosaicGlobalRestriction[]>
     */
    getMosaicGlobalRestrictions(mosaicIds) {
        const mosaicIdsBody = {
            mosaicIds: mosaicIds.map((id) => id.toHex()),
        };
        return rxjs_1.from(this.restrictionMosaicRoutesApi.getMosaicGlobalRestrictions(mosaicIdsBody)).pipe(operators_1.map(({ body }) => body.map(this.toMosaicGlobalRestriction)), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
    /**
     * This method maps a MosaicGlobalRestrictionDTO from rest to the SDK's MosaicGlobalRestriction model object.
     *
     * @internal
     * @param {MosaicGlobalRestrictionDTO} dto the MosaicGlobalRestrictionDTO object from rest.
     * @returns {MosaicGlobalRestriction} a MosaicGlobalRestriction model
     */
    toMosaicGlobalRestriction(dto) {
        const restirctionItems = new Map();
        dto.mosaicRestrictionEntry.restrictions.forEach((restriction) => restirctionItems.set(restriction.key, new MosaicGlobalRestrictionItem_1.MosaicGlobalRestrictionItem(new MosaicId_1.MosaicId(restriction.restriction.referenceMosaicId), restriction.restriction.restrictionValue, restriction.restriction.restrictionType.valueOf())));
        return new MosaicGlobalRestriction_1.MosaicGlobalRestriction(dto.mosaicRestrictionEntry.compositeHash, dto.mosaicRestrictionEntry.entryType.valueOf(), new MosaicId_1.MosaicId(dto.mosaicRestrictionEntry.mosaicId), restirctionItems);
    }
}
exports.RestrictionMosaicHttp = RestrictionMosaicHttp;
//# sourceMappingURL=RestrictionMosaicHttp.js.map