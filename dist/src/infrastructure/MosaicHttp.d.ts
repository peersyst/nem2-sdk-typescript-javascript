import { Observable } from 'rxjs';
import { Address } from '../model/account/Address';
import { NetworkType } from '../model/blockchain/NetworkType';
import { MosaicId } from '../model/mosaic/MosaicId';
import { MosaicInfo } from '../model/mosaic/MosaicInfo';
import { Http } from './Http';
import { MosaicRepository } from './MosaicRepository';
/**
 * Mosaic http repository.
 *
 * @since 1.0
 */
export declare class MosaicHttp extends Http implements MosaicRepository {
    /**
     * Constructor
     * @param url
     * @param networkType
     */
    constructor(url: string, networkType?: NetworkType);
    /**
     * Gets the MosaicInfo for a given mosaicId
     * @param mosaicId - Mosaic id
     * @returns Observable<MosaicInfo>
     */
    getMosaic(mosaicId: MosaicId): Observable<MosaicInfo>;
    /**
     * Gets MosaicInfo for different mosaicIds.
     * @param mosaicIds - Array of mosaic ids
     * @returns Observable<MosaicInfo[]>
     */
    getMosaics(mosaicIds: MosaicId[]): Observable<MosaicInfo[]>;
    /**
     * Gets an array of mosaics created for a given account address.
     * @summary Get mosaics created by an account
     * @param address Account address.
     */
    getMosaicsFromAccount(address: Address): Observable<MosaicInfo[]>;
    /**
     * Gets mosaics created for a given array of addresses.
     * @summary Get mosaics created for given array of addresses
     * @param addresses Array of addresses
     */
    getMosaicsFromAccounts(addresses: Address[]): Observable<MosaicInfo[]>;
}
