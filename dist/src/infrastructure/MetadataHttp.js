"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const rxjs_1 = require("rxjs");
const operators_1 = require("rxjs/operators");
const Convert_1 = require("../core/format/Convert");
const Metadata_1 = require("../model/metadata/Metadata");
const MetadataEntry_1 = require("../model/metadata/MetadataEntry");
const MetadataType_1 = require("../model/metadata/MetadataType");
const MosaicId_1 = require("../model/mosaic/MosaicId");
const NamespaceId_1 = require("../model/namespace/NamespaceId");
const UInt64_1 = require("../model/UInt64");
const api_1 = require("./api");
const Http_1 = require("./Http");
/**
 * Metadata http repository.
 *
 * @since 1.0
 */
class MetadataHttp extends Http_1.Http {
    /**
     * Constructor
     * @param url
     * @param networkType
     */
    constructor(url, networkType) {
        super(url, networkType);
        this.metadataRoutesApi = new api_1.MetadataRoutesApi(url);
    }
    /**
     * Returns the account metadata given an account id.
     * @param address - Account address to be created from PublicKey or RawAddress
     * @param queryParams - Optional query parameters
     * @returns Observable<Metadata[]>
     */
    getAccountMetadata(address, queryParams) {
        return rxjs_1.from(this.metadataRoutesApi.getAccountMetadata(address.plain(), this.queryParams(queryParams).pageSize, this.queryParams(queryParams).id, this.queryParams(queryParams).order)).pipe(operators_1.map(({ body }) => body.metadataEntries.map((metadataEntry) => {
            return this.buildMetadata(metadataEntry);
        })), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
    /**
     * Returns the account metadata given an account id and a key
     * @param address - Account address to be created from PublicKey or RawAddress
     * @param key - Metadata key
     * @returns Observable<Metadata[]>
     */
    getAccountMetadataByKey(address, key) {
        return rxjs_1.from(this.metadataRoutesApi.getAccountMetadataByKey(address.plain(), key)).pipe(operators_1.map(({ body }) => body.metadataEntries.map((metadataEntry) => {
            return this.buildMetadata(metadataEntry);
        })), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
    /**
     * Returns the account metadata given an account id and a key
     * @param address - Account address to be created from PublicKey or RawAddress
     * @param key - Metadata key
     * @param publicKey - Sender public key
     * @returns Observable<Metadata>
     */
    getAccountMetadataByKeyAndSender(address, key, publicKey) {
        return rxjs_1.from(this.metadataRoutesApi.getAccountMetadataByKeyAndSender(address.plain(), key, publicKey)).pipe(operators_1.map(({ body }) => this.buildMetadata(body)), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
    /**
     * Returns the mosaic metadata given a mosaic id.
     * @param mosaicId - Mosaic identifier.
     * @param queryParams - Optional query parameters
     * @returns Observable<Metadata[]>
     */
    getMosaicMetadata(mosaicId, queryParams) {
        return rxjs_1.from(this.metadataRoutesApi.getMosaicMetadata(mosaicId.toHex(), this.queryParams(queryParams).pageSize, this.queryParams(queryParams).id, this.queryParams(queryParams).order)).pipe(operators_1.map(({ body }) => body.metadataEntries.map((metadataEntry) => {
            return this.buildMetadata(metadataEntry);
        })), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
    /**
     * Returns the mosaic metadata given a mosaic id and metadata key.
     * @param mosaicId - Mosaic identifier.
     * @param key - Metadata key.
     * @returns Observable<Metadata[]>
     */
    getMosaicMetadataByKey(mosaicId, key) {
        return rxjs_1.from(this.metadataRoutesApi.getMosaicMetadataByKey(mosaicId.toHex(), key)).pipe(operators_1.map(({ body }) => body.metadataEntries.map((metadataEntry) => {
            return this.buildMetadata(metadataEntry);
        })), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
    /**
     * Returns the mosaic metadata given a mosaic id and metadata key.
     * @param mosaicId - Mosaic identifier.
     * @param key - Metadata key.
     * @param publicKey - Sender public key
     * @returns Observable<Metadata>
     */
    getMosaicMetadataByKeyAndSender(mosaicId, key, publicKey) {
        return rxjs_1.from(this.metadataRoutesApi.getMosaicMetadataByKeyAndSender(mosaicId.toHex(), key, publicKey)).pipe(operators_1.map(({ body }) => this.buildMetadata(body)), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
    /**
     * Returns the mosaic metadata given a mosaic id.
     * @param namespaceId - Namespace identifier.
     * @param queryParams - Optional query parameters
     * @returns Observable<Metadata[]>
     */
    getNamespaceMetadata(namespaceId, queryParams) {
        return rxjs_1.from(this.metadataRoutesApi.getNamespaceMetadata(namespaceId.toHex(), this.queryParams(queryParams).pageSize, this.queryParams(queryParams).id, this.queryParams(queryParams).order)).pipe(operators_1.map(({ body }) => body.metadataEntries.map((metadataEntry) => {
            return this.buildMetadata(metadataEntry);
        })), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
    /**
     * Returns the mosaic metadata given a mosaic id and metadata key.
     * @param namespaceId - Namespace identifier.
     * @param key - Metadata key.
     * @returns Observable<Metadata[]>
     */
    getNamespaceMetadataByKey(namespaceId, key) {
        return rxjs_1.from(this.metadataRoutesApi.getNamespaceMetadataByKey(namespaceId.toHex(), key)).pipe(operators_1.map(({ body }) => body.metadataEntries.map((metadataEntry) => {
            return this.buildMetadata(metadataEntry);
        })), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
    /**
     * Returns the namespace metadata given a mosaic id and metadata key.
     * @param namespaceId - Namespace identifier.
     * @param key - Metadata key.
     * @param publicKey - Sender public key
     * @returns Observable<Metadata>
     */
    getNamespaceMetadataByKeyAndSender(namespaceId, key, publicKey) {
        return rxjs_1.from(this.metadataRoutesApi.getNamespaceMetadataByKeyAndSender(namespaceId.toHex(), key, publicKey)).pipe(operators_1.map(({ body }) => this.buildMetadata(body)), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
    /**
     * Returns the mosaic metadata given a mosaic id.
     * @param namespaceId - Namespace identifier.
     * @param queryParams - Optional query parameters
     * @returns Observable<Metadata[]>
     */
    buildMetadata(metadata) {
        const metadataEntry = metadata.metadataEntry;
        let targetId;
        switch (metadataEntry.metadataType.valueOf()) {
            case MetadataType_1.MetadataType.Mosaic:
                targetId = new MosaicId_1.MosaicId(metadataEntry.targetId);
                break;
            case MetadataType_1.MetadataType.Namespace:
                targetId = NamespaceId_1.NamespaceId.createFromEncoded(metadataEntry.targetId);
                break;
            default:
                targetId = undefined;
        }
        return new Metadata_1.Metadata(metadata.id, new MetadataEntry_1.MetadataEntry(metadataEntry.compositeHash, metadataEntry.senderPublicKey, metadataEntry.targetPublicKey, UInt64_1.UInt64.fromHex(metadataEntry.scopedMetadataKey), metadataEntry.metadataType.valueOf(), Convert_1.Convert.decodeHex(metadataEntry.value), targetId));
    }
}
exports.MetadataHttp = MetadataHttp;
//# sourceMappingURL=MetadataHttp.js.map