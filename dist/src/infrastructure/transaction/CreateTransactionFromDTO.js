"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const format_1 = require("../../core/format");
const UnresolvedMapping_1 = require("../../core/utils/UnresolvedMapping");
const Address_1 = require("../../model/account/Address");
const PublicAccount_1 = require("../../model/account/PublicAccount");
const EncryptedMessage_1 = require("../../model/message/EncryptedMessage");
const MessageType_1 = require("../../model/message/MessageType");
const PersistentHarvestingDelegationMessage_1 = require("../../model/message/PersistentHarvestingDelegationMessage");
const PlainMessage_1 = require("../../model/message/PlainMessage");
const Mosaic_1 = require("../../model/mosaic/Mosaic");
const MosaicFlags_1 = require("../../model/mosaic/MosaicFlags");
const MosaicId_1 = require("../../model/mosaic/MosaicId");
const NamespaceId_1 = require("../../model/namespace/NamespaceId");
const AccountAddressRestrictionTransaction_1 = require("../../model/transaction/AccountAddressRestrictionTransaction");
const AccountLinkTransaction_1 = require("../../model/transaction/AccountLinkTransaction");
const AccountMetadataTransaction_1 = require("../../model/transaction/AccountMetadataTransaction");
const AccountMosaicRestrictionTransaction_1 = require("../../model/transaction/AccountMosaicRestrictionTransaction");
const AccountOperationRestrictionTransaction_1 = require("../../model/transaction/AccountOperationRestrictionTransaction");
const AddressAliasTransaction_1 = require("../../model/transaction/AddressAliasTransaction");
const AggregateTransaction_1 = require("../../model/transaction/AggregateTransaction");
const AggregateTransactionCosignature_1 = require("../../model/transaction/AggregateTransactionCosignature");
const AggregateTransactionInfo_1 = require("../../model/transaction/AggregateTransactionInfo");
const Deadline_1 = require("../../model/transaction/Deadline");
const LockFundsTransaction_1 = require("../../model/transaction/LockFundsTransaction");
const MosaicAddressRestrictionTransaction_1 = require("../../model/transaction/MosaicAddressRestrictionTransaction");
const MosaicAliasTransaction_1 = require("../../model/transaction/MosaicAliasTransaction");
const MosaicDefinitionTransaction_1 = require("../../model/transaction/MosaicDefinitionTransaction");
const MosaicGlobalRestrictionTransaction_1 = require("../../model/transaction/MosaicGlobalRestrictionTransaction");
const MosaicMetadataTransaction_1 = require("../../model/transaction/MosaicMetadataTransaction");
const MosaicSupplyChangeTransaction_1 = require("../../model/transaction/MosaicSupplyChangeTransaction");
const MultisigAccountModificationTransaction_1 = require("../../model/transaction/MultisigAccountModificationTransaction");
const NamespaceMetadataTransaction_1 = require("../../model/transaction/NamespaceMetadataTransaction");
const NamespaceRegistrationTransaction_1 = require("../../model/transaction/NamespaceRegistrationTransaction");
const SecretLockTransaction_1 = require("../../model/transaction/SecretLockTransaction");
const SecretProofTransaction_1 = require("../../model/transaction/SecretProofTransaction");
const SignedTransaction_1 = require("../../model/transaction/SignedTransaction");
const TransactionInfo_1 = require("../../model/transaction/TransactionInfo");
const TransactionType_1 = require("../../model/transaction/TransactionType");
const TransferTransaction_1 = require("../../model/transaction/TransferTransaction");
const UInt64_1 = require("../../model/UInt64");
/**
 * @internal
 * @param transactionDTO
 * @returns {Transaction}
 * @constructor
 */
exports.CreateTransactionFromDTO = (transactionDTO) => {
    if (transactionDTO.transaction.type === TransactionType_1.TransactionType.AGGREGATE_COMPLETE ||
        transactionDTO.transaction.type === TransactionType_1.TransactionType.AGGREGATE_BONDED) {
        const innerTransactions = transactionDTO.transaction.transactions.map((innerTransactionDTO) => {
            const aggregateTransactionInfo = innerTransactionDTO.meta ? new AggregateTransactionInfo_1.AggregateTransactionInfo(UInt64_1.UInt64.fromNumericString(innerTransactionDTO.meta.height), innerTransactionDTO.meta.index, innerTransactionDTO.meta.id, innerTransactionDTO.meta.aggregateHash, innerTransactionDTO.meta.aggregateId) : undefined;
            innerTransactionDTO.transaction.maxFee = transactionDTO.transaction.maxFee;
            innerTransactionDTO.transaction.deadline = transactionDTO.transaction.deadline;
            innerTransactionDTO.transaction.signature = transactionDTO.transaction.signature;
            return CreateStandaloneTransactionFromDTO(innerTransactionDTO.transaction, aggregateTransactionInfo);
        });
        return new AggregateTransaction_1.AggregateTransaction(transactionDTO.transaction.network, transactionDTO.transaction.type, transactionDTO.transaction.version, Deadline_1.Deadline.createFromDTO(transactionDTO.transaction.deadline), UInt64_1.UInt64.fromNumericString(transactionDTO.transaction.maxFee || '0'), innerTransactions, transactionDTO.transaction.cosignatures ? transactionDTO.transaction.cosignatures
            .map((aggregateCosignatureDTO) => {
            return new AggregateTransactionCosignature_1.AggregateTransactionCosignature(aggregateCosignatureDTO.signature, PublicAccount_1.PublicAccount.createFromPublicKey(aggregateCosignatureDTO.signerPublicKey, transactionDTO.transaction.network));
        }) : [], transactionDTO.transaction.signature, transactionDTO.transaction.signerPublicKey ? PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.transaction.signerPublicKey, transactionDTO.transaction.network) : undefined, transactionDTO.meta ? new TransactionInfo_1.TransactionInfo(UInt64_1.UInt64.fromNumericString(transactionDTO.meta.height), transactionDTO.meta.index, transactionDTO.meta.id, transactionDTO.meta.hash, transactionDTO.meta.merkleComponentHash) : undefined);
    }
    else {
        const transactionInfo = transactionDTO.meta ? new TransactionInfo_1.TransactionInfo(UInt64_1.UInt64.fromNumericString(transactionDTO.meta.height), transactionDTO.meta.index, transactionDTO.meta.id, transactionDTO.meta.hash, transactionDTO.meta.merkleComponentHash) : undefined;
        return CreateStandaloneTransactionFromDTO(transactionDTO.transaction, transactionInfo);
    }
};
/**
 * @internal
 * @param transactionDTO
 * @param transactionInfo
 * @returns {any}
 * @constructor
 */
const CreateStandaloneTransactionFromDTO = (transactionDTO, transactionInfo) => {
    if (transactionDTO.type === TransactionType_1.TransactionType.TRANSFER) {
        return new TransferTransaction_1.TransferTransaction(transactionDTO.network, transactionDTO.version, Deadline_1.Deadline.createFromDTO(transactionDTO.deadline), UInt64_1.UInt64.fromNumericString(transactionDTO.maxFee || '0'), exports.extractRecipient(transactionDTO.recipientAddress), exports.extractMosaics(transactionDTO.mosaics), extractMessage(transactionDTO.message !== undefined ? transactionDTO.message : undefined), transactionDTO.signature, transactionDTO.signerPublicKey ? PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.signerPublicKey, transactionDTO.network) : undefined, transactionInfo);
    }
    else if (transactionDTO.type === TransactionType_1.TransactionType.REGISTER_NAMESPACE) {
        return new NamespaceRegistrationTransaction_1.NamespaceRegistrationTransaction(transactionDTO.network, transactionDTO.version, Deadline_1.Deadline.createFromDTO(transactionDTO.deadline), UInt64_1.UInt64.fromNumericString(transactionDTO.maxFee || '0'), transactionDTO.registrationType, transactionDTO.name, NamespaceId_1.NamespaceId.createFromEncoded(transactionDTO.id), transactionDTO.registrationType === 0 ? UInt64_1.UInt64.fromNumericString(transactionDTO.duration) : undefined, transactionDTO.registrationType === 1 ? NamespaceId_1.NamespaceId.createFromEncoded(transactionDTO.parentId) : undefined, transactionDTO.signature, transactionDTO.signerPublicKey ? PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.signerPublicKey, transactionDTO.network) : undefined, transactionInfo);
    }
    else if (transactionDTO.type === TransactionType_1.TransactionType.MOSAIC_DEFINITION) {
        return new MosaicDefinitionTransaction_1.MosaicDefinitionTransaction(transactionDTO.network, transactionDTO.version, Deadline_1.Deadline.createFromDTO(transactionDTO.deadline), UInt64_1.UInt64.fromNumericString(transactionDTO.maxFee || '0'), transactionDTO.nonce, new MosaicId_1.MosaicId(transactionDTO.id), new MosaicFlags_1.MosaicFlags(transactionDTO.flags), transactionDTO.divisibility, UInt64_1.UInt64.fromNumericString(transactionDTO.duration), transactionDTO.signature, transactionDTO.signerPublicKey ? PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.signerPublicKey, transactionDTO.network) : undefined, transactionInfo);
    }
    else if (transactionDTO.type === TransactionType_1.TransactionType.MOSAIC_SUPPLY_CHANGE) {
        return new MosaicSupplyChangeTransaction_1.MosaicSupplyChangeTransaction(transactionDTO.network, transactionDTO.version, Deadline_1.Deadline.createFromDTO(transactionDTO.deadline), UInt64_1.UInt64.fromNumericString(transactionDTO.maxFee || '0'), UnresolvedMapping_1.UnresolvedMapping.toUnresolvedMosaic(transactionDTO.mosaicId), transactionDTO.action, UInt64_1.UInt64.fromNumericString(transactionDTO.delta), transactionDTO.signature, transactionDTO.signerPublicKey ? PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.signerPublicKey, transactionDTO.network) : undefined, transactionInfo);
    }
    else if (transactionDTO.type === TransactionType_1.TransactionType.MODIFY_MULTISIG_ACCOUNT) {
        return new MultisigAccountModificationTransaction_1.MultisigAccountModificationTransaction(transactionDTO.network, transactionDTO.version, Deadline_1.Deadline.createFromDTO(transactionDTO.deadline), UInt64_1.UInt64.fromNumericString(transactionDTO.maxFee || '0'), transactionDTO.minApprovalDelta, transactionDTO.minRemovalDelta, transactionDTO.publicKeyAdditions ? transactionDTO.publicKeyAdditions.map((addition) => PublicAccount_1.PublicAccount.createFromPublicKey(addition, transactionDTO.network)) : [], transactionDTO.publicKeyDeletions ? transactionDTO.publicKeyDeletions.map((deletion) => PublicAccount_1.PublicAccount.createFromPublicKey(deletion, transactionDTO.network)) : [], transactionDTO.signature, transactionDTO.signerPublicKey ? PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.signerPublicKey, transactionDTO.network) : undefined, transactionInfo);
    }
    else if (transactionDTO.type === TransactionType_1.TransactionType.LOCK) {
        const networkType = transactionDTO.network;
        return new LockFundsTransaction_1.LockFundsTransaction(networkType, transactionDTO.version, Deadline_1.Deadline.createFromDTO(transactionDTO.deadline), UInt64_1.UInt64.fromNumericString(transactionDTO.maxFee || '0'), new Mosaic_1.Mosaic(new MosaicId_1.MosaicId(transactionDTO.mosaicId), UInt64_1.UInt64.fromNumericString(transactionDTO.amount)), UInt64_1.UInt64.fromNumericString(transactionDTO.duration), new SignedTransaction_1.SignedTransaction('', transactionDTO.hash, '', TransactionType_1.TransactionType.AGGREGATE_BONDED, networkType), transactionDTO.signature, transactionDTO.signerPublicKey ? PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.signerPublicKey, networkType) : undefined, transactionInfo);
    }
    else if (transactionDTO.type === TransactionType_1.TransactionType.SECRET_LOCK) {
        const recipientAddress = transactionDTO.recipientAddress;
        const mosaicId = UnresolvedMapping_1.UnresolvedMapping.toUnresolvedMosaic(transactionDTO.mosaicId);
        return new SecretLockTransaction_1.SecretLockTransaction(transactionDTO.network, transactionDTO.version, Deadline_1.Deadline.createFromDTO(transactionDTO.deadline), UInt64_1.UInt64.fromNumericString(transactionDTO.maxFee || '0'), new Mosaic_1.Mosaic(mosaicId, UInt64_1.UInt64.fromNumericString(transactionDTO.amount)), UInt64_1.UInt64.fromNumericString(transactionDTO.duration), transactionDTO.hashAlgorithm, transactionDTO.secret, exports.extractRecipient(recipientAddress), transactionDTO.signature, transactionDTO.signerPublicKey ? PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.signerPublicKey, transactionDTO.network) : undefined, transactionInfo);
    }
    else if (transactionDTO.type === TransactionType_1.TransactionType.SECRET_PROOF) {
        const recipientAddress = transactionDTO.recipientAddress;
        return new SecretProofTransaction_1.SecretProofTransaction(transactionDTO.network, transactionDTO.version, Deadline_1.Deadline.createFromDTO(transactionDTO.deadline), UInt64_1.UInt64.fromNumericString(transactionDTO.maxFee || '0'), transactionDTO.hashAlgorithm, transactionDTO.secret, exports.extractRecipient(recipientAddress), transactionDTO.proof, transactionDTO.signature, transactionDTO.signerPublicKey ? PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.signerPublicKey, transactionDTO.network) : undefined, transactionInfo);
    }
    else if (transactionDTO.type === TransactionType_1.TransactionType.MOSAIC_ALIAS) {
        return new MosaicAliasTransaction_1.MosaicAliasTransaction(transactionDTO.network, transactionDTO.version, Deadline_1.Deadline.createFromDTO(transactionDTO.deadline), UInt64_1.UInt64.fromNumericString(transactionDTO.maxFee || '0'), transactionDTO.aliasAction, NamespaceId_1.NamespaceId.createFromEncoded(transactionDTO.namespaceId), new MosaicId_1.MosaicId(transactionDTO.mosaicId), transactionDTO.signature, transactionDTO.signerPublicKey ? PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.signerPublicKey, transactionDTO.network) : undefined, transactionInfo);
    }
    else if (transactionDTO.type === TransactionType_1.TransactionType.ADDRESS_ALIAS) {
        return new AddressAliasTransaction_1.AddressAliasTransaction(transactionDTO.network, transactionDTO.version, Deadline_1.Deadline.createFromDTO(transactionDTO.deadline), UInt64_1.UInt64.fromNumericString(transactionDTO.maxFee || '0'), transactionDTO.aliasAction, NamespaceId_1.NamespaceId.createFromEncoded(transactionDTO.namespaceId), exports.extractRecipient(transactionDTO.address), transactionDTO.signature, transactionDTO.signerPublicKey ? PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.signerPublicKey, transactionDTO.network) : undefined, transactionInfo);
    }
    else if (transactionDTO.type === TransactionType_1.TransactionType.ACCOUNT_RESTRICTION_ADDRESS) {
        return new AccountAddressRestrictionTransaction_1.AccountAddressRestrictionTransaction(transactionDTO.network, transactionDTO.version, Deadline_1.Deadline.createFromDTO(transactionDTO.deadline), UInt64_1.UInt64.fromNumericString(transactionDTO.maxFee || '0'), transactionDTO.restrictionFlags, transactionDTO.restrictionAdditions ? transactionDTO.restrictionAdditions.map((addition) => exports.extractRecipient(addition)) : [], transactionDTO.restrictionDeletions ? transactionDTO.restrictionDeletions.map((deletion) => exports.extractRecipient(deletion)) : [], transactionDTO.signature, transactionDTO.signerPublicKey ? PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.signerPublicKey, transactionDTO.network) : undefined, transactionInfo);
    }
    else if (transactionDTO.type === TransactionType_1.TransactionType.ACCOUNT_RESTRICTION_OPERATION) {
        return new AccountOperationRestrictionTransaction_1.AccountOperationRestrictionTransaction(transactionDTO.network, transactionDTO.version, Deadline_1.Deadline.createFromDTO(transactionDTO.deadline), UInt64_1.UInt64.fromNumericString(transactionDTO.maxFee || '0'), transactionDTO.restrictionFlags, transactionDTO.restrictionAdditions ? transactionDTO.restrictionAdditions : [], transactionDTO.restrictionDeletions ? transactionDTO.restrictionDeletions : [], transactionDTO.signature, transactionDTO.signerPublicKey ? PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.signerPublicKey, transactionDTO.network) : undefined, transactionInfo);
    }
    else if (transactionDTO.type === TransactionType_1.TransactionType.ACCOUNT_RESTRICTION_MOSAIC) {
        return new AccountMosaicRestrictionTransaction_1.AccountMosaicRestrictionTransaction(transactionDTO.network, transactionDTO.version, Deadline_1.Deadline.createFromDTO(transactionDTO.deadline), UInt64_1.UInt64.fromNumericString(transactionDTO.maxFee || '0'), transactionDTO.restrictionFlags, transactionDTO.restrictionAdditions ? transactionDTO.restrictionAdditions.map((addition) => UnresolvedMapping_1.UnresolvedMapping.toUnresolvedMosaic(addition)) : [], transactionDTO.restrictionDeletions ? transactionDTO.restrictionDeletions.map((deletion) => UnresolvedMapping_1.UnresolvedMapping.toUnresolvedMosaic(deletion)) : [], transactionDTO.signature, transactionDTO.signerPublicKey ? PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.signerPublicKey, transactionDTO.network) : undefined, transactionInfo);
    }
    else if (transactionDTO.type === TransactionType_1.TransactionType.LINK_ACCOUNT) {
        return new AccountLinkTransaction_1.AccountLinkTransaction(transactionDTO.network, transactionDTO.version, Deadline_1.Deadline.createFromDTO(transactionDTO.deadline), UInt64_1.UInt64.fromNumericString(transactionDTO.maxFee || '0'), transactionDTO.remotePublicKey, transactionDTO.linkAction, transactionDTO.signature, transactionDTO.signerPublicKey ? PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.signerPublicKey, transactionDTO.network) : undefined, transactionInfo);
    }
    else if (transactionDTO.type === TransactionType_1.TransactionType.MOSAIC_GLOBAL_RESTRICTION) {
        return new MosaicGlobalRestrictionTransaction_1.MosaicGlobalRestrictionTransaction(transactionDTO.network, transactionDTO.version, Deadline_1.Deadline.createFromDTO(transactionDTO.deadline), UInt64_1.UInt64.fromNumericString(transactionDTO.maxFee || '0'), UnresolvedMapping_1.UnresolvedMapping.toUnresolvedMosaic(transactionDTO.mosaicId), UnresolvedMapping_1.UnresolvedMapping.toUnresolvedMosaic(transactionDTO.referenceMosaicId), UInt64_1.UInt64.fromHex(transactionDTO.restrictionKey), UInt64_1.UInt64.fromNumericString(transactionDTO.previousRestrictionValue), transactionDTO.previousRestrictionType, UInt64_1.UInt64.fromNumericString(transactionDTO.newRestrictionValue), transactionDTO.newRestrictionType, transactionDTO.signature, transactionDTO.signerPublicKey ? PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.signerPublicKey, transactionDTO.network) : undefined, transactionInfo);
    }
    else if (transactionDTO.type === TransactionType_1.TransactionType.MOSAIC_ADDRESS_RESTRICTION) {
        return new MosaicAddressRestrictionTransaction_1.MosaicAddressRestrictionTransaction(transactionDTO.network, transactionDTO.version, Deadline_1.Deadline.createFromDTO(transactionDTO.deadline), UInt64_1.UInt64.fromNumericString(transactionDTO.maxFee || '0'), UnresolvedMapping_1.UnresolvedMapping.toUnresolvedMosaic(transactionDTO.mosaicId), UInt64_1.UInt64.fromHex(transactionDTO.restrictionKey), exports.extractRecipient(transactionDTO.targetAddress), UInt64_1.UInt64.fromNumericString(transactionDTO.previousRestrictionValue), UInt64_1.UInt64.fromNumericString(transactionDTO.newRestrictionValue), transactionDTO.signature, transactionDTO.signerPublicKey ? PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.signerPublicKey, transactionDTO.network) : undefined, transactionInfo);
    }
    else if (transactionDTO.type === TransactionType_1.TransactionType.ACCOUNT_METADATA_TRANSACTION) {
        return new AccountMetadataTransaction_1.AccountMetadataTransaction(transactionDTO.network, transactionDTO.version, Deadline_1.Deadline.createFromDTO(transactionDTO.deadline), UInt64_1.UInt64.fromNumericString(transactionDTO.maxFee || '0'), transactionDTO.targetPublicKey, UInt64_1.UInt64.fromHex(transactionDTO.scopedMetadataKey), transactionDTO.valueSizeDelta, format_1.Convert.decodeHex(transactionDTO.value), transactionDTO.signature, transactionDTO.signerPublicKey ? PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.signerPublicKey, transactionDTO.network) : undefined, transactionInfo);
    }
    else if (transactionDTO.type === TransactionType_1.TransactionType.MOSAIC_METADATA_TRANSACTION) {
        return new MosaicMetadataTransaction_1.MosaicMetadataTransaction(transactionDTO.network, transactionDTO.version, Deadline_1.Deadline.createFromDTO(transactionDTO.deadline), UInt64_1.UInt64.fromNumericString(transactionDTO.maxFee || '0'), transactionDTO.targetPublicKey, UInt64_1.UInt64.fromHex(transactionDTO.scopedMetadataKey), UnresolvedMapping_1.UnresolvedMapping.toUnresolvedMosaic(transactionDTO.targetMosaicId), transactionDTO.valueSizeDelta, format_1.Convert.decodeHex(transactionDTO.value), transactionDTO.signature, transactionDTO.signerPublicKey ? PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.signerPublicKey, transactionDTO.network) : undefined, transactionInfo);
    }
    else if (transactionDTO.type === TransactionType_1.TransactionType.NAMESPACE_METADATA_TRANSACTION) {
        return new NamespaceMetadataTransaction_1.NamespaceMetadataTransaction(transactionDTO.network, transactionDTO.version, Deadline_1.Deadline.createFromDTO(transactionDTO.deadline), UInt64_1.UInt64.fromNumericString(transactionDTO.maxFee || '0'), transactionDTO.targetPublicKey, UInt64_1.UInt64.fromHex(transactionDTO.scopedMetadataKey), NamespaceId_1.NamespaceId.createFromEncoded(transactionDTO.targetNamespaceId), transactionDTO.valueSizeDelta, format_1.Convert.decodeHex(transactionDTO.value), transactionDTO.signature, transactionDTO.signerPublicKey ? PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.signerPublicKey, transactionDTO.network) : undefined, transactionInfo);
    }
    throw new Error('Unimplemented transaction with type ' + transactionDTO.type);
};
/**
 * Extract recipientAddress value from encoded hexadecimal notation.
 *
 * If bit 0 of byte 0 is not set (e.g. 0x90), then it is a regular address.
 * Else (e.g. 0x91) it represents a namespace id which starts at byte 1.
 *
 * @param recipientAddress {string} Encoded hexadecimal recipientAddress notation
 * @return {Address | NamespaceId}
 */
exports.extractRecipient = (recipientAddress) => {
    if (typeof recipientAddress === 'string') {
        return UnresolvedMapping_1.UnresolvedMapping.toUnresolvedAddress(recipientAddress);
    }
    else if (typeof recipientAddress === 'object') {
        if (recipientAddress.hasOwnProperty('address')) {
            return Address_1.Address.createFromRawAddress(recipientAddress.address);
        }
        else if (recipientAddress.hasOwnProperty('id')) {
            return NamespaceId_1.NamespaceId.createFromEncoded(recipientAddress.id);
        }
    }
    throw new Error(`Recipient: ${recipientAddress} type is not recognised`);
};
/**
 * Extract mosaics from encoded UInt64 notation.
 *
 * If most significant bit of byte 0 is set, then it is a namespaceId.
 * If most significant bit of byte 0 is not set, then it is a mosaicId.
 *
 * @param mosaics {Array | undefined} The DTO array of mosaics (with UInt64 Id notation)
 * @return {Mosaic[]}
 */
exports.extractMosaics = (mosaics) => {
    if (mosaics === undefined) {
        return [];
    }
    return mosaics.map((mosaicDTO) => {
        const id = UnresolvedMapping_1.UnresolvedMapping.toUnresolvedMosaic(mosaicDTO.id);
        return new Mosaic_1.Mosaic(id, UInt64_1.UInt64.fromNumericString(mosaicDTO.amount));
    });
};
/**
 * Extract message from either JSON payload (unencoded) or DTO (encoded)
 *
 * @param message - message payload
 * @return {PlainMessage}
 */
const extractMessage = (message) => {
    let msgObj = PlainMessage_1.EmptyMessage;
    if (message) {
        if (message.type === MessageType_1.MessageType.PlainMessage) {
            msgObj = format_1.Convert.isHexString(message.payload) ? PlainMessage_1.PlainMessage.createFromPayload(message.payload) :
                PlainMessage_1.PlainMessage.create(message.payload);
        }
        else if (message.type === MessageType_1.MessageType.EncryptedMessage) {
            msgObj = EncryptedMessage_1.EncryptedMessage.createFromPayload(message.payload);
        }
        else if (message.type === MessageType_1.MessageType.PersistentHarvestingDelegationMessage) {
            msgObj = PersistentHarvestingDelegationMessage_1.PersistentHarvestingDelegationMessage.createFromPayload(message.payload);
        }
    }
    return msgObj;
};
/**
 * Extract beneficiary public key from DTO.
 *
 * @todo Upgrade of catapult-rest WITH catapult-service-bootstrap versioning.
 *
 * With `cow` upgrade (nemtech/catapult-server@0.3.0.2), `catapult-rest` block DTO
 * was updated and latest catapult-service-bootstrap uses the wrong block DTO.
 * This will be fixed with next catapult-server upgrade to `dragon`.
 *
 * :warning It is currently not possible to read the block's beneficiary public key
 * except when working with a local instance of `catapult-rest`.
 *
 * @param beneficiary {string | undefined} The beneficiary public key if set
 * @return {Mosaic[]}
 */
exports.extractBeneficiary = (blockDTO, networkType) => {
    let dtoPublicAccount;
    let dtoFieldValue;
    if (blockDTO.beneficiaryPublicKey) {
        dtoFieldValue = blockDTO.beneficiaryPublicKey;
    }
    else if (blockDTO.beneficiary) {
        dtoFieldValue = blockDTO.beneficiary;
    }
    if (!dtoFieldValue) {
        return undefined;
    }
    try {
        // @FIX with latest catapult-service-bootstrap version, catapult-rest still returns
        //      a `string` formatted copy of the public *when it is set at all*.
        dtoPublicAccount = PublicAccount_1.PublicAccount.createFromPublicKey(dtoFieldValue, networkType);
    }
    catch (e) {
        dtoPublicAccount = undefined;
    }
    return dtoPublicAccount;
};
//# sourceMappingURL=CreateTransactionFromDTO.js.map