"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const format_1 = require("../../core/format");
const AccountAddressRestrictionTransaction_1 = require("../../model/transaction/AccountAddressRestrictionTransaction");
const AccountLinkTransaction_1 = require("../../model/transaction/AccountLinkTransaction");
const AccountMetadataTransaction_1 = require("../../model/transaction/AccountMetadataTransaction");
const AccountMosaicRestrictionTransaction_1 = require("../../model/transaction/AccountMosaicRestrictionTransaction");
const AccountOperationRestrictionTransaction_1 = require("../../model/transaction/AccountOperationRestrictionTransaction");
const AddressAliasTransaction_1 = require("../../model/transaction/AddressAliasTransaction");
const AggregateTransaction_1 = require("../../model/transaction/AggregateTransaction");
const LockFundsTransaction_1 = require("../../model/transaction/LockFundsTransaction");
const MosaicAddressRestrictionTransaction_1 = require("../../model/transaction/MosaicAddressRestrictionTransaction");
const MosaicAliasTransaction_1 = require("../../model/transaction/MosaicAliasTransaction");
const MosaicDefinitionTransaction_1 = require("../../model/transaction/MosaicDefinitionTransaction");
const MosaicGlobalRestrictionTransaction_1 = require("../../model/transaction/MosaicGlobalRestrictionTransaction");
const MosaicMetadataTransaction_1 = require("../../model/transaction/MosaicMetadataTransaction");
const MosaicSupplyChangeTransaction_1 = require("../../model/transaction/MosaicSupplyChangeTransaction");
const MultisigAccountModificationTransaction_1 = require("../../model/transaction/MultisigAccountModificationTransaction");
const NamespaceMetadataTransaction_1 = require("../../model/transaction/NamespaceMetadataTransaction");
const NamespaceRegistrationTransaction_1 = require("../../model/transaction/NamespaceRegistrationTransaction");
const SecretLockTransaction_1 = require("../../model/transaction/SecretLockTransaction");
const SecretProofTransaction_1 = require("../../model/transaction/SecretProofTransaction");
const TransactionType_1 = require("../../model/transaction/TransactionType");
const TransferTransaction_1 = require("../../model/transaction/TransferTransaction");
const EmbeddedTransactionBuilder_1 = require("../catbuffer/EmbeddedTransactionBuilder");
const TransactionBuilder_1 = require("../catbuffer/TransactionBuilder");
/**
 * @internal
 * @param payload - The transaction binary data
 * @param isEmbedded - Is the transaction an embedded inner transaction
 * @returns {Transaction | InnerTransaction}
 * @constructor
 */
exports.CreateTransactionFromPayload = (payload, isEmbedded = false) => {
    const transactionBuilder = isEmbedded ? EmbeddedTransactionBuilder_1.EmbeddedTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload)) :
        TransactionBuilder_1.TransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload));
    const type = transactionBuilder.getType().valueOf();
    switch (type) {
        case TransactionType_1.TransactionType.ACCOUNT_RESTRICTION_ADDRESS:
        case TransactionType_1.TransactionType.ACCOUNT_RESTRICTION_OPERATION:
        case TransactionType_1.TransactionType.ACCOUNT_RESTRICTION_MOSAIC:
            switch (type) {
                case TransactionType_1.TransactionType.ACCOUNT_RESTRICTION_ADDRESS:
                    return AccountAddressRestrictionTransaction_1.AccountAddressRestrictionTransaction.createFromPayload(payload, isEmbedded);
                case TransactionType_1.TransactionType.ACCOUNT_RESTRICTION_MOSAIC:
                    return AccountMosaicRestrictionTransaction_1.AccountMosaicRestrictionTransaction.createFromPayload(payload, isEmbedded);
                case TransactionType_1.TransactionType.ACCOUNT_RESTRICTION_OPERATION:
                    return AccountOperationRestrictionTransaction_1.AccountOperationRestrictionTransaction.createFromPayload(payload, isEmbedded);
            }
            throw new Error('Account restriction transaction type not recognised.');
        case TransactionType_1.TransactionType.LINK_ACCOUNT:
            return AccountLinkTransaction_1.AccountLinkTransaction.createFromPayload(payload, isEmbedded);
        case TransactionType_1.TransactionType.ADDRESS_ALIAS:
            return AddressAliasTransaction_1.AddressAliasTransaction.createFromPayload(payload, isEmbedded);
        case TransactionType_1.TransactionType.MOSAIC_ALIAS:
            return MosaicAliasTransaction_1.MosaicAliasTransaction.createFromPayload(payload, isEmbedded);
        case TransactionType_1.TransactionType.MOSAIC_DEFINITION:
            return MosaicDefinitionTransaction_1.MosaicDefinitionTransaction.createFromPayload(payload, isEmbedded);
        case TransactionType_1.TransactionType.MOSAIC_SUPPLY_CHANGE:
            return MosaicSupplyChangeTransaction_1.MosaicSupplyChangeTransaction.createFromPayload(payload, isEmbedded);
        case TransactionType_1.TransactionType.REGISTER_NAMESPACE:
            return NamespaceRegistrationTransaction_1.NamespaceRegistrationTransaction.createFromPayload(payload, isEmbedded);
        case TransactionType_1.TransactionType.TRANSFER:
            return TransferTransaction_1.TransferTransaction.createFromPayload(payload, isEmbedded);
        case TransactionType_1.TransactionType.SECRET_LOCK:
            return SecretLockTransaction_1.SecretLockTransaction.createFromPayload(payload, isEmbedded);
        case TransactionType_1.TransactionType.SECRET_PROOF:
            return SecretProofTransaction_1.SecretProofTransaction.createFromPayload(payload, isEmbedded);
        case TransactionType_1.TransactionType.MODIFY_MULTISIG_ACCOUNT:
            return MultisigAccountModificationTransaction_1.MultisigAccountModificationTransaction.createFromPayload(payload, isEmbedded);
        case TransactionType_1.TransactionType.LOCK:
            return LockFundsTransaction_1.LockFundsTransaction.createFromPayload(payload, isEmbedded);
        case TransactionType_1.TransactionType.MOSAIC_GLOBAL_RESTRICTION:
            return MosaicGlobalRestrictionTransaction_1.MosaicGlobalRestrictionTransaction.createFromPayload(payload, isEmbedded);
        case TransactionType_1.TransactionType.MOSAIC_ADDRESS_RESTRICTION:
            return MosaicAddressRestrictionTransaction_1.MosaicAddressRestrictionTransaction.createFromPayload(payload, isEmbedded);
        case TransactionType_1.TransactionType.ACCOUNT_METADATA_TRANSACTION:
            return AccountMetadataTransaction_1.AccountMetadataTransaction.createFromPayload(payload, isEmbedded);
        case TransactionType_1.TransactionType.MOSAIC_METADATA_TRANSACTION:
            return MosaicMetadataTransaction_1.MosaicMetadataTransaction.createFromPayload(payload, isEmbedded);
        case TransactionType_1.TransactionType.NAMESPACE_METADATA_TRANSACTION:
            return NamespaceMetadataTransaction_1.NamespaceMetadataTransaction.createFromPayload(payload, isEmbedded);
        case TransactionType_1.TransactionType.AGGREGATE_COMPLETE:
        case TransactionType_1.TransactionType.AGGREGATE_BONDED:
            return AggregateTransaction_1.AggregateTransaction.createFromPayload(payload);
        default:
            throw new Error('Transaction type not implemented yet.');
    }
};
//# sourceMappingURL=CreateTransactionFromPayload.js.map