"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const Convert_1 = require("../../core/format/Convert");
const TransactionType_1 = require("../../model/transaction/TransactionType");
/**
 * @internal
 * @param transaction - The transaction class object
 * @returns JSON object
 * @constructor
 */
exports.SerializeTransactionToJSON = (transaction) => {
    switch (transaction.type) {
        case TransactionType_1.TransactionType.LINK_ACCOUNT:
            const accountLinkTx = transaction;
            return {
                remotePublicKey: accountLinkTx.remotePublicKey,
                linkAction: accountLinkTx.linkAction,
            };
        case TransactionType_1.TransactionType.ADDRESS_ALIAS:
            const addressAliasTx = transaction;
            return {
                aliasAction: addressAliasTx.aliasAction,
                namespaceId: addressAliasTx.namespaceId.toHex(),
                address: addressAliasTx.address.toDTO(),
            };
        case TransactionType_1.TransactionType.AGGREGATE_BONDED:
        case TransactionType_1.TransactionType.AGGREGATE_COMPLETE:
            const aggregateTx = transaction;
            return {
                transactions: aggregateTx.innerTransactions.map((innerTransaction) => {
                    return innerTransaction.toJSON();
                }),
                cosignatures: aggregateTx.cosignatures.map((cosignature) => {
                    return cosignature.toDTO();
                }),
            };
        case TransactionType_1.TransactionType.LOCK:
            const LockFundTx = transaction;
            return {
                mosaicId: LockFundTx.mosaic.id.id,
                amount: LockFundTx.mosaic.amount.toString(),
                duration: LockFundTx.duration.toString(),
                hash: LockFundTx.hash,
            };
        case TransactionType_1.TransactionType.ACCOUNT_RESTRICTION_ADDRESS:
            const accountAddressRestrictionTx = transaction;
            return {
                restrictionFlags: accountAddressRestrictionTx.restrictionFlags,
                restrictionAdditionsCount: accountAddressRestrictionTx.restrictionAdditions.length,
                restrictionDeletionsCount: accountAddressRestrictionTx.restrictionDeletions.length,
                restrictionAdditions: accountAddressRestrictionTx.restrictionAdditions.map((addition) => {
                    return addition.toDTO();
                }),
                restrictionDeletions: accountAddressRestrictionTx.restrictionDeletions.map((deletion) => {
                    return deletion.toDTO();
                }),
            };
        case TransactionType_1.TransactionType.ACCOUNT_RESTRICTION_OPERATION:
            const accountOperationRestrictionTx = transaction;
            return {
                restrictionFlags: accountOperationRestrictionTx.restrictionFlags,
                restrictionAdditionsCount: accountOperationRestrictionTx.restrictionAdditions.length,
                restrictionDeletionsCount: accountOperationRestrictionTx.restrictionDeletions.length,
                restrictionAdditions: accountOperationRestrictionTx.restrictionAdditions.map((addition) => {
                    return addition;
                }),
                restrictionDeletions: accountOperationRestrictionTx.restrictionDeletions.map((deletion) => {
                    return deletion;
                }),
            };
        case TransactionType_1.TransactionType.ACCOUNT_RESTRICTION_MOSAIC:
            const accountMosaicRestrictionTx = transaction;
            return {
                restrictionFlags: accountMosaicRestrictionTx.restrictionFlags,
                restrictionAdditionsCount: accountMosaicRestrictionTx.restrictionAdditions.length,
                restrictionDeletionsCount: accountMosaicRestrictionTx.restrictionDeletions.length,
                restrictionAdditions: accountMosaicRestrictionTx.restrictionAdditions.map((addition) => {
                    return addition.toHex();
                }),
                restrictionDeletions: accountMosaicRestrictionTx.restrictionDeletions.map((deletion) => {
                    return deletion.toHex();
                }),
            };
        case TransactionType_1.TransactionType.MODIFY_MULTISIG_ACCOUNT:
            const multisigTx = transaction;
            return {
                minApprovalDelta: multisigTx.minApprovalDelta,
                minRemovalDelta: multisigTx.minRemovalDelta,
                publicKeyAdditions: multisigTx.publicKeyAdditions.map((addition) => {
                    return addition.publicKey;
                }),
                publicKeyDeletions: multisigTx.publicKeyDeletions.map((deletion) => {
                    return deletion.publicKey;
                }),
            };
        case TransactionType_1.TransactionType.MOSAIC_ALIAS:
            const mosaicAliasTx = transaction;
            return {
                aliasAction: mosaicAliasTx.aliasAction,
                namespaceId: mosaicAliasTx.namespaceId.toHex(),
                mosaicId: mosaicAliasTx.mosaicId.toHex(),
            };
        case TransactionType_1.TransactionType.MOSAIC_DEFINITION:
            const mosaicDefinitionTx = transaction;
            return {
                nonce: mosaicDefinitionTx.nonce,
                id: mosaicDefinitionTx.mosaicId.toHex(),
                flags: mosaicDefinitionTx.flags.getValue(),
                divisibility: mosaicDefinitionTx.divisibility,
                duration: mosaicDefinitionTx.duration.toString(),
            };
        case TransactionType_1.TransactionType.MOSAIC_SUPPLY_CHANGE:
            const mosaicSupplyTx = transaction;
            return {
                mosaicId: mosaicSupplyTx.mosaicId.toHex(),
                action: mosaicSupplyTx.action,
                delta: mosaicSupplyTx.delta.toString(),
            };
        case TransactionType_1.TransactionType.REGISTER_NAMESPACE:
            const namespaceTx = transaction;
            const registerNamespaceDuration = namespaceTx.duration;
            const registerNamespaceParentId = namespaceTx.parentId;
            const jsonObject = {
                registrationType: namespaceTx.registrationType,
                namespaceName: namespaceTx.namespaceName,
                id: namespaceTx.namespaceId.toHex(),
            };
            if (registerNamespaceDuration) {
                Object.assign(jsonObject, { duration: registerNamespaceDuration.toString() });
            }
            if (registerNamespaceParentId) {
                Object.assign(jsonObject, { parentId: registerNamespaceParentId.toHex() });
            }
            return jsonObject;
        case TransactionType_1.TransactionType.SECRET_LOCK:
            const secretLockTx = transaction;
            return {
                mosaicId: secretLockTx.mosaic.id.id.toHex(),
                amount: secretLockTx.mosaic.amount.toString(),
                duration: secretLockTx.duration.toString(),
                hashAlgorithm: secretLockTx.hashType,
                secret: secretLockTx.secret,
                recipientAddress: secretLockTx.recipientAddress.toDTO(),
            };
        case TransactionType_1.TransactionType.SECRET_PROOF:
            const secretProofTx = transaction;
            return {
                hashAlgorithm: secretProofTx.hashType,
                secret: secretProofTx.secret,
                recipientAddress: secretProofTx.recipientAddress.toDTO(),
                proof: secretProofTx.proof,
            };
        case TransactionType_1.TransactionType.TRANSFER:
            const transferTx = transaction;
            return {
                recipientAddress: transferTx.recipientAddress.toDTO(),
                mosaics: transferTx.mosaics.map((mosaic) => {
                    return mosaic.toDTO();
                }),
                message: transferTx.message.toDTO(),
            };
        case TransactionType_1.TransactionType.MOSAIC_GLOBAL_RESTRICTION:
            const mosaicGlobalRestrictionTx = transaction;
            return {
                mosaicId: mosaicGlobalRestrictionTx.mosaicId.toHex(),
                referenceMosaicId: mosaicGlobalRestrictionTx.referenceMosaicId.toHex(),
                restrictionKey: mosaicGlobalRestrictionTx.restrictionKey.toHex(),
                previousRestrictionValue: mosaicGlobalRestrictionTx.previousRestrictionValue.toString(),
                previousRestrictionType: mosaicGlobalRestrictionTx.previousRestrictionType,
                newRestrictionValue: mosaicGlobalRestrictionTx.newRestrictionValue.toString(),
                newRestrictionType: mosaicGlobalRestrictionTx.newRestrictionType,
            };
        case TransactionType_1.TransactionType.MOSAIC_ADDRESS_RESTRICTION:
            const mosaicAddressRestrictionTx = transaction;
            return {
                mosaicId: mosaicAddressRestrictionTx.mosaicId.toHex(),
                restrictionKey: mosaicAddressRestrictionTx.restrictionKey.toHex(),
                targetAddress: mosaicAddressRestrictionTx.targetAddress.toDTO(),
                previousRestrictionValue: mosaicAddressRestrictionTx.previousRestrictionValue.toString(),
                newRestrictionValue: mosaicAddressRestrictionTx.newRestrictionValue.toString(),
            };
        case TransactionType_1.TransactionType.ACCOUNT_METADATA_TRANSACTION:
            const accountMetadataTx = transaction;
            return {
                targetPublicKey: accountMetadataTx.targetPublicKey,
                scopedMetadataKey: accountMetadataTx.scopedMetadataKey.toHex(),
                valueSizeDelta: accountMetadataTx.valueSizeDelta,
                valueSize: accountMetadataTx.value.length,
                value: Convert_1.Convert.utf8ToHex(accountMetadataTx.value),
            };
        case TransactionType_1.TransactionType.MOSAIC_METADATA_TRANSACTION:
            const mosaicMetadataTx = transaction;
            return {
                targetPublicKey: mosaicMetadataTx.targetPublicKey,
                scopedMetadataKey: mosaicMetadataTx.scopedMetadataKey.toHex(),
                valueSizeDelta: mosaicMetadataTx.valueSizeDelta,
                targetMosaicId: mosaicMetadataTx.targetMosaicId.id.toHex(),
                valueSize: mosaicMetadataTx.value.length,
                value: Convert_1.Convert.utf8ToHex(mosaicMetadataTx.value),
            };
        case TransactionType_1.TransactionType.NAMESPACE_METADATA_TRANSACTION:
            const namespaceMetaTx = transaction;
            return {
                targetPublicKey: namespaceMetaTx.targetPublicKey,
                scopedMetadataKey: namespaceMetaTx.scopedMetadataKey.toHex(),
                valueSizeDelta: namespaceMetaTx.valueSizeDelta,
                targetNamespaceId: namespaceMetaTx.targetNamespaceId.id.toHex(),
                valueSize: namespaceMetaTx.value.length,
                value: Convert_1.Convert.utf8ToHex(namespaceMetaTx.value),
            };
        default:
            throw new Error('Transaction type not implemented yet.');
    }
};
//# sourceMappingURL=SerializeTransactionToJSON.js.map