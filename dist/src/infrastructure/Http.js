"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
// tslint:disable-next-line: ordered-imports
const rxjs_1 = require("rxjs");
const operators_1 = require("rxjs/operators");
const apis_1 = require("./api/apis");
/**
 * Http extended by all http services
 */
class Http {
    /**
     * Constructor
     * @param url Base catapult-rest url
     * @param networkType
     */
    constructor(url, networkType) {
        if (networkType) {
            this.networkType = networkType;
        }
        this.url = url;
    }
    getNetworkTypeObservable() {
        let networkTypeResolve;
        if (!this.networkType) {
            networkTypeResolve = rxjs_1.from(new apis_1.NodeRoutesApi(this.url).getNodeInfo()).pipe(operators_1.map(({ body }) => {
                this.networkType = body.networkIdentifier;
                return body.networkIdentifier;
            }), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
        }
        else {
            networkTypeResolve = rxjs_1.of(this.networkType);
        }
        return networkTypeResolve;
    }
    queryParams(queryParams) {
        return {
            pageSize: queryParams ? queryParams.pageSize : undefined,
            id: queryParams ? queryParams.id : undefined,
            order: queryParams ? queryParams.order : undefined,
        };
    }
    errorHandling(error) {
        if (error.response && error.response.statusCode && error.body) {
            const formattedError = {
                statusCode: error.response.statusCode,
                errorDetails: error.response,
                body: error.body,
            };
            return new Error(JSON.stringify(formattedError));
        }
        return new Error(error);
    }
}
exports.Http = Http;
//# sourceMappingURL=Http.js.map