import { Observable } from 'rxjs';
import { Address } from '../model/account/Address';
import { NetworkType } from '../model/blockchain/NetworkType';
import { AccountRestriction } from '../model/restriction/AccountRestriction';
import { AccountRestrictions } from '../model/restriction/AccountRestrictions';
import { Http } from './Http';
import { RestrictionAccountRepository } from './RestrictionAccountRespository';
/**
 * RestrictionAccount http repository.
 *
 * @since 1.0
 */
export declare class RestrictionAccountHttp extends Http implements RestrictionAccountRepository {
    /**
     * Constructor
     * @param url
     * @param networkType
     */
    constructor(url: string, networkType?: NetworkType);
    /**
     * Get Account restrictions.
     * @param publicAccount public account
     * @returns Observable<AccountRestrictions[]>
     */
    getAccountRestrictions(address: Address): Observable<AccountRestriction[]>;
    /**
     * Get Account restrictions.
     * @param address list of addresses
     * @returns Observable<AccountRestrictionsInfo[]>
     */
    getAccountRestrictionsFromAccounts(addresses: Address[]): Observable<AccountRestrictions[]>;
}
