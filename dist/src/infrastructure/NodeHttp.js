"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const rxjs_1 = require("rxjs");
const operators_1 = require("rxjs/operators");
const NodeInfo_1 = require("../model/node/NodeInfo");
const NodeTime_1 = require("../model/node/NodeTime");
const UInt64_1 = require("../model/UInt64");
const api_1 = require("./api");
const Http_1 = require("./Http");
/**
 * Node http repository.
 *
 * @since 1.0
 */
class NodeHttp extends Http_1.Http {
    /**
     * Constructor
     * @param url
     */
    constructor(url) {
        super(url);
        this.nodeRoutesApi = new api_1.NodeRoutesApi(url);
    }
    /**
     * Supplies additional information about the application running on a node.
     * @summary Get the node information
     */
    getNodeInfo() {
        return rxjs_1.from(this.nodeRoutesApi.getNodeInfo()).pipe(operators_1.map(({ body }) => new NodeInfo_1.NodeInfo(body.publicKey, body.port, body.networkIdentifier, body.version, body.roles, body.host, body.friendlyName)), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
    /**
     * Gets the node time at the moment the reply was sent and received.
     * @summary Get the node time
     */
    getNodeTime() {
        return rxjs_1.from(this.nodeRoutesApi.getNodeTime()).pipe(operators_1.map(({ body }) => {
            const nodeTimeDTO = body;
            if (nodeTimeDTO.communicationTimestamps.sendTimestamp && nodeTimeDTO.communicationTimestamps.receiveTimestamp) {
                return new NodeTime_1.NodeTime(UInt64_1.UInt64.fromNumericString(nodeTimeDTO.communicationTimestamps.sendTimestamp).toDTO(), UInt64_1.UInt64.fromNumericString(nodeTimeDTO.communicationTimestamps.receiveTimestamp).toDTO());
            }
            throw Error('Node time not available');
        }), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
}
exports.NodeHttp = NodeHttp;
//# sourceMappingURL=NodeHttp.js.map