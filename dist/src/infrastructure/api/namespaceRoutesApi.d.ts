/// <reference types="node" />
import http = require('http');
import { AccountIds } from '../model/accountIds';
import { AccountsNamesDTO } from '../model/accountsNamesDTO';
import { MosaicIds } from '../model/mosaicIds';
import { MosaicsNamesDTO } from '../model/mosaicsNamesDTO';
import { NamespaceIds } from '../model/namespaceIds';
import { NamespaceInfoDTO } from '../model/namespaceInfoDTO';
import { NamespaceNameDTO } from '../model/namespaceNameDTO';
import { NamespacesInfoDTO } from '../model/namespacesInfoDTO';
import { Authentication } from '../model/models';
export declare enum NamespaceRoutesApiApiKeys {
}
export declare class NamespaceRoutesApi {
    protected _basePath: string;
    protected defaultHeaders: any;
    protected _useQuerystring: boolean;
    protected authentications: {
        'default': Authentication;
    };
    constructor(basePath?: string);
    useQuerystring: boolean;
    basePath: string;
    setDefaultAuthentication(auth: Authentication): void;
    setApiKey(key: NamespaceRoutesApiApiKeys, value: string): void;
    /**
     * Returns friendly names for accounts.
     * @summary Get readable names for a set of accountIds
     * @param accountIds
     */
    getAccountsNames(accountIds?: AccountIds, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: AccountsNamesDTO;
    }>;
    /**
     * Returns friendly names for mosaics.
     * @summary Get readable names for a set of mosaics
     * @param mosaicIds
     */
    getMosaicsNames(mosaicIds: MosaicIds, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: MosaicsNamesDTO;
    }>;
    /**
     * Gets the namespace for a given namespace identifier.
     * @summary Get namespace information
     * @param namespaceId Namespace identifier.
     */
    getNamespace(namespaceId: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: NamespaceInfoDTO;
    }>;
    /**
     * Gets an array of namespaces for a given account address.
     * @summary Get namespaces created by an account
     * @param accountId Account public key or address.
     * @param pageSize Number of transactions to return for each request.
     * @param id Namespace identifier up to which transactions are returned.
     */
    getNamespacesFromAccount(accountId: string, pageSize?: number, id?: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: NamespacesInfoDTO;
    }>;
    /**
     * Gets namespaces for a given array of addresses.
     * @summary Get namespaces for given array of addresses
     * @param accountIds
     */
    getNamespacesFromAccounts(accountIds?: AccountIds, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: NamespacesInfoDTO;
    }>;
    /**
     * Returns friendly names for namespaces.
     * @summary Get readable names for a set of namespaces
     * @param namespaceIds
     */
    getNamespacesNames(namespaceIds: NamespaceIds, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: Array<NamespaceNameDTO>;
    }>;
}
