/// <reference types="node" />
import http = require('http');
import { BlockInfoDTO } from '../model/blockInfoDTO';
import { MerkleProofInfoDTO } from '../model/merkleProofInfoDTO';
import { TransactionInfoDTO } from '../model/transactionInfoDTO';
import { Authentication } from '../model/models';
export declare enum BlockRoutesApiApiKeys {
}
export declare class BlockRoutesApi {
    protected _basePath: string;
    protected defaultHeaders: any;
    protected _useQuerystring: boolean;
    protected authentications: {
        'default': Authentication;
    };
    constructor(basePath?: string);
    useQuerystring: boolean;
    basePath: string;
    setDefaultAuthentication(auth: Authentication): void;
    setApiKey(key: BlockRoutesApiApiKeys, value: string): void;
    /**
     * Gets a block from the chain that has the given height.
     * @summary Get block information
     * @param height Block height. If height -1 is not a multiple of the limit provided, the inferior closest multiple + 1 is used instead.
     */
    getBlockByHeight(height: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: BlockInfoDTO;
    }>;
    /**
     * Returns an array of transactions included in a block for a given block height.
     * @summary Get transactions from a block
     * @param height Block height. If height -1 is not a multiple of the limit provided, the inferior closest multiple + 1 is used instead.
     * @param pageSize Number of transactions to return for each request.
     * @param id Transaction identifier up to which transactions are returned.
     * @param ordering Ordering criteria: * -id - Descending order by id. * id - Ascending order by id.
     */
    getBlockTransactions(height: string, pageSize?: number, id?: string, ordering?: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: Array<TransactionInfoDTO>;
    }>;
    /**
     * Gets up to limit number of blocks after given block height.
     * @summary Get blocks information
     * @param height Block height. If height -1 is not a multiple of the limit provided, the inferior closest multiple + 1 is used instead.
     * @param limit Number of elements to be returned.
     */
    getBlocksByHeightWithLimit(height: string, limit: 25 | 50 | 75 | 100, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: Array<BlockInfoDTO>;
    }>;
    /**
     * Returns the merkle path for a transaction included in a block. The path is the complementary data needed to calculate the merkle root. A client can compare if the calculated root equals the one recorded in the block header, verifying that the transaction was included in the block.
     * @summary Get the merkle path for a given a transaction and block
     * @param height Block height. If height -1 is not a multiple of the limit provided, the inferior closest multiple + 1 is used instead.
     * @param hash Transaction hash.
     */
    getMerkleTransaction(height: string, hash: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: MerkleProofInfoDTO;
    }>;
}
