/// <reference types="node" />
import http = require('http');
import { ServerInfoDTO } from '../model/serverInfoDTO';
import { StorageInfoDTO } from '../model/storageInfoDTO';
import { Authentication } from '../model/models';
export declare enum DiagnosticRoutesApiApiKeys {
}
export declare class DiagnosticRoutesApi {
    protected _basePath: string;
    protected defaultHeaders: any;
    protected _useQuerystring: boolean;
    protected authentications: {
        'default': Authentication;
    };
    constructor(basePath?: string);
    useQuerystring: boolean;
    basePath: string;
    setDefaultAuthentication(auth: Authentication): void;
    setApiKey(key: DiagnosticRoutesApiApiKeys, value: string): void;
    /**
     * Returns diagnostic information about the node storage.
     * @summary Get the storage information of the node
     */
    getDiagnosticStorage(options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: StorageInfoDTO;
    }>;
    /**
     * Returns the version of the running catapult-rest component.
     * @summary Get the version of the running rest component
     */
    getServerInfo(options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: ServerInfoDTO;
    }>;
}
