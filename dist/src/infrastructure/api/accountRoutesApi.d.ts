/// <reference types="node" />
import http = require('http');
import { AccountIds } from '../model/accountIds';
import { AccountInfoDTO } from '../model/accountInfoDTO';
import { TransactionInfoDTO } from '../model/transactionInfoDTO';
import { Authentication } from '../model/models';
export declare enum AccountRoutesApiApiKeys {
}
export declare class AccountRoutesApi {
    protected _basePath: string;
    protected defaultHeaders: any;
    protected _useQuerystring: boolean;
    protected authentications: {
        'default': Authentication;
    };
    constructor(basePath?: string);
    useQuerystring: boolean;
    basePath: string;
    setDefaultAuthentication(auth: Authentication): void;
    setApiKey(key: AccountRoutesApiApiKeys, value: string): void;
    /**
     * Gets an array of incoming transactions. A transaction is said to be incoming with respect to an account if the account is the recipient of the transaction.
     * @summary Get incoming transactions
     * @param accountId Account public key or address.
     * @param pageSize Number of transactions to return for each request.
     * @param id Transaction identifier up to which transactions are returned.
     * @param ordering Ordering criteria: * -id - Descending order by id. * id - Ascending order by id.
     */
    getAccountIncomingTransactions(accountId: string, pageSize?: number, id?: string, ordering?: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: Array<TransactionInfoDTO>;
    }>;
    /**
     * Returns the account information.
     * @summary Get account information
     * @param accountId Account public key or address.
     */
    getAccountInfo(accountId: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: AccountInfoDTO;
    }>;
    /**
     * Gets an array of outgoing transactions. A transaction is said to be outgoing with respect to an account if the account is the sender of the transaction.
     * @summary Get outgoing transactions
     * @param accountId Account public key or address.
     * @param pageSize Number of transactions to return for each request.
     * @param id Transaction identifier up to which transactions are returned.
     * @param ordering Ordering criteria: * -id - Descending order by id. * id - Ascending order by id.
     */
    getAccountOutgoingTransactions(accountId: string, pageSize?: number, id?: string, ordering?: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: Array<TransactionInfoDTO>;
    }>;
    /**
     * Gets an array of aggregate bonded transactions where the account is the sender or requires to cosign the transaction.
     * @summary Get aggregate bonded transactions information
     * @param accountId Account public key or address.
     * @param pageSize Number of transactions to return for each request.
     * @param id Transaction identifier up to which transactions are returned.
     * @param ordering Ordering criteria: * -id - Descending order by id. * id - Ascending order by id.
     */
    getAccountPartialTransactions(accountId: string, pageSize?: number, id?: string, ordering?: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: Array<TransactionInfoDTO>;
    }>;
    /**
     * Gets an array of transactions for which an account is the sender or receiver.
     * @summary Get confirmed transactions
     * @param accountId Account public key or address.
     * @param pageSize Number of transactions to return for each request.
     * @param id Transaction identifier up to which transactions are returned.
     * @param ordering Ordering criteria: * -id - Descending order by id. * id - Ascending order by id.
     */
    getAccountTransactions(accountId: string, pageSize?: number, id?: string, ordering?: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: Array<TransactionInfoDTO>;
    }>;
    /**
     * Gets the array of transactions not included in a block where an account is the sender or receiver.
     * @summary Get unconfirmed transactions
     * @param accountId Account public key or address.
     * @param pageSize Number of transactions to return for each request.
     * @param id Transaction identifier up to which transactions are returned.
     * @param ordering Ordering criteria: * -id - Descending order by id. * id - Ascending order by id.
     */
    getAccountUnconfirmedTransactions(accountId: string, pageSize?: number, id?: string, ordering?: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: Array<TransactionInfoDTO>;
    }>;
    /**
     * Returns the account information for an array of accounts.
     * @summary Get accounts information
     * @param accountIds
     */
    getAccountsInfo(accountIds?: AccountIds, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: Array<AccountInfoDTO>;
    }>;
}
