/// <reference types="node" />
import http = require('http');
import { AccountIds } from '../model/accountIds';
import { MosaicIds } from '../model/mosaicIds';
import { MosaicInfoDTO } from '../model/mosaicInfoDTO';
import { MosaicsInfoDTO } from '../model/mosaicsInfoDTO';
import { Authentication } from '../model/models';
export declare enum MosaicRoutesApiApiKeys {
}
export declare class MosaicRoutesApi {
    protected _basePath: string;
    protected defaultHeaders: any;
    protected _useQuerystring: boolean;
    protected authentications: {
        'default': Authentication;
    };
    constructor(basePath?: string);
    useQuerystring: boolean;
    basePath: string;
    setDefaultAuthentication(auth: Authentication): void;
    setApiKey(key: MosaicRoutesApiApiKeys, value: string): void;
    /**
     * Gets the mosaic definition for a given mosaic identifier.
     * @summary Get mosaic information
     * @param mosaicId Mosaic identifier.
     */
    getMosaic(mosaicId: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: MosaicInfoDTO;
    }>;
    /**
     * Gets an array of mosaic definition.
     * @summary Get mosaics information for an array of mosaics
     * @param mosaicIds
     */
    getMosaics(mosaicIds: MosaicIds, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: Array<MosaicInfoDTO>;
    }>;
    /**
     * Gets an array of mosaics created for a given account address.
     * @summary Get mosaics created by an account
     * @param accountId Account public key or address.
     */
    getMosaicsFromAccount(accountId: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: MosaicsInfoDTO;
    }>;
    /**
     * Gets mosaics created for a given array of addresses.
     * @summary Get mosaics created for given array of addresses
     * @param accountIds
     */
    getMosaicsFromAccounts(accountIds?: AccountIds, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: MosaicsInfoDTO;
    }>;
}
