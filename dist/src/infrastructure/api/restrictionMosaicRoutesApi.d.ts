/// <reference types="node" />
import http = require('http');
import { AccountIds } from '../model/accountIds';
import { MosaicAddressRestrictionDTO } from '../model/mosaicAddressRestrictionDTO';
import { MosaicGlobalRestrictionDTO } from '../model/mosaicGlobalRestrictionDTO';
import { MosaicIds } from '../model/mosaicIds';
import { Authentication } from '../model/models';
export declare enum RestrictionMosaicRoutesApiApiKeys {
}
export declare class RestrictionMosaicRoutesApi {
    protected _basePath: string;
    protected defaultHeaders: any;
    protected _useQuerystring: boolean;
    protected authentications: {
        'default': Authentication;
    };
    constructor(basePath?: string);
    useQuerystring: boolean;
    basePath: string;
    setDefaultAuthentication(auth: Authentication): void;
    setApiKey(key: RestrictionMosaicRoutesApiApiKeys, value: string): void;
    /**
     * Get mosaic address restriction.
     * @summary Get mosaic address restrictions for a given mosaic and account identifier.
     * @param mosaicId Mosaic identifier.
     * @param accountId Account public key or address.
     */
    getMosaicAddressRestriction(mosaicId: string, accountId: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: MosaicAddressRestrictionDTO;
    }>;
    /**
     * Get mosaic address restrictions.
     * @summary Get mosaic address restrictions for a given mosaic and account identifiers array.
     * @param mosaicId Mosaic identifier.
     * @param accountIds
     */
    getMosaicAddressRestrictions(mosaicId: string, accountIds?: AccountIds, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: Array<MosaicAddressRestrictionDTO>;
    }>;
    /**
     * Get mosaic global restriction.
     * @summary Get mosaic global restriction for a given mosaic identifier.
     * @param mosaicId Mosaic identifier.
     */
    getMosaicGlobalRestriction(mosaicId: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: MosaicGlobalRestrictionDTO;
    }>;
    /**
     * Get mosaic global restrictions.
     * @summary Get mosaic global restrictions for an array of mosaics.
     * @param mosaicIds
     */
    getMosaicGlobalRestrictions(mosaicIds: MosaicIds, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: Array<MosaicGlobalRestrictionDTO>;
    }>;
}
