/// <reference types="node" />
import http = require('http');
import { MerkleProofInfoDTO } from '../model/merkleProofInfoDTO';
import { StatementsDTO } from '../model/statementsDTO';
import { Authentication } from '../model/models';
export declare enum ReceiptRoutesApiApiKeys {
}
export declare class ReceiptRoutesApi {
    protected _basePath: string;
    protected defaultHeaders: any;
    protected _useQuerystring: boolean;
    protected authentications: {
        'default': Authentication;
    };
    constructor(basePath?: string);
    useQuerystring: boolean;
    basePath: string;
    setDefaultAuthentication(auth: Authentication): void;
    setApiKey(key: ReceiptRoutesApiApiKeys, value: string): void;
    /**
     * Returns the receipts linked to a block.
     * @summary Get receipts from a block
     * @param height Block height. If height -1 is not a multiple of the limit provided, the inferior closest multiple + 1 is used instead.
     */
    getBlockReceipts(height: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: StatementsDTO;
    }>;
    /**
     * Returns the merkle path for a receipt statement or resolution linked to a block. The path is the complementary data needed to calculate the merkle root. A client can compare if the calculated root equals the one recorded in the block header, verifying that the receipt was linked with the block.
     * @summary Get the merkle path for a given a receipt statement hash and block
     * @param height Block height. If height -1 is not a multiple of the limit provided, the inferior closest multiple + 1 is used instead.
     * @param hash Receipt hash.
     */
    getMerkleReceipts(height: string, hash: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: MerkleProofInfoDTO;
    }>;
}
