/// <reference types="node" />
import http = require('http');
import { MultisigAccountGraphInfoDTO } from '../model/multisigAccountGraphInfoDTO';
import { MultisigAccountInfoDTO } from '../model/multisigAccountInfoDTO';
import { Authentication } from '../model/models';
export declare enum MultisigRoutesApiApiKeys {
}
export declare class MultisigRoutesApi {
    protected _basePath: string;
    protected defaultHeaders: any;
    protected _useQuerystring: boolean;
    protected authentications: {
        'default': Authentication;
    };
    constructor(basePath?: string);
    useQuerystring: boolean;
    basePath: string;
    setDefaultAuthentication(auth: Authentication): void;
    setApiKey(key: MultisigRoutesApiApiKeys, value: string): void;
    /**
     * Returns the multisig account information.
     * @summary Get multisig account information
     * @param accountId Account public key or address.
     */
    getAccountMultisig(accountId: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: MultisigAccountInfoDTO;
    }>;
    /**
     * Returns the multisig account graph.
     * @summary Get multisig account graph information
     * @param accountId Account public key or address.
     */
    getAccountMultisigGraph(accountId: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: Array<MultisigAccountGraphInfoDTO>;
    }>;
}
