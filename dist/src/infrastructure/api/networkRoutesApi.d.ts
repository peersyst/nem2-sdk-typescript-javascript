/// <reference types="node" />
import http = require('http');
import { NetworkTypeDTO } from '../model/networkTypeDTO';
import { Authentication } from '../model/models';
export declare enum NetworkRoutesApiApiKeys {
}
export declare class NetworkRoutesApi {
    protected _basePath: string;
    protected defaultHeaders: any;
    protected _useQuerystring: boolean;
    protected authentications: {
        'default': Authentication;
    };
    constructor(basePath?: string);
    useQuerystring: boolean;
    basePath: string;
    setDefaultAuthentication(auth: Authentication): void;
    setApiKey(key: NetworkRoutesApiApiKeys, value: string): void;
    /**
     * Returns the current network type.
     * @summary Get the current network type of the chain
     */
    getNetworkType(options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: NetworkTypeDTO;
    }>;
}
