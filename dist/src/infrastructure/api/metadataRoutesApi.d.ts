/// <reference types="node" />
import http = require('http');
import { MetadataDTO } from '../model/metadataDTO';
import { MetadataEntriesDTO } from '../model/metadataEntriesDTO';
import { Authentication } from '../model/models';
export declare enum MetadataRoutesApiApiKeys {
}
export declare class MetadataRoutesApi {
    protected _basePath: string;
    protected defaultHeaders: any;
    protected _useQuerystring: boolean;
    protected authentications: {
        'default': Authentication;
    };
    constructor(basePath?: string);
    useQuerystring: boolean;
    basePath: string;
    setDefaultAuthentication(auth: Authentication): void;
    setApiKey(key: MetadataRoutesApiApiKeys, value: string): void;
    /**
     * Returns the account metadata given an account id.
     * @summary Get account metadata
     * @param accountId Account public key or address.
     * @param pageSize Number of transactions to return for each request.
     * @param id Metadata identifier up to which metadata are returned.
     * @param ordering Ordering criteria: * -id - Descending order by id. * id - Ascending order by id.
     */
    getAccountMetadata(accountId: string, pageSize?: number, id?: string, ordering?: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: MetadataEntriesDTO;
    }>;
    /**
     * Returns the account metadata given an account id and a key.
     * @summary Get account metadata
     * @param accountId Account public key or address.
     * @param key Metadata key.
     */
    getAccountMetadataByKey(accountId: string, key: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: MetadataEntriesDTO;
    }>;
    /**
     * Returns the account metadata given an account id, a key, and a sender.
     * @summary Get account metadata
     * @param accountId Account public key or address.
     * @param key Metadata key.
     * @param publicKey Account public key.
     */
    getAccountMetadataByKeyAndSender(accountId: string, key: string, publicKey: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: MetadataDTO;
    }>;
    /**
     * Returns the mosaic metadata given a mosaic id.
     * @summary Get mosaic metadata
     * @param mosaicId Mosaic identifier.
     * @param pageSize Number of transactions to return for each request.
     * @param id Metadata identifier up to which metadata are returned.
     * @param ordering Ordering criteria: * -id - Descending order by id. * id - Ascending order by id.
     */
    getMosaicMetadata(mosaicId: string, pageSize?: number, id?: string, ordering?: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: MetadataEntriesDTO;
    }>;
    /**
     * Returns the mosaic metadata given a mosaic id and a key.
     * @summary Get mosaic metadata
     * @param mosaicId Mosaic identifier.
     * @param key Metadata key.
     */
    getMosaicMetadataByKey(mosaicId: string, key: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: MetadataEntriesDTO;
    }>;
    /**
     * Returns the mosaic metadata given a mosaic id, a key, and a sender.
     * @summary Get mosaic metadata
     * @param mosaicId Mosaic identifier.
     * @param key Metadata key.
     * @param publicKey Account public key.
     */
    getMosaicMetadataByKeyAndSender(mosaicId: string, key: string, publicKey: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: MetadataDTO;
    }>;
    /**
     * Returns the namespace metadata given a namespace id.
     * @summary Get namespace metadata
     * @param namespaceId Namespace identifier.
     * @param pageSize Number of transactions to return for each request.
     * @param id Metadata identifier up to which metadata are returned.
     * @param ordering Ordering criteria: * -id - Descending order by id. * id - Ascending order by id.
     */
    getNamespaceMetadata(namespaceId: string, pageSize?: number, id?: string, ordering?: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: MetadataEntriesDTO;
    }>;
    /**
     * Returns the namespace metadata given a namespace id and a key.
     * @summary Get namespace metadata
     * @param namespaceId Namespace identifier.
     * @param key Metadata key.
     */
    getNamespaceMetadataByKey(namespaceId: string, key: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: MetadataEntriesDTO;
    }>;
    /**
     * Returns the namespace metadata given a namespace id, a key, and a sender.
     * @summary Get namespace metadata
     * @param namespaceId Namespace identifier.
     * @param key Metadata key.
     * @param publicKey Account public key.
     */
    getNamespaceMetadataByKeyAndSender(namespaceId: string, key: string, publicKey: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: MetadataDTO;
    }>;
}
