/// <reference types="node" />
import http = require('http');
import { NodeInfoDTO } from '../model/nodeInfoDTO';
import { NodeTimeDTO } from '../model/nodeTimeDTO';
import { Authentication } from '../model/models';
export declare enum NodeRoutesApiApiKeys {
}
export declare class NodeRoutesApi {
    protected _basePath: string;
    protected defaultHeaders: any;
    protected _useQuerystring: boolean;
    protected authentications: {
        'default': Authentication;
    };
    constructor(basePath?: string);
    useQuerystring: boolean;
    basePath: string;
    setDefaultAuthentication(auth: Authentication): void;
    setApiKey(key: NodeRoutesApiApiKeys, value: string): void;
    /**
     * Supplies additional information about the application running on a node.
     * @summary Get the node information
     */
    getNodeInfo(options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: NodeInfoDTO;
    }>;
    /**
     * Gets the node time at the moment the reply was sent and received.
     * @summary Get the node time
     */
    getNodeTime(options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: NodeTimeDTO;
    }>;
}
