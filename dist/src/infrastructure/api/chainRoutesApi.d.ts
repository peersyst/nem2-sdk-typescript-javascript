/// <reference types="node" />
import http = require('http');
import { ChainScoreDTO } from '../model/chainScoreDTO';
import { HeightInfoDTO } from '../model/heightInfoDTO';
import { Authentication } from '../model/models';
export declare enum ChainRoutesApiApiKeys {
}
export declare class ChainRoutesApi {
    protected _basePath: string;
    protected defaultHeaders: any;
    protected _useQuerystring: boolean;
    protected authentications: {
        'default': Authentication;
    };
    constructor(basePath?: string);
    useQuerystring: boolean;
    basePath: string;
    setDefaultAuthentication(auth: Authentication): void;
    setApiKey(key: ChainRoutesApiApiKeys, value: string): void;
    /**
     * Returns the current height of the blockchain.
     * @summary Get the current height of the chain
     */
    getChainHeight(options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: HeightInfoDTO;
    }>;
    /**
     * Gets the current score of the blockchain. The higher the score, the better the chain. During synchronization, nodes try to get the best blockchain in the network.  The score for a block is derived from its difficulty and the time (in seconds) that has elapsed since the last block:      block score = difficulty − time elapsed since last block
     * @summary Get the current score of the chain
     */
    getChainScore(options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: ChainScoreDTO;
    }>;
}
