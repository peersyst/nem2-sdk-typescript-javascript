/// <reference types="node" />
import http = require('http');
import { AccountIds } from '../model/accountIds';
import { AccountRestrictionsInfoDTO } from '../model/accountRestrictionsInfoDTO';
import { Authentication } from '../model/models';
export declare enum RestrictionAccountRoutesApiApiKeys {
}
export declare class RestrictionAccountRoutesApi {
    protected _basePath: string;
    protected defaultHeaders: any;
    protected _useQuerystring: boolean;
    protected authentications: {
        'default': Authentication;
    };
    constructor(basePath?: string);
    useQuerystring: boolean;
    basePath: string;
    setDefaultAuthentication(auth: Authentication): void;
    setApiKey(key: RestrictionAccountRoutesApiApiKeys, value: string): void;
    /**
     * Returns the account restrictions for a given account.
     * @summary Get the account restrictions
     * @param accountId Account public key or address.
     */
    getAccountRestrictions(accountId: string, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: AccountRestrictionsInfoDTO;
    }>;
    /**
     * Returns the account restrictions for a given array of addresses.
     * @summary Get account restrictions for given array of addresses
     * @param accountIds
     */
    getAccountRestrictionsFromAccounts(accountIds?: AccountIds, options?: {
        headers: {
            [name: string]: string;
        };
    }): Promise<{
        response: http.ClientResponse;
        body: Array<AccountRestrictionsInfoDTO>;
    }>;
}
