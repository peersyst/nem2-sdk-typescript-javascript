import { AccountRestrictionFlagsEnum } from './accountRestrictionFlagsEnum';
import { NetworkTypeEnum } from './networkTypeEnum';
import { TransactionTypeEnum } from './transactionTypeEnum';
export declare class EmbeddedAccountOperationRestrictionTransactionDTO {
    'signerPublicKey': string;
    /**
    * Entity version.
    */
    'version': number;
    'network': NetworkTypeEnum;
    'type': number;
    /**
    * Absolute amount. An amount of 123456789 (absolute) for a mosaic with divisibility 6 means 123.456789 (relative).
    */
    'maxFee': string;
    /**
    * Duration expressed in number of blocks.
    */
    'deadline': string;
    'restrictionFlags': AccountRestrictionFlagsEnum;
    /**
    * Account restriction additions.
    */
    'restrictionAdditions': Array<TransactionTypeEnum>;
    /**
    * Account restriction deletions.
    */
    'restrictionDeletions': Array<TransactionTypeEnum>;
    static discriminator: string | undefined;
    static attributeTypeMap: Array<{
        name: string;
        baseName: string;
        type: string;
    }>;
    static getAttributeTypeMap(): {
        name: string;
        baseName: string;
        type: string;
    }[];
}
