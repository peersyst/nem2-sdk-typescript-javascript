"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./accountAddressRestrictionTransactionBodyDTO"));
__export(require("./accountAddressRestrictionTransactionDTO"));
__export(require("./accountDTO"));
__export(require("./accountIds"));
__export(require("./accountInfoDTO"));
__export(require("./accountLinkActionEnum"));
__export(require("./accountLinkTransactionBodyDTO"));
__export(require("./accountLinkTransactionDTO"));
__export(require("./accountMetadataTransactionBodyDTO"));
__export(require("./accountMetadataTransactionDTO"));
__export(require("./accountMosaicRestrictionTransactionBodyDTO"));
__export(require("./accountMosaicRestrictionTransactionDTO"));
__export(require("./accountNamesDTO"));
__export(require("./accountOperationRestrictionTransactionBodyDTO"));
__export(require("./accountOperationRestrictionTransactionDTO"));
__export(require("./accountRestrictionDTO"));
__export(require("./accountRestrictionFlagsEnum"));
__export(require("./accountRestrictionsDTO"));
__export(require("./accountRestrictionsInfoDTO"));
__export(require("./accountTypeEnum"));
__export(require("./accountsNamesDTO"));
__export(require("./activityBucketDTO"));
__export(require("./addressAliasTransactionBodyDTO"));
__export(require("./addressAliasTransactionDTO"));
__export(require("./aggregateTransactionBodyDTO"));
__export(require("./aggregateTransactionDTO"));
__export(require("./aliasActionEnum"));
__export(require("./aliasDTO"));
__export(require("./aliasTypeEnum"));
__export(require("./announceTransactionInfoDTO"));
__export(require("./balanceChangeReceiptDTO"));
__export(require("./balanceChangeReceiptDTOAllOf"));
__export(require("./balanceTransferReceiptDTO"));
__export(require("./balanceTransferReceiptDTOAllOf"));
__export(require("./blockDTO"));
__export(require("./blockDTOAllOf"));
__export(require("./blockInfoDTO"));
__export(require("./blockMetaDTO"));
__export(require("./chainScoreDTO"));
__export(require("./communicationTimestampsDTO"));
__export(require("./cosignature"));
__export(require("./cosignatureDTO"));
__export(require("./cosignatureDTOAllOf"));
__export(require("./embeddedAccountAddressRestrictionTransactionDTO"));
__export(require("./embeddedAccountLinkTransactionDTO"));
__export(require("./embeddedAccountMetadataTransactionDTO"));
__export(require("./embeddedAccountMosaicRestrictionTransactionDTO"));
__export(require("./embeddedAccountOperationRestrictionTransactionDTO"));
__export(require("./embeddedAddressAliasTransactionDTO"));
__export(require("./embeddedHashLockTransactionDTO"));
__export(require("./embeddedMosaicAddressRestrictionTransactionDTO"));
__export(require("./embeddedMosaicAliasTransactionDTO"));
__export(require("./embeddedMosaicDefinitionTransactionDTO"));
__export(require("./embeddedMosaicGlobalRestrictionTransactionDTO"));
__export(require("./embeddedMosaicMetadataTransactionDTO"));
__export(require("./embeddedMosaicSupplyChangeTransactionDTO"));
__export(require("./embeddedMultisigAccountModificationTransactionDTO"));
__export(require("./embeddedNamespaceMetadataTransactionDTO"));
__export(require("./embeddedNamespaceRegistrationTransactionDTO"));
__export(require("./embeddedSecretLockTransactionDTO"));
__export(require("./embeddedSecretProofTransactionDTO"));
__export(require("./embeddedTransactionDTO"));
__export(require("./embeddedTransactionInfoDTO"));
__export(require("./embeddedTransactionMetaDTO"));
__export(require("./embeddedTransferTransactionDTO"));
__export(require("./entityDTO"));
__export(require("./hashLockTransactionBodyDTO"));
__export(require("./hashLockTransactionDTO"));
__export(require("./heightInfoDTO"));
__export(require("./inflationReceiptDTO"));
__export(require("./inflationReceiptDTOAllOf"));
__export(require("./lockHashAlgorithmEnum"));
__export(require("./merklePathItemDTO"));
__export(require("./merkleProofInfoDTO"));
__export(require("./messageDTO"));
__export(require("./messageTypeEnum"));
__export(require("./metadataDTO"));
__export(require("./metadataEntriesDTO"));
__export(require("./metadataEntryDTO"));
__export(require("./metadataTypeEnum"));
__export(require("./modelError"));
__export(require("./mosaic"));
__export(require("./mosaicAddressRestrictionDTO"));
__export(require("./mosaicAddressRestrictionEntryDTO"));
__export(require("./mosaicAddressRestrictionEntryWrapperDTO"));
__export(require("./mosaicAddressRestrictionTransactionBodyDTO"));
__export(require("./mosaicAddressRestrictionTransactionDTO"));
__export(require("./mosaicAliasTransactionBodyDTO"));
__export(require("./mosaicAliasTransactionDTO"));
__export(require("./mosaicDTO"));
__export(require("./mosaicDefinitionTransactionBodyDTO"));
__export(require("./mosaicDefinitionTransactionDTO"));
__export(require("./mosaicExpiryReceiptDTO"));
__export(require("./mosaicExpiryReceiptDTOAllOf"));
__export(require("./mosaicGlobalRestrictionDTO"));
__export(require("./mosaicGlobalRestrictionEntryDTO"));
__export(require("./mosaicGlobalRestrictionEntryRestrictionDTO"));
__export(require("./mosaicGlobalRestrictionEntryWrapperDTO"));
__export(require("./mosaicGlobalRestrictionTransactionBodyDTO"));
__export(require("./mosaicGlobalRestrictionTransactionDTO"));
__export(require("./mosaicIds"));
__export(require("./mosaicInfoDTO"));
__export(require("./mosaicMetadataTransactionBodyDTO"));
__export(require("./mosaicMetadataTransactionDTO"));
__export(require("./mosaicNamesDTO"));
__export(require("./mosaicRestrictionEntryTypeEnum"));
__export(require("./mosaicRestrictionTypeEnum"));
__export(require("./mosaicSupplyChangeActionEnum"));
__export(require("./mosaicSupplyChangeTransactionBodyDTO"));
__export(require("./mosaicSupplyChangeTransactionDTO"));
__export(require("./mosaicsInfoDTO"));
__export(require("./mosaicsNamesDTO"));
__export(require("./multisigAccountGraphInfoDTO"));
__export(require("./multisigAccountInfoDTO"));
__export(require("./multisigAccountModificationTransactionBodyDTO"));
__export(require("./multisigAccountModificationTransactionDTO"));
__export(require("./multisigDTO"));
__export(require("./namespaceDTO"));
__export(require("./namespaceExpiryReceiptDTO"));
__export(require("./namespaceExpiryReceiptDTOAllOf"));
__export(require("./namespaceIds"));
__export(require("./namespaceInfoDTO"));
__export(require("./namespaceMetaDTO"));
__export(require("./namespaceMetadataTransactionBodyDTO"));
__export(require("./namespaceMetadataTransactionDTO"));
__export(require("./namespaceNameDTO"));
__export(require("./namespaceRegistrationTransactionBodyDTO"));
__export(require("./namespaceRegistrationTransactionDTO"));
__export(require("./namespaceRegistrationTypeEnum"));
__export(require("./namespacesInfoDTO"));
__export(require("./networkTypeDTO"));
__export(require("./networkTypeEnum"));
__export(require("./nodeInfoDTO"));
__export(require("./nodeTimeDTO"));
__export(require("./receiptDTO"));
__export(require("./receiptTypeEnum"));
__export(require("./resolutionEntryDTO"));
__export(require("./resolutionStatementBodyDTO"));
__export(require("./resolutionStatementDTO"));
__export(require("./rolesTypeEnum"));
__export(require("./secretLockTransactionBodyDTO"));
__export(require("./secretLockTransactionDTO"));
__export(require("./secretProofTransactionBodyDTO"));
__export(require("./secretProofTransactionDTO"));
__export(require("./serverDTO"));
__export(require("./serverInfoDTO"));
__export(require("./sourceDTO"));
__export(require("./statementsDTO"));
__export(require("./storageInfoDTO"));
__export(require("./transactionBodyDTO"));
__export(require("./transactionDTO"));
__export(require("./transactionHashes"));
__export(require("./transactionIds"));
__export(require("./transactionInfoDTO"));
__export(require("./transactionMetaDTO"));
__export(require("./transactionPayload"));
__export(require("./transactionStatementBodyDTO"));
__export(require("./transactionStatementDTO"));
__export(require("./transactionStatusDTO"));
__export(require("./transactionTypeEnum"));
__export(require("./transferTransactionBodyDTO"));
__export(require("./transferTransactionDTO"));
__export(require("./unresolvedMosaic"));
__export(require("./verifiableEntityDTO"));
const accountAddressRestrictionTransactionBodyDTO_1 = require("./accountAddressRestrictionTransactionBodyDTO");
const accountAddressRestrictionTransactionDTO_1 = require("./accountAddressRestrictionTransactionDTO");
const accountDTO_1 = require("./accountDTO");
const accountIds_1 = require("./accountIds");
const accountInfoDTO_1 = require("./accountInfoDTO");
const accountLinkActionEnum_1 = require("./accountLinkActionEnum");
const accountLinkTransactionBodyDTO_1 = require("./accountLinkTransactionBodyDTO");
const accountLinkTransactionDTO_1 = require("./accountLinkTransactionDTO");
const accountMetadataTransactionBodyDTO_1 = require("./accountMetadataTransactionBodyDTO");
const accountMetadataTransactionDTO_1 = require("./accountMetadataTransactionDTO");
const accountMosaicRestrictionTransactionBodyDTO_1 = require("./accountMosaicRestrictionTransactionBodyDTO");
const accountMosaicRestrictionTransactionDTO_1 = require("./accountMosaicRestrictionTransactionDTO");
const accountNamesDTO_1 = require("./accountNamesDTO");
const accountOperationRestrictionTransactionBodyDTO_1 = require("./accountOperationRestrictionTransactionBodyDTO");
const accountOperationRestrictionTransactionDTO_1 = require("./accountOperationRestrictionTransactionDTO");
const accountRestrictionDTO_1 = require("./accountRestrictionDTO");
const accountRestrictionFlagsEnum_1 = require("./accountRestrictionFlagsEnum");
const accountRestrictionsDTO_1 = require("./accountRestrictionsDTO");
const accountRestrictionsInfoDTO_1 = require("./accountRestrictionsInfoDTO");
const accountTypeEnum_1 = require("./accountTypeEnum");
const accountsNamesDTO_1 = require("./accountsNamesDTO");
const activityBucketDTO_1 = require("./activityBucketDTO");
const addressAliasTransactionBodyDTO_1 = require("./addressAliasTransactionBodyDTO");
const addressAliasTransactionDTO_1 = require("./addressAliasTransactionDTO");
const aggregateTransactionBodyDTO_1 = require("./aggregateTransactionBodyDTO");
const aggregateTransactionDTO_1 = require("./aggregateTransactionDTO");
const aliasActionEnum_1 = require("./aliasActionEnum");
const aliasDTO_1 = require("./aliasDTO");
const aliasTypeEnum_1 = require("./aliasTypeEnum");
const announceTransactionInfoDTO_1 = require("./announceTransactionInfoDTO");
const balanceChangeReceiptDTO_1 = require("./balanceChangeReceiptDTO");
const balanceChangeReceiptDTOAllOf_1 = require("./balanceChangeReceiptDTOAllOf");
const balanceTransferReceiptDTO_1 = require("./balanceTransferReceiptDTO");
const balanceTransferReceiptDTOAllOf_1 = require("./balanceTransferReceiptDTOAllOf");
const blockDTO_1 = require("./blockDTO");
const blockDTOAllOf_1 = require("./blockDTOAllOf");
const blockInfoDTO_1 = require("./blockInfoDTO");
const blockMetaDTO_1 = require("./blockMetaDTO");
const chainScoreDTO_1 = require("./chainScoreDTO");
const communicationTimestampsDTO_1 = require("./communicationTimestampsDTO");
const cosignature_1 = require("./cosignature");
const cosignatureDTO_1 = require("./cosignatureDTO");
const cosignatureDTOAllOf_1 = require("./cosignatureDTOAllOf");
const embeddedAccountAddressRestrictionTransactionDTO_1 = require("./embeddedAccountAddressRestrictionTransactionDTO");
const embeddedAccountLinkTransactionDTO_1 = require("./embeddedAccountLinkTransactionDTO");
const embeddedAccountMetadataTransactionDTO_1 = require("./embeddedAccountMetadataTransactionDTO");
const embeddedAccountMosaicRestrictionTransactionDTO_1 = require("./embeddedAccountMosaicRestrictionTransactionDTO");
const embeddedAccountOperationRestrictionTransactionDTO_1 = require("./embeddedAccountOperationRestrictionTransactionDTO");
const embeddedAddressAliasTransactionDTO_1 = require("./embeddedAddressAliasTransactionDTO");
const embeddedHashLockTransactionDTO_1 = require("./embeddedHashLockTransactionDTO");
const embeddedMosaicAddressRestrictionTransactionDTO_1 = require("./embeddedMosaicAddressRestrictionTransactionDTO");
const embeddedMosaicAliasTransactionDTO_1 = require("./embeddedMosaicAliasTransactionDTO");
const embeddedMosaicDefinitionTransactionDTO_1 = require("./embeddedMosaicDefinitionTransactionDTO");
const embeddedMosaicGlobalRestrictionTransactionDTO_1 = require("./embeddedMosaicGlobalRestrictionTransactionDTO");
const embeddedMosaicMetadataTransactionDTO_1 = require("./embeddedMosaicMetadataTransactionDTO");
const embeddedMosaicSupplyChangeTransactionDTO_1 = require("./embeddedMosaicSupplyChangeTransactionDTO");
const embeddedMultisigAccountModificationTransactionDTO_1 = require("./embeddedMultisigAccountModificationTransactionDTO");
const embeddedNamespaceMetadataTransactionDTO_1 = require("./embeddedNamespaceMetadataTransactionDTO");
const embeddedNamespaceRegistrationTransactionDTO_1 = require("./embeddedNamespaceRegistrationTransactionDTO");
const embeddedSecretLockTransactionDTO_1 = require("./embeddedSecretLockTransactionDTO");
const embeddedSecretProofTransactionDTO_1 = require("./embeddedSecretProofTransactionDTO");
const embeddedTransactionDTO_1 = require("./embeddedTransactionDTO");
const embeddedTransactionInfoDTO_1 = require("./embeddedTransactionInfoDTO");
const embeddedTransactionMetaDTO_1 = require("./embeddedTransactionMetaDTO");
const embeddedTransferTransactionDTO_1 = require("./embeddedTransferTransactionDTO");
const entityDTO_1 = require("./entityDTO");
const hashLockTransactionBodyDTO_1 = require("./hashLockTransactionBodyDTO");
const hashLockTransactionDTO_1 = require("./hashLockTransactionDTO");
const heightInfoDTO_1 = require("./heightInfoDTO");
const inflationReceiptDTO_1 = require("./inflationReceiptDTO");
const inflationReceiptDTOAllOf_1 = require("./inflationReceiptDTOAllOf");
const lockHashAlgorithmEnum_1 = require("./lockHashAlgorithmEnum");
const merklePathItemDTO_1 = require("./merklePathItemDTO");
const merkleProofInfoDTO_1 = require("./merkleProofInfoDTO");
const messageDTO_1 = require("./messageDTO");
const messageTypeEnum_1 = require("./messageTypeEnum");
const metadataDTO_1 = require("./metadataDTO");
const metadataEntriesDTO_1 = require("./metadataEntriesDTO");
const metadataEntryDTO_1 = require("./metadataEntryDTO");
const metadataTypeEnum_1 = require("./metadataTypeEnum");
const modelError_1 = require("./modelError");
const mosaic_1 = require("./mosaic");
const mosaicAddressRestrictionDTO_1 = require("./mosaicAddressRestrictionDTO");
const mosaicAddressRestrictionEntryDTO_1 = require("./mosaicAddressRestrictionEntryDTO");
const mosaicAddressRestrictionEntryWrapperDTO_1 = require("./mosaicAddressRestrictionEntryWrapperDTO");
const mosaicAddressRestrictionTransactionBodyDTO_1 = require("./mosaicAddressRestrictionTransactionBodyDTO");
const mosaicAddressRestrictionTransactionDTO_1 = require("./mosaicAddressRestrictionTransactionDTO");
const mosaicAliasTransactionBodyDTO_1 = require("./mosaicAliasTransactionBodyDTO");
const mosaicAliasTransactionDTO_1 = require("./mosaicAliasTransactionDTO");
const mosaicDTO_1 = require("./mosaicDTO");
const mosaicDefinitionTransactionBodyDTO_1 = require("./mosaicDefinitionTransactionBodyDTO");
const mosaicDefinitionTransactionDTO_1 = require("./mosaicDefinitionTransactionDTO");
const mosaicExpiryReceiptDTO_1 = require("./mosaicExpiryReceiptDTO");
const mosaicExpiryReceiptDTOAllOf_1 = require("./mosaicExpiryReceiptDTOAllOf");
const mosaicGlobalRestrictionDTO_1 = require("./mosaicGlobalRestrictionDTO");
const mosaicGlobalRestrictionEntryDTO_1 = require("./mosaicGlobalRestrictionEntryDTO");
const mosaicGlobalRestrictionEntryRestrictionDTO_1 = require("./mosaicGlobalRestrictionEntryRestrictionDTO");
const mosaicGlobalRestrictionEntryWrapperDTO_1 = require("./mosaicGlobalRestrictionEntryWrapperDTO");
const mosaicGlobalRestrictionTransactionBodyDTO_1 = require("./mosaicGlobalRestrictionTransactionBodyDTO");
const mosaicGlobalRestrictionTransactionDTO_1 = require("./mosaicGlobalRestrictionTransactionDTO");
const mosaicIds_1 = require("./mosaicIds");
const mosaicInfoDTO_1 = require("./mosaicInfoDTO");
const mosaicMetadataTransactionBodyDTO_1 = require("./mosaicMetadataTransactionBodyDTO");
const mosaicMetadataTransactionDTO_1 = require("./mosaicMetadataTransactionDTO");
const mosaicNamesDTO_1 = require("./mosaicNamesDTO");
const mosaicRestrictionEntryTypeEnum_1 = require("./mosaicRestrictionEntryTypeEnum");
const mosaicRestrictionTypeEnum_1 = require("./mosaicRestrictionTypeEnum");
const mosaicSupplyChangeActionEnum_1 = require("./mosaicSupplyChangeActionEnum");
const mosaicSupplyChangeTransactionBodyDTO_1 = require("./mosaicSupplyChangeTransactionBodyDTO");
const mosaicSupplyChangeTransactionDTO_1 = require("./mosaicSupplyChangeTransactionDTO");
const mosaicsInfoDTO_1 = require("./mosaicsInfoDTO");
const mosaicsNamesDTO_1 = require("./mosaicsNamesDTO");
const multisigAccountGraphInfoDTO_1 = require("./multisigAccountGraphInfoDTO");
const multisigAccountInfoDTO_1 = require("./multisigAccountInfoDTO");
const multisigAccountModificationTransactionBodyDTO_1 = require("./multisigAccountModificationTransactionBodyDTO");
const multisigAccountModificationTransactionDTO_1 = require("./multisigAccountModificationTransactionDTO");
const multisigDTO_1 = require("./multisigDTO");
const namespaceDTO_1 = require("./namespaceDTO");
const namespaceExpiryReceiptDTO_1 = require("./namespaceExpiryReceiptDTO");
const namespaceExpiryReceiptDTOAllOf_1 = require("./namespaceExpiryReceiptDTOAllOf");
const namespaceIds_1 = require("./namespaceIds");
const namespaceInfoDTO_1 = require("./namespaceInfoDTO");
const namespaceMetaDTO_1 = require("./namespaceMetaDTO");
const namespaceMetadataTransactionBodyDTO_1 = require("./namespaceMetadataTransactionBodyDTO");
const namespaceMetadataTransactionDTO_1 = require("./namespaceMetadataTransactionDTO");
const namespaceNameDTO_1 = require("./namespaceNameDTO");
const namespaceRegistrationTransactionBodyDTO_1 = require("./namespaceRegistrationTransactionBodyDTO");
const namespaceRegistrationTransactionDTO_1 = require("./namespaceRegistrationTransactionDTO");
const namespaceRegistrationTypeEnum_1 = require("./namespaceRegistrationTypeEnum");
const namespacesInfoDTO_1 = require("./namespacesInfoDTO");
const networkTypeDTO_1 = require("./networkTypeDTO");
const networkTypeEnum_1 = require("./networkTypeEnum");
const nodeInfoDTO_1 = require("./nodeInfoDTO");
const nodeTimeDTO_1 = require("./nodeTimeDTO");
const receiptDTO_1 = require("./receiptDTO");
const receiptTypeEnum_1 = require("./receiptTypeEnum");
const resolutionEntryDTO_1 = require("./resolutionEntryDTO");
const resolutionStatementBodyDTO_1 = require("./resolutionStatementBodyDTO");
const resolutionStatementDTO_1 = require("./resolutionStatementDTO");
const rolesTypeEnum_1 = require("./rolesTypeEnum");
const secretLockTransactionBodyDTO_1 = require("./secretLockTransactionBodyDTO");
const secretLockTransactionDTO_1 = require("./secretLockTransactionDTO");
const secretProofTransactionBodyDTO_1 = require("./secretProofTransactionBodyDTO");
const secretProofTransactionDTO_1 = require("./secretProofTransactionDTO");
const serverDTO_1 = require("./serverDTO");
const serverInfoDTO_1 = require("./serverInfoDTO");
const sourceDTO_1 = require("./sourceDTO");
const statementsDTO_1 = require("./statementsDTO");
const storageInfoDTO_1 = require("./storageInfoDTO");
const transactionBodyDTO_1 = require("./transactionBodyDTO");
const transactionDTO_1 = require("./transactionDTO");
const transactionHashes_1 = require("./transactionHashes");
const transactionIds_1 = require("./transactionIds");
const transactionInfoDTO_1 = require("./transactionInfoDTO");
const transactionMetaDTO_1 = require("./transactionMetaDTO");
const transactionPayload_1 = require("./transactionPayload");
const transactionStatementBodyDTO_1 = require("./transactionStatementBodyDTO");
const transactionStatementDTO_1 = require("./transactionStatementDTO");
const transactionStatusDTO_1 = require("./transactionStatusDTO");
const transactionTypeEnum_1 = require("./transactionTypeEnum");
const transferTransactionBodyDTO_1 = require("./transferTransactionBodyDTO");
const transferTransactionDTO_1 = require("./transferTransactionDTO");
const unresolvedMosaic_1 = require("./unresolvedMosaic");
const verifiableEntityDTO_1 = require("./verifiableEntityDTO");
/* tslint:disable:no-unused-variable */
let primitives = [
    "string",
    "boolean",
    "double",
    "integer",
    "long",
    "float",
    "number",
    "any"
];
let enumsMap = {
    "AccountLinkActionEnum": accountLinkActionEnum_1.AccountLinkActionEnum,
    "AccountRestrictionFlagsEnum": accountRestrictionFlagsEnum_1.AccountRestrictionFlagsEnum,
    "AccountTypeEnum": accountTypeEnum_1.AccountTypeEnum,
    "AliasActionEnum": aliasActionEnum_1.AliasActionEnum,
    "AliasTypeEnum": aliasTypeEnum_1.AliasTypeEnum,
    "LockHashAlgorithmEnum": lockHashAlgorithmEnum_1.LockHashAlgorithmEnum,
    "MessageTypeEnum": messageTypeEnum_1.MessageTypeEnum,
    "MetadataTypeEnum": metadataTypeEnum_1.MetadataTypeEnum,
    "MosaicRestrictionEntryTypeEnum": mosaicRestrictionEntryTypeEnum_1.MosaicRestrictionEntryTypeEnum,
    "MosaicRestrictionTypeEnum": mosaicRestrictionTypeEnum_1.MosaicRestrictionTypeEnum,
    "MosaicSupplyChangeActionEnum": mosaicSupplyChangeActionEnum_1.MosaicSupplyChangeActionEnum,
    "NamespaceRegistrationTypeEnum": namespaceRegistrationTypeEnum_1.NamespaceRegistrationTypeEnum,
    "NetworkTypeEnum": networkTypeEnum_1.NetworkTypeEnum,
    "ReceiptTypeEnum": receiptTypeEnum_1.ReceiptTypeEnum,
    "RolesTypeEnum": rolesTypeEnum_1.RolesTypeEnum,
    "TransactionTypeEnum": transactionTypeEnum_1.TransactionTypeEnum,
};
let typeMap = {
    "AccountAddressRestrictionTransactionBodyDTO": accountAddressRestrictionTransactionBodyDTO_1.AccountAddressRestrictionTransactionBodyDTO,
    "AccountAddressRestrictionTransactionDTO": accountAddressRestrictionTransactionDTO_1.AccountAddressRestrictionTransactionDTO,
    "AccountDTO": accountDTO_1.AccountDTO,
    "AccountIds": accountIds_1.AccountIds,
    "AccountInfoDTO": accountInfoDTO_1.AccountInfoDTO,
    "AccountLinkTransactionBodyDTO": accountLinkTransactionBodyDTO_1.AccountLinkTransactionBodyDTO,
    "AccountLinkTransactionDTO": accountLinkTransactionDTO_1.AccountLinkTransactionDTO,
    "AccountMetadataTransactionBodyDTO": accountMetadataTransactionBodyDTO_1.AccountMetadataTransactionBodyDTO,
    "AccountMetadataTransactionDTO": accountMetadataTransactionDTO_1.AccountMetadataTransactionDTO,
    "AccountMosaicRestrictionTransactionBodyDTO": accountMosaicRestrictionTransactionBodyDTO_1.AccountMosaicRestrictionTransactionBodyDTO,
    "AccountMosaicRestrictionTransactionDTO": accountMosaicRestrictionTransactionDTO_1.AccountMosaicRestrictionTransactionDTO,
    "AccountNamesDTO": accountNamesDTO_1.AccountNamesDTO,
    "AccountOperationRestrictionTransactionBodyDTO": accountOperationRestrictionTransactionBodyDTO_1.AccountOperationRestrictionTransactionBodyDTO,
    "AccountOperationRestrictionTransactionDTO": accountOperationRestrictionTransactionDTO_1.AccountOperationRestrictionTransactionDTO,
    "AccountRestrictionDTO": accountRestrictionDTO_1.AccountRestrictionDTO,
    "AccountRestrictionsDTO": accountRestrictionsDTO_1.AccountRestrictionsDTO,
    "AccountRestrictionsInfoDTO": accountRestrictionsInfoDTO_1.AccountRestrictionsInfoDTO,
    "AccountsNamesDTO": accountsNamesDTO_1.AccountsNamesDTO,
    "ActivityBucketDTO": activityBucketDTO_1.ActivityBucketDTO,
    "AddressAliasTransactionBodyDTO": addressAliasTransactionBodyDTO_1.AddressAliasTransactionBodyDTO,
    "AddressAliasTransactionDTO": addressAliasTransactionDTO_1.AddressAliasTransactionDTO,
    "AggregateTransactionBodyDTO": aggregateTransactionBodyDTO_1.AggregateTransactionBodyDTO,
    "AggregateTransactionDTO": aggregateTransactionDTO_1.AggregateTransactionDTO,
    "AliasDTO": aliasDTO_1.AliasDTO,
    "AnnounceTransactionInfoDTO": announceTransactionInfoDTO_1.AnnounceTransactionInfoDTO,
    "BalanceChangeReceiptDTO": balanceChangeReceiptDTO_1.BalanceChangeReceiptDTO,
    "BalanceChangeReceiptDTOAllOf": balanceChangeReceiptDTOAllOf_1.BalanceChangeReceiptDTOAllOf,
    "BalanceTransferReceiptDTO": balanceTransferReceiptDTO_1.BalanceTransferReceiptDTO,
    "BalanceTransferReceiptDTOAllOf": balanceTransferReceiptDTOAllOf_1.BalanceTransferReceiptDTOAllOf,
    "BlockDTO": blockDTO_1.BlockDTO,
    "BlockDTOAllOf": blockDTOAllOf_1.BlockDTOAllOf,
    "BlockInfoDTO": blockInfoDTO_1.BlockInfoDTO,
    "BlockMetaDTO": blockMetaDTO_1.BlockMetaDTO,
    "ChainScoreDTO": chainScoreDTO_1.ChainScoreDTO,
    "CommunicationTimestampsDTO": communicationTimestampsDTO_1.CommunicationTimestampsDTO,
    "Cosignature": cosignature_1.Cosignature,
    "CosignatureDTO": cosignatureDTO_1.CosignatureDTO,
    "CosignatureDTOAllOf": cosignatureDTOAllOf_1.CosignatureDTOAllOf,
    "EmbeddedAccountAddressRestrictionTransactionDTO": embeddedAccountAddressRestrictionTransactionDTO_1.EmbeddedAccountAddressRestrictionTransactionDTO,
    "EmbeddedAccountLinkTransactionDTO": embeddedAccountLinkTransactionDTO_1.EmbeddedAccountLinkTransactionDTO,
    "EmbeddedAccountMetadataTransactionDTO": embeddedAccountMetadataTransactionDTO_1.EmbeddedAccountMetadataTransactionDTO,
    "EmbeddedAccountMosaicRestrictionTransactionDTO": embeddedAccountMosaicRestrictionTransactionDTO_1.EmbeddedAccountMosaicRestrictionTransactionDTO,
    "EmbeddedAccountOperationRestrictionTransactionDTO": embeddedAccountOperationRestrictionTransactionDTO_1.EmbeddedAccountOperationRestrictionTransactionDTO,
    "EmbeddedAddressAliasTransactionDTO": embeddedAddressAliasTransactionDTO_1.EmbeddedAddressAliasTransactionDTO,
    "EmbeddedHashLockTransactionDTO": embeddedHashLockTransactionDTO_1.EmbeddedHashLockTransactionDTO,
    "EmbeddedMosaicAddressRestrictionTransactionDTO": embeddedMosaicAddressRestrictionTransactionDTO_1.EmbeddedMosaicAddressRestrictionTransactionDTO,
    "EmbeddedMosaicAliasTransactionDTO": embeddedMosaicAliasTransactionDTO_1.EmbeddedMosaicAliasTransactionDTO,
    "EmbeddedMosaicDefinitionTransactionDTO": embeddedMosaicDefinitionTransactionDTO_1.EmbeddedMosaicDefinitionTransactionDTO,
    "EmbeddedMosaicGlobalRestrictionTransactionDTO": embeddedMosaicGlobalRestrictionTransactionDTO_1.EmbeddedMosaicGlobalRestrictionTransactionDTO,
    "EmbeddedMosaicMetadataTransactionDTO": embeddedMosaicMetadataTransactionDTO_1.EmbeddedMosaicMetadataTransactionDTO,
    "EmbeddedMosaicSupplyChangeTransactionDTO": embeddedMosaicSupplyChangeTransactionDTO_1.EmbeddedMosaicSupplyChangeTransactionDTO,
    "EmbeddedMultisigAccountModificationTransactionDTO": embeddedMultisigAccountModificationTransactionDTO_1.EmbeddedMultisigAccountModificationTransactionDTO,
    "EmbeddedNamespaceMetadataTransactionDTO": embeddedNamespaceMetadataTransactionDTO_1.EmbeddedNamespaceMetadataTransactionDTO,
    "EmbeddedNamespaceRegistrationTransactionDTO": embeddedNamespaceRegistrationTransactionDTO_1.EmbeddedNamespaceRegistrationTransactionDTO,
    "EmbeddedSecretLockTransactionDTO": embeddedSecretLockTransactionDTO_1.EmbeddedSecretLockTransactionDTO,
    "EmbeddedSecretProofTransactionDTO": embeddedSecretProofTransactionDTO_1.EmbeddedSecretProofTransactionDTO,
    "EmbeddedTransactionDTO": embeddedTransactionDTO_1.EmbeddedTransactionDTO,
    "EmbeddedTransactionInfoDTO": embeddedTransactionInfoDTO_1.EmbeddedTransactionInfoDTO,
    "EmbeddedTransactionMetaDTO": embeddedTransactionMetaDTO_1.EmbeddedTransactionMetaDTO,
    "EmbeddedTransferTransactionDTO": embeddedTransferTransactionDTO_1.EmbeddedTransferTransactionDTO,
    "EntityDTO": entityDTO_1.EntityDTO,
    "HashLockTransactionBodyDTO": hashLockTransactionBodyDTO_1.HashLockTransactionBodyDTO,
    "HashLockTransactionDTO": hashLockTransactionDTO_1.HashLockTransactionDTO,
    "HeightInfoDTO": heightInfoDTO_1.HeightInfoDTO,
    "InflationReceiptDTO": inflationReceiptDTO_1.InflationReceiptDTO,
    "InflationReceiptDTOAllOf": inflationReceiptDTOAllOf_1.InflationReceiptDTOAllOf,
    "MerklePathItemDTO": merklePathItemDTO_1.MerklePathItemDTO,
    "MerkleProofInfoDTO": merkleProofInfoDTO_1.MerkleProofInfoDTO,
    "MessageDTO": messageDTO_1.MessageDTO,
    "MetadataDTO": metadataDTO_1.MetadataDTO,
    "MetadataEntriesDTO": metadataEntriesDTO_1.MetadataEntriesDTO,
    "MetadataEntryDTO": metadataEntryDTO_1.MetadataEntryDTO,
    "ModelError": modelError_1.ModelError,
    "Mosaic": mosaic_1.Mosaic,
    "MosaicAddressRestrictionDTO": mosaicAddressRestrictionDTO_1.MosaicAddressRestrictionDTO,
    "MosaicAddressRestrictionEntryDTO": mosaicAddressRestrictionEntryDTO_1.MosaicAddressRestrictionEntryDTO,
    "MosaicAddressRestrictionEntryWrapperDTO": mosaicAddressRestrictionEntryWrapperDTO_1.MosaicAddressRestrictionEntryWrapperDTO,
    "MosaicAddressRestrictionTransactionBodyDTO": mosaicAddressRestrictionTransactionBodyDTO_1.MosaicAddressRestrictionTransactionBodyDTO,
    "MosaicAddressRestrictionTransactionDTO": mosaicAddressRestrictionTransactionDTO_1.MosaicAddressRestrictionTransactionDTO,
    "MosaicAliasTransactionBodyDTO": mosaicAliasTransactionBodyDTO_1.MosaicAliasTransactionBodyDTO,
    "MosaicAliasTransactionDTO": mosaicAliasTransactionDTO_1.MosaicAliasTransactionDTO,
    "MosaicDTO": mosaicDTO_1.MosaicDTO,
    "MosaicDefinitionTransactionBodyDTO": mosaicDefinitionTransactionBodyDTO_1.MosaicDefinitionTransactionBodyDTO,
    "MosaicDefinitionTransactionDTO": mosaicDefinitionTransactionDTO_1.MosaicDefinitionTransactionDTO,
    "MosaicExpiryReceiptDTO": mosaicExpiryReceiptDTO_1.MosaicExpiryReceiptDTO,
    "MosaicExpiryReceiptDTOAllOf": mosaicExpiryReceiptDTOAllOf_1.MosaicExpiryReceiptDTOAllOf,
    "MosaicGlobalRestrictionDTO": mosaicGlobalRestrictionDTO_1.MosaicGlobalRestrictionDTO,
    "MosaicGlobalRestrictionEntryDTO": mosaicGlobalRestrictionEntryDTO_1.MosaicGlobalRestrictionEntryDTO,
    "MosaicGlobalRestrictionEntryRestrictionDTO": mosaicGlobalRestrictionEntryRestrictionDTO_1.MosaicGlobalRestrictionEntryRestrictionDTO,
    "MosaicGlobalRestrictionEntryWrapperDTO": mosaicGlobalRestrictionEntryWrapperDTO_1.MosaicGlobalRestrictionEntryWrapperDTO,
    "MosaicGlobalRestrictionTransactionBodyDTO": mosaicGlobalRestrictionTransactionBodyDTO_1.MosaicGlobalRestrictionTransactionBodyDTO,
    "MosaicGlobalRestrictionTransactionDTO": mosaicGlobalRestrictionTransactionDTO_1.MosaicGlobalRestrictionTransactionDTO,
    "MosaicIds": mosaicIds_1.MosaicIds,
    "MosaicInfoDTO": mosaicInfoDTO_1.MosaicInfoDTO,
    "MosaicMetadataTransactionBodyDTO": mosaicMetadataTransactionBodyDTO_1.MosaicMetadataTransactionBodyDTO,
    "MosaicMetadataTransactionDTO": mosaicMetadataTransactionDTO_1.MosaicMetadataTransactionDTO,
    "MosaicNamesDTO": mosaicNamesDTO_1.MosaicNamesDTO,
    "MosaicSupplyChangeTransactionBodyDTO": mosaicSupplyChangeTransactionBodyDTO_1.MosaicSupplyChangeTransactionBodyDTO,
    "MosaicSupplyChangeTransactionDTO": mosaicSupplyChangeTransactionDTO_1.MosaicSupplyChangeTransactionDTO,
    "MosaicsInfoDTO": mosaicsInfoDTO_1.MosaicsInfoDTO,
    "MosaicsNamesDTO": mosaicsNamesDTO_1.MosaicsNamesDTO,
    "MultisigAccountGraphInfoDTO": multisigAccountGraphInfoDTO_1.MultisigAccountGraphInfoDTO,
    "MultisigAccountInfoDTO": multisigAccountInfoDTO_1.MultisigAccountInfoDTO,
    "MultisigAccountModificationTransactionBodyDTO": multisigAccountModificationTransactionBodyDTO_1.MultisigAccountModificationTransactionBodyDTO,
    "MultisigAccountModificationTransactionDTO": multisigAccountModificationTransactionDTO_1.MultisigAccountModificationTransactionDTO,
    "MultisigDTO": multisigDTO_1.MultisigDTO,
    "NamespaceDTO": namespaceDTO_1.NamespaceDTO,
    "NamespaceExpiryReceiptDTO": namespaceExpiryReceiptDTO_1.NamespaceExpiryReceiptDTO,
    "NamespaceExpiryReceiptDTOAllOf": namespaceExpiryReceiptDTOAllOf_1.NamespaceExpiryReceiptDTOAllOf,
    "NamespaceIds": namespaceIds_1.NamespaceIds,
    "NamespaceInfoDTO": namespaceInfoDTO_1.NamespaceInfoDTO,
    "NamespaceMetaDTO": namespaceMetaDTO_1.NamespaceMetaDTO,
    "NamespaceMetadataTransactionBodyDTO": namespaceMetadataTransactionBodyDTO_1.NamespaceMetadataTransactionBodyDTO,
    "NamespaceMetadataTransactionDTO": namespaceMetadataTransactionDTO_1.NamespaceMetadataTransactionDTO,
    "NamespaceNameDTO": namespaceNameDTO_1.NamespaceNameDTO,
    "NamespaceRegistrationTransactionBodyDTO": namespaceRegistrationTransactionBodyDTO_1.NamespaceRegistrationTransactionBodyDTO,
    "NamespaceRegistrationTransactionDTO": namespaceRegistrationTransactionDTO_1.NamespaceRegistrationTransactionDTO,
    "NamespacesInfoDTO": namespacesInfoDTO_1.NamespacesInfoDTO,
    "NetworkTypeDTO": networkTypeDTO_1.NetworkTypeDTO,
    "NodeInfoDTO": nodeInfoDTO_1.NodeInfoDTO,
    "NodeTimeDTO": nodeTimeDTO_1.NodeTimeDTO,
    "ReceiptDTO": receiptDTO_1.ReceiptDTO,
    "ResolutionEntryDTO": resolutionEntryDTO_1.ResolutionEntryDTO,
    "ResolutionStatementBodyDTO": resolutionStatementBodyDTO_1.ResolutionStatementBodyDTO,
    "ResolutionStatementDTO": resolutionStatementDTO_1.ResolutionStatementDTO,
    "SecretLockTransactionBodyDTO": secretLockTransactionBodyDTO_1.SecretLockTransactionBodyDTO,
    "SecretLockTransactionDTO": secretLockTransactionDTO_1.SecretLockTransactionDTO,
    "SecretProofTransactionBodyDTO": secretProofTransactionBodyDTO_1.SecretProofTransactionBodyDTO,
    "SecretProofTransactionDTO": secretProofTransactionDTO_1.SecretProofTransactionDTO,
    "ServerDTO": serverDTO_1.ServerDTO,
    "ServerInfoDTO": serverInfoDTO_1.ServerInfoDTO,
    "SourceDTO": sourceDTO_1.SourceDTO,
    "StatementsDTO": statementsDTO_1.StatementsDTO,
    "StorageInfoDTO": storageInfoDTO_1.StorageInfoDTO,
    "TransactionBodyDTO": transactionBodyDTO_1.TransactionBodyDTO,
    "TransactionDTO": transactionDTO_1.TransactionDTO,
    "TransactionHashes": transactionHashes_1.TransactionHashes,
    "TransactionIds": transactionIds_1.TransactionIds,
    "TransactionInfoDTO": transactionInfoDTO_1.TransactionInfoDTO,
    "TransactionMetaDTO": transactionMetaDTO_1.TransactionMetaDTO,
    "TransactionPayload": transactionPayload_1.TransactionPayload,
    "TransactionStatementBodyDTO": transactionStatementBodyDTO_1.TransactionStatementBodyDTO,
    "TransactionStatementDTO": transactionStatementDTO_1.TransactionStatementDTO,
    "TransactionStatusDTO": transactionStatusDTO_1.TransactionStatusDTO,
    "TransferTransactionBodyDTO": transferTransactionBodyDTO_1.TransferTransactionBodyDTO,
    "TransferTransactionDTO": transferTransactionDTO_1.TransferTransactionDTO,
    "UnresolvedMosaic": unresolvedMosaic_1.UnresolvedMosaic,
    "VerifiableEntityDTO": verifiableEntityDTO_1.VerifiableEntityDTO,
};
class ObjectSerializer {
    static findCorrectType(data, expectedType) {
        if (data == undefined) {
            return expectedType;
        }
        else if (primitives.indexOf(expectedType.toLowerCase()) !== -1) {
            return expectedType;
        }
        else if (expectedType === "Date") {
            return expectedType;
        }
        else {
            if (enumsMap[expectedType]) {
                return expectedType;
            }
            if (!typeMap[expectedType]) {
                return expectedType; // w/e we don't know the type
            }
            // Check the discriminator
            let discriminatorProperty = typeMap[expectedType].discriminator;
            if (discriminatorProperty == null) {
                return expectedType; // the type does not have a discriminator. use it.
            }
            else {
                if (data[discriminatorProperty]) {
                    var discriminatorType = data[discriminatorProperty];
                    if (typeMap[discriminatorType]) {
                        return discriminatorType; // use the type given in the discriminator
                    }
                    else {
                        return expectedType; // discriminator did not map to a type
                    }
                }
                else {
                    return expectedType; // discriminator was not present (or an empty string)
                }
            }
        }
    }
    static serialize(data, type) {
        if (data == undefined) {
            return data;
        }
        else if (primitives.indexOf(type.toLowerCase()) !== -1) {
            return data;
        }
        else if (type.lastIndexOf("Array<", 0) === 0) {
            let subType = type.replace("Array<", ""); // Array<Type> => Type>
            subType = subType.substring(0, subType.length - 1); // Type> => Type
            let transformedData = [];
            for (let index in data) {
                let date = data[index];
                transformedData.push(ObjectSerializer.serialize(date, subType));
            }
            return transformedData;
        }
        else if (type === "Date") {
            return data.toISOString();
        }
        else {
            if (enumsMap[type]) {
                return data;
            }
            if (!typeMap[type]) {
                return data;
            }
            // Get the actual type of this object
            type = this.findCorrectType(data, type);
            // get the map for the correct type.
            let attributeTypes = typeMap[type].getAttributeTypeMap();
            let instance = {};
            for (let index in attributeTypes) {
                let attributeType = attributeTypes[index];
                instance[attributeType.baseName] = ObjectSerializer.serialize(data[attributeType.name], attributeType.type);
            }
            return instance;
        }
    }
    static deserialize(data, type) {
        // polymorphism may change the actual type.
        type = ObjectSerializer.findCorrectType(data, type);
        if (data == undefined) {
            return data;
        }
        else if (primitives.indexOf(type.toLowerCase()) !== -1) {
            return data;
        }
        else if (type.lastIndexOf("Array<", 0) === 0) {
            let subType = type.replace("Array<", ""); // Array<Type> => Type>
            subType = subType.substring(0, subType.length - 1); // Type> => Type
            let transformedData = [];
            for (let index in data) {
                let date = data[index];
                transformedData.push(ObjectSerializer.deserialize(date, subType));
            }
            return transformedData;
        }
        else if (type === "Date") {
            return new Date(data);
        }
        else {
            if (enumsMap[type]) {
                return data;
            }
            if (!typeMap[type]) {
                return data;
            }
            let instance = new typeMap[type]();
            let attributeTypes = typeMap[type].getAttributeTypeMap();
            for (let index in attributeTypes) {
                let attributeType = attributeTypes[index];
                instance[attributeType.name] = ObjectSerializer.deserialize(data[attributeType.baseName], attributeType.type);
            }
            return instance;
        }
    }
}
exports.ObjectSerializer = ObjectSerializer;
class HttpBasicAuth {
    constructor() {
        this.username = '';
        this.password = '';
    }
    applyToRequest(requestOptions) {
        requestOptions.auth = {
            username: this.username, password: this.password
        };
    }
}
exports.HttpBasicAuth = HttpBasicAuth;
class ApiKeyAuth {
    constructor(location, paramName) {
        this.location = location;
        this.paramName = paramName;
        this.apiKey = '';
    }
    applyToRequest(requestOptions) {
        if (this.location == "query") {
            requestOptions.qs[this.paramName] = this.apiKey;
        }
        else if (this.location == "header" && requestOptions && requestOptions.headers) {
            requestOptions.headers[this.paramName] = this.apiKey;
        }
    }
}
exports.ApiKeyAuth = ApiKeyAuth;
class OAuth {
    constructor() {
        this.accessToken = '';
    }
    applyToRequest(requestOptions) {
        if (requestOptions && requestOptions.headers) {
            requestOptions.headers["Authorization"] = "Bearer " + this.accessToken;
        }
    }
}
exports.OAuth = OAuth;
class VoidAuth {
    constructor() {
        this.username = '';
        this.password = '';
    }
    applyToRequest(_) {
        // Do nothing
    }
}
exports.VoidAuth = VoidAuth;
//# sourceMappingURL=models.js.map