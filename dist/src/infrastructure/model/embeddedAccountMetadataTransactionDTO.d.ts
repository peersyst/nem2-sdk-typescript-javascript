import { NetworkTypeEnum } from './networkTypeEnum';
export declare class EmbeddedAccountMetadataTransactionDTO {
    'signerPublicKey': string;
    /**
    * Entity version.
    */
    'version': number;
    'network': NetworkTypeEnum;
    'type': number;
    /**
    * Absolute amount. An amount of 123456789 (absolute) for a mosaic with divisibility 6 means 123.456789 (relative).
    */
    'maxFee': string;
    /**
    * Duration expressed in number of blocks.
    */
    'deadline': string;
    'targetPublicKey': string;
    /**
    * Metadata key scoped to source, target and type.
    */
    'scopedMetadataKey': string;
    /**
    * Change in value size in bytes.
    */
    'valueSizeDelta': number;
    /**
    * Value size in bytes.
    */
    'valueSize': number;
    /**
    * When there is an existing value, the new value is calculated as xor(previous-value, value).
    */
    'value': string;
    static discriminator: string | undefined;
    static attributeTypeMap: Array<{
        name: string;
        baseName: string;
        type: string;
    }>;
    static getAttributeTypeMap(): {
        name: string;
        baseName: string;
        type: string;
    }[];
}
