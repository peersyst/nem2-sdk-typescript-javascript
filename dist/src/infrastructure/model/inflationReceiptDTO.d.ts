import { ReceiptTypeEnum } from './receiptTypeEnum';
/**
* Native currency mosaics that were created due to inflation.
*/
export declare class InflationReceiptDTO {
    /**
    * Version of the receipt.
    */
    'version': number;
    'type': ReceiptTypeEnum;
    /**
    * Mosaic identifier.
    */
    'mosaicId': string;
    /**
    * Absolute amount. An amount of 123456789 (absolute) for a mosaic with divisibility 6 means 123.456789 (relative).
    */
    'amount': string;
    static discriminator: string | undefined;
    static attributeTypeMap: Array<{
        name: string;
        baseName: string;
        type: string;
    }>;
    static getAttributeTypeMap(): {
        name: string;
        baseName: string;
        type: string;
    }[];
}
