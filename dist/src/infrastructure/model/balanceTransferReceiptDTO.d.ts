import { ReceiptTypeEnum } from './receiptTypeEnum';
/**
* Invisible state change that triggered a mosaic transfer.
*/
export declare class BalanceTransferReceiptDTO {
    /**
    * Version of the receipt.
    */
    'version': number;
    'type': ReceiptTypeEnum;
    /**
    * Mosaic identifier.
    */
    'mosaicId': string;
    /**
    * Absolute amount. An amount of 123456789 (absolute) for a mosaic with divisibility 6 means 123.456789 (relative).
    */
    'amount': string;
    'senderPublicKey': string;
    /**
    * Decoded address.
    */
    'recipientAddress': string;
    static discriminator: string | undefined;
    static attributeTypeMap: Array<{
        name: string;
        baseName: string;
        type: string;
    }>;
    static getAttributeTypeMap(): {
        name: string;
        baseName: string;
        type: string;
    }[];
}
