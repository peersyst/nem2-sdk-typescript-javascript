import { NamespaceRegistrationTypeEnum } from './namespaceRegistrationTypeEnum';
import { NetworkTypeEnum } from './networkTypeEnum';
export declare class EmbeddedNamespaceRegistrationTransactionDTO {
    'signerPublicKey': string;
    /**
    * Entity version.
    */
    'version': number;
    'network': NetworkTypeEnum;
    'type': number;
    /**
    * Absolute amount. An amount of 123456789 (absolute) for a mosaic with divisibility 6 means 123.456789 (relative).
    */
    'maxFee': string;
    /**
    * Duration expressed in number of blocks.
    */
    'deadline': string;
    /**
    * Duration expressed in number of blocks.
    */
    'duration': string;
    /**
    * Namespace identifier.
    */
    'parentId': string;
    /**
    * Namespace identifier.
    */
    'id': string;
    'registrationType': NamespaceRegistrationTypeEnum;
    /**
    * Namespace name.
    */
    'name': string;
    static discriminator: string | undefined;
    static attributeTypeMap: Array<{
        name: string;
        baseName: string;
        type: string;
    }>;
    static getAttributeTypeMap(): {
        name: string;
        baseName: string;
        type: string;
    }[];
}
