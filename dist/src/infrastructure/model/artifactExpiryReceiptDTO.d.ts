import { ReceiptTypeEnum } from './receiptTypeEnum';
/**
* An artifact namespace or mosaic expired.
*/
export declare class ArtifactExpiryReceiptDTO {
    /**
    * Version of the receipt.
    */
    'version': number;
    'type': ReceiptTypeEnum;
    'artifactId': any;
    static discriminator: string | undefined;
    static attributeTypeMap: Array<{
        name: string;
        baseName: string;
        type: string;
    }>;
    static getAttributeTypeMap(): {
        name: string;
        baseName: string;
        type: string;
    }[];
}
