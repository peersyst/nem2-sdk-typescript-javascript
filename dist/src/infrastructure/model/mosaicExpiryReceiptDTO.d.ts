import { ReceiptTypeEnum } from './receiptTypeEnum';
/**
* A mosaic expired in this block.
*/
export declare class MosaicExpiryReceiptDTO {
    /**
    * Version of the receipt.
    */
    'version': number;
    'type': ReceiptTypeEnum;
    /**
    * Mosaic identifier.
    */
    'artifactId': string;
    static discriminator: string | undefined;
    static attributeTypeMap: Array<{
        name: string;
        baseName: string;
        type: string;
    }>;
    static getAttributeTypeMap(): {
        name: string;
        baseName: string;
        type: string;
    }[];
}
