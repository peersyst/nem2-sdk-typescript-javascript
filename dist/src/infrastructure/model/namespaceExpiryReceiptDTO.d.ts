import { ReceiptTypeEnum } from './receiptTypeEnum';
/**
* An namespace expired in this block.
*/
export declare class NamespaceExpiryReceiptDTO {
    /**
    * Version of the receipt.
    */
    'version': number;
    'type': ReceiptTypeEnum;
    /**
    * Namespace identifier.
    */
    'artifactId': string;
    static discriminator: string | undefined;
    static attributeTypeMap: Array<{
        name: string;
        baseName: string;
        type: string;
    }>;
    static getAttributeTypeMap(): {
        name: string;
        baseName: string;
        type: string;
    }[];
}
