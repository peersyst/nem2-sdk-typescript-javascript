import { LockHashAlgorithmEnum } from './lockHashAlgorithmEnum';
import { NetworkTypeEnum } from './networkTypeEnum';
export declare class EmbeddedSecretProofTransactionDTO {
    'signerPublicKey': string;
    /**
    * Entity version.
    */
    'version': number;
    'network': NetworkTypeEnum;
    'type': number;
    /**
    * Absolute amount. An amount of 123456789 (absolute) for a mosaic with divisibility 6 means 123.456789 (relative).
    */
    'maxFee': string;
    /**
    * Duration expressed in number of blocks.
    */
    'deadline': string;
    'secret': string;
    'hashAlgorithm': LockHashAlgorithmEnum;
    /**
    * Address decoded. If the bit 0 of byte 0 is not set (like in 0x90), then it is a regular address. Else (e.g. 0x91) it represents a namespace id which starts at byte 1.
    */
    'recipientAddress': string;
    /**
    * Original random set of bytes.
    */
    'proof': string;
    static discriminator: string | undefined;
    static attributeTypeMap: Array<{
        name: string;
        baseName: string;
        type: string;
    }>;
    static getAttributeTypeMap(): {
        name: string;
        baseName: string;
        type: string;
    }[];
}
