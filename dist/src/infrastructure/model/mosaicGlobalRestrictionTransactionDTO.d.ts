import { MosaicRestrictionTypeEnum } from './mosaicRestrictionTypeEnum';
import { NetworkTypeEnum } from './networkTypeEnum';
/**
* Transaction to set a network-wide restriction rule to a mosaic.
*/
export declare class MosaicGlobalRestrictionTransactionDTO {
    /**
    * Entity\'s signature generated by the signer.
    */
    'signature': string;
    'signerPublicKey': string;
    /**
    * Entity version.
    */
    'version': number;
    'network': NetworkTypeEnum;
    'type': number;
    /**
    * Absolute amount. An amount of 123456789 (absolute) for a mosaic with divisibility 6 means 123.456789 (relative).
    */
    'maxFee': string;
    /**
    * Duration expressed in number of blocks.
    */
    'deadline': string;
    /**
    * Mosaic identifier. If the most significant bit of byte 0 is set, a namespaceId (alias) is used instead of the real mosaic identifier.
    */
    'mosaicId': string;
    /**
    * Mosaic identifier. If the most significant bit of byte 0 is set, a namespaceId (alias) is used instead of the real mosaic identifier.
    */
    'referenceMosaicId': string;
    /**
    * Restriction key.
    */
    'restrictionKey': string;
    /**
    * A value in a restriction transaction.
    */
    'previousRestrictionValue': string;
    /**
    * A value in a restriction transaction.
    */
    'newRestrictionValue': string;
    'previousRestrictionType': MosaicRestrictionTypeEnum;
    'newRestrictionType': MosaicRestrictionTypeEnum;
    static discriminator: string | undefined;
    static attributeTypeMap: Array<{
        name: string;
        baseName: string;
        type: string;
    }>;
    static getAttributeTypeMap(): {
        name: string;
        baseName: string;
        type: string;
    }[];
}
