/**
 * Catapult REST Endpoints
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.7.21
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
/**
* * 0 - Unlinked. * 1 - Balance-holding account that is linked to a remote harvester account. * 2 - Remote harvester account that is linked to a balance-holding account. * 3 - Remote harvester eligible account that is unlinked.
*/
export declare enum AccountTypeEnum {
    NUMBER_0,
    NUMBER_1,
    NUMBER_2,
    NUMBER_3,
}
