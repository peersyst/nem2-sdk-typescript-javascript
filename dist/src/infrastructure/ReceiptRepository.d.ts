import { Observable } from 'rxjs';
import { MerkleProofInfo } from '../model/blockchain/MerkleProofInfo';
import { Statement } from '../model/receipt/Statement';
/**
 * Receipt interface repository.
 *
 * @since 1.0
 */
export interface ReceiptRepository {
    /**
     * Get receipts from a block
     * Returns the receipts linked to a block.
     * @param {string} height The height of the block.
     * @return Observable<Statement>
     */
    getBlockReceipts(height: string): Observable<Statement>;
    /**
     * Get the merkle path for a given a receipt statement hash and block
     * Returns the merkle path for a [receipt statement or resolution](https://nemtech.github.io/concepts/receipt.html)
     * linked to a block. The path is the complementary data needed to calculate the merkle root.
     * A client can compare if the calculated root equals the one recorded in the block header,
     * verifying that the receipt was linked with the block.
     * @param height The height of the block.
     * @param hash The hash of the receipt statement or resolution.
     * @return Observable<MerkleProofInfo>
     */
    getMerkleReceipts(height: string, hash: string): Observable<MerkleProofInfo>;
}
