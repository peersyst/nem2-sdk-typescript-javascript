"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const UnresolvedMapping_1 = require("../../core/utils/UnresolvedMapping");
const Address_1 = require("../../model/account/Address");
const PublicAccount_1 = require("../../model/account/PublicAccount");
const MosaicId_1 = require("../../model/mosaic/MosaicId");
const NamespaceId_1 = require("../../model/namespace/NamespaceId");
const ArtifactExpiryReceipt_1 = require("../../model/receipt/ArtifactExpiryReceipt");
const BalanceChangeReceipt_1 = require("../../model/receipt/BalanceChangeReceipt");
const BalanceTransferReceipt_1 = require("../../model/receipt/BalanceTransferReceipt");
const InflationReceipt_1 = require("../../model/receipt/InflationReceipt");
const ReceiptSource_1 = require("../../model/receipt/ReceiptSource");
const ReceiptType_1 = require("../../model/receipt/ReceiptType");
const ResolutionEntry_1 = require("../../model/receipt/ResolutionEntry");
const ResolutionStatement_1 = require("../../model/receipt/ResolutionStatement");
const ResolutionType_1 = require("../../model/receipt/ResolutionType");
const Statement_1 = require("../../model/receipt/Statement");
const TransactionStatement_1 = require("../../model/receipt/TransactionStatement");
const UInt64_1 = require("../../model/UInt64");
/**
 * @param receiptDTO
 * @param networkType
 * @returns {Statement}
 * @see https://github.com/nemtech/catapult-server/blob/master/src/catapult/model/ReceiptType.h
 * @see https://github.com/nemtech/catapult-server/blob/master/src/catapult/model/ReceiptType.cpp
 * @constructor
 */
exports.CreateStatementFromDTO = (receiptDTO, networkType) => {
    return new Statement_1.Statement(receiptDTO.transactionStatements.map((statement) => createTransactionStatement(statement.statement, networkType)), receiptDTO.addressResolutionStatements.map((statement) => createResolutionStatement(statement.statement, ResolutionType_1.ResolutionType.Address)), receiptDTO.mosaicResolutionStatements.map((statement) => createResolutionStatement(statement.statement, ResolutionType_1.ResolutionType.Mosaic)));
};
/**
 * @param receiptDTO
 * @param networkType
 * @returns {Receipt}
 * @constructor
 */
exports.CreateReceiptFromDTO = (receiptDTO, networkType) => {
    switch (receiptDTO.type) {
        case ReceiptType_1.ReceiptType.Harvest_Fee:
        case ReceiptType_1.ReceiptType.LockHash_Created:
        case ReceiptType_1.ReceiptType.LockHash_Completed:
        case ReceiptType_1.ReceiptType.LockHash_Expired:
        case ReceiptType_1.ReceiptType.LockSecret_Created:
        case ReceiptType_1.ReceiptType.LockSecret_Completed:
        case ReceiptType_1.ReceiptType.LockSecret_Expired:
            return createBalanceChangeReceipt(receiptDTO, networkType);
        case ReceiptType_1.ReceiptType.Mosaic_Levy:
        case ReceiptType_1.ReceiptType.Mosaic_Rental_Fee:
        case ReceiptType_1.ReceiptType.Namespace_Rental_Fee:
            return createBalanceTransferReceipt(receiptDTO, networkType);
        case ReceiptType_1.ReceiptType.Mosaic_Expired:
        case ReceiptType_1.ReceiptType.Namespace_Expired:
        case ReceiptType_1.ReceiptType.Namespace_Deleted:
            return createArtifactExpiryReceipt(receiptDTO);
        case ReceiptType_1.ReceiptType.Inflation:
            return createInflationReceipt(receiptDTO);
        default:
            throw new Error(`Receipt type: ${receiptDTO.type} not recognized.`);
    }
};
/**
 * @internal
 * @param statementDTO
 * @param resolutionType
 * @returns {ResolutionStatement}
 * @constructor
 */
const createResolutionStatement = (statementDTO, resolutionType) => {
    switch (resolutionType) {
        case ResolutionType_1.ResolutionType.Address:
            return new ResolutionStatement_1.ResolutionStatement(ResolutionType_1.ResolutionType.Address, UInt64_1.UInt64.fromNumericString(statementDTO.height), extractUnresolvedAddress(statementDTO.unresolved), statementDTO.resolutionEntries.map((entry) => {
                return new ResolutionEntry_1.ResolutionEntry(Address_1.Address.createFromEncoded(entry.resolved), new ReceiptSource_1.ReceiptSource(entry.source.primaryId, entry.source.secondaryId));
            }));
        case ResolutionType_1.ResolutionType.Mosaic:
            return new ResolutionStatement_1.ResolutionStatement(ResolutionType_1.ResolutionType.Mosaic, UInt64_1.UInt64.fromNumericString(statementDTO.height), UnresolvedMapping_1.UnresolvedMapping.toUnresolvedMosaic(statementDTO.unresolved), statementDTO.resolutionEntries.map((entry) => {
                return new ResolutionEntry_1.ResolutionEntry(new MosaicId_1.MosaicId(entry.resolved), new ReceiptSource_1.ReceiptSource(entry.source.primaryId, entry.source.secondaryId));
            }));
        default:
            throw new Error('Resolution type invalid');
    }
};
/**
 * @internal
 * @param statementDTO
 * @param networkType
 * @returns {TransactionStatement}
 * @constructor
 */
const createTransactionStatement = (statementDTO, networkType) => {
    return new TransactionStatement_1.TransactionStatement(UInt64_1.UInt64.fromNumericString(statementDTO.height), new ReceiptSource_1.ReceiptSource(statementDTO.source.primaryId, statementDTO.source.secondaryId), statementDTO.receipts.map((receipt) => {
        return exports.CreateReceiptFromDTO(receipt, networkType);
    }));
};
/**
 * @internal
 * @param receiptDTO
 * @param networkType
 * @returns {BalanceChangeReceipt}
 * @constructor
 */
const createBalanceChangeReceipt = (receiptDTO, networkType) => {
    return new BalanceChangeReceipt_1.BalanceChangeReceipt(PublicAccount_1.PublicAccount.createFromPublicKey(receiptDTO.targetPublicKey, networkType), new MosaicId_1.MosaicId(receiptDTO.mosaicId), UInt64_1.UInt64.fromNumericString(receiptDTO.amount), receiptDTO.version, receiptDTO.type);
};
/**
 * @internal
 * @param receiptDTO
 * @param networkType
 * @returns {BalanceTransferReceipt}
 * @constructor
 */
const createBalanceTransferReceipt = (receiptDTO, networkType) => {
    return new BalanceTransferReceipt_1.BalanceTransferReceipt(PublicAccount_1.PublicAccount.createFromPublicKey(receiptDTO.senderPublicKey, networkType), Address_1.Address.createFromEncoded(receiptDTO.recipientAddress), new MosaicId_1.MosaicId(receiptDTO.mosaicId), UInt64_1.UInt64.fromNumericString(receiptDTO.amount), receiptDTO.version, receiptDTO.type);
};
/**
 * @internal
 * @param receiptDTO
 * @returns {ArtifactExpiryReceipt}
 * @constructor
 */
const createArtifactExpiryReceipt = (receiptDTO) => {
    return new ArtifactExpiryReceipt_1.ArtifactExpiryReceipt(extractArtifactId(receiptDTO.type, receiptDTO.artifactId), receiptDTO.version, receiptDTO.type);
};
/**
 * @internal
 * @param receiptDTO
 * @returns {InflationReceipt}
 * @constructor
 */
const createInflationReceipt = (receiptDTO) => {
    return new InflationReceipt_1.InflationReceipt(new MosaicId_1.MosaicId(receiptDTO.mosaicId), UInt64_1.UInt64.fromNumericString(receiptDTO.amount), receiptDTO.version, receiptDTO.type);
};
/**
 * @internal
 * @param receiptType receipt type
 * @param id Artifact id
 * @returns {MosaicId | NamespaceId}
 */
const extractArtifactId = (receiptType, id) => {
    switch (receiptType) {
        case ReceiptType_1.ReceiptType.Mosaic_Expired:
            return new MosaicId_1.MosaicId(id);
        case ReceiptType_1.ReceiptType.Namespace_Expired:
        case ReceiptType_1.ReceiptType.Namespace_Deleted:
            return NamespaceId_1.NamespaceId.createFromEncoded(id);
        default:
            throw new Error('Receipt type is not supported.');
    }
};
/**
 * @interal
 * @param unresolvedAddress unresolved address
 * @returns {Address | NamespaceId}
 */
const extractUnresolvedAddress = (unresolvedAddress) => {
    if (typeof unresolvedAddress === 'string') {
        return UnresolvedMapping_1.UnresolvedMapping.toUnresolvedAddress(unresolvedAddress);
    }
    else if (typeof unresolvedAddress === 'object') {
        if (unresolvedAddress.hasOwnProperty('address')) {
            return Address_1.Address.createFromRawAddress(unresolvedAddress.address);
        }
        else if (unresolvedAddress.hasOwnProperty('id')) {
            return NamespaceId_1.NamespaceId.createFromEncoded(unresolvedAddress.id);
        }
    }
    throw new Error(`UnresolvedAddress: ${unresolvedAddress} type is not recognised`);
};
//# sourceMappingURL=CreateReceiptFromDTO.js.map