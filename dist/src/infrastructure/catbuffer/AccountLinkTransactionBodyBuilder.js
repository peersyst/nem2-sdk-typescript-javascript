"use strict";
// tslint:disable: jsdoc-format
/**
*** Copyright (c) 2016-present,
*** Jaguar0625, gimre, BloodyRookie, Tech Bureau, Corp. All rights reserved.
***
*** This file is part of Catapult.
***
*** Catapult is free software: you can redistribute it and/or modify
*** it under the terms of the GNU Lesser General Public License as published by
*** the Free Software Foundation, either version 3 of the License, or
*** (at your option) any later version.
***
*** Catapult is distributed in the hope that it will be useful,
*** but WITHOUT ANY WARRANTY; without even the implied warranty of
*** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*** GNU Lesser General Public License for more details.
***
*** You should have received a copy of the GNU Lesser General Public License
*** along with Catapult. If not, see <http://www.gnu.org/licenses/>.
**/
Object.defineProperty(exports, "__esModule", { value: true });
const GeneratorUtils_1 = require("./GeneratorUtils");
const KeyDto_1 = require("./KeyDto");
/** Binary layout for an account link transaction. */
class AccountLinkTransactionBodyBuilder {
    /**
     * Constructor.
     *
     * @param remotePublicKey Remote public key.
     * @param linkAction Account link action.
     */
    constructor(remotePublicKey, linkAction) {
        this.remotePublicKey = remotePublicKey;
        this.linkAction = linkAction;
    }
    /**
     * Creates an instance of AccountLinkTransactionBodyBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of AccountLinkTransactionBodyBuilder.
     */
    static loadFromBinary(payload) {
        const byteArray = Array.from(payload);
        const remotePublicKey = KeyDto_1.KeyDto.loadFromBinary(Uint8Array.from(byteArray));
        byteArray.splice(0, remotePublicKey.getSize());
        const linkAction = GeneratorUtils_1.GeneratorUtils.bufferToUint(GeneratorUtils_1.GeneratorUtils.getBytes(Uint8Array.from(byteArray), 1));
        byteArray.splice(0, 1);
        return new AccountLinkTransactionBodyBuilder(remotePublicKey, linkAction);
    }
    /**
     * Gets remote public key.
     *
     * @return Remote public key.
     */
    getRemotePublicKey() {
        return this.remotePublicKey;
    }
    /**
     * Gets account link action.
     *
     * @return Account link action.
     */
    getLinkAction() {
        return this.linkAction;
    }
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize() {
        let size = 0;
        size += this.remotePublicKey.getSize();
        size += 1; // linkAction
        return size;
    }
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize() {
        let newArray = Uint8Array.from([]);
        const remotePublicKeyBytes = this.remotePublicKey.serialize();
        newArray = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(newArray, remotePublicKeyBytes);
        const linkActionBytes = GeneratorUtils_1.GeneratorUtils.uintToBuffer(this.linkAction, 1);
        newArray = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(newArray, linkActionBytes);
        return newArray;
    }
}
exports.AccountLinkTransactionBodyBuilder = AccountLinkTransactionBodyBuilder;
//# sourceMappingURL=AccountLinkTransactionBodyBuilder.js.map