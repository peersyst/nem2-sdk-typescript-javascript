/** Namespace id. */
export declare class NamespaceIdDto {
    /** Namespace id. */
    namespaceId: number[];
    /**
     * Constructor.
     *
     * @param namespaceId Namespace id.
     */
    constructor(namespaceId: number[]);
    /**
     * Creates an instance of NamespaceIdDto from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of NamespaceIdDto.
     */
    static loadFromBinary(payload: Uint8Array): NamespaceIdDto;
    /**
     * Gets Namespace id.
     *
     * @return Namespace id.
     */
    getNamespaceId(): number[];
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
