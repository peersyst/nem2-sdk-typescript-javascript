"use strict";
// tslint:disable: jsdoc-format
/**
*** Copyright (c) 2016-present,
*** Jaguar0625, gimre, BloodyRookie, Tech Bureau, Corp. All rights reserved.
***
*** This file is part of Catapult.
***
*** Catapult is free software: you can redistribute it and/or modify
*** it under the terms of the GNU Lesser General Public License as published by
*** the Free Software Foundation, either version 3 of the License, or
*** (at your option) any later version.
***
*** Catapult is distributed in the hope that it will be useful,
*** but WITHOUT ANY WARRANTY; without even the implied warranty of
*** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*** GNU Lesser General Public License for more details.
***
*** You should have received a copy of the GNU Lesser General Public License
*** along with Catapult. If not, see <http://www.gnu.org/licenses/>.
**/
Object.defineProperty(exports, "__esModule", { value: true });
/** Enumeration of mosaic restriction types. */
var MosaicRestrictionTypeDto;
(function (MosaicRestrictionTypeDto) {
    /** Uninitialized value indicating no restriction. */
    MosaicRestrictionTypeDto[MosaicRestrictionTypeDto["NONE"] = 0] = "NONE";
    /** Allow if equal. */
    MosaicRestrictionTypeDto[MosaicRestrictionTypeDto["EQ"] = 1] = "EQ";
    /** Allow if not equal. */
    MosaicRestrictionTypeDto[MosaicRestrictionTypeDto["NE"] = 2] = "NE";
    /** Allow if less than. */
    MosaicRestrictionTypeDto[MosaicRestrictionTypeDto["LT"] = 3] = "LT";
    /** Allow if less than or equal. */
    MosaicRestrictionTypeDto[MosaicRestrictionTypeDto["LE"] = 4] = "LE";
    /** Allow if greater than. */
    MosaicRestrictionTypeDto[MosaicRestrictionTypeDto["GT"] = 5] = "GT";
    /** Allow if greater than or equal. */
    MosaicRestrictionTypeDto[MosaicRestrictionTypeDto["GE"] = 6] = "GE";
})(MosaicRestrictionTypeDto = exports.MosaicRestrictionTypeDto || (exports.MosaicRestrictionTypeDto = {}));
//# sourceMappingURL=MosaicRestrictionTypeDto.js.map