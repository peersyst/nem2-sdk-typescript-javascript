import { MosaicRestrictionTypeDto } from './MosaicRestrictionTypeDto';
import { UnresolvedMosaicIdDto } from './UnresolvedMosaicIdDto';
/** Binary layout for a mosaic global restriction transaction. */
export declare class MosaicGlobalRestrictionTransactionBodyBuilder {
    /** Identifier of the mosaic being restricted. */
    mosaicId: UnresolvedMosaicIdDto;
    /** Identifier of the mosaic providing the restriction key. */
    referenceMosaicId: UnresolvedMosaicIdDto;
    /** Restriction key relative to the reference mosaic identifier. */
    restrictionKey: number[];
    /** Previous restriction value. */
    previousRestrictionValue: number[];
    /** New restriction value. */
    newRestrictionValue: number[];
    /** Previous restriction type. */
    previousRestrictionType: MosaicRestrictionTypeDto;
    /** New restriction type. */
    newRestrictionType: MosaicRestrictionTypeDto;
    /**
     * Constructor.
     *
     * @param mosaicId Identifier of the mosaic being restricted.
     * @param referenceMosaicId Identifier of the mosaic providing the restriction key.
     * @param restrictionKey Restriction key relative to the reference mosaic identifier.
     * @param previousRestrictionValue Previous restriction value.
     * @param newRestrictionValue New restriction value.
     * @param previousRestrictionType Previous restriction type.
     * @param newRestrictionType New restriction type.
     */
    constructor(mosaicId: UnresolvedMosaicIdDto, referenceMosaicId: UnresolvedMosaicIdDto, restrictionKey: number[], previousRestrictionValue: number[], newRestrictionValue: number[], previousRestrictionType: MosaicRestrictionTypeDto, newRestrictionType: MosaicRestrictionTypeDto);
    /**
     * Creates an instance of MosaicGlobalRestrictionTransactionBodyBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of MosaicGlobalRestrictionTransactionBodyBuilder.
     */
    static loadFromBinary(payload: Uint8Array): MosaicGlobalRestrictionTransactionBodyBuilder;
    /**
     * Gets identifier of the mosaic being restricted.
     *
     * @return Identifier of the mosaic being restricted.
     */
    getMosaicId(): UnresolvedMosaicIdDto;
    /**
     * Gets identifier of the mosaic providing the restriction key.
     *
     * @return Identifier of the mosaic providing the restriction key.
     */
    getReferenceMosaicId(): UnresolvedMosaicIdDto;
    /**
     * Gets restriction key relative to the reference mosaic identifier.
     *
     * @return Restriction key relative to the reference mosaic identifier.
     */
    getRestrictionKey(): number[];
    /**
     * Gets previous restriction value.
     *
     * @return Previous restriction value.
     */
    getPreviousRestrictionValue(): number[];
    /**
     * Gets new restriction value.
     *
     * @return New restriction value.
     */
    getNewRestrictionValue(): number[];
    /**
     * Gets previous restriction type.
     *
     * @return Previous restriction type.
     */
    getPreviousRestrictionType(): MosaicRestrictionTypeDto;
    /**
     * Gets new restriction type.
     *
     * @return New restriction type.
     */
    getNewRestrictionType(): MosaicRestrictionTypeDto;
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
