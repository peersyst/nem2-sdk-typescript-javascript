/**
*** Copyright (c) 2016-present,
*** Jaguar0625, gimre, BloodyRookie, Tech Bureau, Corp. All rights reserved.
***
*** This file is part of Catapult.
***
*** Catapult is free software: you can redistribute it and/or modify
*** it under the terms of the GNU Lesser General Public License as published by
*** the Free Software Foundation, either version 3 of the License, or
*** (at your option) any later version.
***
*** Catapult is distributed in the hope that it will be useful,
*** but WITHOUT ANY WARRANTY; without even the implied warranty of
*** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*** GNU Lesser General Public License for more details.
***
*** You should have received a copy of the GNU Lesser General Public License
*** along with Catapult. If not, see <http://www.gnu.org/licenses/>.
**/
import { AccountMosaicRestrictionTransactionBodyBuilder } from './AccountMosaicRestrictionTransactionBodyBuilder';
import { EmbeddedTransactionBuilder } from './EmbeddedTransactionBuilder';
import { EntityTypeDto } from './EntityTypeDto';
import { KeyDto } from './KeyDto';
import { NetworkTypeDto } from './NetworkTypeDto';
import { UnresolvedMosaicIdDto } from './UnresolvedMosaicIdDto';
/** Binary layout for an embedded account mosaic restriction transaction. */
export declare class EmbeddedAccountMosaicRestrictionTransactionBuilder extends EmbeddedTransactionBuilder {
    /** Account mosaic restriction transaction body. */
    accountMosaicRestrictionTransactionBody: AccountMosaicRestrictionTransactionBodyBuilder;
    /**
     * Constructor.
     *
     * @param signerPublicKey Entity signer's public key.
     * @param version Entity version.
     * @param network Entity network.
     * @param type Entity type.
     * @param restrictionFlags Account restriction flags.
     * @param restrictionAdditions Account restriction additions.
     * @param restrictionDeletions Account restriction deletions.
     */
    constructor(signerPublicKey: KeyDto, version: number, network: NetworkTypeDto, type: EntityTypeDto, restrictionFlags: number, restrictionAdditions: UnresolvedMosaicIdDto[], restrictionDeletions: UnresolvedMosaicIdDto[]);
    /**
     * Creates an instance of EmbeddedAccountMosaicRestrictionTransactionBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of EmbeddedAccountMosaicRestrictionTransactionBuilder.
     */
    static loadFromBinary(payload: Uint8Array): EmbeddedAccountMosaicRestrictionTransactionBuilder;
    /**
     * Gets account restriction flags.
     *
     * @return Account restriction flags.
     */
    getRestrictionFlags(): number;
    /**
     * Gets reserved padding to align restrictionAdditions on 8-byte boundary.
     *
     * @return Reserved padding to align restrictionAdditions on 8-byte boundary.
     */
    getAccountRestrictionTransactionBody_Reserved1(): number;
    /**
     * Gets account restriction additions.
     *
     * @return Account restriction additions.
     */
    getRestrictionAdditions(): UnresolvedMosaicIdDto[];
    /**
     * Gets account restriction deletions.
     *
     * @return Account restriction deletions.
     */
    getRestrictionDeletions(): UnresolvedMosaicIdDto[];
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
