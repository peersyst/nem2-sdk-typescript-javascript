"use strict";
// tslint:disable: jsdoc-format
/**
*** Copyright (c) 2016-present,
*** Jaguar0625, gimre, BloodyRookie, Tech Bureau, Corp. All rights reserved.
***
*** This file is part of Catapult.
***
*** Catapult is free software: you can redistribute it and/or modify
*** it under the terms of the GNU Lesser General Public License as published by
*** the Free Software Foundation, either version 3 of the License, or
*** (at your option) any later version.
***
*** Catapult is distributed in the hope that it will be useful,
*** but WITHOUT ANY WARRANTY; without even the implied warranty of
*** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*** GNU Lesser General Public License for more details.
***
*** You should have received a copy of the GNU Lesser General Public License
*** along with Catapult. If not, see <http://www.gnu.org/licenses/>.
**/
Object.defineProperty(exports, "__esModule", { value: true });
/** Enumeration of entity types. */
var EntityTypeDto;
(function (EntityTypeDto) {
    /** Reserved entity type. */
    EntityTypeDto[EntityTypeDto["RESERVED"] = 0] = "RESERVED";
    /** Transfer transaction builder. */
    EntityTypeDto[EntityTypeDto["TRANSFER_TRANSACTION_BUILDER"] = 16724] = "TRANSFER_TRANSACTION_BUILDER";
})(EntityTypeDto = exports.EntityTypeDto || (exports.EntityTypeDto = {}));
//# sourceMappingURL=EntityTypeDto.js.map