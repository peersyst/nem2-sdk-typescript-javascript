"use strict";
// tslint:disable: jsdoc-format
/**
*** Copyright (c) 2016-present,
*** Jaguar0625, gimre, BloodyRookie, Tech Bureau, Corp. All rights reserved.
***
*** This file is part of Catapult.
***
*** Catapult is free software: you can redistribute it and/or modify
*** it under the terms of the GNU Lesser General Public License as published by
*** the Free Software Foundation, either version 3 of the License, or
*** (at your option) any later version.
***
*** Catapult is distributed in the hope that it will be useful,
*** but WITHOUT ANY WARRANTY; without even the implied warranty of
*** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*** GNU Lesser General Public License for more details.
***
*** You should have received a copy of the GNU Lesser General Public License
*** along with Catapult. If not, see <http://www.gnu.org/licenses/>.
**/
Object.defineProperty(exports, "__esModule", { value: true });
const GeneratorUtils_1 = require("./GeneratorUtils");
/** Unresolved address. */
class UnresolvedAddressDto {
    /**
     * Constructor.
     *
     * @param unresolvedAddress Unresolved address.
     */
    constructor(unresolvedAddress) {
        this.unresolvedAddress = unresolvedAddress;
    }
    /**
     * Creates an instance of UnresolvedAddressDto from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of UnresolvedAddressDto.
     */
    static loadFromBinary(payload) {
        const byteArray = Array.from(payload);
        const unresolvedAddress = GeneratorUtils_1.GeneratorUtils.getBytes(Uint8Array.from(byteArray), 25);
        byteArray.splice(0, 25);
        return new UnresolvedAddressDto(unresolvedAddress);
    }
    /**
     * Gets Unresolved address.
     *
     * @return Unresolved address.
     */
    getUnresolvedAddress() {
        return this.unresolvedAddress;
    }
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize() {
        return 25;
    }
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize() {
        let newArray = Uint8Array.from([]);
        newArray = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(newArray, this.unresolvedAddress);
        return newArray;
    }
}
exports.UnresolvedAddressDto = UnresolvedAddressDto;
//# sourceMappingURL=UnresolvedAddressDto.js.map