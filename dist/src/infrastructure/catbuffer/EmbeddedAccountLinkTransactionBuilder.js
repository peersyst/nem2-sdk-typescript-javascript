"use strict";
// tslint:disable: jsdoc-format
/**
*** Copyright (c) 2016-present,
*** Jaguar0625, gimre, BloodyRookie, Tech Bureau, Corp. All rights reserved.
***
*** This file is part of Catapult.
***
*** Catapult is free software: you can redistribute it and/or modify
*** it under the terms of the GNU Lesser General Public License as published by
*** the Free Software Foundation, either version 3 of the License, or
*** (at your option) any later version.
***
*** Catapult is distributed in the hope that it will be useful,
*** but WITHOUT ANY WARRANTY; without even the implied warranty of
*** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*** GNU Lesser General Public License for more details.
***
*** You should have received a copy of the GNU Lesser General Public License
*** along with Catapult. If not, see <http://www.gnu.org/licenses/>.
**/
Object.defineProperty(exports, "__esModule", { value: true });
const AccountLinkTransactionBodyBuilder_1 = require("./AccountLinkTransactionBodyBuilder");
const EmbeddedTransactionBuilder_1 = require("./EmbeddedTransactionBuilder");
const GeneratorUtils_1 = require("./GeneratorUtils");
/** Binary layout for an embedded account link transaction. */
class EmbeddedAccountLinkTransactionBuilder extends EmbeddedTransactionBuilder_1.EmbeddedTransactionBuilder {
    /**
     * Constructor.
     *
     * @param signerPublicKey Entity signer's public key.
     * @param version Entity version.
     * @param network Entity network.
     * @param type Entity type.
     * @param remotePublicKey Remote public key.
     * @param linkAction Account link action.
     */
    // tslint:disable-next-line: max-line-length
    constructor(signerPublicKey, version, network, type, remotePublicKey, linkAction) {
        super(signerPublicKey, version, network, type);
        this.accountLinkTransactionBody = new AccountLinkTransactionBodyBuilder_1.AccountLinkTransactionBodyBuilder(remotePublicKey, linkAction);
    }
    /**
     * Creates an instance of EmbeddedAccountLinkTransactionBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of EmbeddedAccountLinkTransactionBuilder.
     */
    static loadFromBinary(payload) {
        const byteArray = Array.from(payload);
        const superObject = EmbeddedTransactionBuilder_1.EmbeddedTransactionBuilder.loadFromBinary(Uint8Array.from(byteArray));
        byteArray.splice(0, superObject.getSize());
        const accountLinkTransactionBody = AccountLinkTransactionBodyBuilder_1.AccountLinkTransactionBodyBuilder.loadFromBinary(Uint8Array.from(byteArray));
        byteArray.splice(0, accountLinkTransactionBody.getSize());
        // tslint:disable-next-line: max-line-length
        return new EmbeddedAccountLinkTransactionBuilder(superObject.signerPublicKey, superObject.version, superObject.network, superObject.type, accountLinkTransactionBody.remotePublicKey, accountLinkTransactionBody.linkAction);
    }
    /**
     * Gets remote public key.
     *
     * @return Remote public key.
     */
    getRemotePublicKey() {
        return this.accountLinkTransactionBody.getRemotePublicKey();
    }
    /**
     * Gets account link action.
     *
     * @return Account link action.
     */
    getLinkAction() {
        return this.accountLinkTransactionBody.getLinkAction();
    }
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize() {
        let size = super.getSize();
        size += this.accountLinkTransactionBody.getSize();
        return size;
    }
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize() {
        let newArray = Uint8Array.from([]);
        const superBytes = super.serialize();
        newArray = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(newArray, superBytes);
        const accountLinkTransactionBodyBytes = this.accountLinkTransactionBody.serialize();
        newArray = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(newArray, accountLinkTransactionBodyBytes);
        return newArray;
    }
}
exports.EmbeddedAccountLinkTransactionBuilder = EmbeddedAccountLinkTransactionBuilder;
//# sourceMappingURL=EmbeddedAccountLinkTransactionBuilder.js.map