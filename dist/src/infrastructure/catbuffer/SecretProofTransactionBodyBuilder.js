"use strict";
// tslint:disable: jsdoc-format
/**
*** Copyright (c) 2016-present,
*** Jaguar0625, gimre, BloodyRookie, Tech Bureau, Corp. All rights reserved.
***
*** This file is part of Catapult.
***
*** Catapult is free software: you can redistribute it and/or modify
*** it under the terms of the GNU Lesser General Public License as published by
*** the Free Software Foundation, either version 3 of the License, or
*** (at your option) any later version.
***
*** Catapult is distributed in the hope that it will be useful,
*** but WITHOUT ANY WARRANTY; without even the implied warranty of
*** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*** GNU Lesser General Public License for more details.
***
*** You should have received a copy of the GNU Lesser General Public License
*** along with Catapult. If not, see <http://www.gnu.org/licenses/>.
**/
Object.defineProperty(exports, "__esModule", { value: true });
const GeneratorUtils_1 = require("./GeneratorUtils");
const Hash256Dto_1 = require("./Hash256Dto");
const UnresolvedAddressDto_1 = require("./UnresolvedAddressDto");
/** Binary layout for a secret proof transaction. */
class SecretProofTransactionBodyBuilder {
    /**
     * Constructor.
     *
     * @param secret Secret.
     * @param hashAlgorithm Hash algorithm.
     * @param recipientAddress Locked mosaic recipient address.
     * @param proof Proof data.
     */
    // tslint:disable-next-line: max-line-length
    constructor(secret, hashAlgorithm, recipientAddress, proof) {
        this.secret = secret;
        this.hashAlgorithm = hashAlgorithm;
        this.recipientAddress = recipientAddress;
        this.proof = proof;
    }
    /**
     * Creates an instance of SecretProofTransactionBodyBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of SecretProofTransactionBodyBuilder.
     */
    static loadFromBinary(payload) {
        const byteArray = Array.from(payload);
        const secret = Hash256Dto_1.Hash256Dto.loadFromBinary(Uint8Array.from(byteArray));
        byteArray.splice(0, secret.getSize());
        const proofSize = GeneratorUtils_1.GeneratorUtils.bufferToUint(GeneratorUtils_1.GeneratorUtils.getBytes(Uint8Array.from(byteArray), 2));
        byteArray.splice(0, 2);
        const hashAlgorithm = GeneratorUtils_1.GeneratorUtils.bufferToUint(GeneratorUtils_1.GeneratorUtils.getBytes(Uint8Array.from(byteArray), 1));
        byteArray.splice(0, 1);
        const recipientAddress = UnresolvedAddressDto_1.UnresolvedAddressDto.loadFromBinary(Uint8Array.from(byteArray));
        byteArray.splice(0, recipientAddress.getSize());
        const proof = GeneratorUtils_1.GeneratorUtils.getBytes(Uint8Array.from(byteArray), proofSize);
        byteArray.splice(0, proofSize);
        return new SecretProofTransactionBodyBuilder(secret, hashAlgorithm, recipientAddress, proof);
    }
    /**
     * Gets secret.
     *
     * @return Secret.
     */
    getSecret() {
        return this.secret;
    }
    /**
     * Gets hash algorithm.
     *
     * @return Hash algorithm.
     */
    getHashAlgorithm() {
        return this.hashAlgorithm;
    }
    /**
     * Gets locked mosaic recipient address.
     *
     * @return Locked mosaic recipient address.
     */
    getRecipientAddress() {
        return this.recipientAddress;
    }
    /**
     * Gets proof data.
     *
     * @return Proof data.
     */
    getProof() {
        return this.proof;
    }
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize() {
        let size = 0;
        size += this.secret.getSize();
        size += 2; // proofSize
        size += 1; // hashAlgorithm
        size += this.recipientAddress.getSize();
        size += this.proof.length;
        return size;
    }
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize() {
        let newArray = Uint8Array.from([]);
        const secretBytes = this.secret.serialize();
        newArray = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(newArray, secretBytes);
        const proofSizeBytes = GeneratorUtils_1.GeneratorUtils.uintToBuffer(this.proof.length, 2);
        newArray = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(newArray, proofSizeBytes);
        const hashAlgorithmBytes = GeneratorUtils_1.GeneratorUtils.uintToBuffer(this.hashAlgorithm, 1);
        newArray = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(newArray, hashAlgorithmBytes);
        const recipientAddressBytes = this.recipientAddress.serialize();
        newArray = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(newArray, recipientAddressBytes);
        newArray = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(newArray, this.proof);
        return newArray;
    }
}
exports.SecretProofTransactionBodyBuilder = SecretProofTransactionBodyBuilder;
//# sourceMappingURL=SecretProofTransactionBodyBuilder.js.map