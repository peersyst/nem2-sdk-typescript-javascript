/** Address. */
export declare class AddressDto {
    /** Address. */
    address: Uint8Array;
    /**
     * Constructor.
     *
     * @param address Address.
     */
    constructor(address: Uint8Array);
    /**
     * Creates an instance of AddressDto from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of AddressDto.
     */
    static loadFromBinary(payload: Uint8Array): AddressDto;
    /**
     * Gets Address.
     *
     * @return Address.
     */
    getAddress(): Uint8Array;
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
