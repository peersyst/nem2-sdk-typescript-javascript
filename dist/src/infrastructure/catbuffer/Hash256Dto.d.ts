/** Hash256. */
export declare class Hash256Dto {
    /** Hash256. */
    hash256: Uint8Array;
    /**
     * Constructor.
     *
     * @param hash256 Hash256.
     */
    constructor(hash256: Uint8Array);
    /**
     * Creates an instance of Hash256Dto from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of Hash256Dto.
     */
    static loadFromBinary(payload: Uint8Array): Hash256Dto;
    /**
     * Gets Hash256.
     *
     * @return Hash256.
     */
    getHash256(): Uint8Array;
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
