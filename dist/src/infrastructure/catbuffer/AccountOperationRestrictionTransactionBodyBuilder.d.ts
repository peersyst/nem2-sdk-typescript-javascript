/** Binary layout for an account operation restriction transaction. */
export declare class AccountOperationRestrictionTransactionBodyBuilder {
    /** Account restriction flags. */
    restrictionFlags: number;
    /** Reserved padding to align restrictionAdditions on 8-byte boundary. */
    accountRestrictionTransactionBody_Reserved1: number;
    /** Account restriction additions. */
    restrictionAdditions: number[];
    /** Account restriction deletions. */
    restrictionDeletions: number[];
    /**
     * Constructor.
     *
     * @param restrictionFlags Account restriction flags.
     * @param restrictionAdditions Account restriction additions.
     * @param restrictionDeletions Account restriction deletions.
     */
    constructor(restrictionFlags: number, restrictionAdditions: number[], restrictionDeletions: number[]);
    /**
     * Creates an instance of AccountOperationRestrictionTransactionBodyBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of AccountOperationRestrictionTransactionBodyBuilder.
     */
    static loadFromBinary(payload: Uint8Array): AccountOperationRestrictionTransactionBodyBuilder;
    /**
     * Gets account restriction flags.
     *
     * @return Account restriction flags.
     */
    getRestrictionFlags(): number;
    /**
     * Gets reserved padding to align restrictionAdditions on 8-byte boundary.
     *
     * @return Reserved padding to align restrictionAdditions on 8-byte boundary.
     */
    getAccountRestrictionTransactionBody_Reserved1(): number;
    /**
     * Gets account restriction additions.
     *
     * @return Account restriction additions.
     */
    getRestrictionAdditions(): number[];
    /**
     * Gets account restriction deletions.
     *
     * @return Account restriction deletions.
     */
    getRestrictionDeletions(): number[];
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
