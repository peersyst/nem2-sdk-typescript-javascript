"use strict";
// tslint:disable: jsdoc-format
/**
*** Copyright (c) 2016-present,
*** Jaguar0625, gimre, BloodyRookie, Tech Bureau, Corp. All rights reserved.
***
*** This file is part of Catapult.
***
*** Catapult is free software: you can redistribute it and/or modify
*** it under the terms of the GNU Lesser General Public License as published by
*** the Free Software Foundation, either version 3 of the License, or
*** (at your option) any later version.
***
*** Catapult is distributed in the hope that it will be useful,
*** but WITHOUT ANY WARRANTY; without even the implied warranty of
*** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*** GNU Lesser General Public License for more details.
***
*** You should have received a copy of the GNU Lesser General Public License
*** along with Catapult. If not, see <http://www.gnu.org/licenses/>.
**/
Object.defineProperty(exports, "__esModule", { value: true });
const GeneratorUtils_1 = require("./GeneratorUtils");
/** Namespace id. */
class NamespaceIdDto {
    /**
     * Constructor.
     *
     * @param namespaceId Namespace id.
     */
    constructor(namespaceId) {
        this.namespaceId = namespaceId;
    }
    /**
     * Creates an instance of NamespaceIdDto from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of NamespaceIdDto.
     */
    static loadFromBinary(payload) {
        const byteArray = Array.from(payload);
        const namespaceId = GeneratorUtils_1.GeneratorUtils.bufferToUint64(GeneratorUtils_1.GeneratorUtils.getBytes(Uint8Array.from(byteArray), 8));
        byteArray.splice(0, 8);
        return new NamespaceIdDto(namespaceId);
    }
    /**
     * Gets Namespace id.
     *
     * @return Namespace id.
     */
    getNamespaceId() {
        return this.namespaceId;
    }
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize() {
        return 8;
    }
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize() {
        let newArray = Uint8Array.from([]);
        const namespaceIdBytes = GeneratorUtils_1.GeneratorUtils.uint64ToBuffer(this.getNamespaceId());
        newArray = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(newArray, namespaceIdBytes);
        return newArray;
    }
}
exports.NamespaceIdDto = NamespaceIdDto;
//# sourceMappingURL=NamespaceIdDto.js.map