/** Mosaic id. */
export declare class MosaicIdDto {
    /** Mosaic id. */
    mosaicId: number[];
    /**
     * Constructor.
     *
     * @param mosaicId Mosaic id.
     */
    constructor(mosaicId: number[]);
    /**
     * Creates an instance of MosaicIdDto from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of MosaicIdDto.
     */
    static loadFromBinary(payload: Uint8Array): MosaicIdDto;
    /**
     * Gets Mosaic id.
     *
     * @return Mosaic id.
     */
    getMosaicId(): number[];
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
