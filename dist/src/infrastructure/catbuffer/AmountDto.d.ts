/** Amount. */
export declare class AmountDto {
    /** Amount. */
    amount: number[];
    /**
     * Constructor.
     *
     * @param amount Amount.
     */
    constructor(amount: number[]);
    /**
     * Creates an instance of AmountDto from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of AmountDto.
     */
    static loadFromBinary(payload: Uint8Array): AmountDto;
    /**
     * Gets Amount.
     *
     * @return Amount.
     */
    getAmount(): number[];
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
