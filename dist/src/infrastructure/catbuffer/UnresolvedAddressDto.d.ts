/** Unresolved address. */
export declare class UnresolvedAddressDto {
    /** Unresolved address. */
    unresolvedAddress: Uint8Array;
    /**
     * Constructor.
     *
     * @param unresolvedAddress Unresolved address.
     */
    constructor(unresolvedAddress: Uint8Array);
    /**
     * Creates an instance of UnresolvedAddressDto from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of UnresolvedAddressDto.
     */
    static loadFromBinary(payload: Uint8Array): UnresolvedAddressDto;
    /**
     * Gets Unresolved address.
     *
     * @return Unresolved address.
     */
    getUnresolvedAddress(): Uint8Array;
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
