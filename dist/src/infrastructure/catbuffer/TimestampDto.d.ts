/** Timestamp. */
export declare class TimestampDto {
    /** Timestamp. */
    timestamp: number[];
    /**
     * Constructor.
     *
     * @param timestamp Timestamp.
     */
    constructor(timestamp: number[]);
    /**
     * Creates an instance of TimestampDto from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of TimestampDto.
     */
    static loadFromBinary(payload: Uint8Array): TimestampDto;
    /**
     * Gets Timestamp.
     *
     * @return Timestamp.
     */
    getTimestamp(): number[];
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
