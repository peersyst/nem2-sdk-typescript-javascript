/**
*** Copyright (c) 2016-present,
*** Jaguar0625, gimre, BloodyRookie, Tech Bureau, Corp. All rights reserved.
***
*** This file is part of Catapult.
***
*** Catapult is free software: you can redistribute it and/or modify
*** it under the terms of the GNU Lesser General Public License as published by
*** the Free Software Foundation, either version 3 of the License, or
*** (at your option) any later version.
***
*** Catapult is distributed in the hope that it will be useful,
*** but WITHOUT ANY WARRANTY; without even the implied warranty of
*** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*** GNU Lesser General Public License for more details.
***
*** You should have received a copy of the GNU Lesser General Public License
*** along with Catapult. If not, see <http://www.gnu.org/licenses/>.
**/
import { AmountDto } from './AmountDto';
import { EntityTypeDto } from './EntityTypeDto';
import { KeyDto } from './KeyDto';
import { MosaicSupplyChangeActionDto } from './MosaicSupplyChangeActionDto';
import { MosaicSupplyChangeTransactionBodyBuilder } from './MosaicSupplyChangeTransactionBodyBuilder';
import { NetworkTypeDto } from './NetworkTypeDto';
import { SignatureDto } from './SignatureDto';
import { TimestampDto } from './TimestampDto';
import { TransactionBuilder } from './TransactionBuilder';
import { UnresolvedMosaicIdDto } from './UnresolvedMosaicIdDto';
/** Binary layout for a non-embedded mosaic supply change transaction. */
export declare class MosaicSupplyChangeTransactionBuilder extends TransactionBuilder {
    /** Mosaic supply change transaction body. */
    mosaicSupplyChangeTransactionBody: MosaicSupplyChangeTransactionBodyBuilder;
    /**
     * Constructor.
     *
     * @param signature Entity signature.
     * @param signerPublicKey Entity signer's public key.
     * @param version Entity version.
     * @param network Entity network.
     * @param type Entity type.
     * @param fee Transaction fee.
     * @param deadline Transaction deadline.
     * @param mosaicId Affected mosaic identifier.
     * @param delta Change amount.
     * @param action Supply change action.
     */
    constructor(signature: SignatureDto, signerPublicKey: KeyDto, version: number, network: NetworkTypeDto, type: EntityTypeDto, fee: AmountDto, deadline: TimestampDto, mosaicId: UnresolvedMosaicIdDto, delta: AmountDto, action: MosaicSupplyChangeActionDto);
    /**
     * Creates an instance of MosaicSupplyChangeTransactionBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of MosaicSupplyChangeTransactionBuilder.
     */
    static loadFromBinary(payload: Uint8Array): MosaicSupplyChangeTransactionBuilder;
    /**
     * Gets affected mosaic identifier.
     *
     * @return Affected mosaic identifier.
     */
    getMosaicId(): UnresolvedMosaicIdDto;
    /**
     * Gets change amount.
     *
     * @return Change amount.
     */
    getDelta(): AmountDto;
    /**
     * Gets supply change action.
     *
     * @return Supply change action.
     */
    getAction(): MosaicSupplyChangeActionDto;
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
