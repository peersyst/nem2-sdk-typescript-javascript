/**
*** Copyright (c) 2016-present,
*** Jaguar0625, gimre, BloodyRookie, Tech Bureau, Corp. All rights reserved.
***
*** This file is part of Catapult.
***
*** Catapult is free software: you can redistribute it and/or modify
*** it under the terms of the GNU Lesser General Public License as published by
*** the Free Software Foundation, either version 3 of the License, or
*** (at your option) any later version.
***
*** Catapult is distributed in the hope that it will be useful,
*** but WITHOUT ANY WARRANTY; without even the implied warranty of
*** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*** GNU Lesser General Public License for more details.
***
*** You should have received a copy of the GNU Lesser General Public License
*** along with Catapult. If not, see <http://www.gnu.org/licenses/>.
**/
import { AmountDto } from './AmountDto';
import { EntityTypeDto } from './EntityTypeDto';
import { KeyDto } from './KeyDto';
import { NetworkTypeDto } from './NetworkTypeDto';
import { SignatureDto } from './SignatureDto';
import { TimestampDto } from './TimestampDto';
/** Binary layout for a transaction. */
export declare class TransactionBuilder {
    /** Entity size. */
    size: number;
    /** Reserved padding to align Signature on 8-byte boundary. */
    verifiableEntityHeader_Reserved1: number;
    /** Entity signature. */
    signature: SignatureDto;
    /** Entity signer's public key. */
    signerPublicKey: KeyDto;
    /** Reserved padding to align end of EntityBody on 8-byte boundary. */
    entityBody_Reserved1: number;
    /** Entity version. */
    version: number;
    /** Entity network. */
    network: NetworkTypeDto;
    /** Entity type. */
    type: EntityTypeDto;
    /** Transaction fee. */
    fee: AmountDto;
    /** Transaction deadline. */
    deadline: TimestampDto;
    /**
     * Constructor.
     *
     * @param signature Entity signature.
     * @param signerPublicKey Entity signer's public key.
     * @param version Entity version.
     * @param network Entity network.
     * @param type Entity type.
     * @param fee Transaction fee.
     * @param deadline Transaction deadline.
     */
    constructor(signature: SignatureDto, signerPublicKey: KeyDto, version: number, network: NetworkTypeDto, type: EntityTypeDto, fee: AmountDto, deadline: TimestampDto);
    /**
     * Creates an instance of TransactionBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of TransactionBuilder.
     */
    static loadFromBinary(payload: Uint8Array): TransactionBuilder;
    /**
     * Gets reserved padding to align Signature on 8-byte boundary.
     *
     * @return Reserved padding to align Signature on 8-byte boundary.
     */
    getVerifiableEntityHeader_Reserved1(): number;
    /**
     * Gets entity signature.
     *
     * @return Entity signature.
     */
    getSignature(): SignatureDto;
    /**
     * Gets entity signer's public key.
     *
     * @return Entity signer's public key.
     */
    getSignerPublicKey(): KeyDto;
    /**
     * Gets reserved padding to align end of EntityBody on 8-byte boundary.
     *
     * @return Reserved padding to align end of EntityBody on 8-byte boundary.
     */
    getEntityBody_Reserved1(): number;
    /**
     * Gets entity version.
     *
     * @return Entity version.
     */
    getVersion(): number;
    /**
     * Gets entity network.
     *
     * @return Entity network.
     */
    getNetwork(): NetworkTypeDto;
    /**
     * Gets entity type.
     *
     * @return Entity type.
     */
    getType(): EntityTypeDto;
    /**
     * Gets transaction fee.
     *
     * @return Transaction fee.
     */
    getFee(): AmountDto;
    /**
     * Gets transaction deadline.
     *
     * @return Transaction deadline.
     */
    getDeadline(): TimestampDto;
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
