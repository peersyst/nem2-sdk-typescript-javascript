import { KeyDto } from './KeyDto';
/** Binary layout for a multisig account modification transaction. */
export declare class MultisigAccountModificationTransactionBodyBuilder {
    /** Relative change of the minimal number of cosignatories required when removing an account. */
    minRemovalDelta: number;
    /** Relative change of the minimal number of cosignatories required when approving a transaction. */
    minApprovalDelta: number;
    /** Reserved padding to align publicKeyAdditions on 8-byte boundary. */
    multisigAccountModificationTransactionBody_Reserved1: number;
    /** Cosignatory public key additions. */
    publicKeyAdditions: KeyDto[];
    /** Cosignatory public key deletions. */
    publicKeyDeletions: KeyDto[];
    /**
     * Constructor.
     *
     * @param minRemovalDelta Relative change of the minimal number of cosignatories required when removing an account.
     * @param minApprovalDelta Relative change of the minimal number of cosignatories required when approving a transaction.
     * @param publicKeyAdditions Cosignatory public key additions.
     * @param publicKeyDeletions Cosignatory public key deletions.
     */
    constructor(minRemovalDelta: number, minApprovalDelta: number, publicKeyAdditions: KeyDto[], publicKeyDeletions: KeyDto[]);
    /**
     * Creates an instance of MultisigAccountModificationTransactionBodyBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of MultisigAccountModificationTransactionBodyBuilder.
     */
    static loadFromBinary(payload: Uint8Array): MultisigAccountModificationTransactionBodyBuilder;
    /**
     * Gets relative change of the minimal number of cosignatories required when removing an account.
     *
     * @return Relative change of the minimal number of cosignatories required when removing an account.
     */
    getMinRemovalDelta(): number;
    /**
     * Gets relative change of the minimal number of cosignatories required when approving a transaction.
     *
     * @return Relative change of the minimal number of cosignatories required when approving a transaction.
     */
    getMinApprovalDelta(): number;
    /**
     * Gets reserved padding to align publicKeyAdditions on 8-byte boundary.
     *
     * @return Reserved padding to align publicKeyAdditions on 8-byte boundary.
     */
    getMultisigAccountModificationTransactionBody_Reserved1(): number;
    /**
     * Gets cosignatory public key additions.
     *
     * @return Cosignatory public key additions.
     */
    getPublicKeyAdditions(): KeyDto[];
    /**
     * Gets cosignatory public key deletions.
     *
     * @return Cosignatory public key deletions.
     */
    getPublicKeyDeletions(): KeyDto[];
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
