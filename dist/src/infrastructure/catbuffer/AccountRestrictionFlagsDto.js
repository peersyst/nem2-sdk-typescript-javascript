"use strict";
// tslint:disable: jsdoc-format
/**
*** Copyright (c) 2016-present,
*** Jaguar0625, gimre, BloodyRookie, Tech Bureau, Corp. All rights reserved.
***
*** This file is part of Catapult.
***
*** Catapult is free software: you can redistribute it and/or modify
*** it under the terms of the GNU Lesser General Public License as published by
*** the Free Software Foundation, either version 3 of the License, or
*** (at your option) any later version.
***
*** Catapult is distributed in the hope that it will be useful,
*** but WITHOUT ANY WARRANTY; without even the implied warranty of
*** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*** GNU Lesser General Public License for more details.
***
*** You should have received a copy of the GNU Lesser General Public License
*** along with Catapult. If not, see <http://www.gnu.org/licenses/>.
**/
Object.defineProperty(exports, "__esModule", { value: true });
/** Enumeration of account restriction flags. */
var AccountRestrictionFlagsDto;
(function (AccountRestrictionFlagsDto) {
    /** Restriction type is an address. */
    AccountRestrictionFlagsDto[AccountRestrictionFlagsDto["ADDRESS"] = 1] = "ADDRESS";
    /** Restriction type is a mosaic identifier. */
    AccountRestrictionFlagsDto[AccountRestrictionFlagsDto["MOSAIC_ID"] = 2] = "MOSAIC_ID";
    /** Restriction type is a transaction type. */
    AccountRestrictionFlagsDto[AccountRestrictionFlagsDto["TRANSACTION_TYPE"] = 4] = "TRANSACTION_TYPE";
    /** Restriction is interpreted as outgoing. */
    AccountRestrictionFlagsDto[AccountRestrictionFlagsDto["OUTGOING"] = 16384] = "OUTGOING";
    /** Restriction is interpreted as blocking operation. */
    AccountRestrictionFlagsDto[AccountRestrictionFlagsDto["BLOCK"] = 32768] = "BLOCK";
})(AccountRestrictionFlagsDto = exports.AccountRestrictionFlagsDto || (exports.AccountRestrictionFlagsDto = {}));
//# sourceMappingURL=AccountRestrictionFlagsDto.js.map