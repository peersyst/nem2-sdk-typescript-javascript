/**
*** Copyright (c) 2016-present,
*** Jaguar0625, gimre, BloodyRookie, Tech Bureau, Corp. All rights reserved.
***
*** This file is part of Catapult.
***
*** Catapult is free software: you can redistribute it and/or modify
*** it under the terms of the GNU Lesser General Public License as published by
*** the Free Software Foundation, either version 3 of the License, or
*** (at your option) any later version.
***
*** Catapult is distributed in the hope that it will be useful,
*** but WITHOUT ANY WARRANTY; without even the implied warranty of
*** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*** GNU Lesser General Public License for more details.
***
*** You should have received a copy of the GNU Lesser General Public License
*** along with Catapult. If not, see <http://www.gnu.org/licenses/>.
**/
import { BlockDurationDto } from './BlockDurationDto';
import { Hash256Dto } from './Hash256Dto';
import { UnresolvedMosaicBuilder } from './UnresolvedMosaicBuilder';
/** Binary layout for a hash lock transaction. */
export declare class HashLockTransactionBodyBuilder {
    /** Lock mosaic. */
    mosaic: UnresolvedMosaicBuilder;
    /** Number of blocks for which a lock should be valid. */
    duration: BlockDurationDto;
    /** Lock hash. */
    hash: Hash256Dto;
    /**
     * Constructor.
     *
     * @param mosaic Lock mosaic.
     * @param duration Number of blocks for which a lock should be valid.
     * @param hash Lock hash.
     */
    constructor(mosaic: UnresolvedMosaicBuilder, duration: BlockDurationDto, hash: Hash256Dto);
    /**
     * Creates an instance of HashLockTransactionBodyBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of HashLockTransactionBodyBuilder.
     */
    static loadFromBinary(payload: Uint8Array): HashLockTransactionBodyBuilder;
    /**
     * Gets lock mosaic.
     *
     * @return Lock mosaic.
     */
    getMosaic(): UnresolvedMosaicBuilder;
    /**
     * Gets number of blocks for which a lock should be valid.
     *
     * @return Number of blocks for which a lock should be valid.
     */
    getDuration(): BlockDurationDto;
    /**
     * Gets lock hash.
     *
     * @return Lock hash.
     */
    getHash(): Hash256Dto;
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
