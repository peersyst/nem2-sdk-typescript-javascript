/**
*** Copyright (c) 2016-present,
*** Jaguar0625, gimre, BloodyRookie, Tech Bureau, Corp. All rights reserved.
***
*** This file is part of Catapult.
***
*** Catapult is free software: you can redistribute it and/or modify
*** it under the terms of the GNU Lesser General Public License as published by
*** the Free Software Foundation, either version 3 of the License, or
*** (at your option) any later version.
***
*** Catapult is distributed in the hope that it will be useful,
*** but WITHOUT ANY WARRANTY; without even the implied warranty of
*** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*** GNU Lesser General Public License for more details.
***
*** You should have received a copy of the GNU Lesser General Public License
*** along with Catapult. If not, see <http://www.gnu.org/licenses/>.
**/
import { AddressDto } from './AddressDto';
import { AliasActionDto } from './AliasActionDto';
import { NamespaceIdDto } from './NamespaceIdDto';
/** Binary layout for an address alias transaction. */
export declare class AddressAliasTransactionBodyBuilder {
    /** Identifier of the namespace that will become an alias. */
    namespaceId: NamespaceIdDto;
    /** Aliased address. */
    address: AddressDto;
    /** Alias action. */
    aliasAction: AliasActionDto;
    /**
     * Constructor.
     *
     * @param namespaceId Identifier of the namespace that will become an alias.
     * @param address Aliased address.
     * @param aliasAction Alias action.
     */
    constructor(namespaceId: NamespaceIdDto, address: AddressDto, aliasAction: AliasActionDto);
    /**
     * Creates an instance of AddressAliasTransactionBodyBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of AddressAliasTransactionBodyBuilder.
     */
    static loadFromBinary(payload: Uint8Array): AddressAliasTransactionBodyBuilder;
    /**
     * Gets identifier of the namespace that will become an alias.
     *
     * @return Identifier of the namespace that will become an alias.
     */
    getNamespaceId(): NamespaceIdDto;
    /**
     * Gets aliased address.
     *
     * @return Aliased address.
     */
    getAddress(): AddressDto;
    /**
     * Gets alias action.
     *
     * @return Alias action.
     */
    getAliasAction(): AliasActionDto;
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
