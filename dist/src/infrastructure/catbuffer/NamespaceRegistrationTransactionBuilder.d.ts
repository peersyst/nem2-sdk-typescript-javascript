/**
*** Copyright (c) 2016-present,
*** Jaguar0625, gimre, BloodyRookie, Tech Bureau, Corp. All rights reserved.
***
*** This file is part of Catapult.
***
*** Catapult is free software: you can redistribute it and/or modify
*** it under the terms of the GNU Lesser General Public License as published by
*** the Free Software Foundation, either version 3 of the License, or
*** (at your option) any later version.
***
*** Catapult is distributed in the hope that it will be useful,
*** but WITHOUT ANY WARRANTY; without even the implied warranty of
*** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*** GNU Lesser General Public License for more details.
***
*** You should have received a copy of the GNU Lesser General Public License
*** along with Catapult. If not, see <http://www.gnu.org/licenses/>.
**/
import { AmountDto } from './AmountDto';
import { BlockDurationDto } from './BlockDurationDto';
import { EntityTypeDto } from './EntityTypeDto';
import { KeyDto } from './KeyDto';
import { NamespaceIdDto } from './NamespaceIdDto';
import { NamespaceRegistrationTransactionBodyBuilder } from './NamespaceRegistrationTransactionBodyBuilder';
import { NamespaceRegistrationTypeDto } from './NamespaceRegistrationTypeDto';
import { NetworkTypeDto } from './NetworkTypeDto';
import { SignatureDto } from './SignatureDto';
import { TimestampDto } from './TimestampDto';
import { TransactionBuilder } from './TransactionBuilder';
/** Binary layout for a non-embedded namespace registration transaction. */
export declare class NamespaceRegistrationTransactionBuilder extends TransactionBuilder {
    /** Namespace registration transaction body. */
    namespaceRegistrationTransactionBody: NamespaceRegistrationTransactionBodyBuilder;
    /**
     * Constructor.
     *
     * @param signature Entity signature.
     * @param signerPublicKey Entity signer's public key.
     * @param version Entity version.
     * @param network Entity network.
     * @param type Entity type.
     * @param fee Transaction fee.
     * @param deadline Transaction deadline.
     * @param duration Namespace duration.
     * @param parentId Parent namespace identifier.
     * @param id Namespace identifier.
     * @param name Namespace name.
     */
    constructor(signature: SignatureDto, signerPublicKey: KeyDto, version: number, network: NetworkTypeDto, type: EntityTypeDto, fee: AmountDto, deadline: TimestampDto, id: NamespaceIdDto, name: Uint8Array, duration?: BlockDurationDto, parentId?: NamespaceIdDto);
    /**
     * Creates an instance of NamespaceRegistrationTransactionBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of NamespaceRegistrationTransactionBuilder.
     */
    static loadFromBinary(payload: Uint8Array): NamespaceRegistrationTransactionBuilder;
    /**
     * Gets namespace duration.
     *
     * @return Namespace duration.
     */
    getDuration(): BlockDurationDto | undefined;
    /**
     * Gets parent namespace identifier.
     *
     * @return Parent namespace identifier.
     */
    getParentId(): NamespaceIdDto | undefined;
    /**
     * Gets namespace identifier.
     *
     * @return Namespace identifier.
     */
    getId(): NamespaceIdDto;
    /**
     * Gets namespace registration type.
     *
     * @return Namespace registration type.
     */
    getRegistrationType(): NamespaceRegistrationTypeDto;
    /**
     * Gets namespace name.
     *
     * @return Namespace name.
     */
    getName(): Uint8Array;
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
