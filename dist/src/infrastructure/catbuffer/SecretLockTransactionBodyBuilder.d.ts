/**
*** Copyright (c) 2016-present,
*** Jaguar0625, gimre, BloodyRookie, Tech Bureau, Corp. All rights reserved.
***
*** This file is part of Catapult.
***
*** Catapult is free software: you can redistribute it and/or modify
*** it under the terms of the GNU Lesser General Public License as published by
*** the Free Software Foundation, either version 3 of the License, or
*** (at your option) any later version.
***
*** Catapult is distributed in the hope that it will be useful,
*** but WITHOUT ANY WARRANTY; without even the implied warranty of
*** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*** GNU Lesser General Public License for more details.
***
*** You should have received a copy of the GNU Lesser General Public License
*** along with Catapult. If not, see <http://www.gnu.org/licenses/>.
**/
import { BlockDurationDto } from './BlockDurationDto';
import { Hash256Dto } from './Hash256Dto';
import { LockHashAlgorithmDto } from './LockHashAlgorithmDto';
import { UnresolvedAddressDto } from './UnresolvedAddressDto';
import { UnresolvedMosaicBuilder } from './UnresolvedMosaicBuilder';
/** Binary layout for a secret lock transaction. */
export declare class SecretLockTransactionBodyBuilder {
    /** Secret. */
    secret: Hash256Dto;
    /** Locked mosaic. */
    mosaic: UnresolvedMosaicBuilder;
    /** Number of blocks for which a lock should be valid. */
    duration: BlockDurationDto;
    /** Hash algorithm. */
    hashAlgorithm: LockHashAlgorithmDto;
    /** Locked mosaic recipient address. */
    recipientAddress: UnresolvedAddressDto;
    /**
     * Constructor.
     *
     * @param secret Secret.
     * @param mosaic Locked mosaic.
     * @param duration Number of blocks for which a lock should be valid.
     * @param hashAlgorithm Hash algorithm.
     * @param recipientAddress Locked mosaic recipient address.
     */
    constructor(secret: Hash256Dto, mosaic: UnresolvedMosaicBuilder, duration: BlockDurationDto, hashAlgorithm: LockHashAlgorithmDto, recipientAddress: UnresolvedAddressDto);
    /**
     * Creates an instance of SecretLockTransactionBodyBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of SecretLockTransactionBodyBuilder.
     */
    static loadFromBinary(payload: Uint8Array): SecretLockTransactionBodyBuilder;
    /**
     * Gets secret.
     *
     * @return Secret.
     */
    getSecret(): Hash256Dto;
    /**
     * Gets locked mosaic.
     *
     * @return Locked mosaic.
     */
    getMosaic(): UnresolvedMosaicBuilder;
    /**
     * Gets number of blocks for which a lock should be valid.
     *
     * @return Number of blocks for which a lock should be valid.
     */
    getDuration(): BlockDurationDto;
    /**
     * Gets hash algorithm.
     *
     * @return Hash algorithm.
     */
    getHashAlgorithm(): LockHashAlgorithmDto;
    /**
     * Gets locked mosaic recipient address.
     *
     * @return Locked mosaic recipient address.
     */
    getRecipientAddress(): UnresolvedAddressDto;
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
