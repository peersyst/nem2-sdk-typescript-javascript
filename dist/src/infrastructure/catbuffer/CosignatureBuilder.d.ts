import { KeyDto } from './KeyDto';
import { SignatureDto } from './SignatureDto';
/** Cosignature attached to an aggregate transaction. */
export declare class CosignatureBuilder {
    /** Cosigner public key. */
    signerPublicKey: KeyDto;
    /** Cosigner signature. */
    signature: SignatureDto;
    /**
     * Constructor.
     *
     * @param signerPublicKey Cosigner public key.
     * @param signature Cosigner signature.
     */
    constructor(signerPublicKey: KeyDto, signature: SignatureDto);
    /**
     * Creates an instance of CosignatureBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of CosignatureBuilder.
     */
    static loadFromBinary(payload: Uint8Array): CosignatureBuilder;
    /**
     * Gets cosigner public key.
     *
     * @return Cosigner public key.
     */
    getSignerPublicKey(): KeyDto;
    /**
     * Gets cosigner signature.
     *
     * @return Cosigner signature.
     */
    getSignature(): SignatureDto;
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
