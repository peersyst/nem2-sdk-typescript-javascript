/**
*** Copyright (c) 2016-present,
*** Jaguar0625, gimre, BloodyRookie, Tech Bureau, Corp. All rights reserved.
***
*** This file is part of Catapult.
***
*** Catapult is free software: you can redistribute it and/or modify
*** it under the terms of the GNU Lesser General Public License as published by
*** the Free Software Foundation, either version 3 of the License, or
*** (at your option) any later version.
***
*** Catapult is distributed in the hope that it will be useful,
*** but WITHOUT ANY WARRANTY; without even the implied warranty of
*** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*** GNU Lesser General Public License for more details.
***
*** You should have received a copy of the GNU Lesser General Public License
*** along with Catapult. If not, see <http://www.gnu.org/licenses/>.
**/
import { BlockDurationDto } from './BlockDurationDto';
import { MosaicIdDto } from './MosaicIdDto';
import { MosaicNonceDto } from './MosaicNonceDto';
/** Binary layout for a mosaic definition transaction. */
export declare class MosaicDefinitionTransactionBodyBuilder {
    /** Mosaic identifier. */
    id: MosaicIdDto;
    /** Mosaic duration. */
    duration: BlockDurationDto;
    /** Mosaic nonce. */
    nonce: MosaicNonceDto;
    /** Mosaic flags. */
    flags: number;
    /** Mosaic divisibility. */
    divisibility: number;
    /**
     * Constructor.
     *
     * @param id Mosaic identifier.
     * @param duration Mosaic duration.
     * @param nonce Mosaic nonce.
     * @param flags Mosaic flags.
     * @param divisibility Mosaic divisibility.
     */
    constructor(id: MosaicIdDto, duration: BlockDurationDto, nonce: MosaicNonceDto, flags: number, divisibility: number);
    /**
     * Creates an instance of MosaicDefinitionTransactionBodyBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of MosaicDefinitionTransactionBodyBuilder.
     */
    static loadFromBinary(payload: Uint8Array): MosaicDefinitionTransactionBodyBuilder;
    /**
     * Gets mosaic identifier.
     *
     * @return Mosaic identifier.
     */
    getId(): MosaicIdDto;
    /**
     * Gets mosaic duration.
     *
     * @return Mosaic duration.
     */
    getDuration(): BlockDurationDto;
    /**
     * Gets mosaic nonce.
     *
     * @return Mosaic nonce.
     */
    getNonce(): MosaicNonceDto;
    /**
     * Gets mosaic flags.
     *
     * @return Mosaic flags.
     */
    getFlags(): number;
    /**
     * Gets mosaic divisibility.
     *
     * @return Mosaic divisibility.
     */
    getDivisibility(): number;
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
