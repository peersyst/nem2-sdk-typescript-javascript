/**
*** Copyright (c) 2016-present,
*** Jaguar0625, gimre, BloodyRookie, Tech Bureau, Corp. All rights reserved.
***
*** This file is part of Catapult.
***
*** Catapult is free software: you can redistribute it and/or modify
*** it under the terms of the GNU Lesser General Public License as published by
*** the Free Software Foundation, either version 3 of the License, or
*** (at your option) any later version.
***
*** Catapult is distributed in the hope that it will be useful,
*** but WITHOUT ANY WARRANTY; without even the implied warranty of
*** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*** GNU Lesser General Public License for more details.
***
*** You should have received a copy of the GNU Lesser General Public License
*** along with Catapult. If not, see <http://www.gnu.org/licenses/>.
**/
import { AmountDto } from './AmountDto';
import { MosaicIdDto } from './MosaicIdDto';
/** Binary layout for a mosaic. */
export declare class MosaicBuilder {
    /** Mosaic identifier. */
    mosaicId: MosaicIdDto;
    /** Mosaic amount. */
    amount: AmountDto;
    /**
     * Constructor.
     *
     * @param mosaicId Mosaic identifier.
     * @param amount Mosaic amount.
     */
    constructor(mosaicId: MosaicIdDto, amount: AmountDto);
    /**
     * Creates an instance of MosaicBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of MosaicBuilder.
     */
    static loadFromBinary(payload: Uint8Array): MosaicBuilder;
    /**
     * Gets mosaic identifier.
     *
     * @return Mosaic identifier.
     */
    getMosaicId(): MosaicIdDto;
    /**
     * Gets mosaic amount.
     *
     * @return Mosaic amount.
     */
    getAmount(): AmountDto;
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
