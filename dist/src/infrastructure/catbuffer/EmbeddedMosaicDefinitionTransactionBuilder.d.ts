/**
*** Copyright (c) 2016-present,
*** Jaguar0625, gimre, BloodyRookie, Tech Bureau, Corp. All rights reserved.
***
*** This file is part of Catapult.
***
*** Catapult is free software: you can redistribute it and/or modify
*** it under the terms of the GNU Lesser General Public License as published by
*** the Free Software Foundation, either version 3 of the License, or
*** (at your option) any later version.
***
*** Catapult is distributed in the hope that it will be useful,
*** but WITHOUT ANY WARRANTY; without even the implied warranty of
*** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*** GNU Lesser General Public License for more details.
***
*** You should have received a copy of the GNU Lesser General Public License
*** along with Catapult. If not, see <http://www.gnu.org/licenses/>.
**/
import { BlockDurationDto } from './BlockDurationDto';
import { EmbeddedTransactionBuilder } from './EmbeddedTransactionBuilder';
import { EntityTypeDto } from './EntityTypeDto';
import { KeyDto } from './KeyDto';
import { MosaicDefinitionTransactionBodyBuilder } from './MosaicDefinitionTransactionBodyBuilder';
import { MosaicIdDto } from './MosaicIdDto';
import { MosaicNonceDto } from './MosaicNonceDto';
import { NetworkTypeDto } from './NetworkTypeDto';
/** Binary layout for an embedded mosaic definition transaction. */
export declare class EmbeddedMosaicDefinitionTransactionBuilder extends EmbeddedTransactionBuilder {
    /** Mosaic definition transaction body. */
    mosaicDefinitionTransactionBody: MosaicDefinitionTransactionBodyBuilder;
    /**
     * Constructor.
     *
     * @param signerPublicKey Entity signer's public key.
     * @param version Entity version.
     * @param network Entity network.
     * @param type Entity type.
     * @param id Mosaic identifier.
     * @param duration Mosaic duration.
     * @param nonce Mosaic nonce.
     * @param flags Mosaic flags.
     * @param divisibility Mosaic divisibility.
     */
    constructor(signerPublicKey: KeyDto, version: number, network: NetworkTypeDto, type: EntityTypeDto, id: MosaicIdDto, duration: BlockDurationDto, nonce: MosaicNonceDto, flags: number, divisibility: number);
    /**
     * Creates an instance of EmbeddedMosaicDefinitionTransactionBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of EmbeddedMosaicDefinitionTransactionBuilder.
     */
    static loadFromBinary(payload: Uint8Array): EmbeddedMosaicDefinitionTransactionBuilder;
    /**
     * Gets mosaic identifier.
     *
     * @return Mosaic identifier.
     */
    getId(): MosaicIdDto;
    /**
     * Gets mosaic duration.
     *
     * @return Mosaic duration.
     */
    getDuration(): BlockDurationDto;
    /**
     * Gets mosaic nonce.
     *
     * @return Mosaic nonce.
     */
    getNonce(): MosaicNonceDto;
    /**
     * Gets mosaic flags.
     *
     * @return Mosaic flags.
     */
    getFlags(): number;
    /**
     * Gets mosaic divisibility.
     *
     * @return Mosaic divisibility.
     */
    getDivisibility(): number;
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
