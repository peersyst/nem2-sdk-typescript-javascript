/** Block fee multiplier. */
export declare class BlockFeeMultiplierDto {
    /** Block fee multiplier. */
    blockFeeMultiplier: number;
    /**
     * Constructor.
     *
     * @param blockFeeMultiplier Block fee multiplier.
     */
    constructor(blockFeeMultiplier: number);
    /**
     * Creates an instance of BlockFeeMultiplierDto from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of BlockFeeMultiplierDto.
     */
    static loadFromBinary(payload: Uint8Array): BlockFeeMultiplierDto;
    /**
     * Gets Block fee multiplier.
     *
     * @return Block fee multiplier.
     */
    getBlockFeeMultiplier(): number;
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
