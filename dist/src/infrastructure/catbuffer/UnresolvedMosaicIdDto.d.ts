/** Unresolved mosaic id. */
export declare class UnresolvedMosaicIdDto {
    /** Unresolved mosaic id. */
    unresolvedMosaicId: number[];
    /**
     * Constructor.
     *
     * @param unresolvedMosaicId Unresolved mosaic id.
     */
    constructor(unresolvedMosaicId: number[]);
    /**
     * Creates an instance of UnresolvedMosaicIdDto from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of UnresolvedMosaicIdDto.
     */
    static loadFromBinary(payload: Uint8Array): UnresolvedMosaicIdDto;
    /**
     * Gets Unresolved mosaic id.
     *
     * @return Unresolved mosaic id.
     */
    getUnresolvedMosaicId(): number[];
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
