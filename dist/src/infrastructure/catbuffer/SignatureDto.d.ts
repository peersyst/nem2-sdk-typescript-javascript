/** Signature. */
export declare class SignatureDto {
    /** Signature. */
    signature: Uint8Array;
    /**
     * Constructor.
     *
     * @param signature Signature.
     */
    constructor(signature: Uint8Array);
    /**
     * Creates an instance of SignatureDto from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of SignatureDto.
     */
    static loadFromBinary(payload: Uint8Array): SignatureDto;
    /**
     * Gets Signature.
     *
     * @return Signature.
     */
    getSignature(): Uint8Array;
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
