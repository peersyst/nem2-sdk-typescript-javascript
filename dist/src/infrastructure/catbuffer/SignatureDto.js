"use strict";
// tslint:disable: jsdoc-format
/**
*** Copyright (c) 2016-present,
*** Jaguar0625, gimre, BloodyRookie, Tech Bureau, Corp. All rights reserved.
***
*** This file is part of Catapult.
***
*** Catapult is free software: you can redistribute it and/or modify
*** it under the terms of the GNU Lesser General Public License as published by
*** the Free Software Foundation, either version 3 of the License, or
*** (at your option) any later version.
***
*** Catapult is distributed in the hope that it will be useful,
*** but WITHOUT ANY WARRANTY; without even the implied warranty of
*** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*** GNU Lesser General Public License for more details.
***
*** You should have received a copy of the GNU Lesser General Public License
*** along with Catapult. If not, see <http://www.gnu.org/licenses/>.
**/
Object.defineProperty(exports, "__esModule", { value: true });
const GeneratorUtils_1 = require("./GeneratorUtils");
/** Signature. */
class SignatureDto {
    /**
     * Constructor.
     *
     * @param signature Signature.
     */
    constructor(signature) {
        this.signature = signature;
    }
    /**
     * Creates an instance of SignatureDto from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of SignatureDto.
     */
    static loadFromBinary(payload) {
        const byteArray = Array.from(payload);
        const signature = GeneratorUtils_1.GeneratorUtils.getBytes(Uint8Array.from(byteArray), 64);
        byteArray.splice(0, 64);
        return new SignatureDto(signature);
    }
    /**
     * Gets Signature.
     *
     * @return Signature.
     */
    getSignature() {
        return this.signature;
    }
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize() {
        return 64;
    }
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize() {
        let newArray = Uint8Array.from([]);
        newArray = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(newArray, this.signature);
        return newArray;
    }
}
exports.SignatureDto = SignatureDto;
//# sourceMappingURL=SignatureDto.js.map