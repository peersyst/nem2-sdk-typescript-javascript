/** Block duration. */
export declare class BlockDurationDto {
    /** Block duration. */
    blockDuration: number[];
    /**
     * Constructor.
     *
     * @param blockDuration Block duration.
     */
    constructor(blockDuration: number[]);
    /**
     * Creates an instance of BlockDurationDto from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of BlockDurationDto.
     */
    static loadFromBinary(payload: Uint8Array): BlockDurationDto;
    /**
     * Gets Block duration.
     *
     * @return Block duration.
     */
    getBlockDuration(): number[];
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
