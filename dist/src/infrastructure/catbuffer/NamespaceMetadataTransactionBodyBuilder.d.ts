import { KeyDto } from './KeyDto';
import { NamespaceIdDto } from './NamespaceIdDto';
/** Binary layout for a namespace metadata transaction. */
export declare class NamespaceMetadataTransactionBodyBuilder {
    /** Metadata target public key. */
    targetPublicKey: KeyDto;
    /** Metadata key scoped to source, target and type. */
    scopedMetadataKey: number[];
    /** Target namespace identifier. */
    targetNamespaceId: NamespaceIdDto;
    /** Change in value size in bytes. */
    valueSizeDelta: number;
    /** Difference between existing value and new value \note when there is no existing value, new value is same this value \note when there is an existing value, new value is calculated as xor(previous-value, value). */
    value: Uint8Array;
    /**
     * Constructor.
     *
     * @param targetPublicKey Metadata target public key.
     * @param scopedMetadataKey Metadata key scoped to source, target and type.
     * @param targetNamespaceId Target namespace identifier.
     * @param valueSizeDelta Change in value size in bytes.
     * @param value Difference between existing value and new value.
     * @note when there is no existing value, new value is same this value.
     * @note when there is an existing value, new value is calculated as xor(previous-value, value).
     */
    constructor(targetPublicKey: KeyDto, scopedMetadataKey: number[], targetNamespaceId: NamespaceIdDto, valueSizeDelta: number, value: Uint8Array);
    /**
     * Creates an instance of NamespaceMetadataTransactionBodyBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of NamespaceMetadataTransactionBodyBuilder.
     */
    static loadFromBinary(payload: Uint8Array): NamespaceMetadataTransactionBodyBuilder;
    /**
     * Gets metadata target public key.
     *
     * @return Metadata target public key.
     */
    getTargetPublicKey(): KeyDto;
    /**
     * Gets metadata key scoped to source, target and type.
     *
     * @return Metadata key scoped to source, target and type.
     */
    getScopedMetadataKey(): number[];
    /**
     * Gets target namespace identifier.
     *
     * @return Target namespace identifier.
     */
    getTargetNamespaceId(): NamespaceIdDto;
    /**
     * Gets change in value size in bytes.
     *
     * @return Change in value size in bytes.
     */
    getValueSizeDelta(): number;
    /**
     * Gets difference between existing value and new value.
     * @note when there is no existing value, new value is same this value.
     * @note when there is an existing value, new value is calculated as xor(previous-value, value).
     *
     * @return Difference between existing value and new value.
     * @note when there is no existing value, new value is same this value.
     * @note when there is an existing value, new value is calculated as xor(previous-value, value).
     */
    getValue(): Uint8Array;
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
