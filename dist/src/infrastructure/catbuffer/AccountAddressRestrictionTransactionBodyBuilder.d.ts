import { UnresolvedAddressDto } from './UnresolvedAddressDto';
/** Binary layout for an account address restriction transaction. */
export declare class AccountAddressRestrictionTransactionBodyBuilder {
    /** Account restriction flags. */
    restrictionFlags: number;
    /** Reserved padding to align restrictionAdditions on 8-byte boundary. */
    accountRestrictionTransactionBody_Reserved1: number;
    /** Account restriction additions. */
    restrictionAdditions: UnresolvedAddressDto[];
    /** Account restriction deletions. */
    restrictionDeletions: UnresolvedAddressDto[];
    /**
     * Constructor.
     *
     * @param restrictionFlags Account restriction flags.
     * @param restrictionAdditions Account restriction additions.
     * @param restrictionDeletions Account restriction deletions.
     */
    constructor(restrictionFlags: number, restrictionAdditions: UnresolvedAddressDto[], restrictionDeletions: UnresolvedAddressDto[]);
    /**
     * Creates an instance of AccountAddressRestrictionTransactionBodyBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of AccountAddressRestrictionTransactionBodyBuilder.
     */
    static loadFromBinary(payload: Uint8Array): AccountAddressRestrictionTransactionBodyBuilder;
    /**
     * Gets account restriction flags.
     *
     * @return Account restriction flags.
     */
    getRestrictionFlags(): number;
    /**
     * Gets reserved padding to align restrictionAdditions on 8-byte boundary.
     *
     * @return Reserved padding to align restrictionAdditions on 8-byte boundary.
     */
    getAccountRestrictionTransactionBody_Reserved1(): number;
    /**
     * Gets account restriction additions.
     *
     * @return Account restriction additions.
     */
    getRestrictionAdditions(): UnresolvedAddressDto[];
    /**
     * Gets account restriction deletions.
     *
     * @return Account restriction deletions.
     */
    getRestrictionDeletions(): UnresolvedAddressDto[];
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
