/**
*** Copyright (c) 2016-present,
*** Jaguar0625, gimre, BloodyRookie, Tech Bureau, Corp. All rights reserved.
***
*** This file is part of Catapult.
***
*** Catapult is free software: you can redistribute it and/or modify
*** it under the terms of the GNU Lesser General Public License as published by
*** the Free Software Foundation, either version 3 of the License, or
*** (at your option) any later version.
***
*** Catapult is distributed in the hope that it will be useful,
*** but WITHOUT ANY WARRANTY; without even the implied warranty of
*** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*** GNU Lesser General Public License for more details.
***
*** You should have received a copy of the GNU Lesser General Public License
*** along with Catapult. If not, see <http://www.gnu.org/licenses/>.
**/
import { AccountLinkActionDto } from './AccountLinkActionDto';
import { AccountLinkTransactionBodyBuilder } from './AccountLinkTransactionBodyBuilder';
import { EmbeddedTransactionBuilder } from './EmbeddedTransactionBuilder';
import { EntityTypeDto } from './EntityTypeDto';
import { KeyDto } from './KeyDto';
import { NetworkTypeDto } from './NetworkTypeDto';
/** Binary layout for an embedded account link transaction. */
export declare class EmbeddedAccountLinkTransactionBuilder extends EmbeddedTransactionBuilder {
    /** Account link transaction body. */
    accountLinkTransactionBody: AccountLinkTransactionBodyBuilder;
    /**
     * Constructor.
     *
     * @param signerPublicKey Entity signer's public key.
     * @param version Entity version.
     * @param network Entity network.
     * @param type Entity type.
     * @param remotePublicKey Remote public key.
     * @param linkAction Account link action.
     */
    constructor(signerPublicKey: KeyDto, version: number, network: NetworkTypeDto, type: EntityTypeDto, remotePublicKey: KeyDto, linkAction: AccountLinkActionDto);
    /**
     * Creates an instance of EmbeddedAccountLinkTransactionBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of EmbeddedAccountLinkTransactionBuilder.
     */
    static loadFromBinary(payload: Uint8Array): EmbeddedAccountLinkTransactionBuilder;
    /**
     * Gets remote public key.
     *
     * @return Remote public key.
     */
    getRemotePublicKey(): KeyDto;
    /**
     * Gets account link action.
     *
     * @return Account link action.
     */
    getLinkAction(): AccountLinkActionDto;
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
