/** Difficulty. */
export declare class DifficultyDto {
    /** Difficulty. */
    difficulty: number[];
    /**
     * Constructor.
     *
     * @param difficulty Difficulty.
     */
    constructor(difficulty: number[]);
    /**
     * Creates an instance of DifficultyDto from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of DifficultyDto.
     */
    static loadFromBinary(payload: Uint8Array): DifficultyDto;
    /**
     * Gets Difficulty.
     *
     * @return Difficulty.
     */
    getDifficulty(): number[];
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
