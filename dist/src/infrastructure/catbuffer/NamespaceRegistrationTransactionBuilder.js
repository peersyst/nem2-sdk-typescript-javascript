"use strict";
// tslint:disable: jsdoc-format
/**
*** Copyright (c) 2016-present,
*** Jaguar0625, gimre, BloodyRookie, Tech Bureau, Corp. All rights reserved.
***
*** This file is part of Catapult.
***
*** Catapult is free software: you can redistribute it and/or modify
*** it under the terms of the GNU Lesser General Public License as published by
*** the Free Software Foundation, either version 3 of the License, or
*** (at your option) any later version.
***
*** Catapult is distributed in the hope that it will be useful,
*** but WITHOUT ANY WARRANTY; without even the implied warranty of
*** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*** GNU Lesser General Public License for more details.
***
*** You should have received a copy of the GNU Lesser General Public License
*** along with Catapult. If not, see <http://www.gnu.org/licenses/>.
**/
Object.defineProperty(exports, "__esModule", { value: true });
const GeneratorUtils_1 = require("./GeneratorUtils");
const NamespaceRegistrationTransactionBodyBuilder_1 = require("./NamespaceRegistrationTransactionBodyBuilder");
const TransactionBuilder_1 = require("./TransactionBuilder");
/** Binary layout for a non-embedded namespace registration transaction. */
class NamespaceRegistrationTransactionBuilder extends TransactionBuilder_1.TransactionBuilder {
    /**
     * Constructor.
     *
     * @param signature Entity signature.
     * @param signerPublicKey Entity signer's public key.
     * @param version Entity version.
     * @param network Entity network.
     * @param type Entity type.
     * @param fee Transaction fee.
     * @param deadline Transaction deadline.
     * @param duration Namespace duration.
     * @param parentId Parent namespace identifier.
     * @param id Namespace identifier.
     * @param name Namespace name.
     */
    // tslint:disable-next-line: max-line-length
    constructor(signature, signerPublicKey, version, network, type, fee, deadline, id, name, duration, parentId) {
        super(signature, signerPublicKey, version, network, type, fee, deadline);
        if ((duration && parentId) || (!duration && !parentId)) {
            throw new Error('Invalid conditional parameters');
        }
        // tslint:disable-next-line: max-line-length
        this.namespaceRegistrationTransactionBody = new NamespaceRegistrationTransactionBodyBuilder_1.NamespaceRegistrationTransactionBodyBuilder(id, name, duration, parentId);
    }
    /**
     * Creates an instance of NamespaceRegistrationTransactionBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of NamespaceRegistrationTransactionBuilder.
     */
    static loadFromBinary(payload) {
        const byteArray = Array.from(payload);
        const superObject = TransactionBuilder_1.TransactionBuilder.loadFromBinary(Uint8Array.from(byteArray));
        byteArray.splice(0, superObject.getSize());
        // tslint:disable-next-line: max-line-length
        const namespaceRegistrationTransactionBody = NamespaceRegistrationTransactionBodyBuilder_1.NamespaceRegistrationTransactionBodyBuilder.loadFromBinary(Uint8Array.from(byteArray));
        byteArray.splice(0, namespaceRegistrationTransactionBody.getSize());
        // tslint:disable-next-line: max-line-length
        return new NamespaceRegistrationTransactionBuilder(superObject.signature, superObject.signerPublicKey, superObject.version, superObject.network, superObject.type, superObject.fee, superObject.deadline, namespaceRegistrationTransactionBody.id, namespaceRegistrationTransactionBody.name, namespaceRegistrationTransactionBody.duration, namespaceRegistrationTransactionBody.parentId);
    }
    /**
     * Gets namespace duration.
     *
     * @return Namespace duration.
     */
    getDuration() {
        return this.namespaceRegistrationTransactionBody.getDuration();
    }
    /**
     * Gets parent namespace identifier.
     *
     * @return Parent namespace identifier.
     */
    getParentId() {
        return this.namespaceRegistrationTransactionBody.getParentId();
    }
    /**
     * Gets namespace identifier.
     *
     * @return Namespace identifier.
     */
    getId() {
        return this.namespaceRegistrationTransactionBody.getId();
    }
    /**
     * Gets namespace registration type.
     *
     * @return Namespace registration type.
     */
    getRegistrationType() {
        return this.namespaceRegistrationTransactionBody.getRegistrationType();
    }
    /**
     * Gets namespace name.
     *
     * @return Namespace name.
     */
    getName() {
        return this.namespaceRegistrationTransactionBody.getName();
    }
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize() {
        let size = super.getSize();
        size += this.namespaceRegistrationTransactionBody.getSize();
        return size;
    }
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize() {
        let newArray = Uint8Array.from([]);
        const superBytes = super.serialize();
        newArray = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(newArray, superBytes);
        const namespaceRegistrationTransactionBodyBytes = this.namespaceRegistrationTransactionBody.serialize();
        newArray = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(newArray, namespaceRegistrationTransactionBodyBytes);
        return newArray;
    }
}
exports.NamespaceRegistrationTransactionBuilder = NamespaceRegistrationTransactionBuilder;
//# sourceMappingURL=NamespaceRegistrationTransactionBuilder.js.map