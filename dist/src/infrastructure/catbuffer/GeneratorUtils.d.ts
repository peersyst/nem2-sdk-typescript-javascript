/**
*** Copyright (c) 2016-present,
*** Jaguar0625, gimre, BloodyRookie, Tech Bureau, Corp. All rights reserved.
***
*** This file is part of Catapult.
***
*** Catapult is free software: you can redistribute it and/or modify
*** it under the terms of the GNU Lesser General Public License as published by
*** the Free Software Foundation, either version 3 of the License, or
*** (at your option) any later version.
***
*** Catapult is distributed in the hope that it will be useful,
*** but WITHOUT ANY WARRANTY; without even the implied warranty of
*** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*** GNU Lesser General Public License for more details.
***
*** You should have received a copy of the GNU Lesser General Public License
*** along with Catapult. If not, see <http://www.gnu.org/licenses/>.
**/
/**
 * Generator utility class.
 */
export declare class GeneratorUtils {
    /**
     * Convert a UInt8Array input into an array of 2 numbers.
     * Numbers in the returned array are cast to UInt32.
     * @param {Uint8Array} input A uint8 array.
     * @returns {number[]} The uint64 representation of the input.
     */
    static bufferToUint64(input: Uint8Array): number[];
    /**
     * Read 4 bytes as a uint32 value from buffer bytes starting at given index.
     * @param {Uint8Array} bytes A uint8 array.
     * @param {number} index Index.
     * @returns {number} 32bits integer.
     */
    static readUint32At(bytes: Uint8Array, index: number): number;
    /**
     * Convert uint value into buffer
     * @param {number} uintValue A uint8 array.
     * @param {number} bufferSize Buffer size.
     * @returns {Uint8Array}
     */
    static uintToBuffer(uintValue: number, bufferSize: number): Uint8Array;
    /**
     * Convert uint8 array buffer into number
     * @param {Uint8Array} buffer A uint8 array.
     * @returns {number}
     */
    static bufferToUint(buffer: Uint8Array): number;
    /**
     * Convert unit64 into buffer
     * @param {number} uintValue Uint64 (number[]).
     * @returns {Uint8Array}
     */
    static uint64ToBuffer(uintValue: number[]): Uint8Array;
    /**
     * Concatenate two arrays
     * @param {Uint8Array} array1 A Uint8Array.
     * @param {Uint8Array} array2 A Uint8Array.
     * @returns {Uint8Array}
     */
    static concatTypedArrays(array1: Uint8Array, array2: Uint8Array): Uint8Array;
    /** Converts an unsigned byte to a signed byte with the same binary representation.
     * @param {number} input An unsigned byte.
     * @returns {number} A signed byte with the same binary representation as the input.
     *
     */
    static uint8ToInt8: (input: number) => number;
    /** Get bytes by given sub array size.
     * @param {Uint8Array} binary Binary bytes array.
     * @param {number} size Subarray size.
     * @returns {Uint8Array}
     *
     */
    static getBytes(binary: Uint8Array, size: number): Uint8Array;
}
