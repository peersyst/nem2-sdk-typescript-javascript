import { Hash256Dto } from './Hash256Dto';
import { LockHashAlgorithmDto } from './LockHashAlgorithmDto';
import { UnresolvedAddressDto } from './UnresolvedAddressDto';
/** Binary layout for a secret proof transaction. */
export declare class SecretProofTransactionBodyBuilder {
    /** Secret. */
    secret: Hash256Dto;
    /** Hash algorithm. */
    hashAlgorithm: LockHashAlgorithmDto;
    /** Locked mosaic recipient address. */
    recipientAddress: UnresolvedAddressDto;
    /** Proof data. */
    proof: Uint8Array;
    /**
     * Constructor.
     *
     * @param secret Secret.
     * @param hashAlgorithm Hash algorithm.
     * @param recipientAddress Locked mosaic recipient address.
     * @param proof Proof data.
     */
    constructor(secret: Hash256Dto, hashAlgorithm: LockHashAlgorithmDto, recipientAddress: UnresolvedAddressDto, proof: Uint8Array);
    /**
     * Creates an instance of SecretProofTransactionBodyBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of SecretProofTransactionBodyBuilder.
     */
    static loadFromBinary(payload: Uint8Array): SecretProofTransactionBodyBuilder;
    /**
     * Gets secret.
     *
     * @return Secret.
     */
    getSecret(): Hash256Dto;
    /**
     * Gets hash algorithm.
     *
     * @return Hash algorithm.
     */
    getHashAlgorithm(): LockHashAlgorithmDto;
    /**
     * Gets locked mosaic recipient address.
     *
     * @return Locked mosaic recipient address.
     */
    getRecipientAddress(): UnresolvedAddressDto;
    /**
     * Gets proof data.
     *
     * @return Proof data.
     */
    getProof(): Uint8Array;
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
