"use strict";
// tslint:disable: jsdoc-format
/**
*** Copyright (c) 2016-present,
*** Jaguar0625, gimre, BloodyRookie, Tech Bureau, Corp. All rights reserved.
***
*** This file is part of Catapult.
***
*** Catapult is free software: you can redistribute it and/or modify
*** it under the terms of the GNU Lesser General Public License as published by
*** the Free Software Foundation, either version 3 of the License, or
*** (at your option) any later version.
***
*** Catapult is distributed in the hope that it will be useful,
*** but WITHOUT ANY WARRANTY; without even the implied warranty of
*** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*** GNU Lesser General Public License for more details.
***
*** You should have received a copy of the GNU Lesser General Public License
*** along with Catapult. If not, see <http://www.gnu.org/licenses/>.
**/
Object.defineProperty(exports, "__esModule", { value: true });
const BlockDurationDto_1 = require("./BlockDurationDto");
const GeneratorUtils_1 = require("./GeneratorUtils");
const NamespaceIdDto_1 = require("./NamespaceIdDto");
const NamespaceRegistrationTypeDto_1 = require("./NamespaceRegistrationTypeDto");
/** Binary layout for a namespace registration transaction. */
class NamespaceRegistrationTransactionBodyBuilder {
    /**
     * Constructor.
     *
     * @param duration Namespace duration.
     * @param parentId Parent namespace identifier.
     * @param id Namespace identifier.
     * @param name Namespace name.
     */
    constructor(id, name, duration, parentId) {
        if ((duration && parentId) || (!duration && !parentId)) {
            throw new Error('Invalid conditional parameters');
        }
        this.duration = duration;
        this.parentId = parentId;
        this.id = id;
        this.name = name;
        if (duration) {
            this.registrationType = NamespaceRegistrationTypeDto_1.NamespaceRegistrationTypeDto.ROOT;
        }
        if (parentId) {
            this.registrationType = NamespaceRegistrationTypeDto_1.NamespaceRegistrationTypeDto.CHILD;
        }
    }
    /**
     * Creates an instance of NamespaceRegistrationTransactionBodyBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of NamespaceRegistrationTransactionBodyBuilder.
     */
    static loadFromBinary(payload) {
        const byteArray = Array.from(payload);
        const registrationTypeConditionBytes = Uint8Array.from(byteArray.slice(0, 8));
        byteArray.splice(0, 8);
        const id = NamespaceIdDto_1.NamespaceIdDto.loadFromBinary(Uint8Array.from(byteArray));
        byteArray.splice(0, id.getSize());
        const registrationType = GeneratorUtils_1.GeneratorUtils.bufferToUint(GeneratorUtils_1.GeneratorUtils.getBytes(Uint8Array.from(byteArray), 1));
        byteArray.splice(0, 1);
        const nameSize = GeneratorUtils_1.GeneratorUtils.bufferToUint(GeneratorUtils_1.GeneratorUtils.getBytes(Uint8Array.from(byteArray), 1));
        byteArray.splice(0, 1);
        const name = GeneratorUtils_1.GeneratorUtils.getBytes(Uint8Array.from(byteArray), nameSize);
        byteArray.splice(0, nameSize);
        let duration;
        if (registrationType === NamespaceRegistrationTypeDto_1.NamespaceRegistrationTypeDto.ROOT) {
            duration = BlockDurationDto_1.BlockDurationDto.loadFromBinary(registrationTypeConditionBytes);
        }
        let parentId;
        if (registrationType === NamespaceRegistrationTypeDto_1.NamespaceRegistrationTypeDto.CHILD) {
            parentId = NamespaceIdDto_1.NamespaceIdDto.loadFromBinary(registrationTypeConditionBytes);
        }
        return new NamespaceRegistrationTransactionBodyBuilder(id, name, duration, parentId);
    }
    /**
     * Gets namespace duration.
     *
     * @return Namespace duration.
     */
    getDuration() {
        if (this.registrationType !== NamespaceRegistrationTypeDto_1.NamespaceRegistrationTypeDto.ROOT) {
            throw new Error('registrationType is not set to ROOT.');
        }
        return this.duration;
    }
    /**
     * Gets parent namespace identifier.
     *
     * @return Parent namespace identifier.
     */
    getParentId() {
        if (this.registrationType !== NamespaceRegistrationTypeDto_1.NamespaceRegistrationTypeDto.CHILD) {
            throw new Error('registrationType is not set to CHILD.');
        }
        return this.parentId;
    }
    /**
     * Gets namespace identifier.
     *
     * @return Namespace identifier.
     */
    getId() {
        return this.id;
    }
    /**
     * Gets namespace registration type.
     *
     * @return Namespace registration type.
     */
    getRegistrationType() {
        return this.registrationType;
    }
    /**
     * Gets namespace name.
     *
     * @return Namespace name.
     */
    getName() {
        return this.name;
    }
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize() {
        let size = 0;
        if (this.registrationType === NamespaceRegistrationTypeDto_1.NamespaceRegistrationTypeDto.ROOT) {
            size += this.duration.getSize();
        }
        if (this.registrationType === NamespaceRegistrationTypeDto_1.NamespaceRegistrationTypeDto.CHILD) {
            size += this.parentId.getSize();
        }
        size += this.id.getSize();
        size += 1; // registrationType
        size += 1; // nameSize
        size += this.name.length;
        return size;
    }
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize() {
        let newArray = Uint8Array.from([]);
        if (this.registrationType === NamespaceRegistrationTypeDto_1.NamespaceRegistrationTypeDto.ROOT) {
            const durationBytes = this.duration.serialize();
            newArray = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(newArray, durationBytes);
        }
        if (this.registrationType === NamespaceRegistrationTypeDto_1.NamespaceRegistrationTypeDto.CHILD) {
            const parentIdBytes = this.parentId.serialize();
            newArray = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(newArray, parentIdBytes);
        }
        const idBytes = this.id.serialize();
        newArray = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(newArray, idBytes);
        const registrationTypeBytes = GeneratorUtils_1.GeneratorUtils.uintToBuffer(this.registrationType, 1);
        newArray = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(newArray, registrationTypeBytes);
        const nameSizeBytes = GeneratorUtils_1.GeneratorUtils.uintToBuffer(this.name.length, 1);
        newArray = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(newArray, nameSizeBytes);
        newArray = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(newArray, this.name);
        return newArray;
    }
}
exports.NamespaceRegistrationTransactionBodyBuilder = NamespaceRegistrationTransactionBodyBuilder;
//# sourceMappingURL=NamespaceRegistrationTransactionBodyBuilder.js.map