"use strict";
// tslint:disable: jsdoc-format
/**
*** Copyright (c) 2016-present,
*** Jaguar0625, gimre, BloodyRookie, Tech Bureau, Corp. All rights reserved.
***
*** This file is part of Catapult.
***
*** Catapult is free software: you can redistribute it and/or modify
*** it under the terms of the GNU Lesser General Public License as published by
*** the Free Software Foundation, either version 3 of the License, or
*** (at your option) any later version.
***
*** Catapult is distributed in the hope that it will be useful,
*** but WITHOUT ANY WARRANTY; without even the implied warranty of
*** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*** GNU Lesser General Public License for more details.
***
*** You should have received a copy of the GNU Lesser General Public License
*** along with Catapult. If not, see <http://www.gnu.org/licenses/>.
**/
Object.defineProperty(exports, "__esModule", { value: true });
const GeneratorUtils_1 = require("./GeneratorUtils");
/** Binary layout for an account operation restriction transaction. */
class AccountOperationRestrictionTransactionBodyBuilder {
    /**
     * Constructor.
     *
     * @param restrictionFlags Account restriction flags.
     * @param restrictionAdditions Account restriction additions.
     * @param restrictionDeletions Account restriction deletions.
     */
    constructor(restrictionFlags, restrictionAdditions, restrictionDeletions) {
        this.restrictionFlags = restrictionFlags;
        this.accountRestrictionTransactionBody_Reserved1 = 0;
        this.restrictionAdditions = restrictionAdditions;
        this.restrictionDeletions = restrictionDeletions;
    }
    /**
     * Creates an instance of AccountOperationRestrictionTransactionBodyBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of AccountOperationRestrictionTransactionBodyBuilder.
     */
    static loadFromBinary(payload) {
        const byteArray = Array.from(payload);
        const restrictionFlags = GeneratorUtils_1.GeneratorUtils.bufferToUint(GeneratorUtils_1.GeneratorUtils.getBytes(Uint8Array.from(byteArray), 2));
        byteArray.splice(0, 2);
        const restrictionAdditionsCount = GeneratorUtils_1.GeneratorUtils.bufferToUint(GeneratorUtils_1.GeneratorUtils.getBytes(Uint8Array.from(byteArray), 1));
        byteArray.splice(0, 1);
        const restrictionDeletionsCount = GeneratorUtils_1.GeneratorUtils.bufferToUint(GeneratorUtils_1.GeneratorUtils.getBytes(Uint8Array.from(byteArray), 1));
        byteArray.splice(0, 1);
        // tslint:disable-next-line: max-line-length
        const accountRestrictionTransactionBody_Reserved1 = GeneratorUtils_1.GeneratorUtils.bufferToUint(GeneratorUtils_1.GeneratorUtils.getBytes(Uint8Array.from(byteArray), 4));
        byteArray.splice(0, 4);
        const restrictionAdditions = [];
        for (let i = 0; i < restrictionAdditionsCount; i++) {
            const item = GeneratorUtils_1.GeneratorUtils.bufferToUint(GeneratorUtils_1.GeneratorUtils.getBytes(Uint8Array.from(byteArray), 2));
            restrictionAdditions.push(item);
            byteArray.splice(0, 2);
        }
        const restrictionDeletions = [];
        for (let i = 0; i < restrictionDeletionsCount; i++) {
            const item = GeneratorUtils_1.GeneratorUtils.bufferToUint(GeneratorUtils_1.GeneratorUtils.getBytes(Uint8Array.from(byteArray), 2));
            restrictionDeletions.push(item);
            byteArray.splice(0, 2);
        }
        // tslint:disable-next-line: max-line-length
        return new AccountOperationRestrictionTransactionBodyBuilder(restrictionFlags, restrictionAdditions, restrictionDeletions);
    }
    /**
     * Gets account restriction flags.
     *
     * @return Account restriction flags.
     */
    getRestrictionFlags() {
        return this.restrictionFlags;
    }
    /**
     * Gets reserved padding to align restrictionAdditions on 8-byte boundary.
     *
     * @return Reserved padding to align restrictionAdditions on 8-byte boundary.
     */
    getAccountRestrictionTransactionBody_Reserved1() {
        return this.accountRestrictionTransactionBody_Reserved1;
    }
    /**
     * Gets account restriction additions.
     *
     * @return Account restriction additions.
     */
    getRestrictionAdditions() {
        return this.restrictionAdditions;
    }
    /**
     * Gets account restriction deletions.
     *
     * @return Account restriction deletions.
     */
    getRestrictionDeletions() {
        return this.restrictionDeletions;
    }
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize() {
        let size = 0;
        size += 2; // restrictionFlags
        size += 1; // restrictionAdditionsCount
        size += 1; // restrictionDeletionsCount
        size += 4; // accountRestrictionTransactionBody_Reserved1
        this.restrictionAdditions.forEach(() => size += 2);
        this.restrictionDeletions.forEach(() => size += 2);
        return size;
    }
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize() {
        let newArray = Uint8Array.from([]);
        const restrictionFlagsBytes = GeneratorUtils_1.GeneratorUtils.uintToBuffer(this.getRestrictionFlags(), 2);
        newArray = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(newArray, restrictionFlagsBytes);
        const restrictionAdditionsCountBytes = GeneratorUtils_1.GeneratorUtils.uintToBuffer(this.restrictionAdditions.length, 1);
        newArray = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(newArray, restrictionAdditionsCountBytes);
        const restrictionDeletionsCountBytes = GeneratorUtils_1.GeneratorUtils.uintToBuffer(this.restrictionDeletions.length, 1);
        newArray = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(newArray, restrictionDeletionsCountBytes);
        // tslint:disable-next-line: max-line-length
        const accountRestrictionTransactionBody_Reserved1Bytes = GeneratorUtils_1.GeneratorUtils.uintToBuffer(this.getAccountRestrictionTransactionBody_Reserved1(), 4);
        newArray = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(newArray, accountRestrictionTransactionBody_Reserved1Bytes);
        this.restrictionAdditions.forEach((item) => {
            const restrictionAdditionsBytes = GeneratorUtils_1.GeneratorUtils.uintToBuffer(item, 2);
            newArray = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(newArray, restrictionAdditionsBytes);
        });
        this.restrictionDeletions.forEach((item) => {
            const restrictionDeletionsBytes = GeneratorUtils_1.GeneratorUtils.uintToBuffer(item, 2);
            newArray = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(newArray, restrictionDeletionsBytes);
        });
        return newArray;
    }
}
exports.AccountOperationRestrictionTransactionBodyBuilder = AccountOperationRestrictionTransactionBodyBuilder;
//# sourceMappingURL=AccountOperationRestrictionTransactionBodyBuilder.js.map