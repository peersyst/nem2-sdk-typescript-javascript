/** Key. */
export declare class KeyDto {
    /** Key. */
    key: Uint8Array;
    /**
     * Constructor.
     *
     * @param key Key.
     */
    constructor(key: Uint8Array);
    /**
     * Creates an instance of KeyDto from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of KeyDto.
     */
    static loadFromBinary(payload: Uint8Array): KeyDto;
    /**
     * Gets Key.
     *
     * @return Key.
     */
    getKey(): Uint8Array;
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
