import { Hash256Dto } from './Hash256Dto';
/** Binary layout for an aggregate transaction. */
export declare class AggregateTransactionBodyBuilder {
    /** Aggregate hash of an aggregate's transactions. */
    transactionsHash: Hash256Dto;
    /** Reserved padding to align end of AggregateTransactionHeader on 8-byte boundary. */
    aggregateTransactionHeader_Reserved1: number;
    /** Sub-transaction data (transactions are variable sized and payload size is in bytes). */
    transactions: Uint8Array;
    /** Cosignatures data (fills remaining body space after transactions). */
    cosignatures: Uint8Array;
    /**
     * Constructor.
     *
     * @param transactionsHash Aggregate hash of an aggregate's transactions.
     * @param transactions Sub-transaction data (transactions are variable sized and payload size is in bytes).
     * @param cosignatures Cosignatures data (fills remaining body space after transactions).
     */
    constructor(transactionsHash: Hash256Dto, transactions: Uint8Array, cosignatures: Uint8Array);
    /**
     * Creates an instance of AggregateTransactionBodyBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of AggregateTransactionBodyBuilder.
     */
    static loadFromBinary(payload: Uint8Array): AggregateTransactionBodyBuilder;
    /**
     * Gets aggregate hash of an aggregate's transactions.
     *
     * @return Aggregate hash of an aggregate's transactions.
     */
    getTransactionsHash(): Hash256Dto;
    /**
     * Gets reserved padding to align end of AggregateTransactionHeader on 8-byte boundary.
     *
     * @return Reserved padding to align end of AggregateTransactionHeader on 8-byte boundary.
     */
    getAggregateTransactionHeader_Reserved1(): number;
    /**
     * Gets sub-transaction data (transactions are variable sized and payload size is in bytes).
     *
     * @return Sub-transaction data (transactions are variable sized and payload size is in bytes).
     */
    getTransactions(): Uint8Array;
    /**
     * Gets cosignatures data (fills remaining body space after transactions).
     *
     * @return Cosignatures data (fills remaining body space after transactions).
     */
    getCosignatures(): Uint8Array;
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
