/**
*** Copyright (c) 2016-present,
*** Jaguar0625, gimre, BloodyRookie, Tech Bureau, Corp. All rights reserved.
***
*** This file is part of Catapult.
***
*** Catapult is free software: you can redistribute it and/or modify
*** it under the terms of the GNU Lesser General Public License as published by
*** the Free Software Foundation, either version 3 of the License, or
*** (at your option) any later version.
***
*** Catapult is distributed in the hope that it will be useful,
*** but WITHOUT ANY WARRANTY; without even the implied warranty of
*** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*** GNU Lesser General Public License for more details.
***
*** You should have received a copy of the GNU Lesser General Public License
*** along with Catapult. If not, see <http://www.gnu.org/licenses/>.
**/
import { BlockDurationDto } from './BlockDurationDto';
import { NamespaceIdDto } from './NamespaceIdDto';
import { NamespaceRegistrationTypeDto } from './NamespaceRegistrationTypeDto';
/** Binary layout for a namespace registration transaction. */
export declare class NamespaceRegistrationTransactionBodyBuilder {
    /** Namespace duration. */
    duration?: BlockDurationDto;
    /** Parent namespace identifier. */
    parentId?: NamespaceIdDto;
    /** Namespace identifier. */
    id: NamespaceIdDto;
    /** Namespace registration type. */
    registrationType: NamespaceRegistrationTypeDto;
    /** Namespace name. */
    name: Uint8Array;
    /**
     * Constructor.
     *
     * @param duration Namespace duration.
     * @param parentId Parent namespace identifier.
     * @param id Namespace identifier.
     * @param name Namespace name.
     */
    constructor(id: NamespaceIdDto, name: Uint8Array, duration?: BlockDurationDto, parentId?: NamespaceIdDto);
    /**
     * Creates an instance of NamespaceRegistrationTransactionBodyBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of NamespaceRegistrationTransactionBodyBuilder.
     */
    static loadFromBinary(payload: Uint8Array): NamespaceRegistrationTransactionBodyBuilder;
    /**
     * Gets namespace duration.
     *
     * @return Namespace duration.
     */
    getDuration(): BlockDurationDto | undefined;
    /**
     * Gets parent namespace identifier.
     *
     * @return Parent namespace identifier.
     */
    getParentId(): NamespaceIdDto | undefined;
    /**
     * Gets namespace identifier.
     *
     * @return Namespace identifier.
     */
    getId(): NamespaceIdDto;
    /**
     * Gets namespace registration type.
     *
     * @return Namespace registration type.
     */
    getRegistrationType(): NamespaceRegistrationTypeDto;
    /**
     * Gets namespace name.
     *
     * @return Namespace name.
     */
    getName(): Uint8Array;
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
