/** Mosaic nonce. */
export declare class MosaicNonceDto {
    /** Mosaic nonce. */
    mosaicNonce: number;
    /**
     * Constructor.
     *
     * @param mosaicNonce Mosaic nonce.
     */
    constructor(mosaicNonce: number);
    /**
     * Creates an instance of MosaicNonceDto from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of MosaicNonceDto.
     */
    static loadFromBinary(payload: Uint8Array): MosaicNonceDto;
    /**
     * Gets Mosaic nonce.
     *
     * @return Mosaic nonce.
     */
    getMosaicNonce(): number;
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
