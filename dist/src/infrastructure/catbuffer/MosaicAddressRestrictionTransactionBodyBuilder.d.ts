import { UnresolvedAddressDto } from './UnresolvedAddressDto';
import { UnresolvedMosaicIdDto } from './UnresolvedMosaicIdDto';
/** Binary layout for a mosaic address restriction transaction. */
export declare class MosaicAddressRestrictionTransactionBodyBuilder {
    /** Identifier of the mosaic to which the restriction applies. */
    mosaicId: UnresolvedMosaicIdDto;
    /** Restriction key. */
    restrictionKey: number[];
    /** Previous restriction value. */
    previousRestrictionValue: number[];
    /** New restriction value. */
    newRestrictionValue: number[];
    /** Address being restricted. */
    targetAddress: UnresolvedAddressDto;
    /**
     * Constructor.
     *
     * @param mosaicId Identifier of the mosaic to which the restriction applies.
     * @param restrictionKey Restriction key.
     * @param previousRestrictionValue Previous restriction value.
     * @param newRestrictionValue New restriction value.
     * @param targetAddress Address being restricted.
     */
    constructor(mosaicId: UnresolvedMosaicIdDto, restrictionKey: number[], previousRestrictionValue: number[], newRestrictionValue: number[], targetAddress: UnresolvedAddressDto);
    /**
     * Creates an instance of MosaicAddressRestrictionTransactionBodyBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of MosaicAddressRestrictionTransactionBodyBuilder.
     */
    static loadFromBinary(payload: Uint8Array): MosaicAddressRestrictionTransactionBodyBuilder;
    /**
     * Gets identifier of the mosaic to which the restriction applies.
     *
     * @return Identifier of the mosaic to which the restriction applies.
     */
    getMosaicId(): UnresolvedMosaicIdDto;
    /**
     * Gets restriction key.
     *
     * @return Restriction key.
     */
    getRestrictionKey(): number[];
    /**
     * Gets previous restriction value.
     *
     * @return Previous restriction value.
     */
    getPreviousRestrictionValue(): number[];
    /**
     * Gets new restriction value.
     *
     * @return New restriction value.
     */
    getNewRestrictionValue(): number[];
    /**
     * Gets address being restricted.
     *
     * @return Address being restricted.
     */
    getTargetAddress(): UnresolvedAddressDto;
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
