"use strict";
// tslint:disable: jsdoc-format
/**
*** Copyright (c) 2016-present,
*** Jaguar0625, gimre, BloodyRookie, Tech Bureau, Corp. All rights reserved.
***
*** This file is part of Catapult.
***
*** Catapult is free software: you can redistribute it and/or modify
*** it under the terms of the GNU Lesser General Public License as published by
*** the Free Software Foundation, either version 3 of the License, or
*** (at your option) any later version.
***
*** Catapult is distributed in the hope that it will be useful,
*** but WITHOUT ANY WARRANTY; without even the implied warranty of
*** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*** GNU Lesser General Public License for more details.
***
*** You should have received a copy of the GNU Lesser General Public License
*** along with Catapult. If not, see <http://www.gnu.org/licenses/>.
**/
Object.defineProperty(exports, "__esModule", { value: true });
const GeneratorUtils_1 = require("./GeneratorUtils");
const KeyDto_1 = require("./KeyDto");
const SignatureDto_1 = require("./SignatureDto");
/** Cosignature attached to an aggregate transaction. */
class CosignatureBuilder {
    /**
     * Constructor.
     *
     * @param signerPublicKey Cosigner public key.
     * @param signature Cosigner signature.
     */
    constructor(signerPublicKey, signature) {
        this.signerPublicKey = signerPublicKey;
        this.signature = signature;
    }
    /**
     * Creates an instance of CosignatureBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of CosignatureBuilder.
     */
    static loadFromBinary(payload) {
        const byteArray = Array.from(payload);
        const signerPublicKey = KeyDto_1.KeyDto.loadFromBinary(Uint8Array.from(byteArray));
        byteArray.splice(0, signerPublicKey.getSize());
        const signature = SignatureDto_1.SignatureDto.loadFromBinary(Uint8Array.from(byteArray));
        byteArray.splice(0, signature.getSize());
        return new CosignatureBuilder(signerPublicKey, signature);
    }
    /**
     * Gets cosigner public key.
     *
     * @return Cosigner public key.
     */
    getSignerPublicKey() {
        return this.signerPublicKey;
    }
    /**
     * Gets cosigner signature.
     *
     * @return Cosigner signature.
     */
    getSignature() {
        return this.signature;
    }
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize() {
        let size = 0;
        size += this.signerPublicKey.getSize();
        size += this.signature.getSize();
        return size;
    }
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize() {
        let newArray = Uint8Array.from([]);
        const signerPublicKeyBytes = this.signerPublicKey.serialize();
        newArray = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(newArray, signerPublicKeyBytes);
        const signatureBytes = this.signature.serialize();
        newArray = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(newArray, signatureBytes);
        return newArray;
    }
}
exports.CosignatureBuilder = CosignatureBuilder;
//# sourceMappingURL=CosignatureBuilder.js.map