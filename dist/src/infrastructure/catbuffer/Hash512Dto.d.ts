/** Hash512. */
export declare class Hash512Dto {
    /** Hash512. */
    hash512: Uint8Array;
    /**
     * Constructor.
     *
     * @param hash512 Hash512.
     */
    constructor(hash512: Uint8Array);
    /**
     * Creates an instance of Hash512Dto from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of Hash512Dto.
     */
    static loadFromBinary(payload: Uint8Array): Hash512Dto;
    /**
     * Gets Hash512.
     *
     * @return Hash512.
     */
    getHash512(): Uint8Array;
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
