import { UnresolvedMosaicIdDto } from './UnresolvedMosaicIdDto';
/** Binary layout for an account mosaic restriction transaction. */
export declare class AccountMosaicRestrictionTransactionBodyBuilder {
    /** Account restriction flags. */
    restrictionFlags: number;
    /** Reserved padding to align restrictionAdditions on 8-byte boundary. */
    accountRestrictionTransactionBody_Reserved1: number;
    /** Account restriction additions. */
    restrictionAdditions: UnresolvedMosaicIdDto[];
    /** Account restriction deletions. */
    restrictionDeletions: UnresolvedMosaicIdDto[];
    /**
     * Constructor.
     *
     * @param restrictionFlags Account restriction flags.
     * @param restrictionAdditions Account restriction additions.
     * @param restrictionDeletions Account restriction deletions.
     */
    constructor(restrictionFlags: number, restrictionAdditions: UnresolvedMosaicIdDto[], restrictionDeletions: UnresolvedMosaicIdDto[]);
    /**
     * Creates an instance of AccountMosaicRestrictionTransactionBodyBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of AccountMosaicRestrictionTransactionBodyBuilder.
     */
    static loadFromBinary(payload: Uint8Array): AccountMosaicRestrictionTransactionBodyBuilder;
    /**
     * Gets account restriction flags.
     *
     * @return Account restriction flags.
     */
    getRestrictionFlags(): number;
    /**
     * Gets reserved padding to align restrictionAdditions on 8-byte boundary.
     *
     * @return Reserved padding to align restrictionAdditions on 8-byte boundary.
     */
    getAccountRestrictionTransactionBody_Reserved1(): number;
    /**
     * Gets account restriction additions.
     *
     * @return Account restriction additions.
     */
    getRestrictionAdditions(): UnresolvedMosaicIdDto[];
    /**
     * Gets account restriction deletions.
     *
     * @return Account restriction deletions.
     */
    getRestrictionDeletions(): UnresolvedMosaicIdDto[];
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
