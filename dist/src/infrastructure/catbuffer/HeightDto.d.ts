/** Height. */
export declare class HeightDto {
    /** Height. */
    height: number[];
    /**
     * Constructor.
     *
     * @param height Height.
     */
    constructor(height: number[]);
    /**
     * Creates an instance of HeightDto from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of HeightDto.
     */
    static loadFromBinary(payload: Uint8Array): HeightDto;
    /**
     * Gets Height.
     *
     * @return Height.
     */
    getHeight(): number[];
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
