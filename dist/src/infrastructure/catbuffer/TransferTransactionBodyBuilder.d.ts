import { UnresolvedAddressDto } from './UnresolvedAddressDto';
import { UnresolvedMosaicBuilder } from './UnresolvedMosaicBuilder';
/** Binary layout for a transfer transaction. */
export declare class TransferTransactionBodyBuilder {
    /** Recipient address. */
    recipientAddress: UnresolvedAddressDto;
    /** Reserved padding to align mosaics on 8-byte boundary. */
    transferTransactionBody_Reserved1: number;
    /** Attached mosaics. */
    mosaics: UnresolvedMosaicBuilder[];
    /** Attached message. */
    message: Uint8Array;
    /**
     * Constructor.
     *
     * @param recipientAddress Recipient address.
     * @param mosaics Attached mosaics.
     * @param message Attached message.
     */
    constructor(recipientAddress: UnresolvedAddressDto, mosaics: UnresolvedMosaicBuilder[], message: Uint8Array);
    /**
     * Creates an instance of TransferTransactionBodyBuilder from binary payload.
     *
     * @param payload Byte payload to use to serialize the object.
     * @return Instance of TransferTransactionBodyBuilder.
     */
    static loadFromBinary(payload: Uint8Array): TransferTransactionBodyBuilder;
    /**
     * Gets recipient address.
     *
     * @return Recipient address.
     */
    getRecipientAddress(): UnresolvedAddressDto;
    /**
     * Gets reserved padding to align mosaics on 8-byte boundary.
     *
     * @return Reserved padding to align mosaics on 8-byte boundary.
     */
    getTransferTransactionBody_Reserved1(): number;
    /**
     * Gets attached mosaics.
     *
     * @return Attached mosaics.
     */
    getMosaics(): UnresolvedMosaicBuilder[];
    /**
     * Gets attached message.
     *
     * @return Attached message.
     */
    getMessage(): Uint8Array;
    /**
     * Gets the size of the object.
     *
     * @return Size in bytes.
     */
    getSize(): number;
    /**
     * Serializes an object to bytes.
     *
     * @return Serialized bytes.
     */
    serialize(): Uint8Array;
}
