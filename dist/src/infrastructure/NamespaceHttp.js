"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const rxjs_1 = require("rxjs");
const operators_1 = require("rxjs/operators");
const format_1 = require("../core/format");
const AccountNames_1 = require("../model/account/AccountNames");
const Address_1 = require("../model/account/Address");
const PublicAccount_1 = require("../model/account/PublicAccount");
const MosaicId_1 = require("../model/mosaic/MosaicId");
const MosaicNames_1 = require("../model/mosaic/MosaicNames");
const AddressAlias_1 = require("../model/namespace/AddressAlias");
const AliasType_1 = require("../model/namespace/AliasType");
const EmptyAlias_1 = require("../model/namespace/EmptyAlias");
const MosaicAlias_1 = require("../model/namespace/MosaicAlias");
const NamespaceId_1 = require("../model/namespace/NamespaceId");
const NamespaceInfo_1 = require("../model/namespace/NamespaceInfo");
const NamespaceName_1 = require("../model/namespace/NamespaceName");
const UInt64_1 = require("../model/UInt64");
const api_1 = require("./api");
const Http_1 = require("./Http");
/**
 * Namespace http repository.
 *
 * @since 1.0
 */
class NamespaceHttp extends Http_1.Http {
    /**
     * Constructor
     * @param url
     * @param networkType
     */
    constructor(url, networkType) {
        super(url, networkType);
        this.namespaceRoutesApi = new api_1.NamespaceRoutesApi(url);
    }
    /**
     * Returns friendly names for array of addresses.
     * @summary Get readable names for a set of array of addresses
     * @param addresses - Array of addresses
     */
    getAccountsNames(addresses) {
        const accountIdsBody = {
            addresses: addresses.map((address) => address.plain()),
        };
        return rxjs_1.from(this.namespaceRoutesApi.getAccountsNames(accountIdsBody)).pipe(operators_1.map(({ body }) => body.accountNames.map((accountName) => {
            return new AccountNames_1.AccountNames(Address_1.Address.createFromEncoded(accountName.address), accountName.names.map((name) => {
                return new NamespaceName_1.NamespaceName(new NamespaceId_1.NamespaceId(name), name);
            }));
        })), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
    /**
     * Get readable names for a set of mosaics
     * Returns friendly names for mosaics.
     * @param mosaicIds - Array of mosaic ids
     * @return Observable<MosaicNames[]>
     */
    getMosaicsNames(mosaicIds) {
        const mosaicIdsBody = {
            mosaicIds: mosaicIds.map((id) => id.toHex()),
        };
        return rxjs_1.from(this.namespaceRoutesApi.getMosaicsNames(mosaicIdsBody)).pipe(operators_1.map(({ body }) => body.mosaicNames.map((mosaic) => {
            return new MosaicNames_1.MosaicNames(new MosaicId_1.MosaicId(mosaic.mosaicId), mosaic.names.map((name) => {
                return new NamespaceName_1.NamespaceName(new NamespaceId_1.NamespaceId(name), name);
            }));
        })), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
    /**
     * Gets the NamespaceInfo for a given namespaceId
     * @param namespaceId - Namespace id
     * @returns Observable<NamespaceInfo>
     */
    getNamespace(namespaceId) {
        return this.getNetworkTypeObservable().pipe(operators_1.mergeMap((networkType) => rxjs_1.from(this.namespaceRoutesApi.getNamespace(namespaceId.toHex())).pipe(operators_1.map(({ body }) => new NamespaceInfo_1.NamespaceInfo(body.meta.active, body.meta.index, body.meta.id, body.namespace.registrationType, body.namespace.depth, this.extractLevels(body.namespace), NamespaceId_1.NamespaceId.createFromEncoded(body.namespace.parentId), PublicAccount_1.PublicAccount.createFromPublicKey(body.namespace.ownerPublicKey, networkType), UInt64_1.UInt64.fromNumericString(body.namespace.startHeight), UInt64_1.UInt64.fromNumericString(body.namespace.endHeight), this.extractAlias(body.namespace))), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))))));
    }
    /**
     * Gets array of NamespaceInfo for an account
     * @param address - Address
     * @param queryParams - (Optional) Query params
     * @returns Observable<NamespaceInfo[]>
     */
    getNamespacesFromAccount(address, queryParams) {
        return this.getNetworkTypeObservable().pipe(operators_1.mergeMap((networkType) => rxjs_1.from(this.namespaceRoutesApi.getNamespacesFromAccount(address.plain(), this.queryParams(queryParams).pageSize, this.queryParams(queryParams).id, this.queryParams(queryParams).order)).pipe(operators_1.map(({ body }) => body.namespaces.map((namespaceInfoDTO) => {
            return new NamespaceInfo_1.NamespaceInfo(namespaceInfoDTO.meta.active, namespaceInfoDTO.meta.index, namespaceInfoDTO.meta.id, namespaceInfoDTO.namespace.registrationType, namespaceInfoDTO.namespace.depth, this.extractLevels(namespaceInfoDTO.namespace), NamespaceId_1.NamespaceId.createFromEncoded(namespaceInfoDTO.namespace.parentId), PublicAccount_1.PublicAccount.createFromPublicKey(namespaceInfoDTO.namespace.ownerPublicKey, networkType), UInt64_1.UInt64.fromNumericString(namespaceInfoDTO.namespace.startHeight), UInt64_1.UInt64.fromNumericString(namespaceInfoDTO.namespace.endHeight), this.extractAlias(namespaceInfoDTO.namespace));
        })), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))))));
    }
    /**
     * Gets array of NamespaceInfo for different account
     * @param addresses - Array of Address
     * @param queryParams - (Optional) Query params
     * @returns Observable<NamespaceInfo[]>
     */
    getNamespacesFromAccounts(addresses, queryParams) {
        const publicKeysBody = {
            addresses: addresses.map((address) => address.plain()),
        };
        return this.getNetworkTypeObservable().pipe(operators_1.mergeMap((networkType) => rxjs_1.from(this.namespaceRoutesApi.getNamespacesFromAccounts(publicKeysBody)).pipe(operators_1.map(({ body }) => body.namespaces.map((namespaceInfoDTO) => {
            return new NamespaceInfo_1.NamespaceInfo(namespaceInfoDTO.meta.active, namespaceInfoDTO.meta.index, namespaceInfoDTO.meta.id, namespaceInfoDTO.namespace.registrationType, namespaceInfoDTO.namespace.depth, this.extractLevels(namespaceInfoDTO.namespace), NamespaceId_1.NamespaceId.createFromEncoded(namespaceInfoDTO.namespace.parentId), PublicAccount_1.PublicAccount.createFromPublicKey(namespaceInfoDTO.namespace.ownerPublicKey, networkType), UInt64_1.UInt64.fromNumericString(namespaceInfoDTO.namespace.startHeight), UInt64_1.UInt64.fromNumericString(namespaceInfoDTO.namespace.endHeight), this.extractAlias(namespaceInfoDTO.namespace));
        })), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))))));
    }
    /**
     * Gets array of NamespaceName for different namespaceIds
     * @param namespaceIds - Array of namespace ids
     * @returns Observable<NamespaceName[]>
     */
    getNamespacesName(namespaceIds) {
        const namespaceIdsBody = {
            namespaceIds: namespaceIds.map((id) => id.toHex()),
        };
        return rxjs_1.from(this.namespaceRoutesApi.getNamespacesNames(namespaceIdsBody)).pipe(operators_1.map(({ body }) => body.map((namespaceNameDTO) => {
            return new NamespaceName_1.NamespaceName(NamespaceId_1.NamespaceId.createFromEncoded(namespaceNameDTO.id), namespaceNameDTO.name, namespaceNameDTO.parentId ? NamespaceId_1.NamespaceId.createFromEncoded(namespaceNameDTO.parentId) : undefined);
        })), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
    /**
     * Gets the MosaicId from a MosaicAlias
     * @param namespaceId - the namespaceId of the namespace
     * @returns Observable<MosaicId | null>
     */
    getLinkedMosaicId(namespaceId) {
        return this.getNetworkTypeObservable().pipe(operators_1.mergeMap(() => rxjs_1.from(this.namespaceRoutesApi.getNamespace(namespaceId.toHex())).pipe(operators_1.map(({ body }) => {
            const namespaceInfoDTO = body;
            if (namespaceInfoDTO.namespace === undefined) {
                // forward catapult-rest error
                throw namespaceInfoDTO;
            }
            if (namespaceInfoDTO.namespace.alias.type.valueOf() === AliasType_1.AliasType.None
                || namespaceInfoDTO.namespace.alias.type.valueOf() !== AliasType_1.AliasType.Mosaic
                || !namespaceInfoDTO.namespace.alias.mosaicId) {
                throw new Error('No mosaicId is linked to namespace \'' + namespaceInfoDTO.namespace.level0 + '\'');
            }
            return new MosaicId_1.MosaicId(namespaceInfoDTO.namespace.alias.mosaicId);
        }), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))))));
    }
    /**
     * Gets the Address from a AddressAlias
     * @param namespaceId - the namespaceId of the namespace
     * @returns Observable<Address>
     */
    getLinkedAddress(namespaceId) {
        return this.getNetworkTypeObservable().pipe(operators_1.mergeMap(() => rxjs_1.from(this.namespaceRoutesApi.getNamespace(namespaceId.toHex())).pipe(operators_1.map(({ body }) => {
            const namespaceInfoDTO = body;
            if (namespaceInfoDTO.namespace === undefined) {
                // forward catapult-rest error
                throw namespaceInfoDTO;
            }
            if (namespaceInfoDTO.namespace.alias.type.valueOf() === AliasType_1.AliasType.None
                || namespaceInfoDTO.namespace.alias.type.valueOf() !== AliasType_1.AliasType.Address
                || !namespaceInfoDTO.namespace.alias.address) {
                throw new Error('No address is linked to namespace \'' + namespaceInfoDTO.namespace.level0 + '\'');
            }
            const addressDecoded = namespaceInfoDTO.namespace.alias.address;
            const address = format_1.RawAddress.addressToString(format_1.Convert.hexToUint8(addressDecoded));
            return Address_1.Address.createFromRawAddress(address);
        }), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))))));
    }
    extractLevels(namespace) {
        const result = [];
        if (namespace.level0) {
            result.push(NamespaceId_1.NamespaceId.createFromEncoded(namespace.level0));
        }
        if (namespace.level1) {
            result.push(NamespaceId_1.NamespaceId.createFromEncoded(namespace.level1));
        }
        if (namespace.level2) {
            result.push(NamespaceId_1.NamespaceId.createFromEncoded(namespace.level2));
        }
        return result;
    }
    /**
     * Extract the alias from a namespace
     *
     * @internal
     * @access private
     * @param namespace
     */
    extractAlias(namespace) {
        if (namespace.alias && namespace.alias.type === AliasType_1.AliasType.Mosaic) {
            return new MosaicAlias_1.MosaicAlias(new MosaicId_1.MosaicId(namespace.alias.mosaicId));
        }
        else if (namespace.alias && namespace.alias.type === AliasType_1.AliasType.Address) {
            return new AddressAlias_1.AddressAlias(Address_1.Address.createFromEncoded(namespace.alias.address));
        }
        return new EmptyAlias_1.EmptyAlias();
    }
}
exports.NamespaceHttp = NamespaceHttp;
//# sourceMappingURL=NamespaceHttp.js.map