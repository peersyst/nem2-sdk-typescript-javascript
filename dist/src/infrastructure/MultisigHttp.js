"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const rxjs_1 = require("rxjs");
const operators_1 = require("rxjs/operators");
const MultisigAccountGraphInfo_1 = require("../model/account/MultisigAccountGraphInfo");
const MultisigAccountInfo_1 = require("../model/account/MultisigAccountInfo");
const PublicAccount_1 = require("../model/account/PublicAccount");
const multisigRoutesApi_1 = require("./api/multisigRoutesApi");
const Http_1 = require("./Http");
/**
 * Multisig http repository.
 *
 * @since 1.0
 */
class MultisigHttp extends Http_1.Http {
    /**
     * Constructor
     * @param url
     * @param networkType
     */
    constructor(url, networkType) {
        super(url, networkType);
        this.multisigRoutesApi = new multisigRoutesApi_1.MultisigRoutesApi(url);
    }
    /**
     * Gets a MultisigAccountInfo for an account.
     * @param address - * Address can be created rawAddress or publicKey
     * @returns Observable<MultisigAccountInfo>
     */
    getMultisigAccountInfo(address) {
        return this.getNetworkTypeObservable().pipe(operators_1.mergeMap((networkType) => rxjs_1.from(this.multisigRoutesApi.getAccountMultisig(address.plain()))
            .pipe(operators_1.map(({ body }) => new MultisigAccountInfo_1.MultisigAccountInfo(PublicAccount_1.PublicAccount.createFromPublicKey(body.multisig.accountPublicKey, networkType), body.multisig.minApproval, body.multisig.minRemoval, body.multisig.cosignatoryPublicKeys
            .map((cosigner) => PublicAccount_1.PublicAccount.createFromPublicKey(cosigner, networkType)), body.multisig.multisigPublicKeys
            .map((multisigAccount) => PublicAccount_1.PublicAccount.createFromPublicKey(multisigAccount, networkType)))), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))))));
    }
    /**
     * Gets a MultisigAccountGraphInfo for an account.
     * @param address - * Address can be created rawAddress or publicKey
     * @returns Observable<MultisigAccountGraphInfo>
     */
    getMultisigAccountGraphInfo(address) {
        return this.getNetworkTypeObservable().pipe(operators_1.mergeMap((networkType) => rxjs_1.from(this.multisigRoutesApi.getAccountMultisigGraph(address.plain()))
            .pipe(operators_1.map(({ body }) => {
            const multisigAccountGraphInfosDTO = body;
            const multisigAccounts = new Map();
            multisigAccountGraphInfosDTO.map((multisigAccountGraphInfoDTO) => {
                multisigAccounts.set(multisigAccountGraphInfoDTO.level, multisigAccountGraphInfoDTO.multisigEntries.map((multisigAccountInfoDTO) => {
                    return new MultisigAccountInfo_1.MultisigAccountInfo(PublicAccount_1.PublicAccount.createFromPublicKey(multisigAccountInfoDTO.multisig.accountPublicKey, networkType), multisigAccountInfoDTO.multisig.minApproval, multisigAccountInfoDTO.multisig.minRemoval, multisigAccountInfoDTO.multisig.cosignatoryPublicKeys
                        .map((cosigner) => PublicAccount_1.PublicAccount.createFromPublicKey(cosigner, networkType)), multisigAccountInfoDTO.multisig.multisigPublicKeys
                        .map((multisigAccountDTO) => PublicAccount_1.PublicAccount.createFromPublicKey(multisigAccountDTO, networkType)));
                }));
            });
            return new MultisigAccountGraphInfo_1.MultisigAccountGraphInfo(multisigAccounts);
        }), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))))));
    }
}
exports.MultisigHttp = MultisigHttp;
//# sourceMappingURL=MultisigHttp.js.map