import { Observable } from 'rxjs';
import { NetworkName } from '../model/blockchain/NetworkName';
import { NetworkType } from '../model/blockchain/NetworkType';
import { Http } from './Http';
import { NetworkRepository } from './NetworkRepository';
/**
 * Network http repository.
 *
 * @since 1.0
 */
export declare class NetworkHttp extends Http implements NetworkRepository {
    private networkRouteApi;
    /**
     * Constructor
     * @param url
     */
    constructor(url: string);
    /**
     * Get current network identifier.
     *
     * @return network identifier.
     */
    getNetworkType(): Observable<NetworkType>;
    /**
     * Get current network type name and description
     *
     * @return current network type name and description
     */
    getNetworkName(): Observable<NetworkName>;
}
