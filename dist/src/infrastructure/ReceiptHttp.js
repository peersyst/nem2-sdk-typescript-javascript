"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const rxjs_1 = require("rxjs");
const operators_1 = require("rxjs/operators");
const MerklePathItem_1 = require("../model/blockchain/MerklePathItem");
const MerkleProofInfo_1 = require("../model/blockchain/MerkleProofInfo");
const receiptRoutesApi_1 = require("./api/receiptRoutesApi");
const Http_1 = require("./Http");
const CreateReceiptFromDTO_1 = require("./receipt/CreateReceiptFromDTO");
/**
 * Receipt http repository.
 *
 * @since 1.0
 */
class ReceiptHttp extends Http_1.Http {
    /**
     * Constructor
     * @param url
     * @param networkType
     */
    constructor(url, networkType) {
        super(url, networkType);
        this.receiptRoutesApi = new receiptRoutesApi_1.ReceiptRoutesApi(url);
    }
    /**
     * Get the merkle path for a given a receipt statement hash and block
     * Returns the merkle path for a [receipt statement or resolution](https://nemtech.github.io/concepts/receipt.html)
     * linked to a block. The path is the complementary data needed to calculate the merkle root.
     * A client can compare if the calculated root equals the one recorded in the block header,
     * verifying that the receipt was linked with the block.
     * @param height The height of the block.
     * @param hash The hash of the receipt statement or resolution.
     * @return Observable<MerkleProofInfo>
     */
    getMerkleReceipts(height, hash) {
        return rxjs_1.from(this.receiptRoutesApi.getMerkleReceipts(height, hash)).pipe(operators_1.map(({ body }) => new MerkleProofInfo_1.MerkleProofInfo(body.merklePath.map((payload) => new MerklePathItem_1.MerklePathItem(payload.position, payload.hash)))), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))));
    }
    /**
     * Gets an array receipts for a block height.
     * @param height - Block height from which will be the first block in the array
     * @param queryParams - (Optional) Query params
     * @returns Observable<Statement>
     */
    getBlockReceipts(height) {
        return this.getNetworkTypeObservable().pipe(operators_1.mergeMap((networkType) => rxjs_1.from(this.receiptRoutesApi.getBlockReceipts(height)).pipe(operators_1.map(({ body }) => CreateReceiptFromDTO_1.CreateStatementFromDTO(body, networkType)), operators_1.catchError((error) => rxjs_1.throwError(this.errorHandling(error))))));
    }
}
exports.ReceiptHttp = ReceiptHttp;
//# sourceMappingURL=ReceiptHttp.js.map