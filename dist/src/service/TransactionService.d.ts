import { Observable } from 'rxjs';
import { Listener } from '../infrastructure/Listener';
import { AggregateTransaction } from '../model/transaction/AggregateTransaction';
import { SignedTransaction } from '../model/transaction/SignedTransaction';
import { Transaction } from '../model/transaction/Transaction';
import { ITransactionService } from './interfaces/ITransactionService';
/**
 * Transaction Service
 */
export declare class TransactionService implements ITransactionService {
    private readonly transactionHttp;
    private readonly receiptHttp;
    /**
     * Constructor
     * @param url Base catapult-rest url
     */
    constructor(url: string);
    /**
     * Resolve unresolved mosaic / address from array of transactions
     * @param transationHashes List of transaction hashes.
     * @param listener Websocket listener
     * @returns Observable<Transaction[]>
     */
    resolveAliases(transationHashes: string[]): Observable<Transaction[]>;
    /**
     * @param signedTransaction Signed transaction to be announced.
     * @param listener Websocket listener
     * @returns {Observable<Transaction>}
     */
    announce(signedTransaction: SignedTransaction, listener: Listener): Observable<Transaction>;
    /**
     * Announce aggregate transaction
     * **NOTE** A lock fund transaction for this aggregate bonded should exists
     * @param signedTransaction Signed aggregate bonded transaction.
     * @param listener Websocket listener
     * @returns {Observable<AggregateTransaction>}
     */
    announceAggregateBonded(signedTransaction: SignedTransaction, listener: Listener): Observable<AggregateTransaction>;
    /**
     * Announce aggregate bonded transaction with lock fund
     * @param signedHashLockTransaction Signed hash lock transaction.
     * @param signedAggregateTransaction Signed aggregate bonded transaction.
     * @param listener Websocket listener
     * @returns {Observable<AggregateTransaction>}
     */
    announceHashLockAggregateBonded(signedHashLockTransaction: SignedTransaction, signedAggregateTransaction: SignedTransaction, listener: Listener): Observable<AggregateTransaction>;
    /**
     * Resolve transaction alias(s)
     * @param transaction Transaction to be resolved
     * @returns {Observable<Transaction>}
     */
    private resolveTransaction(transaction);
}
