import { Observable } from 'rxjs';
import { RestrictionMosaicHttp } from '../infrastructure/RestrictionMosaicHttp';
import { Address } from '../model/account/Address';
import { NetworkType } from '../model/blockchain/NetworkType';
import { MosaicId } from '../model/mosaic/MosaicId';
import { MosaicRestrictionType } from '../model/restriction/MosaicRestrictionType';
import { Deadline } from '../model/transaction/Deadline';
import { Transaction } from '../model/transaction/Transaction';
import { UInt64 } from '../model/UInt64';
/**
 * MosaicRestrictionTransactionService service
 */
export declare class MosaicRestrictionTransactionService {
    private readonly restrictionHttp;
    private readonly defaultMosaicAddressRestrictionVaule;
    private readonly defaultMosaicGlobalRestrictionVaule;
    /**
     * Constructor
     * @param restrictionHttp
     */
    constructor(restrictionHttp: RestrictionMosaicHttp);
    /**
     * Create a MosaicGlobalRestrictionTransaction object without previous restriction data
     * @param deadline - Deadline
     * @param networkType - Network identifier
     * @param mosaicId - MosaicId
     * @param restrictionKey - Restriction key
     * @param restrictionValue - New restriction value
     * @param restrictionType - New restriction type
     * @param referenceMosaicId - Reference mosaic Id
     * @param maxFee - Max fee
     */
    createMosaicGlobalRestrictionTransaction(deadline: Deadline, networkType: NetworkType, mosaicId: MosaicId, restrictionKey: UInt64, restrictionValue: string, restrictionType: MosaicRestrictionType, referenceMosaicId?: MosaicId, maxFee?: UInt64): Observable<Transaction>;
    /**
     * Create a MosaicAddressRestrictionTransaction object without previous restriction data
     * @param deadline - Deadline
     * @param networkType - Network identifier
     * @param mosaicId - MosaicId
     * @param restrictionKey - Restriction key
     * @param targetAddress - Target address
     * @param restrictionValue - New restriction value
     * @param maxFee - Max fee
     */
    createMosaicAddressRestrictionTransaction(deadline: Deadline, networkType: NetworkType, mosaicId: MosaicId, restrictionKey: UInt64, targetAddress: Address, restrictionValue: string, maxFee?: UInt64): Observable<Transaction>;
    /**
     * Get mosaic global restriction prvious value and type
     * @param mosaicId - Mosaic identifier
     * @param restrictionKey - Mosaic global restriction key
     * @return {Observable<MosaicGlobalRestrictionItem | undefined>}
     */
    private getGlobalRestrictionEntry(mosaicId, restrictionKey);
    /**
     * Check if input restriction key and value are invalid or not
     * @param value - Restriction value
     */
    private validateInput(value);
}
