import { MosaicInfo } from '../model/mosaic/MosaicInfo';
/**
 * Class representing mosaic view information
 */
export declare class MosaicView {
    /**
                 * The mosaic information
                 */ readonly mosaicInfo: MosaicInfo;
}
