"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const rxjs_1 = require("rxjs");
const operators_1 = require("rxjs/operators");
const MosaicId_1 = require("../model/mosaic/MosaicId");
const MosaicRestrictionType_1 = require("../model/restriction/MosaicRestrictionType");
const MosaicAddressRestrictionTransaction_1 = require("../model/transaction/MosaicAddressRestrictionTransaction");
const MosaicGlobalRestrictionTransaction_1 = require("../model/transaction/MosaicGlobalRestrictionTransaction");
const UInt64_1 = require("../model/UInt64");
/**
 * MosaicRestrictionTransactionService service
 */
class MosaicRestrictionTransactionService {
    /**
     * Constructor
     * @param restrictionHttp
     */
    constructor(restrictionHttp) {
        this.restrictionHttp = restrictionHttp;
        this.defaultMosaicAddressRestrictionVaule = UInt64_1.UInt64.fromHex('FFFFFFFFFFFFFFFF');
        this.defaultMosaicGlobalRestrictionVaule = UInt64_1.UInt64.fromUint(0);
    }
    /**
     * Create a MosaicGlobalRestrictionTransaction object without previous restriction data
     * @param deadline - Deadline
     * @param networkType - Network identifier
     * @param mosaicId - MosaicId
     * @param restrictionKey - Restriction key
     * @param restrictionValue - New restriction value
     * @param restrictionType - New restriction type
     * @param referenceMosaicId - Reference mosaic Id
     * @param maxFee - Max fee
     */
    createMosaicGlobalRestrictionTransaction(deadline, networkType, mosaicId, restrictionKey, restrictionValue, restrictionType, referenceMosaicId = new MosaicId_1.MosaicId(UInt64_1.UInt64.fromUint(0).toDTO()), maxFee = new UInt64_1.UInt64([0, 0])) {
        this.validateInput(restrictionValue);
        return this.getGlobalRestrictionEntry(mosaicId, restrictionKey).pipe(operators_1.map((restrictionEntry) => {
            const currentValue = restrictionEntry ? UInt64_1.UInt64.fromNumericString(restrictionEntry.restrictionValue) :
                this.defaultMosaicGlobalRestrictionVaule;
            const currentType = restrictionEntry ? restrictionEntry.restrictionType : MosaicRestrictionType_1.MosaicRestrictionType.NONE;
            return MosaicGlobalRestrictionTransaction_1.MosaicGlobalRestrictionTransaction.create(deadline, mosaicId, restrictionKey, currentValue, currentType, UInt64_1.UInt64.fromNumericString(restrictionValue), restrictionType, networkType, referenceMosaicId, maxFee);
        }), operators_1.catchError((err) => {
            throw Error(err);
        }));
    }
    /**
     * Create a MosaicAddressRestrictionTransaction object without previous restriction data
     * @param deadline - Deadline
     * @param networkType - Network identifier
     * @param mosaicId - MosaicId
     * @param restrictionKey - Restriction key
     * @param targetAddress - Target address
     * @param restrictionValue - New restriction value
     * @param maxFee - Max fee
     */
    createMosaicAddressRestrictionTransaction(deadline, networkType, mosaicId, restrictionKey, targetAddress, restrictionValue, maxFee = new UInt64_1.UInt64([0, 0])) {
        this.validateInput(restrictionValue);
        return this.getGlobalRestrictionEntry(mosaicId, restrictionKey).pipe(operators_1.switchMap((restrictionEntry) => {
            if (!restrictionEntry) {
                throw Error('Global restriction is not valid for RetrictionKey: ' + restrictionKey);
            }
            return this.restrictionHttp.getMosaicAddressRestriction(mosaicId, targetAddress).pipe(operators_1.map((addressRestriction) => {
                const addressEntry = addressRestriction.restrictions.get(restrictionKey.toHex());
                const currentValue = addressEntry ? UInt64_1.UInt64.fromNumericString(addressEntry) :
                    this.defaultMosaicAddressRestrictionVaule;
                return MosaicAddressRestrictionTransaction_1.MosaicAddressRestrictionTransaction.create(deadline, mosaicId, restrictionKey, targetAddress, UInt64_1.UInt64.fromNumericString(restrictionValue), networkType, currentValue, maxFee);
            }), operators_1.catchError((err) => {
                const error = JSON.parse(err.message);
                if (error && error.statusCode && error.statusCode === 404) {
                    return rxjs_1.of(MosaicAddressRestrictionTransaction_1.MosaicAddressRestrictionTransaction.create(deadline, mosaicId, restrictionKey, targetAddress, UInt64_1.UInt64.fromNumericString(restrictionValue), networkType, this.defaultMosaicAddressRestrictionVaule, maxFee));
                }
                throw Error(err.message);
            }));
        }));
    }
    /**
     * Get mosaic global restriction prvious value and type
     * @param mosaicId - Mosaic identifier
     * @param restrictionKey - Mosaic global restriction key
     * @return {Observable<MosaicGlobalRestrictionItem | undefined>}
     */
    getGlobalRestrictionEntry(mosaicId, restrictionKey) {
        return this.restrictionHttp.getMosaicGlobalRestriction(mosaicId).pipe(operators_1.map((mosaicRestriction) => {
            return mosaicRestriction.restrictions.get(restrictionKey.toHex());
        }), operators_1.catchError((err) => {
            const error = JSON.parse(err.message);
            if (error && error.statusCode && error.statusCode === 404) {
                return rxjs_1.of(undefined);
            }
            throw Error(err.message);
        }));
    }
    /**
     * Check if input restriction key and value are invalid or not
     * @param value - Restriction value
     */
    validateInput(value) {
        if (!UInt64_1.UInt64.isLongNumericString(value)) {
            throw Error(`RestrictionValue: ${value} is not a valid numeric string.`);
        }
    }
}
exports.MosaicRestrictionTransactionService = MosaicRestrictionTransactionService;
//# sourceMappingURL=MosaicRestrictionTransactionService.js.map