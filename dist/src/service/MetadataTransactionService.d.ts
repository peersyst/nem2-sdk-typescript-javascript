import { Observable } from 'rxjs';
import { MetadataHttp } from '../infrastructure/MetadataHttp';
import { PublicAccount } from '../model/account/PublicAccount';
import { NetworkType } from '../model/blockchain/NetworkType';
import { MetadataType } from '../model/metadata/MetadataType';
import { MosaicId } from '../model/mosaic/MosaicId';
import { NamespaceId } from '../model/namespace/NamespaceId';
import { AccountMetadataTransaction } from '../model/transaction/AccountMetadataTransaction';
import { Deadline } from '../model/transaction/Deadline';
import { MosaicMetadataTransaction } from '../model/transaction/MosaicMetadataTransaction';
import { NamespaceMetadataTransaction } from '../model/transaction/NamespaceMetadataTransaction';
import { UInt64 } from '../model/UInt64';
/**
 * MetadataTransaction service
 */
export declare class MetadataTransactionService {
    private readonly metadataHttp;
    /**
     * Constructor
     * @param metadataHttp
     */
    constructor(metadataHttp: MetadataHttp);
    /**
     * Create a Metadata Transaction object without knowing previous metadata value
     * @param deadline - Deadline
     * @param networkType - Network identifier
     * @param metadataType - Matadata type
     * @param targetPublicAccount - Target public account
     * @param key - Metadata scoped key
     * @param value - New metadata value
     * @param senderPublicAccount - sender (signer) public account
     * @param targetId - Target Id (MosaicId | NamespaceId)
     * @param maxFee - Max fee
     * @return {AccountMetadataTransaction | MosaicMetadataTransaction | NamespaceMetadataTransaction}
     */
    createMetadataTransaction(deadline: Deadline, networkType: NetworkType, metadataType: MetadataType, targetPublicAccount: PublicAccount, key: UInt64, value: string, senderPublicAccount: PublicAccount, targetId?: MosaicId | NamespaceId, maxFee?: UInt64): Observable<AccountMetadataTransaction | MosaicMetadataTransaction | NamespaceMetadataTransaction>;
}
