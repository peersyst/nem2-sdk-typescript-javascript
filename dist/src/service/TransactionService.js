"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const rxjs_1 = require("rxjs");
const operators_1 = require("rxjs/operators");
const ReceiptHttp_1 = require("../infrastructure/ReceiptHttp");
const TransactionHttp_1 = require("../infrastructure/TransactionHttp");
const NamespaceId_1 = require("../model/namespace/NamespaceId");
const TransactionType_1 = require("../model/transaction/TransactionType");
/**
 * Transaction Service
 */
class TransactionService {
    /**
     * Constructor
     * @param url Base catapult-rest url
     */
    constructor(url) {
        this.transactionHttp = new TransactionHttp_1.TransactionHttp(url);
        this.receiptHttp = new ReceiptHttp_1.ReceiptHttp(url);
    }
    /**
     * Resolve unresolved mosaic / address from array of transactions
     * @param transationHashes List of transaction hashes.
     * @param listener Websocket listener
     * @returns Observable<Transaction[]>
     */
    resolveAliases(transationHashes) {
        return this.transactionHttp.getTransactions(transationHashes).pipe(operators_1.mergeMap((_) => _), operators_1.mergeMap((transaction) => this.resolveTransaction(transaction)), operators_1.toArray());
    }
    /**
     * @param signedTransaction Signed transaction to be announced.
     * @param listener Websocket listener
     * @returns {Observable<Transaction>}
     */
    announce(signedTransaction, listener) {
        return this.transactionHttp.announce(signedTransaction).pipe(operators_1.flatMap(() => listener.confirmed(signedTransaction.getSignerAddress(), signedTransaction.hash)));
    }
    /**
     * Announce aggregate transaction
     * **NOTE** A lock fund transaction for this aggregate bonded should exists
     * @param signedTransaction Signed aggregate bonded transaction.
     * @param listener Websocket listener
     * @returns {Observable<AggregateTransaction>}
     */
    announceAggregateBonded(signedTransaction, listener) {
        return this.transactionHttp.announceAggregateBonded(signedTransaction).pipe(operators_1.flatMap(() => listener.aggregateBondedAdded(signedTransaction.getSignerAddress(), signedTransaction.hash)));
    }
    /**
     * Announce aggregate bonded transaction with lock fund
     * @param signedHashLockTransaction Signed hash lock transaction.
     * @param signedAggregateTransaction Signed aggregate bonded transaction.
     * @param listener Websocket listener
     * @returns {Observable<AggregateTransaction>}
     */
    announceHashLockAggregateBonded(signedHashLockTransaction, signedAggregateTransaction, listener) {
        return this.announce(signedHashLockTransaction, listener).pipe(operators_1.flatMap(() => this.announceAggregateBonded(signedAggregateTransaction, listener)));
    }
    /**
     * Resolve transaction alias(s)
     * @param transaction Transaction to be resolved
     * @returns {Observable<Transaction>}
     */
    resolveTransaction(transaction) {
        if ([TransactionType_1.TransactionType.AGGREGATE_BONDED, TransactionType_1.TransactionType.AGGREGATE_COMPLETE].includes(transaction.type)) {
            if (transaction.innerTransactions.find((tx) => this.checkShouldResolve(tx))) {
                return this.resolvedFromReceipt(transaction, transaction.transactionInfo.index);
            }
            return rxjs_1.of(transaction);
        }
        return this.checkShouldResolve(transaction) ? this.resolvedFromReceipt(transaction, 0) : rxjs_1.of(transaction);
    }
    /**
     * @internal
     * Check if receiptHttp needs to be called to resolve transaction alias
     * @param transaction Transaction
     * @return {boolean}
     */
    checkShouldResolve(transaction) {
        switch (transaction.type) {
            case TransactionType_1.TransactionType.LINK_ACCOUNT:
            case TransactionType_1.TransactionType.ACCOUNT_METADATA_TRANSACTION:
            case TransactionType_1.TransactionType.ACCOUNT_RESTRICTION_OPERATION:
            case TransactionType_1.TransactionType.ADDRESS_ALIAS:
            case TransactionType_1.TransactionType.MOSAIC_ALIAS:
            case TransactionType_1.TransactionType.MOSAIC_DEFINITION:
            case TransactionType_1.TransactionType.MODIFY_MULTISIG_ACCOUNT:
            case TransactionType_1.TransactionType.NAMESPACE_METADATA_TRANSACTION:
            case TransactionType_1.TransactionType.REGISTER_NAMESPACE:
                return false;
            case TransactionType_1.TransactionType.ACCOUNT_RESTRICTION_ADDRESS:
                const accountAddressRestriction = transaction;
                return accountAddressRestriction.restrictionAdditions.find((address) => address instanceof NamespaceId_1.NamespaceId) !== undefined ||
                    accountAddressRestriction.restrictionDeletions.find((address) => address instanceof NamespaceId_1.NamespaceId) !== undefined;
            case TransactionType_1.TransactionType.ACCOUNT_RESTRICTION_MOSAIC:
                const accountMosaicRestriction = transaction;
                return accountMosaicRestriction.restrictionAdditions.find((mosaicId) => mosaicId instanceof NamespaceId_1.NamespaceId) !== undefined ||
                    accountMosaicRestriction.restrictionDeletions.find((mosaicId) => mosaicId instanceof NamespaceId_1.NamespaceId) !== undefined;
            case TransactionType_1.TransactionType.LOCK:
                return transaction.mosaic.id instanceof NamespaceId_1.NamespaceId;
            case TransactionType_1.TransactionType.MOSAIC_ADDRESS_RESTRICTION:
                const mosaicAddressRestriction = transaction;
                return mosaicAddressRestriction.targetAddress instanceof NamespaceId_1.NamespaceId ||
                    mosaicAddressRestriction.mosaicId instanceof NamespaceId_1.NamespaceId;
            case TransactionType_1.TransactionType.MOSAIC_GLOBAL_RESTRICTION:
                const mosaicGlobalRestriction = transaction;
                return mosaicGlobalRestriction.referenceMosaicId instanceof NamespaceId_1.NamespaceId ||
                    mosaicGlobalRestriction.mosaicId instanceof NamespaceId_1.NamespaceId;
            case TransactionType_1.TransactionType.MOSAIC_METADATA_TRANSACTION:
                return transaction.targetMosaicId instanceof NamespaceId_1.NamespaceId;
            case TransactionType_1.TransactionType.MOSAIC_SUPPLY_CHANGE:
                return transaction.mosaicId instanceof NamespaceId_1.NamespaceId;
            case TransactionType_1.TransactionType.SECRET_PROOF:
                return transaction.recipientAddress instanceof NamespaceId_1.NamespaceId;
            case TransactionType_1.TransactionType.SECRET_LOCK:
                const secretLock = transaction;
                return secretLock.recipientAddress instanceof NamespaceId_1.NamespaceId ||
                    secretLock.mosaic.id instanceof NamespaceId_1.NamespaceId;
            case TransactionType_1.TransactionType.TRANSFER:
                const transfer = transaction;
                return transfer.recipientAddress instanceof NamespaceId_1.NamespaceId ||
                    transfer.mosaics.find((mosaic) => mosaic.id instanceof NamespaceId_1.NamespaceId) !== undefined;
            default:
                throw new Error('Transaction type not not recogonised.');
        }
    }
    /**
     * @internal
     * Resolve transaction alais(s) from block receipt by calling receiptHttp
     * @param transaction Transaction to be resolved
     * @param aggregateIndex Aggregate transaction index
     * @return {Observable<Transaction>}
     */
    resolvedFromReceipt(transaction, aggregateIndex) {
        return this.receiptHttp.getBlockReceipts(transaction.transactionInfo.height.toString()).pipe(operators_1.map((statement) => transaction.resolveAliases(statement, aggregateIndex)));
    }
}
exports.TransactionService = TransactionService;
//# sourceMappingURL=TransactionService.js.map