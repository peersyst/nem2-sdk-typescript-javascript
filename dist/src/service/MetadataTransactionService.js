"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const rxjs_1 = require("rxjs");
const operators_1 = require("rxjs/operators");
const Convert_1 = require("../core/format/Convert");
const Address_1 = require("../model/account/Address");
const MetadataType_1 = require("../model/metadata/MetadataType");
const MosaicId_1 = require("../model/mosaic/MosaicId");
const NamespaceId_1 = require("../model/namespace/NamespaceId");
const AccountMetadataTransaction_1 = require("../model/transaction/AccountMetadataTransaction");
const MosaicMetadataTransaction_1 = require("../model/transaction/MosaicMetadataTransaction");
const NamespaceMetadataTransaction_1 = require("../model/transaction/NamespaceMetadataTransaction");
const UInt64_1 = require("../model/UInt64");
/**
 * MetadataTransaction service
 */
class MetadataTransactionService {
    /**
     * Constructor
     * @param metadataHttp
     */
    constructor(metadataHttp) {
        this.metadataHttp = metadataHttp;
    }
    /**
     * Create a Metadata Transaction object without knowing previous metadata value
     * @param deadline - Deadline
     * @param networkType - Network identifier
     * @param metadataType - Matadata type
     * @param targetPublicAccount - Target public account
     * @param key - Metadata scoped key
     * @param value - New metadata value
     * @param senderPublicAccount - sender (signer) public account
     * @param targetId - Target Id (MosaicId | NamespaceId)
     * @param maxFee - Max fee
     * @return {AccountMetadataTransaction | MosaicMetadataTransaction | NamespaceMetadataTransaction}
     */
    createMetadataTransaction(deadline, networkType, metadataType, targetPublicAccount, key, value, senderPublicAccount, targetId, maxFee = new UInt64_1.UInt64([0, 0])) {
        switch (metadataType) {
            case MetadataType_1.MetadataType.Account:
                return this.createAccountMetadataTransaction(deadline, networkType, targetPublicAccount.publicKey, key, value, senderPublicAccount.publicKey, maxFee);
            case MetadataType_1.MetadataType.Mosaic:
                if (!targetId || !(targetId instanceof MosaicId_1.MosaicId)) {
                    throw Error('TargetId for MosaicMetadataTransaction is invalid');
                }
                return this.createMosaicMetadataTransaction(deadline, networkType, targetPublicAccount.publicKey, targetId, key, value, senderPublicAccount.publicKey, maxFee);
            case MetadataType_1.MetadataType.Namespace:
                if (!targetId || !(targetId instanceof NamespaceId_1.NamespaceId)) {
                    throw Error('TargetId for NamespaceMetadataTransaction is invalid');
                }
                return this.createNamespaceMetadataTransaction(deadline, networkType, targetPublicAccount.publicKey, targetId, key, value, senderPublicAccount.publicKey, maxFee);
            default:
                throw Error('Metadata type invalid');
        }
    }
    /**
     * @internal
     * @param deadline - Deadline
     * @param networkType - Network identifier
     * @param targetPublicKey - Target public key
     * @param key - Metadata key
     * @param value - New metadata value
     * @param senderPublicKey - sender (signer) public key
     * @param maxFee - max fee
     * @returns {Observable<AccountMetadataTransaction>}
     */
    createAccountMetadataTransaction(deadline, networkType, targetPublicKey, key, value, senderPublicKey, maxFee) {
        return this.metadataHttp.getAccountMetadataByKeyAndSender(Address_1.Address.createFromPublicKey(targetPublicKey, networkType), key.toHex(), senderPublicKey)
            .pipe(operators_1.map((metadata) => {
            const currentValueByte = Convert_1.Convert.utf8ToUint8(metadata.metadataEntry.value);
            const newValueBytes = Convert_1.Convert.utf8ToUint8(value);
            return AccountMetadataTransaction_1.AccountMetadataTransaction.create(deadline, targetPublicKey, key, newValueBytes.length - currentValueByte.length, Convert_1.Convert.decodeHex(Convert_1.Convert.xor(currentValueByte, newValueBytes)), networkType, maxFee);
        }), operators_1.catchError((err) => {
            const error = JSON.parse(err.message);
            if (error && error.statusCode && error.statusCode === 404) {
                const newValueBytes = Convert_1.Convert.utf8ToUint8(value);
                return rxjs_1.of(AccountMetadataTransaction_1.AccountMetadataTransaction.create(deadline, targetPublicKey, key, newValueBytes.length, value, networkType, maxFee));
            }
            throw Error(err.message);
        }));
    }
    /**
     * @internal
     * @param deadline - Deadline
     * @param networkType - Network identifier
     * @param targetPublicKey - Target public key
     * @param mosaicId - Mosaic Id
     * @param key - Metadata key
     * @param value - New metadata value
     * @param senderPublicKey - sender (signer) public key
     * @param maxFee - max fee
     * @returns {Observable<MosaicMetadataTransaction>}
     */
    createMosaicMetadataTransaction(deadline, networkType, targetPublicKey, mosaicId, key, value, senderPublicKey, maxFee) {
        return this.metadataHttp.getMosaicMetadataByKeyAndSender(mosaicId, key.toHex(), senderPublicKey)
            .pipe(operators_1.map((metadata) => {
            const currentValueByte = Convert_1.Convert.utf8ToUint8(metadata.metadataEntry.value);
            const newValueBytes = Convert_1.Convert.utf8ToUint8(value);
            return MosaicMetadataTransaction_1.MosaicMetadataTransaction.create(deadline, targetPublicKey, key, mosaicId, newValueBytes.length - currentValueByte.length, Convert_1.Convert.decodeHex(Convert_1.Convert.xor(currentValueByte, newValueBytes)), networkType, maxFee);
        }), operators_1.catchError((err) => {
            const error = JSON.parse(err.message);
            if (error && error.statusCode && error.statusCode === 404) {
                const newValueBytes = Convert_1.Convert.utf8ToUint8(value);
                return rxjs_1.of(MosaicMetadataTransaction_1.MosaicMetadataTransaction.create(deadline, targetPublicKey, key, mosaicId, newValueBytes.length, value, networkType, maxFee));
            }
            throw Error(err.message);
        }));
    }
    /**
     * @internal
     * @param deadline - Deadline
     * @param networkType - Network identifier
     * @param targetPublicKey - Target public key
     * @param namespaceId - Namespace Id
     * @param key - Metadata key
     * @param value - New metadata value
     * @param senderPublicKey - sender (signer) public key
     * @param maxFee - max fee
     * @returns {Observable<NamespaceMetadataTransaction>}
     */
    createNamespaceMetadataTransaction(deadline, networkType, targetPublicKey, namespaceId, key, value, senderPublicKey, maxFee) {
        return this.metadataHttp.getNamespaceMetadataByKeyAndSender(namespaceId, key.toHex(), senderPublicKey)
            .pipe(operators_1.map((metadata) => {
            const currentValueByte = Convert_1.Convert.utf8ToUint8(metadata.metadataEntry.value);
            const newValueBytes = Convert_1.Convert.utf8ToUint8(value);
            return NamespaceMetadataTransaction_1.NamespaceMetadataTransaction.create(deadline, targetPublicKey, key, namespaceId, newValueBytes.length - currentValueByte.length, Convert_1.Convert.decodeHex(Convert_1.Convert.xor(currentValueByte, newValueBytes)), networkType, maxFee);
        }), operators_1.catchError((err) => {
            const error = JSON.parse(err.message);
            if (error && error.statusCode && error.statusCode === 404) {
                const newValueBytes = Convert_1.Convert.utf8ToUint8(value);
                return rxjs_1.of(NamespaceMetadataTransaction_1.NamespaceMetadataTransaction.create(deadline, targetPublicKey, key, namespaceId, newValueBytes.length, value, networkType, maxFee));
            }
            throw Error(err.message);
        }));
    }
}
exports.MetadataTransactionService = MetadataTransactionService;
//# sourceMappingURL=MetadataTransactionService.js.map