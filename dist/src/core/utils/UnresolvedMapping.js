"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const Address_1 = require("../../model/account/Address");
const MosaicId_1 = require("../../model/mosaic/MosaicId");
const NamespaceId_1 = require("../../model/namespace/NamespaceId");
const Convert_1 = require("../format/Convert");
const RawAddress_1 = require("../format/RawAddress");
/**
 * @internal
 */
class UnresolvedMapping {
    /**
     * @internal
     * Map unresolved mosaic string to MosaicId or NamespaceId
     * @param {string} mosaicId The unresolvedMosaic id in hex.
     * @returns {MosaicId | NamespaceId}
     */
    static toUnresolvedMosaic(mosaicId) {
        if (!Convert_1.Convert.isHexString(mosaicId)) {
            throw new Error('Input string is not in valid hexadecimal notation.');
        }
        const bytes = Convert_1.Convert.hexToUint8(mosaicId);
        const byte0 = bytes[0];
        // if most significant bit of byte 0 is set, then we have a namespaceId
        if ((byte0 & 128) === 128) {
            return NamespaceId_1.NamespaceId.createFromEncoded(mosaicId);
        }
        // most significant bit of byte 0 is not set => mosaicId
        return new MosaicId_1.MosaicId(mosaicId);
    }
    /**
     * Map unresolved address string to Address or NamespaceId
     * @param {string} address The unresolved address in hex
     * @returns {Address | NamespaceId}
     */
    static toUnresolvedAddress(address) {
        if (!Convert_1.Convert.isHexString(address)) {
            throw new Error('Input string is not in valid hexadecimal notation.');
        }
        // If bit 0 of byte 0 is not set (like in 0x90), then it is a regular address.
        // Else (e.g. 0x91) it represents a namespace id which starts at byte 1.
        const bit0 = Convert_1.Convert.hexToUint8(address.substr(1, 2))[0];
        if ((bit0 & 16) === 16) {
            // namespaceId encoded hexadecimal notation provided
            // only 8 bytes are relevant to resolve the NamespaceId
            const relevantPart = address.substr(2, 16);
            return NamespaceId_1.NamespaceId.createFromEncoded(Convert_1.Convert.uint8ToHex(Convert_1.Convert.hexToUint8Reverse(relevantPart)));
        }
        // read address from encoded hexadecimal notation
        return Address_1.Address.createFromEncoded(address);
    }
    /**
     * Return unresolved address bytes of the unresolved address
     * @internal
     * @param {Address | NamespaceId} unresolvedAddress The unresolved address
     * @param {networkType} the network type serialized in the output.
     * @return {Uint8Array}
     */
    static toUnresolvedAddressBytes(unresolvedAddress, networkType) {
        if (unresolvedAddress instanceof NamespaceId_1.NamespaceId) {
            // received hexadecimal notation of namespaceId (alias)
            return RawAddress_1.RawAddress.aliasToRecipient(Convert_1.Convert.hexToUint8(unresolvedAddress.toHex()), networkType);
        }
        else {
            // received recipient address
            return RawAddress_1.RawAddress.stringToAddress(unresolvedAddress.plain());
        }
    }
}
exports.UnresolvedMapping = UnresolvedMapping;
//# sourceMappingURL=UnresolvedMapping.js.map