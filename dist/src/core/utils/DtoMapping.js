"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const Address_1 = require("../../model/account/Address");
const MosaicId_1 = require("../../model/mosaic/MosaicId");
const AccountRestriction_1 = require("../../model/restriction/AccountRestriction");
const AccountRestrictions_1 = require("../../model/restriction/AccountRestrictions");
const AccountRestrictionsInfo_1 = require("../../model/restriction/AccountRestrictionsInfo");
const AccountRestrictionType_1 = require("../../model/restriction/AccountRestrictionType");
class DtoMapping {
    /**
     * Create AccountRestrictionsInfo class from Json.
     * @param {object} dataJson The account restriction json object.
     * @returns {module: model/Account/AccountRestrictionsInfo} The AccountRestrictionsInfo class.
     */
    static extractAccountRestrictionFromDto(accountRestrictions) {
        return new AccountRestrictionsInfo_1.AccountRestrictionsInfo(accountRestrictions.meta, new AccountRestrictions_1.AccountRestrictions(Address_1.Address.createFromEncoded(accountRestrictions.accountRestrictions.address), accountRestrictions.accountRestrictions.restrictions.map((prop) => {
            switch (prop.restrictionFlags) {
                case AccountRestrictionType_1.AccountRestrictionFlags.AllowIncomingAddress:
                case AccountRestrictionType_1.AccountRestrictionFlags.BlockIncomingAddress:
                case AccountRestrictionType_1.AccountRestrictionFlags.AllowOutgoingAddress:
                case AccountRestrictionType_1.AccountRestrictionFlags.BlockOutgoingAddress:
                    return new AccountRestriction_1.AccountRestriction(prop.restrictionFlags, prop.values.map((value) => Address_1.Address.createFromEncoded(value)));
                case AccountRestrictionType_1.AccountRestrictionFlags.AllowMosaic:
                case AccountRestrictionType_1.AccountRestrictionFlags.BlockMosaic:
                    return new AccountRestriction_1.AccountRestriction(prop.restrictionFlags, prop.values.map((value) => new MosaicId_1.MosaicId(value)));
                case AccountRestrictionType_1.AccountRestrictionFlags.AllowIncomingTransactionType:
                case AccountRestrictionType_1.AccountRestrictionFlags.AllowOutgoingTransactionType:
                case AccountRestrictionType_1.AccountRestrictionFlags.BlockIncomingTransactionType:
                case AccountRestrictionType_1.AccountRestrictionFlags.BlockOutgoingTransactionType:
                    return new AccountRestriction_1.AccountRestriction(prop.restrictionFlags, prop.values);
                default:
                    throw new Error(`Invalid restriction type: ${prop.restrictionFlags}`);
            }
        })));
    }
}
exports.DtoMapping = DtoMapping;
//# sourceMappingURL=DtoMapping.js.map