"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const format_1 = require("../format");
const Utility = require("./Utilities");
class KeyPair {
}
/**
 * Creates a key pair from a private key string.
 * @param {string} privateKeyString A hex encoded private key string.
 * @param {SignSchema} signSchema The Sign Schema. (KECCAK(NIS1) / SHA3(Catapult))
 * @returns {module:crypto/keyPair~KeyPair} The key pair.
 */
KeyPair.createKeyPairFromPrivateKeyString = (privateKeyString, signSchema) => {
    const privateKey = format_1.Convert.hexToUint8(privateKeyString);
    if (Utility.Key_Size !== privateKey.length) {
        throw Error(`private key has unexpected size: ${privateKey.length}`);
    }
    const publicKey = Utility.catapult_crypto.extractPublicKey(privateKey, Utility.catapult_hash.func, signSchema);
    return {
        privateKey,
        publicKey,
    };
};
/**
 * Signs a data buffer with a key pair.
 * @param {module:crypto/keyPair~KeyPair} keyPair The key pair to use for signing.
 * @param {Uint8Array} data The data to sign.
 * @param {SignSchema} signSchema The Sign Schema. (KECCAK(NIS1) / SHA3(Catapult))
 * @returns {Uint8Array} The signature.
 */
KeyPair.sign = (keyPair, data, signSchema) => {
    return Utility.catapult_crypto.sign(data, keyPair.publicKey, keyPair.privateKey, Utility.catapult_hash.createHasher(64, signSchema));
};
/**
 * Verifies a signature.
 * @param {module:crypto/keyPair~PublicKey} publicKey The public key to use for verification.
 * @param {Uint8Array} data The data to verify.
 * @param {Uint8Array} signature The signature to verify.
 * @param {SignSchema} signSchema The Sign Schema. (KECCAK(NIS1) / SHA3(Catapult))
 * @returns {boolean} true if the signature is verifiable, false otherwise.
 */
KeyPair.verify = (publicKey, data, signature, signSchema) => {
    return Utility.catapult_crypto.verify(publicKey, data, signature, Utility.catapult_hash.createHasher(64, signSchema));
};
/**
 * Creates a shared key given a key pair and an arbitrary public key.
 * The shared key can be used for encrypted message passing between the two.
 * @param {module:crypto/keyPair~KeyPair} keyPair The key pair for which to create the shared key.
 * @param {Uint8Array} publicKey The public key for which to create the shared key.
 * @param {Uint8Array} salt A salt that should be applied to the shared key.
 * @param {SignSchema} signSchema The Sign Schema. (KECCAK(NIS1) / SHA3(Catapult))
 * @returns {Uint8Array} The shared key.
 */
KeyPair.deriveSharedKey = (keyPair, publicKey, salt, signSchema) => {
    if (Utility.Key_Size !== salt.length) {
        throw Error(`salt has unexpected size: ${salt.length}`);
    }
    if (Utility.Key_Size !== publicKey.length) {
        throw Error(`public key has unexpected size: ${salt.length}`);
    }
    return Utility.catapult_crypto.deriveSharedKey(salt, keyPair.privateKey, publicKey, Utility.catapult_hash.func, signSchema);
};
exports.KeyPair = KeyPair;
//# sourceMappingURL=KeyPair.js.map