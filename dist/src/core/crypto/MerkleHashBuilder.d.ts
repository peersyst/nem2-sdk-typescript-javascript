import { SignSchema } from './SignSchema';
export declare class MerkleHashBuilder {
    /**
                 * Length of produced merkle hash in bytes.
                 *
                 * @var {number}
                 */ readonly length: number;
    /**
     * Signature schema used (hash algorithm diff)
     *
     * @var {SignSchema}
     */
    readonly signSchema: SignSchema;
    /**
     * The list of hashes used to calculate root hash.
     *
     * @var {Uint8Array}
     */
    protected hashes: Uint8Array[];
    /**
     * Constructor
     * @param signSchema Sign schema
     * @param length Hash size
     */
    constructor(/**
                     * Length of produced merkle hash in bytes.
                     *
                     * @var {number}
                     */ length: number, 
        /**
         * Signature schema used (hash algorithm diff)
         *
         * @var {SignSchema}
         */
        signSchema: SignSchema);
    /**
     * Get root hash of Merkle tree
     *
     * @return {Uint8Array}
     */
    getRootHash(): Uint8Array;
    /**
     * Update hashes array (add hash)
     *
     * @param hash Inner transaction hash buffer
     * @return {MerkleHashBuilder}
     */
    update(hash: Uint8Array): MerkleHashBuilder;
}
