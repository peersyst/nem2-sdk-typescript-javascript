/**
 * [KECCAK]: Keccak hash algorithm.
 * [SHA3]: SHA3 hash algorithm without key reversal
 */
export declare enum SignSchema {
    KECCAK = 1,
    SHA3 = 2,
}
