import { MetadataEntry } from './MetadataEntry';
/**
 * A mosaic describes an instance of a mosaic definition.
 * Mosaics can be transferred by means of a transfer transaction.
 */
export declare class Metadata {
    /**
     * The metadata id
     */
    readonly id: string;
    /**
     * The metadata entry
     */
    readonly metadataEntry: MetadataEntry;
    /**
     * Constructor
     * @param id - The metadata id
     * @param metadataEntry - The metadata entry
     */
    constructor(
        /**
         * The metadata id
         */
        id: string, 
        /**
         * The metadata entry
         */
        metadataEntry: MetadataEntry);
}
