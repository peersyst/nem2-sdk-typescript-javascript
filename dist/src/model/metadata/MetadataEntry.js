"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * A mosaic describes an instance of a mosaic definition.
 * Mosaics can be transferred by means of a transfer transaction.
 */
class MetadataEntry {
    /**
     * Constructor
     * @param {string} compositeHash - The composite hash
     * @param {string} senderPublicKey - The metadata sender's public key
     * @param {string} targetPublicKey - The metadata target public key
     * @param {UInt64} scopedMetadataKey - The key scoped to source, target and type
     * @param {MetadatType} metadataType - The metadata type (Account | Mosaic | Namespace)
     * @param {string} value - The metadata value
     * @param {MosaicId | NamespaceId | undefined} targetId - The target mosaic or namespace identifier
     */
    constructor(
    /**
     * The composite hash
     */
    compositeHash, 
    /**
     * The metadata sender's public key
     */
    senderPublicKey, 
    /**
     * The metadata target public key
     */
    targetPublicKey, 
    /**
     * The key scoped to source, target and type
     */
    scopedMetadataKey, 
    /**
     * The metadata type
     */
    metadataType, 
    /**
     * The metadata value
     */
    value, 
    /**
     * The target mosaic or namespace identifier
     */
    targetId) {
        this.compositeHash = compositeHash;
        this.senderPublicKey = senderPublicKey;
        this.targetPublicKey = targetPublicKey;
        this.scopedMetadataKey = scopedMetadataKey;
        this.metadataType = metadataType;
        this.value = value;
        this.targetId = targetId;
    }
}
exports.MetadataEntry = MetadataEntry;
//# sourceMappingURL=MetadataEntry.js.map