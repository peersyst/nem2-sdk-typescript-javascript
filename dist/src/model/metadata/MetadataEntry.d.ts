import { MosaicId } from '../mosaic/MosaicId';
import { NamespaceId } from '../namespace/NamespaceId';
import { UInt64 } from '../UInt64';
import { MetadataType } from './MetadataType';
/**
 * A mosaic describes an instance of a mosaic definition.
 * Mosaics can be transferred by means of a transfer transaction.
 */
export declare class MetadataEntry {
    /**
     * The composite hash
     */
    readonly compositeHash: string;
    /**
     * The metadata sender's public key
     */
    readonly senderPublicKey: string;
    /**
     * The metadata target public key
     */
    readonly targetPublicKey: string;
    /**
     * The key scoped to source, target and type
     */
    readonly scopedMetadataKey: UInt64;
    /**
     * The metadata type
     */
    readonly metadataType: MetadataType;
    /**
     * The metadata value
     */
    readonly value: string;
    /**
     * The target mosaic or namespace identifier
     */
    readonly targetId: NamespaceId | MosaicId | undefined;
    /**
     * Constructor
     * @param {string} compositeHash - The composite hash
     * @param {string} senderPublicKey - The metadata sender's public key
     * @param {string} targetPublicKey - The metadata target public key
     * @param {UInt64} scopedMetadataKey - The key scoped to source, target and type
     * @param {MetadatType} metadataType - The metadata type (Account | Mosaic | Namespace)
     * @param {string} value - The metadata value
     * @param {MosaicId | NamespaceId | undefined} targetId - The target mosaic or namespace identifier
     */
    constructor(
        /**
         * The composite hash
         */
        compositeHash: string, 
        /**
         * The metadata sender's public key
         */
        senderPublicKey: string, 
        /**
         * The metadata target public key
         */
        targetPublicKey: string, 
        /**
         * The key scoped to source, target and type
         */
        scopedMetadataKey: UInt64, 
        /**
         * The metadata type
         */
        metadataType: MetadataType, 
        /**
         * The metadata value
         */
        value: string, 
        /**
         * The target mosaic or namespace identifier
         */
        targetId?: NamespaceId | MosaicId | undefined);
}
