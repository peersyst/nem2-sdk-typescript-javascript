export declare enum MetadataType {
    Account = 0,
    Mosaic = 1,
    Namespace = 2,
}
