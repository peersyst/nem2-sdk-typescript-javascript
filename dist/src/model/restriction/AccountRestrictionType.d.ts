export declare enum AccountRestrictionFlags {
    /**
     * Allow only incoming transactions from a given address.
     */
    AllowIncomingAddress = 1,
    /**
     * Allow only incoming transactions containing a a given mosaic identifier.
     */
    AllowMosaic = 2,
    /**
     * Allow only outgoing transactions with a given transaction type.
     */
    AllowIncomingTransactionType = 4,
    /**
     * Allow only outgoing transactions to a given address.
     */
    AllowOutgoingAddress = 16385,
    /**
     * Allow only outgoing transactions with a given transaction type.
     */
    AllowOutgoingTransactionType = 16388,
    /**
     * Block incoming transactions from a given address.
     */
    BlockIncomingAddress = 32769,
    /**
     * Block incoming transactions containing a given mosaic identifier.
     */
    BlockMosaic = 32770,
    /**
     * Block incoming transactions with a given transaction type.
     */
    BlockIncomingTransactionType = 32772,
    /**
     * Block outgoing transactions from a given address.
     */
    BlockOutgoingAddress = 49153,
    /**
     * Block outgoing transactions with a given transaction type.
     */
    BlockOutgoingTransactionType = 49156,
}
