"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var MosaicRestrictionType;
(function (MosaicRestrictionType) {
    /**
     * uninitialized value indicating no restriction
     */
    MosaicRestrictionType[MosaicRestrictionType["NONE"] = 0] = "NONE";
    /**
     * allow if equal
     */
    MosaicRestrictionType[MosaicRestrictionType["EQ"] = 1] = "EQ";
    /**
     * allow if not equal
     */
    MosaicRestrictionType[MosaicRestrictionType["NE"] = 2] = "NE";
    /**
     * allow if less than
     */
    MosaicRestrictionType[MosaicRestrictionType["LT"] = 3] = "LT";
    /**
     * allow if less than or equal
     */
    MosaicRestrictionType[MosaicRestrictionType["LE"] = 4] = "LE";
    /**
     * allow if greater than
     */
    MosaicRestrictionType[MosaicRestrictionType["GT"] = 5] = "GT";
    /**
     * allow if greater than or equal
     */
    MosaicRestrictionType[MosaicRestrictionType["GE"] = 6] = "GE";
})(MosaicRestrictionType = exports.MosaicRestrictionType || (exports.MosaicRestrictionType = {}));
//# sourceMappingURL=MosaicRestrictionType.js.map