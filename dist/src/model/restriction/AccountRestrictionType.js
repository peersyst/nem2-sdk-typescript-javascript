"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License"),
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Account restriction type
 * 0x01	Account restriction type is an address.
 * 0x02	Account restriction type is a mosaic id.
 * 0x04	Account restriction type is a transaction type.
 * 0x05	restriction type sentinel.
 * 0x40 Account restriction is interpreted as outgoing restriction.
 * 0x80 Account restriction is interpreted as blocking operation.
 */
// !!This enum will be deprecated once catbuffer code applied.
var AccountRestrictionTypeEnum;
(function (AccountRestrictionTypeEnum) {
    AccountRestrictionTypeEnum[AccountRestrictionTypeEnum["Address"] = 1] = "Address";
    AccountRestrictionTypeEnum[AccountRestrictionTypeEnum["Mosaic"] = 2] = "Mosaic";
    AccountRestrictionTypeEnum[AccountRestrictionTypeEnum["TransactionType"] = 4] = "TransactionType";
    AccountRestrictionTypeEnum[AccountRestrictionTypeEnum["Outgoing"] = 16384] = "Outgoing";
    AccountRestrictionTypeEnum[AccountRestrictionTypeEnum["Block"] = 32768] = "Block";
})(AccountRestrictionTypeEnum || (AccountRestrictionTypeEnum = {}));
var AccountRestrictionFlags;
(function (AccountRestrictionFlags) {
    /**
     * Allow only incoming transactions from a given address.
     */
    AccountRestrictionFlags[AccountRestrictionFlags["AllowIncomingAddress"] = 1] = "AllowIncomingAddress";
    /**
     * Allow only incoming transactions containing a a given mosaic identifier.
     */
    AccountRestrictionFlags[AccountRestrictionFlags["AllowMosaic"] = 2] = "AllowMosaic";
    /**
     * Allow only outgoing transactions with a given transaction type.
     */
    AccountRestrictionFlags[AccountRestrictionFlags["AllowIncomingTransactionType"] = 4] = "AllowIncomingTransactionType";
    /**
     * Allow only outgoing transactions to a given address.
     */
    AccountRestrictionFlags[AccountRestrictionFlags["AllowOutgoingAddress"] = 16385] = "AllowOutgoingAddress";
    /**
     * Allow only outgoing transactions with a given transaction type.
     */
    AccountRestrictionFlags[AccountRestrictionFlags["AllowOutgoingTransactionType"] = 16388] = "AllowOutgoingTransactionType";
    /**
     * Block incoming transactions from a given address.
     */
    AccountRestrictionFlags[AccountRestrictionFlags["BlockIncomingAddress"] = 32769] = "BlockIncomingAddress";
    /**
     * Block incoming transactions containing a given mosaic identifier.
     */
    AccountRestrictionFlags[AccountRestrictionFlags["BlockMosaic"] = 32770] = "BlockMosaic";
    /**
     * Block incoming transactions with a given transaction type.
     */
    AccountRestrictionFlags[AccountRestrictionFlags["BlockIncomingTransactionType"] = 32772] = "BlockIncomingTransactionType";
    /**
     * Block outgoing transactions from a given address.
     */
    AccountRestrictionFlags[AccountRestrictionFlags["BlockOutgoingAddress"] = 49153] = "BlockOutgoingAddress";
    /**
     * Block outgoing transactions with a given transaction type.
     */
    AccountRestrictionFlags[AccountRestrictionFlags["BlockOutgoingTransactionType"] = 49156] = "BlockOutgoingTransactionType";
})(AccountRestrictionFlags = exports.AccountRestrictionFlags || (exports.AccountRestrictionFlags = {}));
//# sourceMappingURL=AccountRestrictionType.js.map