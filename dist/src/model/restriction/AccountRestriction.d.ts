import { AccountRestrictionFlags } from './AccountRestrictionType';
/**
 * Account restriction structure describes restriction information.
 */
export declare class AccountRestriction {
    /**
     * Account restriction type
     */
    readonly restrictionFlags: AccountRestrictionFlags;
    /**
     * Restriction values.
     */
    readonly values: object[];
    /**
     * Constructor
     * @param restrictionFlags
     * @param values
     */
    constructor(
        /**
         * Account restriction type
         */
        restrictionFlags: AccountRestrictionFlags, 
        /**
         * Restriction values.
         */
        values: object[]);
}
