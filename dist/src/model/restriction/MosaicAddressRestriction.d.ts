import { Address } from '../account/Address';
import { MosaicId } from '../mosaic/MosaicId';
import { MosaicRestrictionEntryType } from './MosaicRestrictionEntryType';
/**
 * Mosaic address restriction structure describes restriction information for an mosaic.
 */
export declare class MosaicAddressRestriction {
    /**
     * composite hash
     */
    readonly compositeHash: string;
    /**
     * Mosaic restriction entry type.
     */
    readonly entryType: MosaicRestrictionEntryType;
    /**
     * Mosaic identifier.
     */
    readonly mosaicId: MosaicId;
    /**
     * Target address
     */
    readonly targetAddress: Address;
    /**
     * Mosaic restriction items
     */
    readonly restrictions: Map<string, string>;
    /**
     * Constructor
     * @param compositeHash
     * @param entryType
     * @param mosaicId
     * @param targetAddress
     * @param restrictions
     */
    constructor(
        /**
         * composite hash
         */
        compositeHash: string, 
        /**
         * Mosaic restriction entry type.
         */
        entryType: MosaicRestrictionEntryType, 
        /**
         * Mosaic identifier.
         */
        mosaicId: MosaicId, 
        /**
         * Target address
         */
        targetAddress: Address, 
        /**
         * Mosaic restriction items
         */
        restrictions: Map<string, string>);
}
