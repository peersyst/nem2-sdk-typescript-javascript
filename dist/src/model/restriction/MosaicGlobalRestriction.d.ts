import { MosaicId } from '../mosaic/MosaicId';
import { MosaicGlobalRestrictionItem } from './MosaicGlobalRestrictionItem';
import { MosaicRestrictionEntryType } from './MosaicRestrictionEntryType';
/**
 * Mosaic global restriction structure describes restriction information for an mosaic.
 */
export declare class MosaicGlobalRestriction {
    /**
     * composite hash
     */
    readonly compositeHash: string;
    /**
     * Mosaic restriction entry type.
     */
    readonly entryType: MosaicRestrictionEntryType;
    /**
     * Mosaic identifier.
     */
    readonly mosaicId: MosaicId;
    /**
     * Mosaic restriction items
     */
    readonly restrictions: Map<string, MosaicGlobalRestrictionItem>;
    /**
     * Constructor
     * @param compositeHash
     * @param entryType
     * @param mosaicId
     * @param targetAddress
     * @param restrictions
     */
    constructor(
        /**
         * composite hash
         */
        compositeHash: string, 
        /**
         * Mosaic restriction entry type.
         */
        entryType: MosaicRestrictionEntryType, 
        /**
         * Mosaic identifier.
         */
        mosaicId: MosaicId, 
        /**
         * Mosaic restriction items
         */
        restrictions: Map<string, MosaicGlobalRestrictionItem>);
}
