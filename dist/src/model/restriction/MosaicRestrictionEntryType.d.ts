export declare enum MosaicRestrictionEntryType {
    /**
     * Mosaic address restriction
     */
    ADDRESS = 0,
    /**
     * Mosaic global restriction
     */
    GLOBAL = 1,
}
