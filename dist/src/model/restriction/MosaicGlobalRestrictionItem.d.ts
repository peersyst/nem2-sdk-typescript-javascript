import { MosaicId } from '../mosaic/MosaicId';
import { MosaicRestrictionType } from './MosaicRestrictionType';
/**
 * Mosaic global restriction item structure .
 */
export declare class MosaicGlobalRestrictionItem {
    /**
     * Reference mosaic identifier
     */
    readonly referenceMosaicId: MosaicId;
    /**
     * Mosaic restriction value.
     */
    readonly restrictionValue: string;
    /**
     * Mosaic restriction type.
     */
    readonly restrictionType: MosaicRestrictionType;
    /**
     * Constructor
     * @param referenceMosaicId
     * @param restrictionValue
     * @param restrictionType
     */
    constructor(
        /**
         * Reference mosaic identifier
         */
        referenceMosaicId: MosaicId, 
        /**
         * Mosaic restriction value.
         */
        restrictionValue: string, 
        /**
         * Mosaic restriction type.
         */
        restrictionType: MosaicRestrictionType);
}
