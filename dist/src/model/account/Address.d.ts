import { NetworkType } from '../blockchain/NetworkType';
/**
 * The address structure describes an address with its network
 */
export declare class Address {
    /**
                         * The address value.
                         */ private readonly address;
    /**
     * The NEM network type.
     */
    readonly networkType: NetworkType;
    /**
     * Create from private key
     * @param publicKey - The account public key.
     * @param networkType - The NEM network type.
     * @returns {Address}
     */
    static createFromPublicKey(publicKey: string, networkType: NetworkType): Address;
    /**
     * Create an Address from a given raw address.
     * @param rawAddress - Address in string format.
     *                  ex: SB3KUBHATFCPV7UZQLWAQ2EUR6SIHBSBEOEDDDF3 or SB3KUB-HATFCP-V7UZQL-WAQ2EU-R6SIHB-SBEOED-DDF3
     * @returns {Address}
     */
    static createFromRawAddress(rawAddress: string): Address;
    /**
     * Create an Address from a given encoded address.
     * @param {string} encoded address. Expected format: 9085215E4620D383C2DF70235B9EF7607F6A28EF6D16FD7B9C.
     * @return {Address}
     */
    static createFromEncoded(encoded: string): Address;
    /**
     * Determines the validity of an raw address string.
     * @param {string} rawAddress The raw address string. Expected format SCHCZBZ6QVJAHGJTKYVPW5FBSO2IXXJQBPV5XE6P
     * @param {NetworkType} networkType The network identifier.
     * @returns {boolean} true if the raw address string is valid, false otherwise.
     */
    static isValidRawAddress: (rawAddress: string, networkType: NetworkType) => boolean;
    /**
     * Determines the validity of an encoded address string.
     * @param {string} encoded The encoded address string. Expected format: 9085215E4620D383C2DF70235B9EF7607F6A28EF6D16FD7B9C
     * @param {NetworkType} networkType The network identifier.
     * @returns {boolean} true if the encoded address string is valid, false otherwise.
     */
    static isValidEncodedAddress: (encoded: string, networkType: NetworkType) => boolean;
    /**
     * Get address in plain format ex: SB3KUBHATFCPV7UZQLWAQ2EUR6SIHBSBEOEDDDF3.
     * @returns {string}
     */
    plain(): string;
    /**
     * Get address in the encoded format ex: NAR3W7B4BCOZSZMFIZRYB3N5YGOUSWIYJCJ6HDFH.
     * @returns {string}
     */
    encoded(): string;
    /**
     * Get address in pretty format ex: SB3KUB-HATFCP-V7UZQL-WAQ2EU-R6SIHB-SBEOED-DDF3.
     * @returns {string}
     */
    pretty(): string;
    /**
     * Compares addresses for equality
     * @param address - Address
     * @returns {boolean}
     */
    equals(address: Address): boolean;
    /**
     * Create DTO object
     */
    toDTO(): {
        address: string;
        networkType: NetworkType;
    };
}
