"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const PublicAccount_1 = require("./PublicAccount");
/**
 * The account info structure describes basic information for an account.
 */
class AccountInfo {
    /**
     *
     */
    constructor(
    /**
     * Address of the account.
     */
    address, 
    /**
     * Height when the address was published.
     */
    addressHeight, 
    /**
     * Public key of the account.
     */
    publicKey, 
    /**
     * Height when the public key was published.
     */
    publicKeyHeight, 
    /**
     * Account type
     */
    accountType, 
    /**
     * Linked account key
     */
    linkedAccountKey, 
    /**
     * Account activity bucket
     */
    activityBucket, 
    /**
     * Mosaics hold by the account.
     */
    mosaics, 
    /**
     * Importance of the account.
     */
    importance, 
    /**
     * Importance height of the account.
     */
    importanceHeight) {
        this.address = address;
        this.addressHeight = addressHeight;
        this.publicKey = publicKey;
        this.publicKeyHeight = publicKeyHeight;
        this.accountType = accountType;
        this.linkedAccountKey = linkedAccountKey;
        this.activityBucket = activityBucket;
        this.mosaics = mosaics;
        this.importance = importance;
        this.importanceHeight = importanceHeight;
    }
    /**
     * Returns account public account.
     * @returns {PublicAccount}
     */
    get publicAccount() {
        return PublicAccount_1.PublicAccount.createFromPublicKey(this.publicKey, this.address.networkType);
    }
}
exports.AccountInfo = AccountInfo;
//# sourceMappingURL=AccountInfo.js.map