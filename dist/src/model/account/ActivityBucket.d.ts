/**
 * Account activity bucket.
 */
export declare class ActivityBucket {
    /**
     * Start height
     */
    readonly startHeight: string;
    /**
     * Total fees paid.
     */
    readonly totalFeesPaid: number;
    /**
     * Beneficiary count.
     */
    readonly beneficiaryCount: number;
    /**
     * Raw score.
     */
    readonly rawScore: number;
    /**
     * Constructor
     * @param meta
     * @param accountRestrictions
     */
    constructor(
        /**
         * Start height
         */
        startHeight: string, 
        /**
         * Total fees paid.
         */
        totalFeesPaid: number, 
        /**
         * Beneficiary count.
         */
        beneficiaryCount: number, 
        /**
         * Raw score.
         */
        rawScore: number);
}
