"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const GeneratorUtils_1 = require("../../infrastructure/catbuffer/GeneratorUtils");
const UInt64_1 = require("../UInt64");
const Receipt_1 = require("./Receipt");
const ReceiptVersion_1 = require("./ReceiptVersion");
/**
 * Balance Transfer: A mosaic transfer was triggered.
 */
class InflationReceipt extends Receipt_1.Receipt {
    /**
     * Balance transfer expiry receipt
     * @param mosaicId - The mosaic id.
     * @param amount - The amount of mosaic.
     * @param version - The receipt version
     * @param type - The receipt type
     * @param size - the receipt size
     */
    constructor(
    /**
     * The mosaic id.
     */
    mosaicId, 
    /**
     * The amount of mosaic.
     */
    amount, version, type, size) {
        super(version, type, size);
        this.mosaicId = mosaicId;
        this.amount = amount;
    }
    /**
     * @internal
     * Generate buffer
     * @return {Uint8Array}
     */
    serialize() {
        const buffer = new Uint8Array(20);
        buffer.set(GeneratorUtils_1.GeneratorUtils.uintToBuffer(ReceiptVersion_1.ReceiptVersion.INFLATION_RECEIPT, 2));
        buffer.set(GeneratorUtils_1.GeneratorUtils.uintToBuffer(this.type, 2), 2);
        buffer.set(GeneratorUtils_1.GeneratorUtils.uint64ToBuffer(UInt64_1.UInt64.fromHex(this.mosaicId.toHex()).toDTO()), 4);
        buffer.set(GeneratorUtils_1.GeneratorUtils.uint64ToBuffer(UInt64_1.UInt64.fromHex(this.amount.toHex()).toDTO()), 12);
        return buffer;
    }
}
exports.InflationReceipt = InflationReceipt;
//# sourceMappingURL=InflationReceipt.js.map