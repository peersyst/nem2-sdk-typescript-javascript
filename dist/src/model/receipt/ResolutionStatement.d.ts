import { Address } from '../account/Address';
import { NetworkType } from '../blockchain/NetworkType';
import { MosaicId } from '../mosaic/MosaicId';
import { NamespaceId } from '../namespace/NamespaceId';
import { UInt64 } from '../UInt64';
import { ResolutionEntry } from './ResolutionEntry';
import { ResolutionType } from './ResolutionType';
/**
 * When a transaction includes an alias, a so called resolution statement reflects the resolved value for that block:
 * - Address Resolution: An account alias was used in the block.
 * - Mosaic Resolution: A mosaic alias was used in the block.
 */
export declare class ResolutionStatement {
    /**
     * Resolution type
     */
    readonly resolutionType: ResolutionType;
    /**
     * The block height.
     */
    readonly height: UInt64;
    /**
     * An unresolved address or unresolved mosaicId.
     */
    readonly unresolved: Address | MosaicId | NamespaceId;
    /**
     * The array of resolution entries.
     */
    readonly resolutionEntries: ResolutionEntry[];
    /**
     * Receipt - resolution statement object
     * @param resolutionType - The resolution type
     * @param height - The block height
     * @param unresolved - An unresolved address or unresolved mosaicId.
     * @param resolutionEntries - The array of resolution entries.
     */
    constructor(
        /**
         * Resolution type
         */
        resolutionType: ResolutionType, 
        /**
         * The block height.
         */
        height: UInt64, 
        /**
         * An unresolved address or unresolved mosaicId.
         */
        unresolved: Address | MosaicId | NamespaceId, 
        /**
         * The array of resolution entries.
         */
        resolutionEntries: ResolutionEntry[]);
    /**
     * Generate receipt hash
     * @param {networkType} the network type serialized in the output.
     * @return {string} receipt hash in hex
     */
    generateHash(networkType: NetworkType): string;
    /**
     * Get most `recent` available secondary id by a given primaryId
     * @param primaryId Primary source id
     * @param secondaryId Secondary source id
     * @returns {number}
     */
    private getMaxSecondaryIdByPrimaryIdAndSecondaryId(primaryId, secondaryId);
}
