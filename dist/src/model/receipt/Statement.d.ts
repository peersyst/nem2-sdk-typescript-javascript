import { Address } from '../account/Address';
import { Mosaic } from '../mosaic/Mosaic';
import { MosaicId } from '../mosaic/MosaicId';
import { NamespaceId } from '../namespace/NamespaceId';
import { ResolutionStatement } from './ResolutionStatement';
import { TransactionStatement } from './TransactionStatement';
export declare class Statement {
    /**
     * The transaction statements.
     */
    readonly transactionStatements: TransactionStatement[];
    /**
     * The address resolution statements.
     */
    readonly addressResolutionStatements: ResolutionStatement[];
    /**
     * The mosaic resolution statements.
     */
    readonly mosaicResolutionStatements: ResolutionStatement[];
    /**
     * Receipt - transaction statement object
     * @param transactionStatements - The transaction statements.
     * @param addressResolutionStatements - The address resolution statements.
     * @param mosaicResolutionStatements - The mosaic resolution statements.
     */
    constructor(
        /**
         * The transaction statements.
         */
        transactionStatements: TransactionStatement[], 
        /**
         * The address resolution statements.
         */
        addressResolutionStatements: ResolutionStatement[], 
        /**
         * The mosaic resolution statements.
         */
        mosaicResolutionStatements: ResolutionStatement[]);
    /**
     * Resolve unresolvedAddress from statement
     * @param unresolvedAddress Unresolved address
     * @param height Block height
     * @param transactionIndex Transaction index
     * @param aggregateTransactionIndex Aggregate transaction index
     * @returns {Address}
     */
    resolveAddress(unresolvedAddress: Address | NamespaceId, height: string, transactionIndex: number, aggregateTransactionIndex?: number): Address;
    /**
     * Resolve unresolvedMosaicId from statement
     * @param unresolvedMosaicId Unresolved mosaic id
     * @param height Block height
     * @param transactionIndex Transaction index
     * @param aggregateTransactionIndex Aggregate transaction index
     * @returns {MosaicId}
     */
    resolveMosaicId(unresolvedMosaicId: MosaicId | NamespaceId, height: string, transactionIndex: number, aggregateTransactionIndex?: number): MosaicId;
    /**
     * Resolve unresolvedMosaic from statement
     * @param unresolvedMosaic Unresolved mosaic
     * @param height Block height
     * @param transactionIndex Transaction index
     * @param aggregateTransactionIndex Aggregate transaction index
     * @returns {Mosaic}
     */
    resolveMosaic(unresolvedMosaic: Mosaic, height: string, transactionIndex: number, aggregateTransactionIndex?: number): Mosaic;
}
