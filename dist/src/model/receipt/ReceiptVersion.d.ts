/**
 * Receipt version constants.
 *
 * @see https://github.com/nemtech/catapult-server/blob/master/src/catapult/model/ReceiptType.h
 * @see https://github.com/nemtech/catapult-server/blob/master/src/catapult/model/ReceiptType.cpp
 */
export declare class ReceiptVersion {
    /**
     * Balance transfer receipt version.
     * @type {number}
     */
    static readonly BALANCE_TRANSFER: number;
    /**
     * Balance change receipt version
     * @type {number}
     */
    static readonly BALANCE_CHANGE: number;
    /**
     * Artifact expiry receipt version
     * @type {number}
     */
    static readonly ARTIFACT_EXPIRY: number;
    /**
     * Transaction statement version
     * @type {number}
     */
    static readonly TRANSACTION_STATEMENT: number;
    /**
     * Resolution statement version
     * @type {number}
     */
    static readonly RESOLUTION_STATEMENT: number;
    /**
     * Resolution statement version
     * @type {number}
     */
    static readonly INFLATION_RECEIPT: number;
}
