import { Address } from '../account/Address';
import { MosaicId } from '../mosaic/MosaicId';
import { ReceiptSource } from './ReceiptSource';
/**
 * The receipt source object.
 */
export declare class ResolutionEntry {
    /**
     * A resolved address or resolved mosaicId (alias).
     */
    readonly resolved: Address | MosaicId;
    /**
     * The receipt source.
     */
    readonly source: ReceiptSource;
    /**
     * @constructor
     * @param resolved - A resolved address or resolved mosaicId (alias).
     * @param source - The receipt source.
     */
    constructor(
        /**
         * A resolved address or resolved mosaicId (alias).
         */
        resolved: Address | MosaicId, 
        /**
         * The receipt source.
         */
        source: ReceiptSource);
}
