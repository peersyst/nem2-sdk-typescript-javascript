"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const Mosaic_1 = require("../mosaic/Mosaic");
const NamespaceId_1 = require("../namespace/NamespaceId");
const ResolutionType_1 = require("./ResolutionType");
class Statement {
    /**
     * Receipt - transaction statement object
     * @param transactionStatements - The transaction statements.
     * @param addressResolutionStatements - The address resolution statements.
     * @param mosaicResolutionStatements - The mosaic resolution statements.
     */
    constructor(
    /**
     * The transaction statements.
     */
    transactionStatements, 
    /**
     * The address resolution statements.
     */
    addressResolutionStatements, 
    /**
     * The mosaic resolution statements.
     */
    mosaicResolutionStatements) {
        this.transactionStatements = transactionStatements;
        this.addressResolutionStatements = addressResolutionStatements;
        this.mosaicResolutionStatements = mosaicResolutionStatements;
    }
    /**
     * Resolve unresolvedAddress from statement
     * @param unresolvedAddress Unresolved address
     * @param height Block height
     * @param transactionIndex Transaction index
     * @param aggregateTransactionIndex Aggregate transaction index
     * @returns {Address}
     */
    resolveAddress(unresolvedAddress, height, transactionIndex, aggregateTransactionIndex = 0) {
        return unresolvedAddress instanceof NamespaceId_1.NamespaceId ?
            this.getResolvedFromReceipt(ResolutionType_1.ResolutionType.Address, unresolvedAddress, transactionIndex, height, aggregateTransactionIndex) :
            unresolvedAddress;
    }
    /**
     * Resolve unresolvedMosaicId from statement
     * @param unresolvedMosaicId Unresolved mosaic id
     * @param height Block height
     * @param transactionIndex Transaction index
     * @param aggregateTransactionIndex Aggregate transaction index
     * @returns {MosaicId}
     */
    resolveMosaicId(unresolvedMosaicId, height, transactionIndex, aggregateTransactionIndex = 0) {
        return unresolvedMosaicId instanceof NamespaceId_1.NamespaceId ?
            this.getResolvedFromReceipt(ResolutionType_1.ResolutionType.Mosaic, unresolvedMosaicId, transactionIndex, height, aggregateTransactionIndex) :
            unresolvedMosaicId;
    }
    /**
     * Resolve unresolvedMosaic from statement
     * @param unresolvedMosaic Unresolved mosaic
     * @param height Block height
     * @param transactionIndex Transaction index
     * @param aggregateTransactionIndex Aggregate transaction index
     * @returns {Mosaic}
     */
    resolveMosaic(unresolvedMosaic, height, transactionIndex, aggregateTransactionIndex = 0) {
        return unresolvedMosaic.id instanceof NamespaceId_1.NamespaceId ?
            new Mosaic_1.Mosaic(this.getResolvedFromReceipt(ResolutionType_1.ResolutionType.Mosaic, unresolvedMosaic.id, transactionIndex, height, aggregateTransactionIndex), unresolvedMosaic.amount) :
            unresolvedMosaic;
    }
    /**
     * @internal
     * Extract resolved address | mosaic from block receipt
     * @param resolutionType Resolution type: Address / Mosaic
     * @param unresolved Unresolved address / mosaicId
     * @param transactionIndex Transaction index
     * @param height Transaction height
     * @param aggregateTransactionIndex Transaction index for aggregate
     * @returns {MosaicId | Address}
     */
    getResolvedFromReceipt(resolutionType, unresolved, transactionIndex, height, aggregateTransactionIndex) {
        const resolutionStatement = (resolutionType === ResolutionType_1.ResolutionType.Address ? this.addressResolutionStatements :
            this.mosaicResolutionStatements).find((resolution) => resolution.height.toString() === height &&
            resolution.unresolved.equals(unresolved));
        if (!resolutionStatement) {
            throw new Error(`No resolution statement found on block: ${height} for unresolved: ${unresolved.toHex()}`);
        }
        // If only one entry exists on the statement, just return
        if (resolutionStatement.resolutionEntries.length === 1) {
            return resolutionStatement.resolutionEntries[0].resolved;
        }
        // Get the most recent resolution entry
        const resolutionEntry = resolutionStatement.getResolutionEntryById(aggregateTransactionIndex !== undefined ? aggregateTransactionIndex + 1 : transactionIndex + 1, aggregateTransactionIndex !== undefined ? transactionIndex + 1 : 0);
        if (!resolutionEntry) {
            throw new Error(`No resolution entry found on block: ${height} for unresolved: ${unresolved.toHex()}`);
        }
        return resolutionEntry.resolved;
    }
}
exports.Statement = Statement;
//# sourceMappingURL=Statement.js.map