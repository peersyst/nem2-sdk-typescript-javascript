"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * The mosaic info structure describes a mosaic.
 */
class MosaicInfo {
    /**
     * @param active
     * @param index
     * @param nonce
     * @param supply
     * @param height
     * @param owner
     * @param properties
     */
    constructor(
    /**
     * The mosaic id.
     */
    id, 
    /**
     * The mosaic supply.
     */
    supply, 
    /**
     * The block height were mosaic was created.
     */
    height, 
    /**
     * The public key of the mosaic creator.
     */
    owner, 
    /**
     * The mosaic revision
     */
    revision, 
    /**
     * The mosaic flags.
     */
    flags, 
    /**
     * Mosaic divisibility
     */
    divisibility, 
    /**
     * Mosaic duration
     */
    duration) {
        this.id = id;
        this.supply = supply;
        this.height = height;
        this.owner = owner;
        this.revision = revision;
        this.flags = flags;
        this.divisibility = divisibility;
        this.duration = duration;
    }
    /**
     * Is mosaic supply mutable
     * @returns {boolean}
     */
    isSupplyMutable() {
        return this.flags.supplyMutable;
    }
    /**
     * Is mosaic transferable
     * @returns {boolean}
     */
    isTransferable() {
        return this.flags.transferable;
    }
    /**
     * Is mosaic restrictable
     * @returns {boolean}
     */
    isRestrictable() {
        return this.flags.restrictable;
    }
}
exports.MosaicInfo = MosaicInfo;
//# sourceMappingURL=MosaicInfo.js.map