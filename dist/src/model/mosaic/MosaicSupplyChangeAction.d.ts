/**
 * The supply type. Supported supply types are:
 * 0: Increase in supply.
 * 1: Decrease in supply.
 */
export declare enum MosaicSupplyChangeAction {
    Decrease = 0,
    Increase = 1,
}
