/**
 * Mosaic flags model
 */
export declare class MosaicFlags {
    /**
     * The creator can choose between a definition that allows a mosaic supply change at a later point or an immutable supply.
     * Allowed values for the property are "true" and "false". The default value is "false".
     */
    readonly supplyMutable: boolean;
    /**
     * The creator can choose if the mosaic definition should allow for transfers of the mosaic among accounts other than the creator.
     * If the property 'transferable' is set to "false", only transfer transactions
     * having the creator as sender or as recipient can transfer mosaics of that type.
     * If set to "true" the mosaics can be transferred to and from arbitrary accounts.
     * Allowed values for the property are thus "true" and "false". The default value is "true".
     */
    readonly transferable: boolean;
    /**
     * Not all the mosaics of a given network will be subject to mosaic restrictions. The feature will only affect
     * those to which the issuer adds the "restrictable" property explicitly at the moment of its creation. This
     * property appears disabled by default, as it is undesirable for autonomous tokens like the public network currency.
     */
    readonly restrictable: boolean;
    /**
     * @param flags
     * @param divisibility
     * @param duration
     */
    constructor(flags: number);
    /**
     * Static constructor function with default parameters
     * @returns {MosaicFlags}
     * @param supplyMutable
     * @param transferable
     * @param restrictable
     */
    static create(supplyMutable: boolean, transferable: boolean, restrictable?: boolean): MosaicFlags;
    /**
     * Get mosaic flag value in number
     * @returns {number}
     */
    getValue(): number;
    /**
     * Create DTO object
     */
    toDTO(): {
        flags: number;
    };
}
