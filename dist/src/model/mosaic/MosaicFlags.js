"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Mosaic flags model
 */
class MosaicFlags {
    /**
     * @param flags
     * @param divisibility
     * @param duration
     */
    constructor(flags) {
        let binaryFlags = '00' + (flags >>> 0).toString(2);
        binaryFlags = binaryFlags.substr(binaryFlags.length - 3, 3);
        this.supplyMutable = binaryFlags[2] === '1';
        this.transferable = binaryFlags[1] === '1';
        this.restrictable = binaryFlags[0] === '1';
    }
    /**
     * Static constructor function with default parameters
     * @returns {MosaicFlags}
     * @param supplyMutable
     * @param transferable
     * @param restrictable
     */
    static create(supplyMutable, transferable, restrictable = false) {
        const flags = (supplyMutable ? 1 : 0) + (transferable ? 2 : 0) + (restrictable ? 4 : 0);
        return new MosaicFlags(flags);
    }
    /**
     * Get mosaic flag value in number
     * @returns {number}
     */
    getValue() {
        return (this.supplyMutable ? 1 : 0) + (this.transferable ? 2 : 0) + (this.restrictable ? 4 : 0);
    }
    /**
     * Create DTO object
     */
    toDTO() {
        return {
            flags: this.getValue(),
        };
    }
}
exports.MosaicFlags = MosaicFlags;
//# sourceMappingURL=MosaicFlags.js.map