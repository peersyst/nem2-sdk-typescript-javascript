import { PublicAccount } from '../account/PublicAccount';
import { UInt64 } from '../UInt64';
import { MosaicFlags } from './MosaicFlags';
import { MosaicId } from './MosaicId';
/**
 * The mosaic info structure describes a mosaic.
 */
export declare class MosaicInfo {
    /**
     * The mosaic id.
     */
    readonly id: MosaicId;
    /**
     * The mosaic supply.
     */
    readonly supply: UInt64;
    /**
     * The block height were mosaic was created.
     */
    readonly height: UInt64;
    /**
     * The public key of the mosaic creator.
     */
    readonly owner: PublicAccount;
    /**
     * The mosaic revision
     */
    readonly revision: number;
    /**
     * The mosaic flags.
     */
    readonly flags: MosaicFlags;
    /**
     * Mosaic divisibility
     */
    readonly divisibility: number;
    /**
     * Mosaic duration
     */
    readonly duration: UInt64;
    /**
     * @param active
     * @param index
     * @param nonce
     * @param supply
     * @param height
     * @param owner
     * @param properties
     */
    constructor(
        /**
         * The mosaic id.
         */
        id: MosaicId, 
        /**
         * The mosaic supply.
         */
        supply: UInt64, 
        /**
         * The block height were mosaic was created.
         */
        height: UInt64, 
        /**
         * The public key of the mosaic creator.
         */
        owner: PublicAccount, 
        /**
         * The mosaic revision
         */
        revision: number, 
        /**
         * The mosaic flags.
         */
        flags: MosaicFlags, 
        /**
         * Mosaic divisibility
         */
        divisibility: number, 
        /**
         * Mosaic duration
         */
        duration: UInt64);
    /**
     * Is mosaic supply mutable
     * @returns {boolean}
     */
    isSupplyMutable(): boolean;
    /**
     * Is mosaic transferable
     * @returns {boolean}
     */
    isTransferable(): boolean;
    /**
     * Is mosaic restrictable
     * @returns {boolean}
     */
    isRestrictable(): boolean;
}
