/**
 * Static class containing network type constants.
 */
export declare enum NetworkType {
    /**
     * Main net network
     * @type {number}
     */
    MAIN_NET = 104,
    /**
     * Test net network
     * @type {number}
     */
    TEST_NET = 152,
    /**
     * Mijin net network
     * @type {number}
     */
    MIJIN = 96,
    /**
     * Mijin test net network
     * @type {number}
     */
    MIJIN_TEST = 144,
}
