import { NetworkType } from '../blockchain/NetworkType';
import { Message } from './Message';
export declare class PersistentHarvestingDelegationMessage extends Message {
    constructor(payload: string);
    /**
     *
     * @param delegatedPrivateKey - Private key of delegated account
     * @param senderPrivateKey - Sender private key
     * @param recipientPublicKey - Recipient public key
     * @param {NetworkType} networkType - Catapult network type
     * @return {PersistentHarvestingDelegationMessage}
     */
    static create(delegatedPrivateKey: string, senderPrivateKey: string, recipientPublicKey: string, networkType: NetworkType): PersistentHarvestingDelegationMessage;
    /**
     * Create PersistentHarvestingDelegationMessage from DTO payload
     * @param payload
     */
    static createFromPayload(payload: string): PersistentHarvestingDelegationMessage;
    /**
     *
     * @param encryptMessage - Encrypted message to be decrypted
     * @param privateKey - Recipient private key
     * @param senderPublicKey - Sender public key
     * @param {NetworkType} networkType - Catapult network type
     * @return {string}
     */
    static decrypt(encryptMessage: PersistentHarvestingDelegationMessage, privateKey: string, senderPublicKey: string, networkType: NetworkType): string;
}
