import { PublicAccount } from '../account/PublicAccount';
import { NetworkType } from '../blockchain/NetworkType';
import { Message } from './Message';
import { PlainMessage } from './PlainMessage';
/**
 * Encrypted Message model
 */
export declare class EncryptedMessage extends Message {
    readonly recipientPublicAccount?: PublicAccount;
    constructor(payload: string, recipientPublicAccount?: PublicAccount);
    /**
     *
     * @param message - Plain message to be encrypted
     * @param recipientPublicAccount - Recipient public account
     * @param privateKey - Sender private key
     * @param {NetworkType} networkType - Catapult network type
     * @return {EncryptedMessage}
     */
    static create(message: string, recipientPublicAccount: PublicAccount, privateKey: string, networkType: NetworkType): EncryptedMessage;
    /**
     *
     * @param payload
     */
    static createFromPayload(payload: string): EncryptedMessage;
    /**
     *
     * @param encryptMessage - Encrypted message to be decrypted
     * @param privateKey - Recipient private key
     * @param recipientPublicAccount - Sender public account
     * @param {NetworkType} networkType - Catapult network type
     * @return {PlainMessage}
     */
    static decrypt(encryptMessage: EncryptedMessage, privateKey: any, recipientPublicAccount: PublicAccount, networkType: NetworkType): PlainMessage;
}
