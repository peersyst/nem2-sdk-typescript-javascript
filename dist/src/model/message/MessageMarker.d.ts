export declare class MessageMarker {
    /**
     * 8-byte marker: FE CC 71 C7 64 BF E5 98 for PersistentDelegationRequestTransaction message
     */
    static readonly PersistentDelegationUnlock: string;
}
