"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const crypto_1 = require("../../core/crypto");
const Convert_1 = require("../../core/format/Convert");
const Message_1 = require("./Message");
const MessageMarker_1 = require("./MessageMarker");
const MessageType_1 = require("./MessageType");
class PersistentHarvestingDelegationMessage extends Message_1.Message {
    constructor(payload) {
        super(MessageType_1.MessageType.PersistentHarvestingDelegationMessage, payload);
        if (!Convert_1.Convert.isHexString(payload)) {
            throw Error('Payload format is not valid hexadecimal string');
        }
    }
    /**
     *
     * @param delegatedPrivateKey - Private key of delegated account
     * @param senderPrivateKey - Sender private key
     * @param recipientPublicKey - Recipient public key
     * @param {NetworkType} networkType - Catapult network type
     * @return {PersistentHarvestingDelegationMessage}
     */
    static create(delegatedPrivateKey, senderPrivateKey, recipientPublicKey, networkType) {
        const signSchema = crypto_1.SHA3Hasher.resolveSignSchema(networkType);
        const encrypted = MessageMarker_1.MessageMarker.PersistentDelegationUnlock +
            crypto_1.Crypto.encode(senderPrivateKey, recipientPublicKey, delegatedPrivateKey, signSchema, true).toUpperCase();
        return new PersistentHarvestingDelegationMessage(encrypted);
    }
    /**
     * Create PersistentHarvestingDelegationMessage from DTO payload
     * @param payload
     */
    static createFromPayload(payload) {
        const msgTypeHex = MessageType_1.MessageType.PersistentHarvestingDelegationMessage.toString(16).toUpperCase();
        return new PersistentHarvestingDelegationMessage(msgTypeHex + payload.toUpperCase());
    }
    /**
     *
     * @param encryptMessage - Encrypted message to be decrypted
     * @param privateKey - Recipient private key
     * @param senderPublicKey - Sender public key
     * @param {NetworkType} networkType - Catapult network type
     * @return {string}
     */
    static decrypt(encryptMessage, privateKey, senderPublicKey, networkType) {
        const signSchema = crypto_1.SHA3Hasher.resolveSignSchema(networkType);
        const payload = encryptMessage.payload.substring(MessageMarker_1.MessageMarker.PersistentDelegationUnlock.length);
        const decrypted = crypto_1.Crypto.decode(privateKey, senderPublicKey, payload, signSchema);
        return decrypted.toUpperCase();
    }
}
exports.PersistentHarvestingDelegationMessage = PersistentHarvestingDelegationMessage;
//# sourceMappingURL=PersistentHarvestingDelegationMessage.js.map