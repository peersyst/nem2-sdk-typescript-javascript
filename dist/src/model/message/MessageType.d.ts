/**
 * The Message type. Supported supply types are:
 * 0: PlainMessage
 * 1: EncryptedMessage.
 * 254: Persistent harvesting delegation.
 */
export declare enum MessageType {
    PlainMessage = 0,
    EncryptedMessage = 1,
    PersistentHarvestingDelegationMessage = 254,
}
