"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Alias_1 = require("./Alias");
const AliasType_1 = require("./AliasType");
/**
 * The MosaicAlias structure describe mosaic aliases
 *
 * @since 0.10.2
 */
class MosaicAlias extends Alias_1.Alias {
    /**
     * Create AddressAlias object
     * @param mosaicId
     */
    constructor(/**
                 * The alias address
                 */ mosaicId) {
        super(AliasType_1.AliasType.Mosaic, undefined, mosaicId);
        this.mosaicId = mosaicId;
    }
    /**
     * Compares AddressAlias for equality.
     *
     * @return boolean
     */
    equals(alias) {
        if (alias instanceof MosaicAlias) {
            return this.mosaicId.equals(alias.mosaicId);
        }
        return false;
    }
    /**
     * Get string value of mosaicId
     * @returns {string}
     */
    toHex() {
        return this.mosaicId.toHex();
    }
}
exports.MosaicAlias = MosaicAlias;
//# sourceMappingURL=MosaicAlias.js.map