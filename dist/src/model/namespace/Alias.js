"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Abtract class for Aliases
 *
 * @since 0.10.2
 */
class Alias {
    /**
     * @internal
     * @param type - Alias type
     * @param address - Address for AddressAlias
     * @param mosaicId - MosaicId for MosaicAlias
     */
    constructor(
    /**
     * The alias type
     *
     * - 0 : No alias
     * - 1 : Mosaic id alias
     * - 2 : Address alias
     */
    type, 
    /**
     * The alias address
     */
    address, 
    /**
     * The alias mosaicId
     */
    mosaicId) {
        this.type = type;
        this.address = address;
        this.mosaicId = mosaicId;
    }
}
exports.Alias = Alias;
//# sourceMappingURL=Alias.js.map