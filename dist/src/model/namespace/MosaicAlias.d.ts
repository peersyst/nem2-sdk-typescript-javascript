import { MosaicId } from '../mosaic/MosaicId';
import { Alias } from './Alias';
/**
 * The MosaicAlias structure describe mosaic aliases
 *
 * @since 0.10.2
 */
export declare class MosaicAlias extends Alias {
    /**
                 * The alias address
                 */ readonly mosaicId: MosaicId;
    /**
     * Create AddressAlias object
     * @param mosaicId
     */
    constructor(/**
                     * The alias address
                     */ mosaicId: MosaicId);
    /**
     * Compares AddressAlias for equality.
     *
     * @return boolean
     */
    equals(alias: any): boolean;
    /**
     * Get string value of mosaicId
     * @returns {string}
     */
    toHex(): string;
}
