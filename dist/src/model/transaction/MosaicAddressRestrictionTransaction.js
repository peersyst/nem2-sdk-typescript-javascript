"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const format_1 = require("../../core/format");
const UnresolvedMapping_1 = require("../../core/utils/UnresolvedMapping");
const AmountDto_1 = require("../../infrastructure/catbuffer/AmountDto");
const EmbeddedMosaicAddressRestrictionTransactionBuilder_1 = require("../../infrastructure/catbuffer/EmbeddedMosaicAddressRestrictionTransactionBuilder");
const KeyDto_1 = require("../../infrastructure/catbuffer/KeyDto");
const MosaicAddressRestrictionTransactionBuilder_1 = require("../../infrastructure/catbuffer/MosaicAddressRestrictionTransactionBuilder");
const SignatureDto_1 = require("../../infrastructure/catbuffer/SignatureDto");
const TimestampDto_1 = require("../../infrastructure/catbuffer/TimestampDto");
const UnresolvedAddressDto_1 = require("../../infrastructure/catbuffer/UnresolvedAddressDto");
const UnresolvedMosaicIdDto_1 = require("../../infrastructure/catbuffer/UnresolvedMosaicIdDto");
const PublicAccount_1 = require("../account/PublicAccount");
const NamespaceId_1 = require("../namespace/NamespaceId");
const UInt64_1 = require("../UInt64");
const Deadline_1 = require("./Deadline");
const Transaction_1 = require("./Transaction");
const TransactionType_1 = require("./TransactionType");
const TransactionVersion_1 = require("./TransactionVersion");
class MosaicAddressRestrictionTransaction extends Transaction_1.Transaction {
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param maxFee
     * @param mosaicId
     * @param signature
     * @param restrictionKey
     * @param targetAddress
     * @param previousRestrictionValue
     * @param newRestrictionValue
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    constructor(networkType, version, deadline, maxFee, 
    /**
     * The mosaic id.
     */
    mosaicId, 
    /**
     * The restriction key.
     */
    restrictionKey, 
    /**
     * The affected unresolved address.
     */
    targetAddress, 
    /**
     * The previous restriction value.
     */
    previousRestrictionValue, 
    /**
     * The new restriction value.
     */
    newRestrictionValue, signature, signer, transactionInfo) {
        super(TransactionType_1.TransactionType.MOSAIC_ADDRESS_RESTRICTION, networkType, version, deadline, maxFee, signature, signer, transactionInfo);
        this.mosaicId = mosaicId;
        this.restrictionKey = restrictionKey;
        this.targetAddress = targetAddress;
        this.previousRestrictionValue = previousRestrictionValue;
        this.newRestrictionValue = newRestrictionValue;
    }
    /**
     * Create a mosaic address restriction transaction object
     *
     * Enabling accounts to transact with the token is similar to the process of
     * adding elevated permissions to a user in a company computer network.
     *
     * The mosaic creator can modify the permissions of an account by sending a
     * mosaic restriction transaction targeting the account address.
     *
     * **MosaicAddressRestrictionTransaction can only be announced in with Aggregate Transaction
     *
     * @param deadline - The deadline to include the transaction.
     * @param mosaicId - The unresolved mosaic identifier.
     * @param restrictionKey - The restriction key.
     * @param targetAddress - The affected unresolved address.
     * @param newRestrictionValue - The new restriction value.
     * @param networkType - The network type.
     * @param previousRestrictionValue - (Optional) The previous restriction value.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {MosaicAddressRestrictionTransaction}
     */
    static create(deadline, mosaicId, restrictionKey, targetAddress, newRestrictionValue, networkType, previousRestrictionValue = UInt64_1.UInt64.fromHex('FFFFFFFFFFFFFFFF'), maxFee = new UInt64_1.UInt64([0, 0])) {
        return new MosaicAddressRestrictionTransaction(networkType, TransactionVersion_1.TransactionVersion.MOSAIC_ADDRESS_RESTRICTION, deadline, maxFee, mosaicId, restrictionKey, targetAddress, previousRestrictionValue, newRestrictionValue);
    }
    /**
     * Create a transaction object from payload
     * @param {string} payload Binary payload
     * @param {Boolean} isEmbedded Is embedded transaction (Default: false)
     * @returns {Transaction | InnerTransaction}
     */
    static createFromPayload(payload, isEmbedded = false) {
        const builder = isEmbedded ? EmbeddedMosaicAddressRestrictionTransactionBuilder_1.EmbeddedMosaicAddressRestrictionTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload)) :
            MosaicAddressRestrictionTransactionBuilder_1.MosaicAddressRestrictionTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload));
        const signerPublicKey = format_1.Convert.uint8ToHex(builder.getSignerPublicKey().key);
        const networkType = builder.getNetwork().valueOf();
        const transaction = MosaicAddressRestrictionTransaction.create(isEmbedded ? Deadline_1.Deadline.create() : Deadline_1.Deadline.createFromDTO(builder.getDeadline().timestamp), UnresolvedMapping_1.UnresolvedMapping.toUnresolvedMosaic(new UInt64_1.UInt64(builder.getMosaicId().unresolvedMosaicId).toHex()), new UInt64_1.UInt64(builder.getRestrictionKey()), UnresolvedMapping_1.UnresolvedMapping.toUnresolvedAddress(format_1.Convert.uint8ToHex(builder.getTargetAddress().unresolvedAddress)), new UInt64_1.UInt64(builder.getNewRestrictionValue()), networkType, new UInt64_1.UInt64(builder.getPreviousRestrictionValue()), isEmbedded ? new UInt64_1.UInt64([0, 0]) : new UInt64_1.UInt64(builder.fee.amount));
        return isEmbedded ?
            transaction.toAggregate(PublicAccount_1.PublicAccount.createFromPublicKey(signerPublicKey, networkType)) : transaction;
    }
    /**
     * @override Transaction.size()
     * @description get the byte size of a MosaicDefinitionTransaction
     * @returns {number}
     * @memberof MosaicAddressRestrictionTransaction
     */
    get size() {
        const byteSize = super.size;
        // set static byte size fields
        const byteMosaicId = 8;
        const byteRestrictionKey = 8;
        const bytePreviousRestrictionValue = 8;
        const byteNewRestrictionValue = 8;
        const byteTargetAddress = 25;
        return byteSize + byteMosaicId + byteRestrictionKey +
            byteTargetAddress + bytePreviousRestrictionValue + byteNewRestrictionValue;
    }
    /**
     * Return the string notation for the set recipient
     * @internal
     * @returns {string}
     */
    targetAddressToString() {
        if (this.targetAddress instanceof NamespaceId_1.NamespaceId) {
            // namespaceId recipient, return hexadecimal notation
            return this.targetAddress.toHex();
        }
        // address recipient
        return this.targetAddress.plain();
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateBytes() {
        const signerBuffer = new Uint8Array(32);
        const signatureBuffer = new Uint8Array(64);
        const transactionBuilder = new MosaicAddressRestrictionTransactionBuilder_1.MosaicAddressRestrictionTransactionBuilder(new SignatureDto_1.SignatureDto(signatureBuffer), new KeyDto_1.KeyDto(signerBuffer), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.MOSAIC_ADDRESS_RESTRICTION.valueOf(), new AmountDto_1.AmountDto(this.maxFee.toDTO()), new TimestampDto_1.TimestampDto(this.deadline.toDTO()), new UnresolvedMosaicIdDto_1.UnresolvedMosaicIdDto(this.mosaicId.id.toDTO()), this.restrictionKey.toDTO(), this.previousRestrictionValue.toDTO(), this.newRestrictionValue.toDTO(), new UnresolvedAddressDto_1.UnresolvedAddressDto(UnresolvedMapping_1.UnresolvedMapping.toUnresolvedAddressBytes(this.targetAddress, this.networkType)));
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateEmbeddedBytes() {
        const transactionBuilder = new EmbeddedMosaicAddressRestrictionTransactionBuilder_1.EmbeddedMosaicAddressRestrictionTransactionBuilder(new KeyDto_1.KeyDto(format_1.Convert.hexToUint8(this.signer.publicKey)), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.MOSAIC_ADDRESS_RESTRICTION.valueOf(), new UnresolvedMosaicIdDto_1.UnresolvedMosaicIdDto(this.mosaicId.id.toDTO()), this.restrictionKey.toDTO(), this.previousRestrictionValue.toDTO(), this.newRestrictionValue.toDTO(), new UnresolvedAddressDto_1.UnresolvedAddressDto(UnresolvedMapping_1.UnresolvedMapping.toUnresolvedAddressBytes(this.targetAddress, this.networkType)));
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @param statement Block receipt statement
     * @param aggregateTransactionIndex Transaction index for aggregated transaction
     * @returns {MosaicAddressRestrictionTransaction}
     */
    resolveAliases(statement, aggregateTransactionIndex = 0) {
        const transactionInfo = this.checkTransactionHeightAndIndex();
        return Object.assign({}, Object.getPrototypeOf(this), { mosaicId: statement.resolveMosaicId(this.mosaicId, transactionInfo.height.toString(), transactionInfo.index, aggregateTransactionIndex), targetAddress: statement.resolveAddress(this.targetAddress, transactionInfo.height.toString(), transactionInfo.index, aggregateTransactionIndex) });
    }
}
exports.MosaicAddressRestrictionTransaction = MosaicAddressRestrictionTransaction;
//# sourceMappingURL=MosaicAddressRestrictionTransaction.js.map