"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const format_1 = require("../../core/format");
const UnresolvedMapping_1 = require("../../core/utils/UnresolvedMapping");
const AmountDto_1 = require("../../infrastructure/catbuffer/AmountDto");
const BlockDurationDto_1 = require("../../infrastructure/catbuffer/BlockDurationDto");
const EmbeddedSecretLockTransactionBuilder_1 = require("../../infrastructure/catbuffer/EmbeddedSecretLockTransactionBuilder");
const Hash256Dto_1 = require("../../infrastructure/catbuffer/Hash256Dto");
const KeyDto_1 = require("../../infrastructure/catbuffer/KeyDto");
const SecretLockTransactionBuilder_1 = require("../../infrastructure/catbuffer/SecretLockTransactionBuilder");
const SignatureDto_1 = require("../../infrastructure/catbuffer/SignatureDto");
const TimestampDto_1 = require("../../infrastructure/catbuffer/TimestampDto");
const UnresolvedAddressDto_1 = require("../../infrastructure/catbuffer/UnresolvedAddressDto");
const UnresolvedMosaicBuilder_1 = require("../../infrastructure/catbuffer/UnresolvedMosaicBuilder");
const UnresolvedMosaicIdDto_1 = require("../../infrastructure/catbuffer/UnresolvedMosaicIdDto");
const PublicAccount_1 = require("../account/PublicAccount");
const Mosaic_1 = require("../mosaic/Mosaic");
const UInt64_1 = require("../UInt64");
const Deadline_1 = require("./Deadline");
const HashType_1 = require("./HashType");
const Transaction_1 = require("./Transaction");
const TransactionType_1 = require("./TransactionType");
const TransactionVersion_1 = require("./TransactionVersion");
class SecretLockTransaction extends Transaction_1.Transaction {
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param maxFee
     * @param mosaic
     * @param duration
     * @param hashType
     * @param secret
     * @param recipientAddress
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    constructor(networkType, version, deadline, maxFee, 
    /**
     * The locked mosaic.
     */
    mosaic, 
    /**
     * The duration for the funds to be released or returned.
     */
    duration, 
    /**
     * The hash algorithm, secret is generated with.
     */
    hashType, 
    /**
     * The proof hashed.
     */
    secret, 
    /**
     * The unresolved recipientAddress of the funds.
     */
    recipientAddress, signature, signer, transactionInfo) {
        super(TransactionType_1.TransactionType.SECRET_LOCK, networkType, version, deadline, maxFee, signature, signer, transactionInfo);
        this.mosaic = mosaic;
        this.duration = duration;
        this.hashType = hashType;
        this.secret = secret;
        this.recipientAddress = recipientAddress;
        if (!HashType_1.HashTypeLengthValidator(hashType, this.secret)) {
            throw new Error('HashType and Secret have incompatible length or not hexadecimal string');
        }
    }
    /**
     * Create a secret lock transaction object.
     *
     * @param deadline - The deadline to include the transaction.
     * @param mosaic - The locked mosaic.
     * @param duration - The funds lock duration.
     * @param hashType - The hash algorithm secret is generated with.
     * @param secret - The proof hashed.
     * @param recipientAddress - The unresolved recipient address of the funds.
     * @param networkType - The network type.
     * @param maxFee - (Optional) Max fee defined by the sender
     *
     * @return a SecretLockTransaction instance
     */
    static create(deadline, mosaic, duration, hashType, secret, recipientAddress, networkType, maxFee = new UInt64_1.UInt64([0, 0])) {
        return new SecretLockTransaction(networkType, TransactionVersion_1.TransactionVersion.SECRET_LOCK, deadline, maxFee, mosaic, duration, hashType, secret, recipientAddress);
    }
    /**
     * Create a transaction object from payload
     * @param {string} payload Binary payload
     * @param {Boolean} isEmbedded Is embedded transaction (Default: false)
     * @returns {Transaction | InnerTransaction}
     */
    static createFromPayload(payload, isEmbedded = false) {
        const builder = isEmbedded ? EmbeddedSecretLockTransactionBuilder_1.EmbeddedSecretLockTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload)) :
            SecretLockTransactionBuilder_1.SecretLockTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload));
        const signerPublicKey = format_1.Convert.uint8ToHex(builder.getSignerPublicKey().key);
        const networkType = builder.getNetwork().valueOf();
        const transaction = SecretLockTransaction.create(isEmbedded ? Deadline_1.Deadline.create() : Deadline_1.Deadline.createFromDTO(builder.getDeadline().timestamp), new Mosaic_1.Mosaic(UnresolvedMapping_1.UnresolvedMapping.toUnresolvedMosaic(new UInt64_1.UInt64(builder.getMosaic().mosaicId.unresolvedMosaicId).toHex()), new UInt64_1.UInt64(builder.getMosaic().amount.amount)), new UInt64_1.UInt64(builder.getDuration().blockDuration), builder.getHashAlgorithm().valueOf(), format_1.Convert.uint8ToHex(builder.getSecret().hash256), UnresolvedMapping_1.UnresolvedMapping.toUnresolvedAddress(format_1.Convert.uint8ToHex(builder.getRecipientAddress().unresolvedAddress)), networkType, isEmbedded ? new UInt64_1.UInt64([0, 0]) : new UInt64_1.UInt64(builder.fee.amount));
        return isEmbedded ?
            transaction.toAggregate(PublicAccount_1.PublicAccount.createFromPublicKey(signerPublicKey, networkType)) : transaction;
    }
    /**
     * @override Transaction.size()
     * @description get the byte size of a SecretLockTransaction
     * @returns {number}
     * @memberof SecretLockTransaction
     */
    get size() {
        const byteSize = super.size;
        // set static byte size fields
        const byteMosaicId = 8;
        const byteAmount = 8;
        const byteDuration = 8;
        const byteAlgorithm = 1;
        const byteRecipient = 25;
        // convert secret to uint8
        const byteSecret = format_1.Convert.hexToUint8(this.secret).length;
        return byteSize + byteMosaicId + byteAmount + byteDuration + byteAlgorithm + byteRecipient + byteSecret;
    }
    /**
     * @description Get secret bytes
     * @returns {Uint8Array}
     * @memberof SecretLockTransaction
     */
    getSecretByte() {
        return format_1.Convert.hexToUint8(64 > this.secret.length ? this.secret + '0'.repeat(64 - this.secret.length) : this.secret);
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateBytes() {
        const signerBuffer = new Uint8Array(32);
        const signatureBuffer = new Uint8Array(64);
        const transactionBuilder = new SecretLockTransactionBuilder_1.SecretLockTransactionBuilder(new SignatureDto_1.SignatureDto(signatureBuffer), new KeyDto_1.KeyDto(signerBuffer), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.SECRET_LOCK.valueOf(), new AmountDto_1.AmountDto(this.maxFee.toDTO()), new TimestampDto_1.TimestampDto(this.deadline.toDTO()), new Hash256Dto_1.Hash256Dto(this.getSecretByte()), new UnresolvedMosaicBuilder_1.UnresolvedMosaicBuilder(new UnresolvedMosaicIdDto_1.UnresolvedMosaicIdDto(this.mosaic.id.id.toDTO()), new AmountDto_1.AmountDto(this.mosaic.amount.toDTO())), new BlockDurationDto_1.BlockDurationDto(this.duration.toDTO()), this.hashType.valueOf(), new UnresolvedAddressDto_1.UnresolvedAddressDto(UnresolvedMapping_1.UnresolvedMapping.toUnresolvedAddressBytes(this.recipientAddress, this.networkType)));
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateEmbeddedBytes() {
        const transactionBuilder = new EmbeddedSecretLockTransactionBuilder_1.EmbeddedSecretLockTransactionBuilder(new KeyDto_1.KeyDto(format_1.Convert.hexToUint8(this.signer.publicKey)), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.SECRET_LOCK.valueOf(), new Hash256Dto_1.Hash256Dto(this.getSecretByte()), new UnresolvedMosaicBuilder_1.UnresolvedMosaicBuilder(new UnresolvedMosaicIdDto_1.UnresolvedMosaicIdDto(this.mosaic.id.id.toDTO()), new AmountDto_1.AmountDto(this.mosaic.amount.toDTO())), new BlockDurationDto_1.BlockDurationDto(this.duration.toDTO()), this.hashType.valueOf(), new UnresolvedAddressDto_1.UnresolvedAddressDto(UnresolvedMapping_1.UnresolvedMapping.toUnresolvedAddressBytes(this.recipientAddress, this.networkType)));
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @param statement Block receipt statement
     * @param aggregateTransactionIndex Transaction index for aggregated transaction
     * @returns {SecretLockTransaction}
     */
    resolveAliases(statement, aggregateTransactionIndex = 0) {
        const transactionInfo = this.checkTransactionHeightAndIndex();
        return Object.assign({}, Object.getPrototypeOf(this), { recipientAddress: statement.resolveAddress(this.recipientAddress, transactionInfo.height.toString(), transactionInfo.index, aggregateTransactionIndex), mosaic: statement.resolveMosaic(this.mosaic, transactionInfo.height.toString(), transactionInfo.index, aggregateTransactionIndex) });
    }
}
exports.SecretLockTransaction = SecretLockTransaction;
//# sourceMappingURL=SecretLockTransaction.js.map