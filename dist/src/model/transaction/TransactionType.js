"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Enum containing transaction type constants.
 */
var TransactionType;
(function (TransactionType) {
    /** Reserved entity type. */
    TransactionType[TransactionType["RESERVED"] = 0] = "RESERVED";
    /**
     * Transfer Transaction transaction type.
     * @type {number}
     */
    TransactionType[TransactionType["TRANSFER"] = 16724] = "TRANSFER";
    /**
     * Register namespace transaction type.
     * @type {number}
     */
    TransactionType[TransactionType["REGISTER_NAMESPACE"] = 16718] = "REGISTER_NAMESPACE";
    /**
     * Address alias transaction type
     * @type {number}
     */
    TransactionType[TransactionType["ADDRESS_ALIAS"] = 16974] = "ADDRESS_ALIAS";
    /**
     * Mosaic alias transaction type
     * @type {number}
     */
    TransactionType[TransactionType["MOSAIC_ALIAS"] = 17230] = "MOSAIC_ALIAS";
    /**
     * Mosaic definition transaction type.
     * @type {number}
     */
    TransactionType[TransactionType["MOSAIC_DEFINITION"] = 16717] = "MOSAIC_DEFINITION";
    /**
     * Mosaic supply change transaction.
     * @type {number}
     */
    TransactionType[TransactionType["MOSAIC_SUPPLY_CHANGE"] = 16973] = "MOSAIC_SUPPLY_CHANGE";
    /**
     * Modify multisig account transaction type.
     * @type {number}
     */
    TransactionType[TransactionType["MODIFY_MULTISIG_ACCOUNT"] = 16725] = "MODIFY_MULTISIG_ACCOUNT";
    /**
     * Aggregate complete transaction type.
     * @type {number}
     */
    TransactionType[TransactionType["AGGREGATE_COMPLETE"] = 16705] = "AGGREGATE_COMPLETE";
    /**
     * Aggregate bonded transaction type
     */
    TransactionType[TransactionType["AGGREGATE_BONDED"] = 16961] = "AGGREGATE_BONDED";
    /**
     * Lock transaction type
     * @type {number}
     */
    TransactionType[TransactionType["LOCK"] = 16712] = "LOCK";
    /**
     * Secret Lock Transaction type
     * @type {number}
     */
    TransactionType[TransactionType["SECRET_LOCK"] = 16722] = "SECRET_LOCK";
    /**
     * Secret Proof transaction type
     * @type {number}
     */
    TransactionType[TransactionType["SECRET_PROOF"] = 16978] = "SECRET_PROOF";
    /**
     * Account restriction address transaction type
     * @type {number}
     */
    TransactionType[TransactionType["ACCOUNT_RESTRICTION_ADDRESS"] = 16720] = "ACCOUNT_RESTRICTION_ADDRESS";
    /**
     * Account restriction mosaic transaction type
     * @type {number}
     */
    TransactionType[TransactionType["ACCOUNT_RESTRICTION_MOSAIC"] = 16976] = "ACCOUNT_RESTRICTION_MOSAIC";
    /**
     * Account restriction operation transaction type
     * @type {number}
     */
    TransactionType[TransactionType["ACCOUNT_RESTRICTION_OPERATION"] = 17232] = "ACCOUNT_RESTRICTION_OPERATION";
    /**
     * Link account transaction type
     * @type {number}
     */
    TransactionType[TransactionType["LINK_ACCOUNT"] = 16716] = "LINK_ACCOUNT";
    /**
     * Mosaic address restriction type
     * @type {number}
     */
    TransactionType[TransactionType["MOSAIC_ADDRESS_RESTRICTION"] = 16977] = "MOSAIC_ADDRESS_RESTRICTION";
    /**
     * Mosaic global restriction type
     * @type {number}
     */
    TransactionType[TransactionType["MOSAIC_GLOBAL_RESTRICTION"] = 16721] = "MOSAIC_GLOBAL_RESTRICTION";
    /**
     * Account metadata transaction
     * @type {number}
     */
    TransactionType[TransactionType["ACCOUNT_METADATA_TRANSACTION"] = 16708] = "ACCOUNT_METADATA_TRANSACTION";
    /**
     * Mosaic metadata transaction
     * @type {number}
     */
    TransactionType[TransactionType["MOSAIC_METADATA_TRANSACTION"] = 16964] = "MOSAIC_METADATA_TRANSACTION";
    /**
     * Namespace metadata transaction
     * @type {number}
     */
    TransactionType[TransactionType["NAMESPACE_METADATA_TRANSACTION"] = 17220] = "NAMESPACE_METADATA_TRANSACTION";
})(TransactionType = exports.TransactionType || (exports.TransactionType = {}));
//# sourceMappingURL=TransactionType.js.map