"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const format_1 = require("../../core/format");
const AccountOperationRestrictionTransactionBuilder_1 = require("../../infrastructure/catbuffer/AccountOperationRestrictionTransactionBuilder");
const AmountDto_1 = require("../../infrastructure/catbuffer/AmountDto");
const EmbeddedAccountOperationRestrictionTransactionBuilder_1 = require("../../infrastructure/catbuffer/EmbeddedAccountOperationRestrictionTransactionBuilder");
const KeyDto_1 = require("../../infrastructure/catbuffer/KeyDto");
const SignatureDto_1 = require("../../infrastructure/catbuffer/SignatureDto");
const TimestampDto_1 = require("../../infrastructure/catbuffer/TimestampDto");
const PublicAccount_1 = require("../account/PublicAccount");
const UInt64_1 = require("../UInt64");
const Deadline_1 = require("./Deadline");
const Transaction_1 = require("./Transaction");
const TransactionType_1 = require("./TransactionType");
const TransactionVersion_1 = require("./TransactionVersion");
class AccountOperationRestrictionTransaction extends Transaction_1.Transaction {
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param maxFee
     * @param restrictionFlags
     * @param restrictionAdditions
     * @param restrictionDeletions
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    constructor(networkType, version, deadline, maxFee, restrictionFlags, restrictionAdditions, restrictionDeletions, signature, signer, transactionInfo) {
        super(TransactionType_1.TransactionType.ACCOUNT_RESTRICTION_OPERATION, networkType, version, deadline, maxFee, signature, signer, transactionInfo);
        this.restrictionFlags = restrictionFlags;
        this.restrictionAdditions = restrictionAdditions;
        this.restrictionDeletions = restrictionDeletions;
    }
    /**
     * Create a modify account operation restriction type transaction object
     * @param deadline - The deadline to include the transaction.
     * @param restrictionFlags - The account restriction flags.
     * @param restrictionAdditions - Account restriction additions.
     * @param restrictionDeletions - Account restriction deletions.
     * @param networkType - The network type.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {AccountOperationRestrictionTransaction}
     */
    static create(deadline, restrictionFlags, restrictionAdditions, restrictionDeletions, networkType, maxFee = new UInt64_1.UInt64([0, 0])) {
        return new AccountOperationRestrictionTransaction(networkType, TransactionVersion_1.TransactionVersion.MODIFY_ACCOUNT_RESTRICTION_ENTITY_TYPE, deadline, maxFee, restrictionFlags, restrictionAdditions, restrictionDeletions);
    }
    /**
     * Create a transaction object from payload
     * @param {string} payload Binary payload
     * @param {Boolean} isEmbedded Is embedded transaction (Default: false)
     * @returns {Transaction | InnerTransaction}
     */
    static createFromPayload(payload, isEmbedded = false) {
        const builder = isEmbedded ? EmbeddedAccountOperationRestrictionTransactionBuilder_1.EmbeddedAccountOperationRestrictionTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload)) :
            AccountOperationRestrictionTransactionBuilder_1.AccountOperationRestrictionTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload));
        const signer = format_1.Convert.uint8ToHex(builder.getSignerPublicKey().key);
        const networkType = builder.getNetwork().valueOf();
        const transaction = AccountOperationRestrictionTransaction.create(isEmbedded ? Deadline_1.Deadline.create() : Deadline_1.Deadline.createFromDTO(builder.getDeadline().timestamp), builder.getRestrictionFlags().valueOf(), builder.getRestrictionAdditions(), builder.getRestrictionDeletions(), networkType, isEmbedded ? new UInt64_1.UInt64([0, 0]) : new UInt64_1.UInt64(builder.fee.amount));
        return isEmbedded ? transaction.toAggregate(PublicAccount_1.PublicAccount.createFromPublicKey(signer, networkType)) : transaction;
    }
    /**
     * @override Transaction.size()
     * @description get the byte size of a AccountOperationRestrictionTransaction
     * @returns {number}
     * @memberof AccountOperationRestrictionTransaction
     */
    get size() {
        const byteSize = super.size;
        // set static byte size fields
        const byteRestrictionType = 2;
        const byteAdditionCount = 1;
        const byteDeletionCount = 1;
        const byteAccountRestrictionTransactionBody_Reserved1 = 4;
        const byteRestrictionAdditions = 2 * this.restrictionAdditions.length;
        const byteRestrictionDeletions = 2 * this.restrictionDeletions.length;
        return byteSize + byteRestrictionType + byteAdditionCount + byteDeletionCount +
            byteRestrictionAdditions + byteRestrictionDeletions +
            byteAccountRestrictionTransactionBody_Reserved1;
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateBytes() {
        const signerBuffer = new Uint8Array(32);
        const signatureBuffer = new Uint8Array(64);
        const transactionBuilder = new AccountOperationRestrictionTransactionBuilder_1.AccountOperationRestrictionTransactionBuilder(new SignatureDto_1.SignatureDto(signatureBuffer), new KeyDto_1.KeyDto(signerBuffer), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.ACCOUNT_RESTRICTION_OPERATION.valueOf(), new AmountDto_1.AmountDto(this.maxFee.toDTO()), new TimestampDto_1.TimestampDto(this.deadline.toDTO()), this.restrictionFlags.valueOf(), this.restrictionAdditions, this.restrictionDeletions);
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateEmbeddedBytes() {
        const transactionBuilder = new EmbeddedAccountOperationRestrictionTransactionBuilder_1.EmbeddedAccountOperationRestrictionTransactionBuilder(new KeyDto_1.KeyDto(format_1.Convert.hexToUint8(this.signer.publicKey)), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.ACCOUNT_RESTRICTION_OPERATION.valueOf(), this.restrictionFlags.valueOf(), this.restrictionAdditions, this.restrictionDeletions);
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @returns {AccountOperationRestrictionTransaction}
     */
    resolveAliases() {
        return this;
    }
}
exports.AccountOperationRestrictionTransaction = AccountOperationRestrictionTransaction;
//# sourceMappingURL=AccountOperationRestrictionTransaction.js.map