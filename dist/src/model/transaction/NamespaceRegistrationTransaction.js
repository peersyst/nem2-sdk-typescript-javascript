"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const format_1 = require("../../core/format");
const AmountDto_1 = require("../../infrastructure/catbuffer/AmountDto");
const BlockDurationDto_1 = require("../../infrastructure/catbuffer/BlockDurationDto");
const EmbeddedNamespaceRegistrationTransactionBuilder_1 = require("../../infrastructure/catbuffer/EmbeddedNamespaceRegistrationTransactionBuilder");
const KeyDto_1 = require("../../infrastructure/catbuffer/KeyDto");
const NamespaceIdDto_1 = require("../../infrastructure/catbuffer/NamespaceIdDto");
const NamespaceRegistrationTransactionBuilder_1 = require("../../infrastructure/catbuffer/NamespaceRegistrationTransactionBuilder");
const SignatureDto_1 = require("../../infrastructure/catbuffer/SignatureDto");
const TimestampDto_1 = require("../../infrastructure/catbuffer/TimestampDto");
const NamespaceMosaicIdGenerator_1 = require("../../infrastructure/transaction/NamespaceMosaicIdGenerator");
const PublicAccount_1 = require("../account/PublicAccount");
const NamespaceId_1 = require("../namespace/NamespaceId");
const NamespaceRegistrationType_1 = require("../namespace/NamespaceRegistrationType");
const UInt64_1 = require("../UInt64");
const Deadline_1 = require("./Deadline");
const Transaction_1 = require("./Transaction");
const TransactionType_1 = require("./TransactionType");
const TransactionVersion_1 = require("./TransactionVersion");
/**
 * Accounts can rent a namespace for an amount of blocks and after a this renew the contract.
 * This is done via a NamespaceRegistrationTransaction.
 */
class NamespaceRegistrationTransaction extends Transaction_1.Transaction {
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param maxFee
     * @param registrationType
     * @param namespaceName
     * @param namespaceId
     * @param duration
     * @param parentId
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    constructor(networkType, version, deadline, maxFee, 
    /**
     * The namespace type could be namespace or sub namespace
     */
    registrationType, 
    /**
     * The namespace name
     */
    namespaceName, 
    /**
     * The id of the namespace derived from namespaceName.
     * When creating a sub namespace the namespaceId is derived from namespaceName and parentName.
     */
    namespaceId, 
    /**
     * The number of blocks a namespace is active
     */
    duration, 
    /**
     * The id of the parent sub namespace
     */
    parentId, signature, signer, transactionInfo) {
        super(TransactionType_1.TransactionType.REGISTER_NAMESPACE, networkType, version, deadline, maxFee, signature, signer, transactionInfo);
        this.registrationType = registrationType;
        this.namespaceName = namespaceName;
        this.namespaceId = namespaceId;
        this.duration = duration;
        this.parentId = parentId;
    }
    /**
     * Create a root namespace object
     * @param deadline - The deadline to include the transaction.
     * @param namespaceName - The namespace name.
     * @param duration - The duration of the namespace.
     * @param networkType - The network type.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {NamespaceRegistrationTransaction}
     */
    static createRootNamespace(deadline, namespaceName, duration, networkType, maxFee = new UInt64_1.UInt64([0, 0])) {
        return new NamespaceRegistrationTransaction(networkType, TransactionVersion_1.TransactionVersion.REGISTER_NAMESPACE, deadline, maxFee, NamespaceRegistrationType_1.NamespaceRegistrationType.RootNamespace, namespaceName, new NamespaceId_1.NamespaceId(namespaceName), duration);
    }
    /**
     * Create a sub namespace object
     * @param deadline - The deadline to include the transaction.
     * @param namespaceName - The namespace name.
     * @param parentNamespace - The parent namespace name.
     * @param networkType - The network type.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {NamespaceRegistrationTransaction}
     */
    static createSubNamespace(deadline, namespaceName, parentNamespace, networkType, maxFee = new UInt64_1.UInt64([0, 0])) {
        let parentId;
        if (typeof parentNamespace === 'string') {
            parentId = new NamespaceId_1.NamespaceId(NamespaceMosaicIdGenerator_1.NamespaceMosaicIdGenerator.subnamespaceParentId(parentNamespace, namespaceName));
        }
        else {
            parentId = parentNamespace;
        }
        return new NamespaceRegistrationTransaction(networkType, TransactionVersion_1.TransactionVersion.REGISTER_NAMESPACE, deadline, maxFee, NamespaceRegistrationType_1.NamespaceRegistrationType.SubNamespace, namespaceName, typeof parentNamespace === 'string' ?
            new NamespaceId_1.NamespaceId(NamespaceMosaicIdGenerator_1.NamespaceMosaicIdGenerator.subnamespaceNamespaceId(parentNamespace, namespaceName)) :
            new NamespaceId_1.NamespaceId(NamespaceMosaicIdGenerator_1.NamespaceMosaicIdGenerator.namespaceId(namespaceName)), undefined, parentId);
    }
    /**
     * Create a transaction object from payload
     * @param {string} payload Binary payload
     * @param {Boolean} isEmbedded Is embedded transaction (Default: false)
     * @returns {Transaction | InnerTransaction}
     */
    static createFromPayload(payload, isEmbedded = false) {
        const builder = isEmbedded ? EmbeddedNamespaceRegistrationTransactionBuilder_1.EmbeddedNamespaceRegistrationTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload)) :
            NamespaceRegistrationTransactionBuilder_1.NamespaceRegistrationTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload));
        const registrationType = builder.getRegistrationType().valueOf();
        const signerPublicKey = format_1.Convert.uint8ToHex(builder.getSignerPublicKey().key);
        const networkType = builder.getNetwork().valueOf();
        const transaction = registrationType === NamespaceRegistrationType_1.NamespaceRegistrationType.RootNamespace ?
            NamespaceRegistrationTransaction.createRootNamespace(isEmbedded ? Deadline_1.Deadline.create() : Deadline_1.Deadline.createFromDTO(builder.getDeadline().timestamp), format_1.Convert.decodeHex(format_1.Convert.uint8ToHex(builder.getName())), new UInt64_1.UInt64(builder.getDuration().blockDuration), networkType, isEmbedded ? new UInt64_1.UInt64([0, 0]) : new UInt64_1.UInt64(builder.fee.amount)) : NamespaceRegistrationTransaction.createSubNamespace(isEmbedded ? Deadline_1.Deadline.create() : Deadline_1.Deadline.createFromDTO(builder.getDeadline().timestamp), format_1.Convert.decodeHex(format_1.Convert.uint8ToHex(builder.getName())), new NamespaceId_1.NamespaceId(builder.getParentId().namespaceId), networkType, isEmbedded ? new UInt64_1.UInt64([0, 0]) : new UInt64_1.UInt64(builder.fee.amount));
        return isEmbedded ?
            transaction.toAggregate(PublicAccount_1.PublicAccount.createFromPublicKey(signerPublicKey, networkType)) : transaction;
    }
    /**
     * @override Transaction.size()
     * @description get the byte size of a NamespaceRegistrationTransaction
     * @returns {number}
     * @memberof NamespaceRegistrationTransaction
     */
    get size() {
        const byteSize = super.size;
        // set static byte size fields
        const byteType = 1;
        const byteDurationParentId = 8;
        const byteNamespaceId = 8;
        const byteNameSize = 1;
        // convert name to uint8
        const byteName = format_1.Convert.utf8ToHex(this.namespaceName).length / 2;
        return byteSize + byteType + byteDurationParentId + byteNamespaceId + byteNameSize + byteName;
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateBytes() {
        const signerBuffer = new Uint8Array(32);
        const signatureBuffer = new Uint8Array(64);
        let transactionBuilder;
        if (this.registrationType === NamespaceRegistrationType_1.NamespaceRegistrationType.RootNamespace) {
            transactionBuilder = new NamespaceRegistrationTransactionBuilder_1.NamespaceRegistrationTransactionBuilder(new SignatureDto_1.SignatureDto(signatureBuffer), new KeyDto_1.KeyDto(signerBuffer), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.REGISTER_NAMESPACE.valueOf(), new AmountDto_1.AmountDto(this.maxFee.toDTO()), new TimestampDto_1.TimestampDto(this.deadline.toDTO()), new NamespaceIdDto_1.NamespaceIdDto(this.namespaceId.id.toDTO()), format_1.Convert.hexToUint8(format_1.Convert.utf8ToHex(this.namespaceName)), new BlockDurationDto_1.BlockDurationDto(this.duration.toDTO()), undefined);
        }
        else {
            transactionBuilder = new NamespaceRegistrationTransactionBuilder_1.NamespaceRegistrationTransactionBuilder(new SignatureDto_1.SignatureDto(signatureBuffer), new KeyDto_1.KeyDto(signerBuffer), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.REGISTER_NAMESPACE.valueOf(), new AmountDto_1.AmountDto(this.maxFee.toDTO()), new TimestampDto_1.TimestampDto(this.deadline.toDTO()), new NamespaceIdDto_1.NamespaceIdDto(this.namespaceId.id.toDTO()), format_1.Convert.hexToUint8(format_1.Convert.utf8ToHex(this.namespaceName)), undefined, new NamespaceIdDto_1.NamespaceIdDto(this.parentId.id.toDTO()));
        }
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateEmbeddedBytes() {
        let transactionBuilder;
        if (this.registrationType === NamespaceRegistrationType_1.NamespaceRegistrationType.RootNamespace) {
            transactionBuilder = new EmbeddedNamespaceRegistrationTransactionBuilder_1.EmbeddedNamespaceRegistrationTransactionBuilder(new KeyDto_1.KeyDto(format_1.Convert.hexToUint8(this.signer.publicKey)), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.REGISTER_NAMESPACE.valueOf(), new NamespaceIdDto_1.NamespaceIdDto(this.namespaceId.id.toDTO()), format_1.Convert.hexToUint8(format_1.Convert.utf8ToHex(this.namespaceName)), new BlockDurationDto_1.BlockDurationDto(this.duration.toDTO()), undefined);
        }
        else {
            transactionBuilder = new EmbeddedNamespaceRegistrationTransactionBuilder_1.EmbeddedNamespaceRegistrationTransactionBuilder(new KeyDto_1.KeyDto(format_1.Convert.hexToUint8(this.signer.publicKey)), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.REGISTER_NAMESPACE.valueOf(), new NamespaceIdDto_1.NamespaceIdDto(this.namespaceId.id.toDTO()), format_1.Convert.hexToUint8(format_1.Convert.utf8ToHex(this.namespaceName)), undefined, new NamespaceIdDto_1.NamespaceIdDto(this.parentId.id.toDTO()));
        }
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @returns {NamespaceRegistrationTransaction}
     */
    resolveAliases() {
        return this;
    }
}
exports.NamespaceRegistrationTransaction = NamespaceRegistrationTransaction;
//# sourceMappingURL=NamespaceRegistrationTransaction.js.map