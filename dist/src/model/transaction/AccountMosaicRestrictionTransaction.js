"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const format_1 = require("../../core/format");
const UnresolvedMapping_1 = require("../../core/utils/UnresolvedMapping");
const AccountMosaicRestrictionTransactionBuilder_1 = require("../../infrastructure/catbuffer/AccountMosaicRestrictionTransactionBuilder");
const AmountDto_1 = require("../../infrastructure/catbuffer/AmountDto");
const EmbeddedAccountMosaicRestrictionTransactionBuilder_1 = require("../../infrastructure/catbuffer/EmbeddedAccountMosaicRestrictionTransactionBuilder");
const KeyDto_1 = require("../../infrastructure/catbuffer/KeyDto");
const SignatureDto_1 = require("../../infrastructure/catbuffer/SignatureDto");
const TimestampDto_1 = require("../../infrastructure/catbuffer/TimestampDto");
const UnresolvedMosaicIdDto_1 = require("../../infrastructure/catbuffer/UnresolvedMosaicIdDto");
const PublicAccount_1 = require("../account/PublicAccount");
const UInt64_1 = require("../UInt64");
const Deadline_1 = require("./Deadline");
const Transaction_1 = require("./Transaction");
const TransactionType_1 = require("./TransactionType");
const TransactionVersion_1 = require("./TransactionVersion");
class AccountMosaicRestrictionTransaction extends Transaction_1.Transaction {
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param maxFee
     * @param restrictionFlags
     * @param restrictionAdditions
     * @param restrictionDeletions
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    constructor(networkType, version, deadline, maxFee, restrictionFlags, restrictionAdditions, restrictionDeletions, signature, signer, transactionInfo) {
        super(TransactionType_1.TransactionType.ACCOUNT_RESTRICTION_MOSAIC, networkType, version, deadline, maxFee, signature, signer, transactionInfo);
        this.restrictionFlags = restrictionFlags;
        this.restrictionAdditions = restrictionAdditions;
        this.restrictionDeletions = restrictionDeletions;
    }
    /**
     * Create a modify account mosaic restriction transaction object
     * @param deadline - The deadline to include the transaction.
     * @param restrictionFlags - The account restriction flags.
     * @param restrictionAdditions - Account restriction additions.
     * @param restrictionDeletions - Account restriction deletions.
     * @param networkType - The network type.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {AccountAddressRestrictionTransaction}
     */
    static create(deadline, restrictionFlags, restrictionAdditions, restrictionDeletions, networkType, maxFee = new UInt64_1.UInt64([0, 0])) {
        return new AccountMosaicRestrictionTransaction(networkType, TransactionVersion_1.TransactionVersion.ACCOUNT_RESTRICTION_MOSAIC, deadline, maxFee, restrictionFlags, restrictionAdditions, restrictionDeletions);
    }
    /**
     * Create a transaction object from payload
     * @param {string} payload Binary payload
     * @param {Boolean} isEmbedded Is embedded transaction (Default: false)
     * @returns {Transaction | InnerTransaction}
     */
    static createFromPayload(payload, isEmbedded = false) {
        const builder = isEmbedded ? EmbeddedAccountMosaicRestrictionTransactionBuilder_1.EmbeddedAccountMosaicRestrictionTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload)) :
            AccountMosaicRestrictionTransactionBuilder_1.AccountMosaicRestrictionTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload));
        const signerPublicKey = format_1.Convert.uint8ToHex(builder.getSignerPublicKey().key);
        const networkType = builder.getNetwork().valueOf();
        const transaction = AccountMosaicRestrictionTransaction.create(isEmbedded ? Deadline_1.Deadline.create() : Deadline_1.Deadline.createFromDTO(builder.getDeadline().timestamp), builder.getRestrictionFlags().valueOf(), builder.getRestrictionAdditions().map((addition) => {
            return UnresolvedMapping_1.UnresolvedMapping.toUnresolvedMosaic(new UInt64_1.UInt64(addition.unresolvedMosaicId).toHex());
        }), builder.getRestrictionDeletions().map((deletion) => {
            return UnresolvedMapping_1.UnresolvedMapping.toUnresolvedMosaic(new UInt64_1.UInt64(deletion.unresolvedMosaicId).toHex());
        }), networkType, isEmbedded ? new UInt64_1.UInt64([0, 0]) : new UInt64_1.UInt64(builder.fee.amount));
        return isEmbedded ?
            transaction.toAggregate(PublicAccount_1.PublicAccount.createFromPublicKey(signerPublicKey, networkType)) : transaction;
    }
    /**
     * @override Transaction.size()
     * @description get the byte size of a AccountMosaicRestrictionTransaction
     * @returns {number}
     * @memberof AccountMosaicRestrictionTransaction
     */
    get size() {
        const byteSize = super.size;
        // set static byte size fields
        const byteRestrictionType = 2;
        const byteAdditionCount = 1;
        const byteDeletionCount = 1;
        const byteAccountRestrictionTransactionBody_Reserved1 = 4;
        const byteRestrictionAdditions = 8 * this.restrictionAdditions.length;
        const byteRestrictionDeletions = 8 * this.restrictionDeletions.length;
        return byteSize + byteRestrictionType + byteAdditionCount + byteDeletionCount +
            byteRestrictionAdditions + byteRestrictionDeletions +
            byteAccountRestrictionTransactionBody_Reserved1;
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateBytes() {
        const signerBuffer = new Uint8Array(32);
        const signatureBuffer = new Uint8Array(64);
        const transactionBuilder = new AccountMosaicRestrictionTransactionBuilder_1.AccountMosaicRestrictionTransactionBuilder(new SignatureDto_1.SignatureDto(signatureBuffer), new KeyDto_1.KeyDto(signerBuffer), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.ACCOUNT_RESTRICTION_MOSAIC.valueOf(), new AmountDto_1.AmountDto(this.maxFee.toDTO()), new TimestampDto_1.TimestampDto(this.deadline.toDTO()), this.restrictionFlags.valueOf(), this.restrictionAdditions.map((addition) => {
            return new UnresolvedMosaicIdDto_1.UnresolvedMosaicIdDto(addition.id.toDTO());
        }), this.restrictionDeletions.map((deletion) => {
            return new UnresolvedMosaicIdDto_1.UnresolvedMosaicIdDto(deletion.id.toDTO());
        }));
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateEmbeddedBytes() {
        const transactionBuilder = new EmbeddedAccountMosaicRestrictionTransactionBuilder_1.EmbeddedAccountMosaicRestrictionTransactionBuilder(new KeyDto_1.KeyDto(format_1.Convert.hexToUint8(this.signer.publicKey)), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.ACCOUNT_RESTRICTION_MOSAIC.valueOf(), this.restrictionFlags.valueOf(), this.restrictionAdditions.map((addition) => {
            return new UnresolvedMosaicIdDto_1.UnresolvedMosaicIdDto(addition.id.toDTO());
        }), this.restrictionDeletions.map((deletion) => {
            return new UnresolvedMosaicIdDto_1.UnresolvedMosaicIdDto(deletion.id.toDTO());
        }));
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @param statement Block receipt statement
     * @param aggregateTransactionIndex Transaction index for aggregated transaction
     * @returns {AccountMosaicRestrictionTransaction}
     */
    resolveAliases(statement, aggregateTransactionIndex = 0) {
        const transactionInfo = this.checkTransactionHeightAndIndex();
        return Object.assign({}, Object.getPrototypeOf(this), { restrictionAdditions: this.restrictionAdditions.map((addition) => statement.resolveMosaicId(addition, transactionInfo.height.toString(), transactionInfo.index, aggregateTransactionIndex)), restrictionDeletions: this.restrictionDeletions.map((deletion) => statement.resolveMosaicId(deletion, transactionInfo.height.toString(), transactionInfo.index, aggregateTransactionIndex)) });
    }
}
exports.AccountMosaicRestrictionTransaction = AccountMosaicRestrictionTransaction;
//# sourceMappingURL=AccountMosaicRestrictionTransaction.js.map