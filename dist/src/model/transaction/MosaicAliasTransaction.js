"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const format_1 = require("../../core/format");
const AmountDto_1 = require("../../infrastructure/catbuffer/AmountDto");
const EmbeddedMosaicAliasTransactionBuilder_1 = require("../../infrastructure/catbuffer/EmbeddedMosaicAliasTransactionBuilder");
const KeyDto_1 = require("../../infrastructure/catbuffer/KeyDto");
const MosaicAliasTransactionBuilder_1 = require("../../infrastructure/catbuffer/MosaicAliasTransactionBuilder");
const MosaicIdDto_1 = require("../../infrastructure/catbuffer/MosaicIdDto");
const NamespaceIdDto_1 = require("../../infrastructure/catbuffer/NamespaceIdDto");
const SignatureDto_1 = require("../../infrastructure/catbuffer/SignatureDto");
const TimestampDto_1 = require("../../infrastructure/catbuffer/TimestampDto");
const PublicAccount_1 = require("../account/PublicAccount");
const MosaicId_1 = require("../mosaic/MosaicId");
const NamespaceId_1 = require("../namespace/NamespaceId");
const UInt64_1 = require("../UInt64");
const Deadline_1 = require("./Deadline");
const Transaction_1 = require("./Transaction");
const TransactionType_1 = require("./TransactionType");
const TransactionVersion_1 = require("./TransactionVersion");
class MosaicAliasTransaction extends Transaction_1.Transaction {
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param maxFee
     * @param aliasAction
     * @param namespaceId
     * @param mosaicId
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    constructor(networkType, version, deadline, maxFee, 
    /**
     * The alias action type.
     */
    aliasAction, 
    /**
     * The namespace id that will be an alias.
     */
    namespaceId, 
    /**
     * The mosaic id.
     */
    mosaicId, signature, signer, transactionInfo) {
        super(TransactionType_1.TransactionType.MOSAIC_ALIAS, networkType, version, deadline, maxFee, signature, signer, transactionInfo);
        this.aliasAction = aliasAction;
        this.namespaceId = namespaceId;
        this.mosaicId = mosaicId;
    }
    /**
     * Create a mosaic alias transaction object
     * @param deadline - The deadline to include the transaction.
     * @param aliasAction - The alias action type.
     * @param namespaceId - The namespace id.
     * @param mosaicId - The mosaic id.
     * @param networkType - The network type.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {MosaicAliasTransaction}
     */
    static create(deadline, aliasAction, namespaceId, mosaicId, networkType, maxFee = new UInt64_1.UInt64([0, 0])) {
        return new MosaicAliasTransaction(networkType, TransactionVersion_1.TransactionVersion.MOSAIC_ALIAS, deadline, maxFee, aliasAction, namespaceId, mosaicId);
    }
    /**
     * Create a transaction object from payload
     * @param {string} payload Binary payload
     * @param {Boolean} isEmbedded Is embedded transaction (Default: false)
     * @returns {Transaction | InnerTransaction}
     */
    static createFromPayload(payload, isEmbedded = false) {
        const builder = isEmbedded ? EmbeddedMosaicAliasTransactionBuilder_1.EmbeddedMosaicAliasTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload)) :
            MosaicAliasTransactionBuilder_1.MosaicAliasTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload));
        const signerPublicKey = format_1.Convert.uint8ToHex(builder.getSignerPublicKey().key);
        const networkType = builder.getNetwork().valueOf();
        const transaction = MosaicAliasTransaction.create(isEmbedded ? Deadline_1.Deadline.create() : Deadline_1.Deadline.createFromDTO(builder.getDeadline().timestamp), builder.getAliasAction().valueOf(), new NamespaceId_1.NamespaceId(builder.getNamespaceId().namespaceId), new MosaicId_1.MosaicId(builder.getMosaicId().mosaicId), networkType, isEmbedded ? new UInt64_1.UInt64([0, 0]) : new UInt64_1.UInt64(builder.fee.amount));
        return isEmbedded ?
            transaction.toAggregate(PublicAccount_1.PublicAccount.createFromPublicKey(signerPublicKey, networkType)) : transaction;
    }
    /**
     * @override Transaction.size()
     * @description get the byte size of a MosaicAliasTransaction
     * @returns {number}
     * @memberof MosaicAliasTransaction
     */
    get size() {
        const byteSize = super.size;
        // set static byte size fields
        const byteType = 1;
        const byteNamespaceId = 8;
        const byteMosaicId = 8;
        return byteSize + byteType + byteNamespaceId + byteMosaicId;
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateBytes() {
        const signerBuffer = new Uint8Array(32);
        const signatureBuffer = new Uint8Array(64);
        const transactionBuilder = new MosaicAliasTransactionBuilder_1.MosaicAliasTransactionBuilder(new SignatureDto_1.SignatureDto(signatureBuffer), new KeyDto_1.KeyDto(signerBuffer), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.MOSAIC_ALIAS.valueOf(), new AmountDto_1.AmountDto(this.maxFee.toDTO()), new TimestampDto_1.TimestampDto(this.deadline.toDTO()), new NamespaceIdDto_1.NamespaceIdDto(this.namespaceId.id.toDTO()), new MosaicIdDto_1.MosaicIdDto(this.mosaicId.id.toDTO()), this.aliasAction.valueOf());
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateEmbeddedBytes() {
        const transactionBuilder = new EmbeddedMosaicAliasTransactionBuilder_1.EmbeddedMosaicAliasTransactionBuilder(new KeyDto_1.KeyDto(format_1.Convert.hexToUint8(this.signer.publicKey)), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.MOSAIC_ALIAS.valueOf(), new NamespaceIdDto_1.NamespaceIdDto(this.namespaceId.id.toDTO()), new MosaicIdDto_1.MosaicIdDto(this.mosaicId.id.toDTO()), this.aliasAction.valueOf());
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @returns {MosaicAliasTransaction}
     */
    resolveAliases() {
        return this;
    }
}
exports.MosaicAliasTransaction = MosaicAliasTransaction;
//# sourceMappingURL=MosaicAliasTransaction.js.map