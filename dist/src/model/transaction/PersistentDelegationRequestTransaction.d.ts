import { NetworkType } from '../blockchain/NetworkType';
import { UInt64 } from '../UInt64';
import { Deadline } from './Deadline';
import { TransferTransaction } from './TransferTransaction';
export declare class PersistentDelegationRequestTransaction extends TransferTransaction {
    /**
     * Create a PersistentDelegationRequestTransaction with special message payload
     * for presistent harvesting delegation unlocking
     * @param deadline - The deadline to include the transaction.
     * @param delegatedPrivateKey - The private key of delegated account
     * @param recipientPublicKey - The recipient public key
     * @param senderPrivateKey - The sender's private key
     * @param networkType - The network type.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {TransferTransaction}
     */
    static createPersistentDelegationRequestTransaction(deadline: Deadline, delegatedPrivateKey: string, recipientPublicKey: string, senderPrivateKey: string, networkType: NetworkType, maxFee?: UInt64): PersistentDelegationRequestTransaction;
}
