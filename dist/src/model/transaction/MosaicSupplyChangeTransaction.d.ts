import { PublicAccount } from '../account/PublicAccount';
import { NetworkType } from '../blockchain/NetworkType';
import { MosaicId } from '../mosaic/MosaicId';
import { MosaicSupplyChangeAction } from '../mosaic/MosaicSupplyChangeAction';
import { NamespaceId } from '../namespace/NamespaceId';
import { UInt64 } from '../UInt64';
import { Deadline } from './Deadline';
import { InnerTransaction } from './InnerTransaction';
import { Transaction } from './Transaction';
import { TransactionInfo } from './TransactionInfo';
/**
 * In case a mosaic has the flag 'supplyMutable' set to true, the creator of the mosaic can change the supply,
 * i.e. increase or decrease the supply.
 */
export declare class MosaicSupplyChangeTransaction extends Transaction {
    /**
     * The unresolved mosaic id.
     */
    readonly mosaicId: MosaicId | NamespaceId;
    /**
     * The supply type.
     */
    readonly action: MosaicSupplyChangeAction;
    /**
     * The supply change in units for the mosaic.
     */
    readonly delta: UInt64;
    /**
     * Create a mosaic supply change transaction object
     * @param deadline - The deadline to include the transaction.
     * @param mosaicId - The unresolved mosaic id.
     * @param action - The supply change action (increase | decrease).
     * @param delta - The supply change in units for the mosaic.
     * @param networkType - The network type.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {MosaicSupplyChangeTransaction}
     */
    static create(deadline: Deadline, mosaicId: MosaicId | NamespaceId, action: MosaicSupplyChangeAction, delta: UInt64, networkType: NetworkType, maxFee?: UInt64): MosaicSupplyChangeTransaction;
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param maxFee
     * @param mosaicId
     * @param action
     * @param delta
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    constructor(networkType: NetworkType, version: number, deadline: Deadline, maxFee: UInt64, 
        /**
         * The unresolved mosaic id.
         */
        mosaicId: MosaicId | NamespaceId, 
        /**
         * The supply type.
         */
        action: MosaicSupplyChangeAction, 
        /**
         * The supply change in units for the mosaic.
         */
        delta: UInt64, signature?: string, signer?: PublicAccount, transactionInfo?: TransactionInfo);
    /**
     * Create a transaction object from payload
     * @param {string} payload Binary payload
     * @param {Boolean} isEmbedded Is embedded transaction (Default: false)
     * @returns {Transaction | InnerTransaction}
     */
    static createFromPayload(payload: string, isEmbedded?: boolean): Transaction | InnerTransaction;
    /**
     * @override Transaction.size()
     * @description get the byte size of a MosaicSupplyChangeTransaction
     * @returns {number}
     * @memberof MosaicSupplyChangeTransaction
     */
    readonly size: number;
}
