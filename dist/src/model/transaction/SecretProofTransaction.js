"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const format_1 = require("../../core/format");
const UnresolvedMapping_1 = require("../../core/utils/UnresolvedMapping");
const AmountDto_1 = require("../../infrastructure/catbuffer/AmountDto");
const EmbeddedSecretProofTransactionBuilder_1 = require("../../infrastructure/catbuffer/EmbeddedSecretProofTransactionBuilder");
const Hash256Dto_1 = require("../../infrastructure/catbuffer/Hash256Dto");
const KeyDto_1 = require("../../infrastructure/catbuffer/KeyDto");
const SecretProofTransactionBuilder_1 = require("../../infrastructure/catbuffer/SecretProofTransactionBuilder");
const SignatureDto_1 = require("../../infrastructure/catbuffer/SignatureDto");
const TimestampDto_1 = require("../../infrastructure/catbuffer/TimestampDto");
const UnresolvedAddressDto_1 = require("../../infrastructure/catbuffer/UnresolvedAddressDto");
const PublicAccount_1 = require("../account/PublicAccount");
const UInt64_1 = require("../UInt64");
const Deadline_1 = require("./Deadline");
const HashType_1 = require("./HashType");
const Transaction_1 = require("./Transaction");
const TransactionType_1 = require("./TransactionType");
const TransactionVersion_1 = require("./TransactionVersion");
class SecretProofTransaction extends Transaction_1.Transaction {
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param maxFee
     * @param hashType
     * @param secret
     * @param recipientAddress
     * @param proof
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    constructor(networkType, version, deadline, maxFee, hashType, secret, recipientAddress, proof, signature, signer, transactionInfo) {
        super(TransactionType_1.TransactionType.SECRET_PROOF, networkType, version, deadline, maxFee, signature, signer, transactionInfo);
        this.hashType = hashType;
        this.secret = secret;
        this.recipientAddress = recipientAddress;
        this.proof = proof;
        if (!HashType_1.HashTypeLengthValidator(hashType, this.secret)) {
            throw new Error('HashType and Secret have incompatible length or not hexadecimal string');
        }
    }
    /**
     * Create a secret proof transaction object.
     *
     * @param deadline - The deadline to include the transaction.
     * @param hashType - The hash algorithm secret is generated with.
     * @param secret - The seed proof hashed.
     * @param recipientAddress - UnresolvedAddress
     * @param proof - The seed proof.
     * @param networkType - The network type.
     * @param maxFee - (Optional) Max fee defined by the sender
     *
     * @return a SecretProofTransaction instance
     */
    static create(deadline, hashType, secret, recipientAddress, proof, networkType, maxFee = new UInt64_1.UInt64([0, 0])) {
        return new SecretProofTransaction(networkType, TransactionVersion_1.TransactionVersion.SECRET_PROOF, deadline, maxFee, hashType, secret, recipientAddress, proof);
    }
    /**
     * Create a transaction object from payload
     * @param {string} payload Binary payload
     * @param {Boolean} isEmbedded Is embedded transaction (Default: false)
     * @returns {Transaction | InnerTransaction}
     */
    static createFromPayload(payload, isEmbedded = false) {
        const builder = isEmbedded ? EmbeddedSecretProofTransactionBuilder_1.EmbeddedSecretProofTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload)) :
            SecretProofTransactionBuilder_1.SecretProofTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload));
        const signerPublicKey = format_1.Convert.uint8ToHex(builder.getSignerPublicKey().key);
        const networkType = builder.getNetwork().valueOf();
        const transaction = SecretProofTransaction.create(isEmbedded ? Deadline_1.Deadline.create() : Deadline_1.Deadline.createFromDTO(builder.getDeadline().timestamp), builder.getHashAlgorithm().valueOf(), format_1.Convert.uint8ToHex(builder.getSecret().hash256), UnresolvedMapping_1.UnresolvedMapping.toUnresolvedAddress(format_1.Convert.uint8ToHex(builder.getRecipientAddress().unresolvedAddress)), format_1.Convert.uint8ToHex(builder.getProof()), networkType, isEmbedded ? new UInt64_1.UInt64([0, 0]) : new UInt64_1.UInt64(builder.fee.amount));
        return isEmbedded ?
            transaction.toAggregate(PublicAccount_1.PublicAccount.createFromPublicKey(signerPublicKey, networkType)) : transaction;
    }
    /**
     * @override Transaction.size()
     * @description get the byte size of a SecretProofTransaction
     * @returns {number}
     * @memberof SecretProofTransaction
     */
    get size() {
        const byteSize = super.size;
        // hash algorithm and proof size static byte size
        const byteAlgorithm = 1;
        const byteProofSize = 2;
        const byteRecipient = 25;
        // convert secret and proof to uint8
        const byteSecret = format_1.Convert.hexToUint8(this.secret).length;
        const byteProof = format_1.Convert.hexToUint8(this.proof).length;
        return byteSize + byteAlgorithm + byteSecret + byteRecipient + byteProofSize + byteProof;
    }
    /**
     * @description Get secret bytes
     * @returns {Uint8Array}
     * @memberof SecretLockTransaction
     */
    getSecretByte() {
        return format_1.Convert.hexToUint8(64 > this.secret.length ? this.secret + '0'.repeat(64 - this.secret.length) : this.secret);
    }
    /**
     * @description Get proof bytes
     * @returns {Uint8Array}
     * @memberof SecretLockTransaction
     */
    getProofByte() {
        return format_1.Convert.hexToUint8(this.proof);
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateBytes() {
        const signerBuffer = new Uint8Array(32);
        const signatureBuffer = new Uint8Array(64);
        const transactionBuilder = new SecretProofTransactionBuilder_1.SecretProofTransactionBuilder(new SignatureDto_1.SignatureDto(signatureBuffer), new KeyDto_1.KeyDto(signerBuffer), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.SECRET_PROOF.valueOf(), new AmountDto_1.AmountDto(this.maxFee.toDTO()), new TimestampDto_1.TimestampDto(this.deadline.toDTO()), new Hash256Dto_1.Hash256Dto(this.getSecretByte()), this.hashType.valueOf(), new UnresolvedAddressDto_1.UnresolvedAddressDto(UnresolvedMapping_1.UnresolvedMapping.toUnresolvedAddressBytes(this.recipientAddress, this.networkType)), this.getProofByte());
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateEmbeddedBytes() {
        const transactionBuilder = new EmbeddedSecretProofTransactionBuilder_1.EmbeddedSecretProofTransactionBuilder(new KeyDto_1.KeyDto(format_1.Convert.hexToUint8(this.signer.publicKey)), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.SECRET_PROOF.valueOf(), new Hash256Dto_1.Hash256Dto(this.getSecretByte()), this.hashType.valueOf(), new UnresolvedAddressDto_1.UnresolvedAddressDto(UnresolvedMapping_1.UnresolvedMapping.toUnresolvedAddressBytes(this.recipientAddress, this.networkType)), this.getProofByte());
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @param statement Block receipt statement
     * @param aggregateTransactionIndex Transaction index for aggregated transaction
     * @returns {SecretProofTransaction}
     */
    resolveAliases(statement, aggregateTransactionIndex = 0) {
        const transactionInfo = this.checkTransactionHeightAndIndex();
        return Object.assign({}, Object.getPrototypeOf(this), { recipientAddress: statement.resolveAddress(this.recipientAddress, transactionInfo.height.toString(), transactionInfo.index, aggregateTransactionIndex) });
    }
}
exports.SecretProofTransaction = SecretProofTransaction;
//# sourceMappingURL=SecretProofTransaction.js.map