"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const crypto_1 = require("../../core/crypto");
const Convert_1 = require("../../core/format/Convert");
const CosignatureSignedTransaction_1 = require("./CosignatureSignedTransaction");
const Transaction_1 = require("./Transaction");
/**
 * Cosignature transaction is used to sign an aggregate transactions with missing cosignatures.
 */
class CosignatureTransaction {
    /**
     * @param transactionToCosign
     */
    constructor(/**
                 * Transaction to cosign.
                 */ transactionToCosign) {
        this.transactionToCosign = transactionToCosign;
    }
    /**
     * Create a cosignature transaction
     * @param transactionToCosign - Transaction to cosign.
     * @returns {CosignatureTransaction}
     */
    static create(transactionToCosign) {
        return new CosignatureTransaction(transactionToCosign);
    }
    /**
     * Co-sign transaction with transaction payload (off chain)
     * Creating a new CosignatureSignedTransaction
     * @param account - The signing account
     * @param payload - off transaction payload (aggregated transaction is unannounced)
     * @param generationHash - Network generation hash
     * @returns {CosignatureSignedTransaction}
     */
    static signTransactionPayload(account, payload, generationHash) {
        /**
         * For aggregated complete transaction, cosignatories are gathered off chain announced.
         */
        const transactionHash = Transaction_1.Transaction.createTransactionHash(payload, Array.from(Convert_1.Convert.hexToUint8(generationHash)), account.networkType);
        const hashBytes = Convert_1.Convert.hexToUint8(transactionHash);
        const signSchema = crypto_1.SHA3Hasher.resolveSignSchema(account.networkType);
        const signature = crypto_1.KeyPair.sign(account, new Uint8Array(hashBytes), signSchema);
        return new CosignatureSignedTransaction_1.CosignatureSignedTransaction(Convert_1.Convert.uint8ToHex(hashBytes), Convert_1.Convert.uint8ToHex(signature), account.publicKey);
    }
    /**
     * Serialize and sign transaction creating a new SignedTransaction
     * @param account
     * @returns {CosignatureSignedTransaction}
     */
    signWith(account) {
        if (!this.transactionToCosign.transactionInfo.hash) {
            throw new Error('Transaction to cosign should be announced first');
        }
        const hash = this.transactionToCosign.transactionInfo.hash;
        const hashBytes = Convert_1.Convert.hexToUint8(hash ? hash : '');
        const signSchema = crypto_1.SHA3Hasher.resolveSignSchema(account.networkType);
        const signature = crypto_1.KeyPair.sign(account, new Uint8Array(hashBytes), signSchema);
        return new CosignatureSignedTransaction_1.CosignatureSignedTransaction(hash ? hash : '', Convert_1.Convert.uint8ToHex(signature), account.publicKey);
    }
}
exports.CosignatureTransaction = CosignatureTransaction;
//# sourceMappingURL=CosignatureTransaction.js.map