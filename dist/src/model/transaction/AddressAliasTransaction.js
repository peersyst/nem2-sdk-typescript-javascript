"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const format_1 = require("../../core/format");
const AddressAliasTransactionBuilder_1 = require("../../infrastructure/catbuffer/AddressAliasTransactionBuilder");
const AddressDto_1 = require("../../infrastructure/catbuffer/AddressDto");
const AmountDto_1 = require("../../infrastructure/catbuffer/AmountDto");
const EmbeddedAddressAliasTransactionBuilder_1 = require("../../infrastructure/catbuffer/EmbeddedAddressAliasTransactionBuilder");
const KeyDto_1 = require("../../infrastructure/catbuffer/KeyDto");
const NamespaceIdDto_1 = require("../../infrastructure/catbuffer/NamespaceIdDto");
const SignatureDto_1 = require("../../infrastructure/catbuffer/SignatureDto");
const TimestampDto_1 = require("../../infrastructure/catbuffer/TimestampDto");
const Address_1 = require("../account/Address");
const PublicAccount_1 = require("../account/PublicAccount");
const NamespaceId_1 = require("../namespace/NamespaceId");
const UInt64_1 = require("../UInt64");
const Deadline_1 = require("./Deadline");
const Transaction_1 = require("./Transaction");
const TransactionType_1 = require("./TransactionType");
const TransactionVersion_1 = require("./TransactionVersion");
/**
 * In case a mosaic has the flag 'supplyMutable' set to true, the creator of the mosaic can change the supply,
 * i.e. increase or decrease the supply.
 */
class AddressAliasTransaction extends Transaction_1.Transaction {
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param maxFee
     * @param aliasAction
     * @param namespaceId
     * @param address
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    constructor(networkType, version, deadline, maxFee, 
    /**
     * The alias action type.
     */
    aliasAction, 
    /**
     * The namespace id that will be an alias.
     */
    namespaceId, 
    /**
     * The address.
     */
    address, signature, signer, transactionInfo) {
        super(TransactionType_1.TransactionType.ADDRESS_ALIAS, networkType, version, deadline, maxFee, signature, signer, transactionInfo);
        this.aliasAction = aliasAction;
        this.namespaceId = namespaceId;
        this.address = address;
    }
    /**
     * Create a address alias transaction object
     * @param deadline - The deadline to include the transaction.
     * @param aliasAction - The alias action type.
     * @param namespaceId - The namespace id.
     * @param address - The address.
     * @param networkType - The network type.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {AddressAliasTransaction}
     */
    static create(deadline, aliasAction, namespaceId, address, networkType, maxFee = new UInt64_1.UInt64([0, 0])) {
        return new AddressAliasTransaction(networkType, TransactionVersion_1.TransactionVersion.ADDRESS_ALIAS, deadline, maxFee, aliasAction, namespaceId, address);
    }
    /**
     * Create a transaction object from payload
     * @param {string} payload Binary payload
     * @param {Boolean} isEmbedded Is embedded transaction (Default: false)
     * @returns {Transaction | InnerTransaction}
     */
    static createFromPayload(payload, isEmbedded = false) {
        const builder = isEmbedded ? EmbeddedAddressAliasTransactionBuilder_1.EmbeddedAddressAliasTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload)) :
            AddressAliasTransactionBuilder_1.AddressAliasTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload));
        const signerPublicKey = format_1.Convert.uint8ToHex(builder.getSignerPublicKey().key);
        const networkType = builder.getNetwork().valueOf();
        const transaction = AddressAliasTransaction.create(isEmbedded ? Deadline_1.Deadline.create() : Deadline_1.Deadline.createFromDTO(builder.getDeadline().timestamp), builder.getAliasAction().valueOf(), new NamespaceId_1.NamespaceId(builder.getNamespaceId().namespaceId), Address_1.Address.createFromEncoded(format_1.Convert.uint8ToHex(builder.getAddress().address)), networkType, isEmbedded ? new UInt64_1.UInt64([0, 0]) : new UInt64_1.UInt64(builder.fee.amount));
        return isEmbedded ?
            transaction.toAggregate(PublicAccount_1.PublicAccount.createFromPublicKey(signerPublicKey, networkType)) : transaction;
    }
    /**
     * @override Transaction.size()
     * @description get the byte size of a AddressAliasTransaction
     * @returns {number}
     * @memberof AddressAliasTransaction
     */
    get size() {
        const byteSize = super.size;
        // set static byte size fields
        const byteActionType = 1;
        const byteNamespaceId = 8;
        const byteAddress = 25;
        return byteSize + byteActionType + byteNamespaceId + byteAddress;
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateBytes() {
        const signerBuffer = new Uint8Array(32);
        const signatureBuffer = new Uint8Array(64);
        const transactionBuilder = new AddressAliasTransactionBuilder_1.AddressAliasTransactionBuilder(new SignatureDto_1.SignatureDto(signatureBuffer), new KeyDto_1.KeyDto(signerBuffer), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.ADDRESS_ALIAS.valueOf(), new AmountDto_1.AmountDto(this.maxFee.toDTO()), new TimestampDto_1.TimestampDto(this.deadline.toDTO()), new NamespaceIdDto_1.NamespaceIdDto(this.namespaceId.id.toDTO()), new AddressDto_1.AddressDto(format_1.RawAddress.stringToAddress(this.address.plain())), this.aliasAction.valueOf());
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateEmbeddedBytes() {
        const transactionBuilder = new EmbeddedAddressAliasTransactionBuilder_1.EmbeddedAddressAliasTransactionBuilder(new KeyDto_1.KeyDto(format_1.Convert.hexToUint8(this.signer.publicKey)), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.ADDRESS_ALIAS.valueOf(), new NamespaceIdDto_1.NamespaceIdDto(this.namespaceId.id.toDTO()), new AddressDto_1.AddressDto(format_1.RawAddress.stringToAddress(this.address.plain())), this.aliasAction.valueOf());
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @returns {AddressAliasTransaction}
     */
    resolveAliases() {
        return this;
    }
}
exports.AddressAliasTransaction = AddressAliasTransaction;
//# sourceMappingURL=AddressAliasTransaction.js.map