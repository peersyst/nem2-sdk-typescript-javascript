"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const format_1 = require("../../core/format");
const AccountLinkTransactionBuilder_1 = require("../../infrastructure/catbuffer/AccountLinkTransactionBuilder");
const AmountDto_1 = require("../../infrastructure/catbuffer/AmountDto");
const EmbeddedAccountLinkTransactionBuilder_1 = require("../../infrastructure/catbuffer/EmbeddedAccountLinkTransactionBuilder");
const KeyDto_1 = require("../../infrastructure/catbuffer/KeyDto");
const SignatureDto_1 = require("../../infrastructure/catbuffer/SignatureDto");
const TimestampDto_1 = require("../../infrastructure/catbuffer/TimestampDto");
const PublicAccount_1 = require("../account/PublicAccount");
const UInt64_1 = require("../UInt64");
const Deadline_1 = require("./Deadline");
const Transaction_1 = require("./Transaction");
const TransactionType_1 = require("./TransactionType");
const TransactionVersion_1 = require("./TransactionVersion");
/**
 * Announce an AccountLinkTransaction to delegate the account importance to a proxy account.
 * By doing so, you can enable delegated harvesting
 */
class AccountLinkTransaction extends Transaction_1.Transaction {
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param maxFee
     * @param remotePublicKey
     * @param linkAction
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    constructor(networkType, version, deadline, maxFee, 
    /**
     * The public key of the remote account.
     */
    remotePublicKey, 
    /**
     * The account link action.
     */
    linkAction, signature, signer, transactionInfo) {
        super(TransactionType_1.TransactionType.LINK_ACCOUNT, networkType, version, deadline, maxFee, signature, signer, transactionInfo);
        this.remotePublicKey = remotePublicKey;
        this.linkAction = linkAction;
    }
    /**
     * Create a link account transaction object
     * @param deadline - The deadline to include the transaction.
     * @param remotePublicKey - The public key of the remote account.
     * @param linkAction - The account link action.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {AccountLinkTransaction}
     */
    static create(deadline, remotePublicKey, linkAction, networkType, maxFee = new UInt64_1.UInt64([0, 0])) {
        return new AccountLinkTransaction(networkType, TransactionVersion_1.TransactionVersion.LINK_ACCOUNT, deadline, maxFee, remotePublicKey, linkAction);
    }
    /**
     * Create a transaction object from payload
     * @param {string} payload Binary payload
     * @param {Boolean} isEmbedded Is embedded transaction (Default: false)
     * @returns {Transaction | InnerTransaction}
     */
    static createFromPayload(payload, isEmbedded = false) {
        const builder = isEmbedded ? EmbeddedAccountLinkTransactionBuilder_1.EmbeddedAccountLinkTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload)) :
            AccountLinkTransactionBuilder_1.AccountLinkTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload));
        const signerPublicKey = format_1.Convert.uint8ToHex(builder.getSignerPublicKey().key);
        const networkType = builder.getNetwork().valueOf();
        const transaction = AccountLinkTransaction.create(isEmbedded ? Deadline_1.Deadline.create() : Deadline_1.Deadline.createFromDTO(builder.getDeadline().timestamp), format_1.Convert.uint8ToHex(builder.getRemotePublicKey().key), builder.getLinkAction().valueOf(), networkType, isEmbedded ? new UInt64_1.UInt64([0, 0]) : new UInt64_1.UInt64(builder.fee.amount));
        return isEmbedded ?
            transaction.toAggregate(PublicAccount_1.PublicAccount.createFromPublicKey(signerPublicKey, networkType)) : transaction;
    }
    /**
     * @override Transaction.size()
     * @description get the byte size of a AccountLinkTransaction
     * @returns {number}
     * @memberof AccountLinkTransaction
     */
    get size() {
        const byteSize = super.size;
        // set static byte size fields
        const bytePublicKey = 32;
        const byteLinkAction = 1;
        return byteSize + bytePublicKey + byteLinkAction;
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateBytes() {
        const signerBuffer = new Uint8Array(32);
        const signatureBuffer = new Uint8Array(64);
        const transactionBuilder = new AccountLinkTransactionBuilder_1.AccountLinkTransactionBuilder(new SignatureDto_1.SignatureDto(signatureBuffer), new KeyDto_1.KeyDto(signerBuffer), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.LINK_ACCOUNT.valueOf(), new AmountDto_1.AmountDto(this.maxFee.toDTO()), new TimestampDto_1.TimestampDto(this.deadline.toDTO()), new KeyDto_1.KeyDto(format_1.Convert.hexToUint8(this.remotePublicKey)), this.linkAction.valueOf());
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateEmbeddedBytes() {
        const transactionBuilder = new EmbeddedAccountLinkTransactionBuilder_1.EmbeddedAccountLinkTransactionBuilder(new KeyDto_1.KeyDto(format_1.Convert.hexToUint8(this.signer.publicKey)), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.LINK_ACCOUNT.valueOf(), new KeyDto_1.KeyDto(format_1.Convert.hexToUint8(this.remotePublicKey)), this.linkAction.valueOf());
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @returns {AccountLinkTransaction}
     */
    resolveAliases() {
        return this;
    }
}
exports.AccountLinkTransaction = AccountLinkTransaction;
//# sourceMappingURL=AccountLinkTransaction.js.map