"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const Long = require("long");
const format_1 = require("../../core/format");
const UnresolvedMapping_1 = require("../../core/utils/UnresolvedMapping");
const AmountDto_1 = require("../../infrastructure/catbuffer/AmountDto");
const EmbeddedTransferTransactionBuilder_1 = require("../../infrastructure/catbuffer/EmbeddedTransferTransactionBuilder");
const GeneratorUtils_1 = require("../../infrastructure/catbuffer/GeneratorUtils");
const KeyDto_1 = require("../../infrastructure/catbuffer/KeyDto");
const SignatureDto_1 = require("../../infrastructure/catbuffer/SignatureDto");
const TimestampDto_1 = require("../../infrastructure/catbuffer/TimestampDto");
const TransferTransactionBuilder_1 = require("../../infrastructure/catbuffer/TransferTransactionBuilder");
const UnresolvedAddressDto_1 = require("../../infrastructure/catbuffer/UnresolvedAddressDto");
const UnresolvedMosaicBuilder_1 = require("../../infrastructure/catbuffer/UnresolvedMosaicBuilder");
const UnresolvedMosaicIdDto_1 = require("../../infrastructure/catbuffer/UnresolvedMosaicIdDto");
const PublicAccount_1 = require("../account/PublicAccount");
const EncryptedMessage_1 = require("../message/EncryptedMessage");
const MessageType_1 = require("../message/MessageType");
const PlainMessage_1 = require("../message/PlainMessage");
const Mosaic_1 = require("../mosaic/Mosaic");
const NamespaceId_1 = require("../namespace/NamespaceId");
const UInt64_1 = require("../UInt64");
const Deadline_1 = require("./Deadline");
const Transaction_1 = require("./Transaction");
const TransactionType_1 = require("./TransactionType");
const TransactionVersion_1 = require("./TransactionVersion");
/**
 * Transfer transactions contain data about transfers of mosaics and message to another account.
 */
class TransferTransaction extends Transaction_1.Transaction {
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param maxFee
     * @param recipientAddress
     * @param mosaics
     * @param message
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    constructor(networkType, version, deadline, maxFee, 
    /**
     * The address of the recipient address.
     */
    recipientAddress, 
    /**
     * The array of Mosaic objects.
     */
    mosaics, 
    /**
     * The transaction message of 2048 characters.
     */
    message, signature, signer, transactionInfo) {
        super(TransactionType_1.TransactionType.TRANSFER, networkType, version, deadline, maxFee, signature, signer, transactionInfo);
        this.recipientAddress = recipientAddress;
        this.mosaics = mosaics;
        this.message = message;
        this.validate();
    }
    /**
     * Create a transfer transaction object.
     *
     * - This method can also be used to create PersistentDelegationRequestTransaction
     * with `PersistentHarvestingDelegationMessage` provided.
     * @param deadline - The deadline to include the transaction.
     * @param recipientAddress - The recipient address of the transaction.
     * @param mosaics - The array of mosaics.
     * @param message - The transaction message.
     * @param networkType - The network type.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {TransferTransaction}
     */
    static create(deadline, recipientAddress, mosaics, message, networkType, maxFee = new UInt64_1.UInt64([0, 0])) {
        return new TransferTransaction(networkType, TransactionVersion_1.TransactionVersion.TRANSFER, deadline, maxFee, recipientAddress, mosaics, message);
    }
    /**
     * Create a transaction object from payload
     * @param {string} payload Binary payload
     * @param {Boolean} isEmbedded Is embedded transaction (Default: false)
     * @returns {Transaction | InnerTransaction}
     */
    static createFromPayload(payload, isEmbedded = false) {
        const builder = isEmbedded ? EmbeddedTransferTransactionBuilder_1.EmbeddedTransferTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload)) :
            TransferTransactionBuilder_1.TransferTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload));
        const messageType = builder.getMessage()[0];
        const messageHex = format_1.Convert.uint8ToHex(builder.getMessage()).substring(2);
        const signerPublicKey = format_1.Convert.uint8ToHex(builder.getSignerPublicKey().key);
        const networkType = builder.getNetwork().valueOf();
        const transaction = TransferTransaction.create(isEmbedded ? Deadline_1.Deadline.create() : Deadline_1.Deadline.createFromDTO(builder.getDeadline().timestamp), UnresolvedMapping_1.UnresolvedMapping.toUnresolvedAddress(format_1.Convert.uint8ToHex(builder.getRecipientAddress().unresolvedAddress)), builder.getMosaics().map((mosaic) => {
            const id = new UInt64_1.UInt64(mosaic.mosaicId.unresolvedMosaicId).toHex();
            return new Mosaic_1.Mosaic(UnresolvedMapping_1.UnresolvedMapping.toUnresolvedMosaic(id), new UInt64_1.UInt64(mosaic.amount.amount));
        }), messageType === MessageType_1.MessageType.PlainMessage ?
            PlainMessage_1.PlainMessage.createFromPayload(messageHex) :
            EncryptedMessage_1.EncryptedMessage.createFromPayload(messageHex), networkType, isEmbedded ? new UInt64_1.UInt64([0, 0]) : new UInt64_1.UInt64(builder.fee.amount));
        return isEmbedded ?
            transaction.toAggregate(PublicAccount_1.PublicAccount.createFromPublicKey(signerPublicKey, networkType)) : transaction;
    }
    /**
     * Validate Transfer transaction creation with provided message
     * @internal
     */
    validate() {
        if (this.message.type === MessageType_1.MessageType.PersistentHarvestingDelegationMessage) {
            if (this.mosaics.length > 0) {
                throw new Error('PersistentDelegationRequestTransaction should be created without Mosaic');
            }
            else if (!/^[0-9a-fA-F]{208}$/.test(this.message.payload)) {
                throw new Error('PersistentDelegationRequestTransaction message is invalid');
            }
        }
    }
    /**
     * Return the string notation for the set recipient
     * @internal
     * @returns {string}
     */
    recipientToString() {
        if (this.recipientAddress instanceof NamespaceId_1.NamespaceId) {
            // namespaceId recipient, return hexadecimal notation
            return this.recipientAddress.toHex();
        }
        // address recipient
        return this.recipientAddress.plain();
    }
    /**
     * Return sorted mosaic arrays
     * @internal
     * @returns {Mosaic[]}
     */
    sortMosaics() {
        return this.mosaics.sort((a, b) => {
            const long_a = Long.fromBits(a.id.id.lower, a.id.id.higher, true);
            const long_b = Long.fromBits(b.id.id.lower, b.id.id.higher, true);
            return long_a.compare(long_b);
        });
    }
    /**
     * Return message buffer
     * @internal
     * @returns {Uint8Array}
     */
    getMessageBuffer() {
        const messgeHex = this.message.type === MessageType_1.MessageType.PersistentHarvestingDelegationMessage ?
            this.message.payload : format_1.Convert.utf8ToHex(this.message.payload);
        const payloadBuffer = format_1.Convert.hexToUint8(messgeHex);
        const typeBuffer = GeneratorUtils_1.GeneratorUtils.uintToBuffer(this.message.type, 1);
        return this.message.type === MessageType_1.MessageType.PersistentHarvestingDelegationMessage ?
            payloadBuffer : GeneratorUtils_1.GeneratorUtils.concatTypedArrays(typeBuffer, payloadBuffer);
    }
    /**
     * @override Transaction.size()
     * @description get the byte size of a TransferTransaction
     * @returns {number}
     * @memberof TransferTransaction
     */
    get size() {
        const byteSize = super.size;
        // recipient and number of mosaics are static byte size
        const byteRecipientAddress = 25;
        const byteMosaicsCount = 1;
        const byteMessageSize = 2;
        const byteTransferTransactionBody_Reserved1 = 4;
        // read message payload size
        const bytePayload = this.getMessageBuffer().length;
        // mosaicId / namespaceId are written on 8 bytes + 8 bytes for the amount.
        const byteMosaics = (8 + 8) * this.mosaics.length;
        return byteSize + byteMosaicsCount + byteRecipientAddress +
            +byteTransferTransactionBody_Reserved1 + byteMessageSize + bytePayload + byteMosaics;
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateBytes() {
        const signerBuffer = new Uint8Array(32);
        const signatureBuffer = new Uint8Array(64);
        const transactionBuilder = new TransferTransactionBuilder_1.TransferTransactionBuilder(new SignatureDto_1.SignatureDto(signatureBuffer), new KeyDto_1.KeyDto(signerBuffer), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.TRANSFER.valueOf(), new AmountDto_1.AmountDto(this.maxFee.toDTO()), new TimestampDto_1.TimestampDto(this.deadline.toDTO()), new UnresolvedAddressDto_1.UnresolvedAddressDto(UnresolvedMapping_1.UnresolvedMapping.toUnresolvedAddressBytes(this.recipientAddress, this.networkType)), this.sortMosaics().map((mosaic) => {
            return new UnresolvedMosaicBuilder_1.UnresolvedMosaicBuilder(new UnresolvedMosaicIdDto_1.UnresolvedMosaicIdDto(mosaic.id.id.toDTO()), new AmountDto_1.AmountDto(mosaic.amount.toDTO()));
        }), this.getMessageBuffer());
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateEmbeddedBytes() {
        const transactionBuilder = new EmbeddedTransferTransactionBuilder_1.EmbeddedTransferTransactionBuilder(new KeyDto_1.KeyDto(format_1.Convert.hexToUint8(this.signer.publicKey)), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.TRANSFER.valueOf(), new UnresolvedAddressDto_1.UnresolvedAddressDto(UnresolvedMapping_1.UnresolvedMapping.toUnresolvedAddressBytes(this.recipientAddress, this.networkType)), this.sortMosaics().map((mosaic) => {
            return new UnresolvedMosaicBuilder_1.UnresolvedMosaicBuilder(new UnresolvedMosaicIdDto_1.UnresolvedMosaicIdDto(mosaic.id.id.toDTO()), new AmountDto_1.AmountDto(mosaic.amount.toDTO()));
        }), this.getMessageBuffer());
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @param statement Block receipt statement
     * @param aggregateTransactionIndex Transaction index for aggregated transaction
     * @returns {TransferTransaction}
     */
    resolveAliases(statement, aggregateTransactionIndex = 0) {
        const transactionInfo = this.checkTransactionHeightAndIndex();
        return Object.assign({}, Object.getPrototypeOf(this), { recipientAddress: statement.resolveAddress(this.recipientAddress, transactionInfo.height.toString(), transactionInfo.index, aggregateTransactionIndex), mosaics: this.mosaics.map((mosaic) => statement.resolveMosaic(mosaic, transactionInfo.height.toString(), transactionInfo.index, aggregateTransactionIndex)) });
    }
}
exports.TransferTransaction = TransferTransaction;
//# sourceMappingURL=TransferTransaction.js.map