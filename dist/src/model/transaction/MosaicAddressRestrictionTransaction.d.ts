import { Address } from '../account/Address';
import { PublicAccount } from '../account/PublicAccount';
import { NetworkType } from '../blockchain/NetworkType';
import { MosaicId } from '../mosaic/MosaicId';
import { NamespaceId } from '../namespace/NamespaceId';
import { UInt64 } from '../UInt64';
import { Deadline } from './Deadline';
import { InnerTransaction } from './InnerTransaction';
import { Transaction } from './Transaction';
import { TransactionInfo } from './TransactionInfo';
export declare class MosaicAddressRestrictionTransaction extends Transaction {
    /**
     * The mosaic id.
     */
    readonly mosaicId: MosaicId | NamespaceId;
    /**
     * The restriction key.
     */
    readonly restrictionKey: UInt64;
    /**
     * The affected unresolved address.
     */
    readonly targetAddress: Address | NamespaceId;
    /**
     * The previous restriction value.
     */
    readonly previousRestrictionValue: UInt64;
    /**
     * The new restriction value.
     */
    readonly newRestrictionValue: UInt64;
    /**
     * Create a mosaic address restriction transaction object
     *
     * Enabling accounts to transact with the token is similar to the process of
     * adding elevated permissions to a user in a company computer network.
     *
     * The mosaic creator can modify the permissions of an account by sending a
     * mosaic restriction transaction targeting the account address.
     *
     * **MosaicAddressRestrictionTransaction can only be announced in with Aggregate Transaction
     *
     * @param deadline - The deadline to include the transaction.
     * @param mosaicId - The unresolved mosaic identifier.
     * @param restrictionKey - The restriction key.
     * @param targetAddress - The affected unresolved address.
     * @param newRestrictionValue - The new restriction value.
     * @param networkType - The network type.
     * @param previousRestrictionValue - (Optional) The previous restriction value.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {MosaicAddressRestrictionTransaction}
     */
    static create(deadline: Deadline, mosaicId: MosaicId | NamespaceId, restrictionKey: UInt64, targetAddress: Address | NamespaceId, newRestrictionValue: UInt64, networkType: NetworkType, previousRestrictionValue?: UInt64, maxFee?: UInt64): MosaicAddressRestrictionTransaction;
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param maxFee
     * @param mosaicId
     * @param signature
     * @param restrictionKey
     * @param targetAddress
     * @param previousRestrictionValue
     * @param newRestrictionValue
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    constructor(networkType: NetworkType, version: number, deadline: Deadline, maxFee: UInt64, 
        /**
         * The mosaic id.
         */
        mosaicId: MosaicId | NamespaceId, 
        /**
         * The restriction key.
         */
        restrictionKey: UInt64, 
        /**
         * The affected unresolved address.
         */
        targetAddress: Address | NamespaceId, 
        /**
         * The previous restriction value.
         */
        previousRestrictionValue: UInt64, 
        /**
         * The new restriction value.
         */
        newRestrictionValue: UInt64, signature?: string, signer?: PublicAccount, transactionInfo?: TransactionInfo);
    /**
     * Create a transaction object from payload
     * @param {string} payload Binary payload
     * @param {Boolean} isEmbedded Is embedded transaction (Default: false)
     * @returns {Transaction | InnerTransaction}
     */
    static createFromPayload(payload: string, isEmbedded?: boolean): Transaction | InnerTransaction;
    /**
     * @override Transaction.size()
     * @description get the byte size of a MosaicDefinitionTransaction
     * @returns {number}
     * @memberof MosaicAddressRestrictionTransaction
     */
    readonly size: number;
}
