"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const format_1 = require("../../core/format");
const AmountDto_1 = require("../../infrastructure/catbuffer/AmountDto");
const BlockDurationDto_1 = require("../../infrastructure/catbuffer/BlockDurationDto");
const EmbeddedHashLockTransactionBuilder_1 = require("../../infrastructure/catbuffer/EmbeddedHashLockTransactionBuilder");
const Hash256Dto_1 = require("../../infrastructure/catbuffer/Hash256Dto");
const HashLockTransactionBuilder_1 = require("../../infrastructure/catbuffer/HashLockTransactionBuilder");
const KeyDto_1 = require("../../infrastructure/catbuffer/KeyDto");
const SignatureDto_1 = require("../../infrastructure/catbuffer/SignatureDto");
const TimestampDto_1 = require("../../infrastructure/catbuffer/TimestampDto");
const UnresolvedMosaicBuilder_1 = require("../../infrastructure/catbuffer/UnresolvedMosaicBuilder");
const UnresolvedMosaicIdDto_1 = require("../../infrastructure/catbuffer/UnresolvedMosaicIdDto");
const PublicAccount_1 = require("../account/PublicAccount");
const Mosaic_1 = require("../mosaic/Mosaic");
const MosaicId_1 = require("../mosaic/MosaicId");
const UInt64_1 = require("../UInt64");
const Deadline_1 = require("./Deadline");
const SignedTransaction_1 = require("./SignedTransaction");
const Transaction_1 = require("./Transaction");
const TransactionType_1 = require("./TransactionType");
const TransactionVersion_1 = require("./TransactionVersion");
/**
 * Lock funds transaction is used before sending an Aggregate bonded transaction, as a deposit to announce the transaction.
 * When aggregate bonded transaction is confirmed funds are returned to LockFundsTransaction signer.
 *
 * @since 1.0
 */
class LockFundsTransaction extends Transaction_1.Transaction {
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param maxFee
     * @param mosaic
     * @param duration
     * @param signedTransaction
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    constructor(networkType, version, deadline, maxFee, 
    /**
     * The locked mosaic.
     */
    mosaic, 
    /**
     * The funds lock duration.
     */
    duration, signedTransaction, signature, signer, transactionInfo) {
        super(TransactionType_1.TransactionType.LOCK, networkType, version, deadline, maxFee, signature, signer, transactionInfo);
        this.mosaic = mosaic;
        this.duration = duration;
        this.hash = signedTransaction.hash;
        this.signedTransaction = signedTransaction;
        if (signedTransaction.type !== TransactionType_1.TransactionType.AGGREGATE_BONDED) {
            throw new Error('Signed transaction must be Aggregate Bonded Transaction');
        }
    }
    /**
     * Create a Lock funds transaction object
     * @param deadline - The deadline to include the transaction.
     * @param mosaic - The locked mosaic.
     * @param duration - The funds lock duration.
     * @param signedTransaction - The signed transaction for which funds are locked.
     * @param networkType - The network type.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {LockFundsTransaction}
     */
    static create(deadline, mosaic, duration, signedTransaction, networkType, maxFee = new UInt64_1.UInt64([0, 0])) {
        return new LockFundsTransaction(networkType, TransactionVersion_1.TransactionVersion.LOCK, deadline, maxFee, mosaic, duration, signedTransaction);
    }
    /**
     * Create a transaction object from payload
     * @param {string} payload Binary payload
     * @param {Boolean} isEmbedded Is embedded transaction (Default: false)
     * @returns {Transaction | InnerTransaction}
     */
    static createFromPayload(payload, isEmbedded = false) {
        const builder = isEmbedded ? EmbeddedHashLockTransactionBuilder_1.EmbeddedHashLockTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload)) :
            HashLockTransactionBuilder_1.HashLockTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload));
        const signerPublicKey = format_1.Convert.uint8ToHex(builder.getSignerPublicKey().key);
        const networkType = builder.getNetwork().valueOf();
        const transaction = LockFundsTransaction.create(isEmbedded ? Deadline_1.Deadline.create() : Deadline_1.Deadline.createFromDTO(builder.getDeadline().timestamp), new Mosaic_1.Mosaic(new MosaicId_1.MosaicId(builder.getMosaic().mosaicId.unresolvedMosaicId), new UInt64_1.UInt64(builder.getMosaic().amount.amount)), new UInt64_1.UInt64(builder.getDuration().blockDuration), new SignedTransaction_1.SignedTransaction('', format_1.Convert.uint8ToHex(builder.getHash().hash256), '', TransactionType_1.TransactionType.AGGREGATE_BONDED, networkType), networkType, isEmbedded ? new UInt64_1.UInt64([0, 0]) : new UInt64_1.UInt64(builder.fee.amount));
        return isEmbedded ?
            transaction.toAggregate(PublicAccount_1.PublicAccount.createFromPublicKey(signerPublicKey, networkType)) : transaction;
    }
    /**
     * @override Transaction.size()
     * @description get the byte size of a LockFundsTransaction
     * @returns {number}
     * @memberof LockFundsTransaction
     */
    get size() {
        const byteSize = super.size;
        // set static byte size fields
        const byteMosaicId = 8;
        const byteAmount = 8;
        const byteDuration = 8;
        const byteHash = 32;
        return byteSize + byteMosaicId + byteAmount + byteDuration + byteHash;
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateBytes() {
        const signerBuffer = new Uint8Array(32);
        const signatureBuffer = new Uint8Array(64);
        const transactionBuilder = new HashLockTransactionBuilder_1.HashLockTransactionBuilder(new SignatureDto_1.SignatureDto(signatureBuffer), new KeyDto_1.KeyDto(signerBuffer), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.LOCK.valueOf(), new AmountDto_1.AmountDto(this.maxFee.toDTO()), new TimestampDto_1.TimestampDto(this.deadline.toDTO()), new UnresolvedMosaicBuilder_1.UnresolvedMosaicBuilder(new UnresolvedMosaicIdDto_1.UnresolvedMosaicIdDto(this.mosaic.id.id.toDTO()), new AmountDto_1.AmountDto(this.mosaic.amount.toDTO())), new BlockDurationDto_1.BlockDurationDto(this.duration.toDTO()), new Hash256Dto_1.Hash256Dto(format_1.Convert.hexToUint8(this.hash)));
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateEmbeddedBytes() {
        const transactionBuilder = new EmbeddedHashLockTransactionBuilder_1.EmbeddedHashLockTransactionBuilder(new KeyDto_1.KeyDto(format_1.Convert.hexToUint8(this.signer.publicKey)), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.LOCK.valueOf(), new UnresolvedMosaicBuilder_1.UnresolvedMosaicBuilder(new UnresolvedMosaicIdDto_1.UnresolvedMosaicIdDto(this.mosaic.id.id.toDTO()), new AmountDto_1.AmountDto(this.mosaic.amount.toDTO())), new BlockDurationDto_1.BlockDurationDto(this.duration.toDTO()), new Hash256Dto_1.Hash256Dto(format_1.Convert.hexToUint8(this.hash)));
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @param statement Block receipt statement
     * @param aggregateTransactionIndex Transaction index for aggregated transaction
     * @returns {LockFundsTransaction}
     */
    resolveAliases(statement, aggregateTransactionIndex = 0) {
        const transactionInfo = this.checkTransactionHeightAndIndex();
        return Object.assign({}, Object.getPrototypeOf(this), { mosaic: statement.resolveMosaic(this.mosaic, transactionInfo.height.toString(), transactionInfo.index, aggregateTransactionIndex) });
    }
}
exports.LockFundsTransaction = LockFundsTransaction;
//# sourceMappingURL=LockFundsTransaction.js.map