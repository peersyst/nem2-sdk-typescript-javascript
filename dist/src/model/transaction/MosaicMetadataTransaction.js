"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const format_1 = require("../../core/format");
const UnresolvedMapping_1 = require("../../core/utils/UnresolvedMapping");
const AmountDto_1 = require("../../infrastructure/catbuffer/AmountDto");
const EmbeddedMosaicMetadataTransactionBuilder_1 = require("../../infrastructure/catbuffer/EmbeddedMosaicMetadataTransactionBuilder");
const KeyDto_1 = require("../../infrastructure/catbuffer/KeyDto");
const MosaicMetadataTransactionBuilder_1 = require("../../infrastructure/catbuffer/MosaicMetadataTransactionBuilder");
const SignatureDto_1 = require("../../infrastructure/catbuffer/SignatureDto");
const TimestampDto_1 = require("../../infrastructure/catbuffer/TimestampDto");
const UnresolvedMosaicIdDto_1 = require("../../infrastructure/catbuffer/UnresolvedMosaicIdDto");
const PublicAccount_1 = require("../account/PublicAccount");
const UInt64_1 = require("../UInt64");
const Deadline_1 = require("./Deadline");
const Transaction_1 = require("./Transaction");
const TransactionType_1 = require("./TransactionType");
const TransactionVersion_1 = require("./TransactionVersion");
/**
 * Announce an mosaic metadata transaction to associate a key-value state to an account.
 */
class MosaicMetadataTransaction extends Transaction_1.Transaction {
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param maxFee
     * @param targetPublicKey
     * @param scopedMetadataKey
     * @param targetMosaicId
     * @param valueSizeDelta
     * @param value
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    constructor(networkType, version, deadline, maxFee, 
    /**
     * Public key of the target account.
     */
    targetPublicKey, 
    /**
     * Metadata key scoped to source, target and type.
     */
    scopedMetadataKey, 
    /**
     * Target mosaic identifier.
     */
    targetMosaicId, 
    /**
     * Change in value size in bytes.
     */
    valueSizeDelta, 
    /**
     * String value with UTF-8 encoding.
     * Difference between the previous value and new value.
     */
    value, signature, signer, transactionInfo) {
        super(TransactionType_1.TransactionType.MOSAIC_METADATA_TRANSACTION, networkType, version, deadline, maxFee, signature, signer, transactionInfo);
        this.targetPublicKey = targetPublicKey;
        this.scopedMetadataKey = scopedMetadataKey;
        this.targetMosaicId = targetMosaicId;
        this.valueSizeDelta = valueSizeDelta;
        this.value = value;
        if (value.length > 1024) {
            throw new Error('The maximum value size is 1024');
        }
    }
    /**
     * Create a mosaic meta data transaction object
     * @param deadline - transaction deadline
     * @param targetPublicKey - Public key of the target account.
     * @param scopedMetadataKey - Metadata key scoped to source, target and type.
     * @param targetMosaicId - Target unresolved mosaic identifier.
     * @param valueSizeDelta - Change in value size in bytes.
     * @param value - String value with UTF-8 encoding
     *                Difference between the previous value and new value.
     *                You can calculate value as xor(previous-value, new-value).
     *                If there is no previous value, use directly the new value.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {MosaicMetadataTransaction}
     */
    static create(deadline, targetPublicKey, scopedMetadataKey, targetMosaicId, valueSizeDelta, value, networkType, maxFee = new UInt64_1.UInt64([0, 0])) {
        return new MosaicMetadataTransaction(networkType, TransactionVersion_1.TransactionVersion.MOSAIC_METADATA_TRANSACTION, deadline, maxFee, targetPublicKey, scopedMetadataKey, targetMosaicId, valueSizeDelta, value);
    }
    /**
     * Create a transaction object from payload
     * @param {string} payload Binary payload
     * @param {Boolean} isEmbedded Is embedded transaction (Default: false)
     * @returns {Transaction | InnerTransaction}
     */
    static createFromPayload(payload, isEmbedded = false) {
        const builder = isEmbedded ? EmbeddedMosaicMetadataTransactionBuilder_1.EmbeddedMosaicMetadataTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload)) :
            MosaicMetadataTransactionBuilder_1.MosaicMetadataTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload));
        const signerPublicKey = format_1.Convert.uint8ToHex(builder.getSignerPublicKey().key);
        const networkType = builder.getNetwork().valueOf();
        const transaction = MosaicMetadataTransaction.create(isEmbedded ? Deadline_1.Deadline.create() : Deadline_1.Deadline.createFromDTO(builder.getDeadline().timestamp), format_1.Convert.uint8ToHex(builder.getTargetPublicKey().key), new UInt64_1.UInt64(builder.getScopedMetadataKey()), UnresolvedMapping_1.UnresolvedMapping.toUnresolvedMosaic(new UInt64_1.UInt64(builder.getTargetMosaicId().unresolvedMosaicId).toHex()), builder.getValueSizeDelta(), format_1.Convert.uint8ToUtf8(builder.getValue()), networkType, isEmbedded ? new UInt64_1.UInt64([0, 0]) : new UInt64_1.UInt64(builder.fee.amount));
        return isEmbedded ?
            transaction.toAggregate(PublicAccount_1.PublicAccount.createFromPublicKey(signerPublicKey, networkType)) : transaction;
    }
    /**
     * @override Transaction.size()
     * @description get the byte size of a AccountLinkTransaction
     * @returns {number}
     * @memberof AccountLinkTransaction
     */
    get size() {
        const byteSize = super.size;
        // set static byte size fields
        const targetPublicKey = 32;
        const byteScopedMetadataKey = 8;
        const byteTargetMosaicId = 8;
        const byteValueSizeDelta = 2;
        const valueSize = 2;
        return byteSize + targetPublicKey + byteScopedMetadataKey +
            byteTargetMosaicId + byteValueSizeDelta + valueSize + this.value.length;
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateBytes() {
        const signerBuffer = new Uint8Array(32);
        const signatureBuffer = new Uint8Array(64);
        const transactionBuilder = new MosaicMetadataTransactionBuilder_1.MosaicMetadataTransactionBuilder(new SignatureDto_1.SignatureDto(signatureBuffer), new KeyDto_1.KeyDto(signerBuffer), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.MOSAIC_METADATA_TRANSACTION.valueOf(), new AmountDto_1.AmountDto(this.maxFee.toDTO()), new TimestampDto_1.TimestampDto(this.deadline.toDTO()), new KeyDto_1.KeyDto(format_1.Convert.hexToUint8(this.targetPublicKey)), this.scopedMetadataKey.toDTO(), new UnresolvedMosaicIdDto_1.UnresolvedMosaicIdDto(this.targetMosaicId.id.toDTO()), this.valueSizeDelta, format_1.Convert.utf8ToUint8(this.value));
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateEmbeddedBytes() {
        const transactionBuilder = new EmbeddedMosaicMetadataTransactionBuilder_1.EmbeddedMosaicMetadataTransactionBuilder(new KeyDto_1.KeyDto(format_1.Convert.hexToUint8(this.signer.publicKey)), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.MOSAIC_METADATA_TRANSACTION.valueOf(), new KeyDto_1.KeyDto(format_1.Convert.hexToUint8(this.targetPublicKey)), this.scopedMetadataKey.toDTO(), new UnresolvedMosaicIdDto_1.UnresolvedMosaicIdDto(this.targetMosaicId.id.toDTO()), this.valueSizeDelta, format_1.Convert.utf8ToUint8(this.value));
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @param statement Block receipt statement
     * @param aggregateTransactionIndex Transaction index for aggregated transaction
     * @returns {MosaicMetadataTransaction}
     */
    resolveAliases(statement, aggregateTransactionIndex = 0) {
        const transactionInfo = this.checkTransactionHeightAndIndex();
        return Object.assign({}, Object.getPrototypeOf(this), { targetMosaicId: statement.resolveMosaicId(this.targetMosaicId, transactionInfo.height.toString(), transactionInfo.index, aggregateTransactionIndex) });
    }
}
exports.MosaicMetadataTransaction = MosaicMetadataTransaction;
//# sourceMappingURL=MosaicMetadataTransaction.js.map