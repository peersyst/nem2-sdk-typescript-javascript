import { PublicAccount } from '../account/PublicAccount';
import { NetworkType } from '../blockchain/NetworkType';
import { NamespaceId } from '../namespace/NamespaceId';
import { UInt64 } from '../UInt64';
import { Deadline } from './Deadline';
import { InnerTransaction } from './InnerTransaction';
import { Transaction } from './Transaction';
import { TransactionInfo } from './TransactionInfo';
/**
 * Announce an namespace metadata transaction to associate a key-value state to an account.
 */
export declare class NamespaceMetadataTransaction extends Transaction {
    /**
     * Public key of the target account.
     */
    readonly targetPublicKey: string;
    /**
     * Metadata key scoped to source, target and type.
     */
    readonly scopedMetadataKey: UInt64;
    /**
     * Target namespace identifier.
     */
    readonly targetNamespaceId: NamespaceId;
    /**
     * Change in value size in bytes.
     */
    readonly valueSizeDelta: number;
    /**
     * String value with UTF-8 encoding.
     * Difference between the previous value and new value.
     */
    readonly value: string;
    /**
     * Create a mosaic meta data transaction object
     * @param deadline - transaction deadline
     * @param targetPublicKey - Public key of the target account.
     * @param scopedMetadataKey - Metadata key scoped to source, target and type.
     * @param targetNamespaceId - Target namespace identifier.
     * @param valueSizeDelta - Change in value size in bytes.
     * @param value - String value with UTF-8 encoding
     *                Difference between the previous value and new value.
     *                You can calculate value as xor(previous-value, new-value).
     *                If there is no previous value, use directly the new value.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {NamespaceMetadataTransaction}
     */
    static create(deadline: Deadline, targetPublicKey: string, scopedMetadataKey: UInt64, targetNamespaceId: NamespaceId, valueSizeDelta: number, value: string, networkType: NetworkType, maxFee?: UInt64): NamespaceMetadataTransaction;
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param maxFee
     * @param targetPublicKey
     * @param scopedMetadataKey
     * @param targetNamespaceId
     * @param valueSizeDelta
     * @param value
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    constructor(networkType: NetworkType, version: number, deadline: Deadline, maxFee: UInt64, 
        /**
         * Public key of the target account.
         */
        targetPublicKey: string, 
        /**
         * Metadata key scoped to source, target and type.
         */
        scopedMetadataKey: UInt64, 
        /**
         * Target namespace identifier.
         */
        targetNamespaceId: NamespaceId, 
        /**
         * Change in value size in bytes.
         */
        valueSizeDelta: number, 
        /**
         * String value with UTF-8 encoding.
         * Difference between the previous value and new value.
         */
        value: string, signature?: string, signer?: PublicAccount, transactionInfo?: TransactionInfo);
    /**
     * Create a transaction object from payload
     * @param {string} payload Binary payload
     * @param {Boolean} isEmbedded Is embedded transaction (Default: false)
     * @returns {Transaction | InnerTransaction}
     */
    static createFromPayload(payload: string, isEmbedded?: boolean): Transaction | InnerTransaction;
    /**
     * @override Transaction.size()
     * @description get the byte size of a AccountLinkTransaction
     * @returns {number}
     * @memberof AccountLinkTransaction
     */
    readonly size: number;
}
