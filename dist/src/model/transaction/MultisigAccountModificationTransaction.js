"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const format_1 = require("../../core/format");
const AmountDto_1 = require("../../infrastructure/catbuffer/AmountDto");
const EmbeddedMultisigAccountModificationTransactionBuilder_1 = require("../../infrastructure/catbuffer/EmbeddedMultisigAccountModificationTransactionBuilder");
const KeyDto_1 = require("../../infrastructure/catbuffer/KeyDto");
const MultisigAccountModificationTransactionBuilder_1 = require("../../infrastructure/catbuffer/MultisigAccountModificationTransactionBuilder");
const SignatureDto_1 = require("../../infrastructure/catbuffer/SignatureDto");
const TimestampDto_1 = require("../../infrastructure/catbuffer/TimestampDto");
const PublicAccount_1 = require("../account/PublicAccount");
const UInt64_1 = require("../UInt64");
const Deadline_1 = require("./Deadline");
const Transaction_1 = require("./Transaction");
const TransactionType_1 = require("./TransactionType");
const TransactionVersion_1 = require("./TransactionVersion");
/**
 * Modify multisig account transactions are part of the NEM's multisig account system.
 * A modify multisig account transaction holds an array of multisig cosignatory modifications,
 * min number of signatures to approve a transaction and a min number of signatures to remove a cosignatory.
 * @since 1.0
 */
class MultisigAccountModificationTransaction extends Transaction_1.Transaction {
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param maxFee
     * @param minApprovalDelta
     * @param minRemovalDelta
     * @param publicKeyAdditions
     * @param publicKeyDeletions
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    constructor(networkType, version, deadline, maxFee, 
    /**
     * The number of signatures needed to approve a transaction.
     * If we are modifying and existing multi-signature account this indicates the relative change of the minimum cosignatories.
     */
    minApprovalDelta, 
    /**
     * The number of signatures needed to remove a cosignatory.
     * If we are modifying and existing multi-signature account this indicates the relative change of the minimum cosignatories.
     */
    minRemovalDelta, 
    /**
     * The Cosignatory public key additions.
     */
    publicKeyAdditions, 
    /**
     * The Cosignatory public key deletion.
     */
    publicKeyDeletions, signature, signer, transactionInfo) {
        super(TransactionType_1.TransactionType.MODIFY_MULTISIG_ACCOUNT, networkType, version, deadline, maxFee, signature, signer, transactionInfo);
        this.minApprovalDelta = minApprovalDelta;
        this.minRemovalDelta = minRemovalDelta;
        this.publicKeyAdditions = publicKeyAdditions;
        this.publicKeyDeletions = publicKeyDeletions;
    }
    /**
     * Create a modify multisig account transaction object
     * @param deadline - The deadline to include the transaction.
     * @param minApprovalDelta - The min approval relative change.
     * @param minRemovalDelta - The min removal relative change.
     * @param publicKeyAdditions - Cosignatory public key additions.
     * @param publicKeyDeletions - Cosignatory public key deletions.
     * @param networkType - The network type.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {MultisigAccountModificationTransaction}
     */
    static create(deadline, minApprovalDelta, minRemovalDelta, publicKeyAdditions, publicKeyDeletions, networkType, maxFee = new UInt64_1.UInt64([0, 0])) {
        return new MultisigAccountModificationTransaction(networkType, TransactionVersion_1.TransactionVersion.MODIFY_MULTISIG_ACCOUNT, deadline, maxFee, minApprovalDelta, minRemovalDelta, publicKeyAdditions, publicKeyDeletions);
    }
    /**
     * Create a transaction object from payload
     * @param {string} payload Binary payload
     * @param {Boolean} isEmbedded Is embedded transaction (Default: false)
     * @returns {Transaction | InnerTransaction}
     */
    static createFromPayload(payload, isEmbedded = false) {
        const builder = isEmbedded ? EmbeddedMultisigAccountModificationTransactionBuilder_1.EmbeddedMultisigAccountModificationTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload)) :
            MultisigAccountModificationTransactionBuilder_1.MultisigAccountModificationTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload));
        const signerPublicKey = format_1.Convert.uint8ToHex(builder.getSignerPublicKey().key);
        const networkType = builder.getNetwork().valueOf();
        const transaction = MultisigAccountModificationTransaction.create(isEmbedded ? Deadline_1.Deadline.create() : Deadline_1.Deadline.createFromDTO(builder.getDeadline().timestamp), builder.getMinApprovalDelta(), builder.getMinRemovalDelta(), builder.getPublicKeyAdditions().map((addition) => {
            return PublicAccount_1.PublicAccount.createFromPublicKey(format_1.Convert.uint8ToHex(addition.getKey()), networkType);
        }), builder.getPublicKeyDeletions().map((deletion) => {
            return PublicAccount_1.PublicAccount.createFromPublicKey(format_1.Convert.uint8ToHex(deletion.getKey()), networkType);
        }), networkType, isEmbedded ? new UInt64_1.UInt64([0, 0]) : new UInt64_1.UInt64(builder.fee.amount));
        return isEmbedded ?
            transaction.toAggregate(PublicAccount_1.PublicAccount.createFromPublicKey(signerPublicKey, networkType)) : transaction;
    }
    /**
     * @override Transaction.size()
     * @description get the byte size of a MultisigAccountModificationTransaction
     * @returns {number}
     * @memberof MultisigAccountModificationTransaction
     */
    get size() {
        const byteSize = super.size;
        // set static byte size fields
        const byteRemovalDelta = 1;
        const byteApprovalDelta = 1;
        const byteAdditionCount = 1;
        const byteDeletionCount = 1;
        const bytePublicKeyAdditions = 32 * this.publicKeyAdditions.length;
        const bytePublicKeyDeletions = 32 * this.publicKeyDeletions.length;
        const byteReserved1 = 4;
        return byteSize + byteRemovalDelta + byteApprovalDelta + byteAdditionCount +
            byteDeletionCount + bytePublicKeyAdditions + bytePublicKeyDeletions + byteReserved1;
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateBytes() {
        const signerBuffer = new Uint8Array(32);
        const signatureBuffer = new Uint8Array(64);
        const transactionBuilder = new MultisigAccountModificationTransactionBuilder_1.MultisigAccountModificationTransactionBuilder(new SignatureDto_1.SignatureDto(signatureBuffer), new KeyDto_1.KeyDto(signerBuffer), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.MODIFY_MULTISIG_ACCOUNT.valueOf(), new AmountDto_1.AmountDto(this.maxFee.toDTO()), new TimestampDto_1.TimestampDto(this.deadline.toDTO()), this.minRemovalDelta, this.minApprovalDelta, this.publicKeyAdditions.map((addition) => {
            return new KeyDto_1.KeyDto(format_1.Convert.hexToUint8(addition.publicKey));
        }), this.publicKeyDeletions.map((deletion) => {
            return new KeyDto_1.KeyDto(format_1.Convert.hexToUint8(deletion.publicKey));
        }));
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateEmbeddedBytes() {
        const transactionBuilder = new EmbeddedMultisigAccountModificationTransactionBuilder_1.EmbeddedMultisigAccountModificationTransactionBuilder(new KeyDto_1.KeyDto(format_1.Convert.hexToUint8(this.signer.publicKey)), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.MODIFY_MULTISIG_ACCOUNT.valueOf(), this.minRemovalDelta, this.minApprovalDelta, this.publicKeyAdditions.map((addition) => {
            return new KeyDto_1.KeyDto(format_1.Convert.hexToUint8(addition.publicKey));
        }), this.publicKeyDeletions.map((deletion) => {
            return new KeyDto_1.KeyDto(format_1.Convert.hexToUint8(deletion.publicKey));
        }));
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @returns {MultisigAccountModificationTransaction}
     */
    resolveAliases() {
        return this;
    }
}
exports.MultisigAccountModificationTransaction = MultisigAccountModificationTransaction;
//# sourceMappingURL=MultisigAccountModificationTransaction.js.map