"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const AccountRestrictionType_1 = require("../restriction/AccountRestrictionType");
const UInt64_1 = require("../UInt64");
const AccountAddressRestrictionTransaction_1 = require("./AccountAddressRestrictionTransaction");
const AccountMosaicRestrictionTransaction_1 = require("./AccountMosaicRestrictionTransaction");
const AccountOperationRestrictionTransaction_1 = require("./AccountOperationRestrictionTransaction");
class AccountRestrictionTransaction {
    /**
     * Create an account address restriction transaction object
     * @param deadline - The deadline to include the transaction.
     * @param restrictionFlags - Type of account restriction transaction
     * @param restrictionAdditions - Account restriction additions.
     * @param restrictionDeletions - Account restriction deletions.
     * @param networkType - The network type.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {AccountAddressRestrictionTransaction}
     */
    static createAddressRestrictionModificationTransaction(deadline, restrictionFlags, restrictionAdditions, restrictionDeletions, networkType, maxFee = new UInt64_1.UInt64([0, 0])) {
        if (![AccountRestrictionType_1.AccountRestrictionFlags.AllowIncomingAddress,
            AccountRestrictionType_1.AccountRestrictionFlags.AllowOutgoingAddress,
            AccountRestrictionType_1.AccountRestrictionFlags.BlockOutgoingAddress,
            AccountRestrictionType_1.AccountRestrictionFlags.BlockIncomingAddress].includes(restrictionFlags)) {
            throw new Error('Restriction type is not allowed.');
        }
        return AccountAddressRestrictionTransaction_1.AccountAddressRestrictionTransaction.create(deadline, restrictionFlags, restrictionAdditions, restrictionDeletions, networkType, maxFee);
    }
    /**
     * Create an account mosaic restriction transaction object
     * @param deadline - The deadline to include the transaction.
     * @param restrictionFlags - Type of account restriction transaction
     * @param restrictionAdditions - Account restriction additions.
     * @param restrictionDeletions - Account restriction deletions.
     * @param networkType - The network type.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {AccountMosaicRestrictionTransaction}
     */
    static createMosaicRestrictionModificationTransaction(deadline, restrictionFlags, restrictionAdditions, restrictionDeletions, networkType, maxFee = new UInt64_1.UInt64([0, 0])) {
        if (![AccountRestrictionType_1.AccountRestrictionFlags.AllowMosaic, AccountRestrictionType_1.AccountRestrictionFlags.BlockMosaic].includes(restrictionFlags)) {
            throw new Error('Restriction type is not allowed.');
        }
        return AccountMosaicRestrictionTransaction_1.AccountMosaicRestrictionTransaction.create(deadline, restrictionFlags, restrictionAdditions, restrictionDeletions, networkType, maxFee);
    }
    /**
     * Create an account operation restriction transaction object
     * @param deadline - The deadline to include the transaction.
     * @param restrictionFlags - Type of account restriction transaction
     * @param restrictionAdditions - Account restriction additions.
     * @param restrictionDeletions - Account restriction deletions.
     * @param networkType - The network type.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {AccountOperationRestrictionTransaction}
     */
    static createOperationRestrictionModificationTransaction(deadline, restrictionFlags, restrictionAdditions, restrictionDeletions, networkType, maxFee = new UInt64_1.UInt64([0, 0])) {
        if (![AccountRestrictionType_1.AccountRestrictionFlags.AllowIncomingTransactionType,
            AccountRestrictionType_1.AccountRestrictionFlags.AllowOutgoingTransactionType,
            AccountRestrictionType_1.AccountRestrictionFlags.BlockIncomingTransactionType,
            AccountRestrictionType_1.AccountRestrictionFlags.BlockOutgoingTransactionType].includes(restrictionFlags)) {
            throw new Error('Restriction type is not allowed.');
        }
        return AccountOperationRestrictionTransaction_1.AccountOperationRestrictionTransaction.create(deadline, restrictionFlags, restrictionAdditions, restrictionDeletions, networkType, maxFee);
    }
}
exports.AccountRestrictionTransaction = AccountRestrictionTransaction;
//# sourceMappingURL=AccountRestrictionTransaction.js.map