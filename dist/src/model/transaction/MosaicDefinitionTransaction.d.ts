import { PublicAccount } from '../account/PublicAccount';
import { NetworkType } from '../blockchain/NetworkType';
import { MosaicFlags } from '../mosaic/MosaicFlags';
import { MosaicId } from '../mosaic/MosaicId';
import { MosaicNonce } from '../mosaic/MosaicNonce';
import { UInt64 } from '../UInt64';
import { Deadline } from './Deadline';
import { InnerTransaction } from './InnerTransaction';
import { Transaction } from './Transaction';
import { TransactionInfo } from './TransactionInfo';
/**
 * Before a mosaic can be created or transferred, a corresponding definition of the mosaic has to be created and published to the network.
 * This is done via a mosaic definition transaction.
 */
export declare class MosaicDefinitionTransaction extends Transaction {
    /**
     * The mosaic nonce.
     */
    readonly nonce: MosaicNonce;
    /**
     * The mosaic id.
     */
    readonly mosaicId: MosaicId;
    /**
     * The mosaic properties.
     */
    readonly flags: MosaicFlags;
    /**
     * Mosaic divisibility
     */
    readonly divisibility: number;
    /**
     * Mosaic duration, 0 value for eternal mosaic
     */
    readonly duration: UInt64;
    /**
     * Create a mosaic creation transaction object
     * @param deadline - The deadline to include the transaction.
     * @param nonce - The mosaic nonce ex: MosaicNonce.createRandom().
     * @param mosaicId - The mosaic id ex: new MosaicId([481110499, 231112638]).
     * @param flags - The mosaic flags.
     * @param divisibility - The mosaic divicibility.
     * @param duration - The mosaic duration.
     * @param networkType - The network type.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {MosaicDefinitionTransaction}
     */
    static create(deadline: Deadline, nonce: MosaicNonce, mosaicId: MosaicId, flags: MosaicFlags, divisibility: number, duration: UInt64, networkType: NetworkType, maxFee?: UInt64): MosaicDefinitionTransaction;
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param maxFee
     * @param nonce
     * @param mosaicId
     * @param flags
     * @param divisibility
     * @param duration
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    constructor(networkType: NetworkType, version: number, deadline: Deadline, maxFee: UInt64, 
        /**
         * The mosaic nonce.
         */
        nonce: MosaicNonce, 
        /**
         * The mosaic id.
         */
        mosaicId: MosaicId, 
        /**
         * The mosaic properties.
         */
        flags: MosaicFlags, 
        /**
         * Mosaic divisibility
         */
        divisibility: number, 
        /**
         * Mosaic duration, 0 value for eternal mosaic
         */
        duration?: UInt64, signature?: string, signer?: PublicAccount, transactionInfo?: TransactionInfo);
    /**
     * Create a transaction object from payload
     * @param {string} payload Binary payload
     * @param {Boolean} isEmbedded Is embedded transaction (Default: false)
     * @returns {Transaction | InnerTransaction}
     */
    static createFromPayload(payload: string, isEmbedded?: boolean): Transaction | InnerTransaction;
    /**
     * @override Transaction.size()
     * @description get the byte size of a MosaicDefinitionTransaction
     * @returns {number}
     * @memberof MosaicDefinitionTransaction
     */
    readonly size: number;
    /**
     * @description Get mosaic nonce int value
     * @returns {number}
     * @memberof MosaicDefinitionTransaction
     */
    getMosaicNonceIntValue(): number;
}
