import { PublicAccount } from '../account/PublicAccount';
import { NetworkType } from '../blockchain/NetworkType';
import { UInt64 } from '../UInt64';
import { Deadline } from './Deadline';
import { InnerTransaction } from './InnerTransaction';
import { Transaction } from './Transaction';
import { TransactionInfo } from './TransactionInfo';
/**
 * Modify multisig account transactions are part of the NEM's multisig account system.
 * A modify multisig account transaction holds an array of multisig cosignatory modifications,
 * min number of signatures to approve a transaction and a min number of signatures to remove a cosignatory.
 * @since 1.0
 */
export declare class MultisigAccountModificationTransaction extends Transaction {
    /**
     * The number of signatures needed to approve a transaction.
     * If we are modifying and existing multi-signature account this indicates the relative change of the minimum cosignatories.
     */
    readonly minApprovalDelta: number;
    /**
     * The number of signatures needed to remove a cosignatory.
     * If we are modifying and existing multi-signature account this indicates the relative change of the minimum cosignatories.
     */
    readonly minRemovalDelta: number;
    /**
     * The Cosignatory public key additions.
     */
    readonly publicKeyAdditions: PublicAccount[];
    /**
     * The Cosignatory public key deletion.
     */
    readonly publicKeyDeletions: PublicAccount[];
    /**
     * Create a modify multisig account transaction object
     * @param deadline - The deadline to include the transaction.
     * @param minApprovalDelta - The min approval relative change.
     * @param minRemovalDelta - The min removal relative change.
     * @param publicKeyAdditions - Cosignatory public key additions.
     * @param publicKeyDeletions - Cosignatory public key deletions.
     * @param networkType - The network type.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {MultisigAccountModificationTransaction}
     */
    static create(deadline: Deadline, minApprovalDelta: number, minRemovalDelta: number, publicKeyAdditions: PublicAccount[], publicKeyDeletions: PublicAccount[], networkType: NetworkType, maxFee?: UInt64): MultisigAccountModificationTransaction;
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param maxFee
     * @param minApprovalDelta
     * @param minRemovalDelta
     * @param publicKeyAdditions
     * @param publicKeyDeletions
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    constructor(networkType: NetworkType, version: number, deadline: Deadline, maxFee: UInt64, 
        /**
         * The number of signatures needed to approve a transaction.
         * If we are modifying and existing multi-signature account this indicates the relative change of the minimum cosignatories.
         */
        minApprovalDelta: number, 
        /**
         * The number of signatures needed to remove a cosignatory.
         * If we are modifying and existing multi-signature account this indicates the relative change of the minimum cosignatories.
         */
        minRemovalDelta: number, 
        /**
         * The Cosignatory public key additions.
         */
        publicKeyAdditions: PublicAccount[], 
        /**
         * The Cosignatory public key deletion.
         */
        publicKeyDeletions: PublicAccount[], signature?: string, signer?: PublicAccount, transactionInfo?: TransactionInfo);
    /**
     * Create a transaction object from payload
     * @param {string} payload Binary payload
     * @param {Boolean} isEmbedded Is embedded transaction (Default: false)
     * @returns {Transaction | InnerTransaction}
     */
    static createFromPayload(payload: string, isEmbedded?: boolean): Transaction | InnerTransaction;
    /**
     * @override Transaction.size()
     * @description get the byte size of a MultisigAccountModificationTransaction
     * @returns {number}
     * @memberof MultisigAccountModificationTransaction
     */
    readonly size: number;
}
