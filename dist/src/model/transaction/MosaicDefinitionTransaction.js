"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const format_1 = require("../../core/format");
const AmountDto_1 = require("../../infrastructure/catbuffer/AmountDto");
const BlockDurationDto_1 = require("../../infrastructure/catbuffer/BlockDurationDto");
const EmbeddedMosaicDefinitionTransactionBuilder_1 = require("../../infrastructure/catbuffer/EmbeddedMosaicDefinitionTransactionBuilder");
const GeneratorUtils_1 = require("../../infrastructure/catbuffer/GeneratorUtils");
const KeyDto_1 = require("../../infrastructure/catbuffer/KeyDto");
const MosaicDefinitionTransactionBuilder_1 = require("../../infrastructure/catbuffer/MosaicDefinitionTransactionBuilder");
const MosaicIdDto_1 = require("../../infrastructure/catbuffer/MosaicIdDto");
const MosaicNonceDto_1 = require("../../infrastructure/catbuffer/MosaicNonceDto");
const SignatureDto_1 = require("../../infrastructure/catbuffer/SignatureDto");
const TimestampDto_1 = require("../../infrastructure/catbuffer/TimestampDto");
const PublicAccount_1 = require("../account/PublicAccount");
const MosaicFlags_1 = require("../mosaic/MosaicFlags");
const MosaicId_1 = require("../mosaic/MosaicId");
const MosaicNonce_1 = require("../mosaic/MosaicNonce");
const UInt64_1 = require("../UInt64");
const Deadline_1 = require("./Deadline");
const Transaction_1 = require("./Transaction");
const TransactionType_1 = require("./TransactionType");
const TransactionVersion_1 = require("./TransactionVersion");
/**
 * Before a mosaic can be created or transferred, a corresponding definition of the mosaic has to be created and published to the network.
 * This is done via a mosaic definition transaction.
 */
class MosaicDefinitionTransaction extends Transaction_1.Transaction {
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param maxFee
     * @param nonce
     * @param mosaicId
     * @param flags
     * @param divisibility
     * @param duration
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    constructor(networkType, version, deadline, maxFee, 
    /**
     * The mosaic nonce.
     */
    nonce, 
    /**
     * The mosaic id.
     */
    mosaicId, 
    /**
     * The mosaic properties.
     */
    flags, 
    /**
     * Mosaic divisibility
     */
    divisibility, 
    /**
     * Mosaic duration, 0 value for eternal mosaic
     */
    duration = UInt64_1.UInt64.fromUint(0), signature, signer, transactionInfo) {
        super(TransactionType_1.TransactionType.MOSAIC_DEFINITION, networkType, version, deadline, maxFee, signature, signer, transactionInfo);
        this.nonce = nonce;
        this.mosaicId = mosaicId;
        this.flags = flags;
        this.divisibility = divisibility;
        this.duration = duration;
    }
    /**
     * Create a mosaic creation transaction object
     * @param deadline - The deadline to include the transaction.
     * @param nonce - The mosaic nonce ex: MosaicNonce.createRandom().
     * @param mosaicId - The mosaic id ex: new MosaicId([481110499, 231112638]).
     * @param flags - The mosaic flags.
     * @param divisibility - The mosaic divicibility.
     * @param duration - The mosaic duration.
     * @param networkType - The network type.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {MosaicDefinitionTransaction}
     */
    static create(deadline, nonce, mosaicId, flags, divisibility, duration, networkType, maxFee = new UInt64_1.UInt64([0, 0])) {
        return new MosaicDefinitionTransaction(networkType, TransactionVersion_1.TransactionVersion.MOSAIC_DEFINITION, deadline, maxFee, nonce, mosaicId, flags, divisibility, duration);
    }
    /**
     * Create a transaction object from payload
     * @param {string} payload Binary payload
     * @param {Boolean} isEmbedded Is embedded transaction (Default: false)
     * @returns {Transaction | InnerTransaction}
     */
    static createFromPayload(payload, isEmbedded = false) {
        const builder = isEmbedded ? EmbeddedMosaicDefinitionTransactionBuilder_1.EmbeddedMosaicDefinitionTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload)) :
            MosaicDefinitionTransactionBuilder_1.MosaicDefinitionTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload));
        const signerPublicKey = format_1.Convert.uint8ToHex(builder.getSignerPublicKey().key);
        const networkType = builder.getNetwork().valueOf();
        const transaction = MosaicDefinitionTransaction.create(isEmbedded ? Deadline_1.Deadline.create() : Deadline_1.Deadline.createFromDTO(builder.getDeadline().timestamp), new MosaicNonce_1.MosaicNonce(builder.getNonce().serialize()), new MosaicId_1.MosaicId(builder.getId().mosaicId), MosaicFlags_1.MosaicFlags.create((builder.getFlags() & 1) === 1, (builder.getFlags() & 2) === 2, (builder.getFlags() & 4) === 4), builder.getDivisibility(), new UInt64_1.UInt64(builder.getDuration().blockDuration), networkType, isEmbedded ? new UInt64_1.UInt64([0, 0]) : new UInt64_1.UInt64(builder.fee.amount));
        return isEmbedded ?
            transaction.toAggregate(PublicAccount_1.PublicAccount.createFromPublicKey(signerPublicKey, networkType)) : transaction;
    }
    /**
     * @override Transaction.size()
     * @description get the byte size of a MosaicDefinitionTransaction
     * @returns {number}
     * @memberof MosaicDefinitionTransaction
     */
    get size() {
        const byteSize = super.size;
        // set static byte size fields
        const byteMosaicId = 8;
        const byteDuration = 8;
        const byteNonce = 4;
        const byteFlags = 1;
        const byteDivisibility = 1;
        return byteSize + byteNonce + byteMosaicId + byteFlags + byteDivisibility + byteDuration;
    }
    /**
     * @description Get mosaic nonce int value
     * @returns {number}
     * @memberof MosaicDefinitionTransaction
     */
    getMosaicNonceIntValue() {
        return GeneratorUtils_1.GeneratorUtils.readUint32At(this.nonce.toDTO(), 0);
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateBytes() {
        const signerBuffer = new Uint8Array(32);
        const signatureBuffer = new Uint8Array(64);
        const transactionBuilder = new MosaicDefinitionTransactionBuilder_1.MosaicDefinitionTransactionBuilder(new SignatureDto_1.SignatureDto(signatureBuffer), new KeyDto_1.KeyDto(signerBuffer), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.MOSAIC_DEFINITION.valueOf(), new AmountDto_1.AmountDto(this.maxFee.toDTO()), new TimestampDto_1.TimestampDto(this.deadline.toDTO()), new MosaicIdDto_1.MosaicIdDto(this.mosaicId.id.toDTO()), new BlockDurationDto_1.BlockDurationDto(this.duration.toDTO()), new MosaicNonceDto_1.MosaicNonceDto(this.getMosaicNonceIntValue()), this.flags.getValue(), this.divisibility);
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateEmbeddedBytes() {
        const transactionBuilder = new EmbeddedMosaicDefinitionTransactionBuilder_1.EmbeddedMosaicDefinitionTransactionBuilder(new KeyDto_1.KeyDto(format_1.Convert.hexToUint8(this.signer.publicKey)), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.MOSAIC_DEFINITION.valueOf(), new MosaicIdDto_1.MosaicIdDto(this.mosaicId.id.toDTO()), new BlockDurationDto_1.BlockDurationDto(this.duration.toDTO()), new MosaicNonceDto_1.MosaicNonceDto(this.getMosaicNonceIntValue()), this.flags.getValue(), this.divisibility);
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @returns {MosaicDefinitionTransaction}
     */
    resolveAliases() {
        return this;
    }
}
exports.MosaicDefinitionTransaction = MosaicDefinitionTransaction;
//# sourceMappingURL=MosaicDefinitionTransaction.js.map