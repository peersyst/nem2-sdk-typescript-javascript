import { Address } from '../account/Address';
import { PublicAccount } from '../account/PublicAccount';
import { NetworkType } from '../blockchain/NetworkType';
import { Message } from '../message/Message';
import { Mosaic } from '../mosaic/Mosaic';
import { NamespaceId } from '../namespace/NamespaceId';
import { UInt64 } from '../UInt64';
import { Deadline } from './Deadline';
import { InnerTransaction } from './InnerTransaction';
import { Transaction } from './Transaction';
import { TransactionInfo } from './TransactionInfo';
/**
 * Transfer transactions contain data about transfers of mosaics and message to another account.
 */
export declare class TransferTransaction extends Transaction {
    /**
     * The address of the recipient address.
     */
    readonly recipientAddress: Address | NamespaceId;
    /**
     * The array of Mosaic objects.
     */
    readonly mosaics: Mosaic[];
    /**
     * The transaction message of 2048 characters.
     */
    readonly message: Message;
    /**
     * Create a transfer transaction object.
     *
     * - This method can also be used to create PersistentDelegationRequestTransaction
     * with `PersistentHarvestingDelegationMessage` provided.
     * @param deadline - The deadline to include the transaction.
     * @param recipientAddress - The recipient address of the transaction.
     * @param mosaics - The array of mosaics.
     * @param message - The transaction message.
     * @param networkType - The network type.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {TransferTransaction}
     */
    static create(deadline: Deadline, recipientAddress: Address | NamespaceId, mosaics: Mosaic[], message: Message, networkType: NetworkType, maxFee?: UInt64): TransferTransaction;
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param maxFee
     * @param recipientAddress
     * @param mosaics
     * @param message
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    constructor(networkType: NetworkType, version: number, deadline: Deadline, maxFee: UInt64, 
        /**
         * The address of the recipient address.
         */
        recipientAddress: Address | NamespaceId, 
        /**
         * The array of Mosaic objects.
         */
        mosaics: Mosaic[], 
        /**
         * The transaction message of 2048 characters.
         */
        message: Message, signature?: string, signer?: PublicAccount, transactionInfo?: TransactionInfo);
    /**
     * Create a transaction object from payload
     * @param {string} payload Binary payload
     * @param {Boolean} isEmbedded Is embedded transaction (Default: false)
     * @returns {Transaction | InnerTransaction}
     */
    static createFromPayload(payload: string, isEmbedded?: boolean): Transaction | InnerTransaction;
    /**
     * @override Transaction.size()
     * @description get the byte size of a TransferTransaction
     * @returns {number}
     * @memberof TransferTransaction
     */
    readonly size: number;
}
