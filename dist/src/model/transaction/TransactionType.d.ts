/**
 * Enum containing transaction type constants.
 */
export declare enum TransactionType {
    /** Reserved entity type. */
    RESERVED = 0,
    /**
     * Transfer Transaction transaction type.
     * @type {number}
     */
    TRANSFER = 16724,
    /**
     * Register namespace transaction type.
     * @type {number}
     */
    REGISTER_NAMESPACE = 16718,
    /**
     * Address alias transaction type
     * @type {number}
     */
    ADDRESS_ALIAS = 16974,
    /**
     * Mosaic alias transaction type
     * @type {number}
     */
    MOSAIC_ALIAS = 17230,
    /**
     * Mosaic definition transaction type.
     * @type {number}
     */
    MOSAIC_DEFINITION = 16717,
    /**
     * Mosaic supply change transaction.
     * @type {number}
     */
    MOSAIC_SUPPLY_CHANGE = 16973,
    /**
     * Modify multisig account transaction type.
     * @type {number}
     */
    MODIFY_MULTISIG_ACCOUNT = 16725,
    /**
     * Aggregate complete transaction type.
     * @type {number}
     */
    AGGREGATE_COMPLETE = 16705,
    /**
     * Aggregate bonded transaction type
     */
    AGGREGATE_BONDED = 16961,
    /**
     * Lock transaction type
     * @type {number}
     */
    LOCK = 16712,
    /**
     * Secret Lock Transaction type
     * @type {number}
     */
    SECRET_LOCK = 16722,
    /**
     * Secret Proof transaction type
     * @type {number}
     */
    SECRET_PROOF = 16978,
    /**
     * Account restriction address transaction type
     * @type {number}
     */
    ACCOUNT_RESTRICTION_ADDRESS = 16720,
    /**
     * Account restriction mosaic transaction type
     * @type {number}
     */
    ACCOUNT_RESTRICTION_MOSAIC = 16976,
    /**
     * Account restriction operation transaction type
     * @type {number}
     */
    ACCOUNT_RESTRICTION_OPERATION = 17232,
    /**
     * Link account transaction type
     * @type {number}
     */
    LINK_ACCOUNT = 16716,
    /**
     * Mosaic address restriction type
     * @type {number}
     */
    MOSAIC_ADDRESS_RESTRICTION = 16977,
    /**
     * Mosaic global restriction type
     * @type {number}
     */
    MOSAIC_GLOBAL_RESTRICTION = 16721,
    /**
     * Account metadata transaction
     * @type {number}
     */
    ACCOUNT_METADATA_TRANSACTION = 16708,
    /**
     * Mosaic metadata transaction
     * @type {number}
     */
    MOSAIC_METADATA_TRANSACTION = 16964,
    /**
     * Namespace metadata transaction
     * @type {number}
     */
    NAMESPACE_METADATA_TRANSACTION = 17220,
}
