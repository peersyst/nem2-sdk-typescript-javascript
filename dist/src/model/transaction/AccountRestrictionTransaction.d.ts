import { Address } from '../account/Address';
import { NetworkType } from '../blockchain/NetworkType';
import { MosaicId } from '../mosaic/MosaicId';
import { NamespaceId } from '../namespace/NamespaceId';
import { AccountRestrictionFlags } from '../restriction/AccountRestrictionType';
import { UInt64 } from '../UInt64';
import { AccountAddressRestrictionTransaction } from './AccountAddressRestrictionTransaction';
import { AccountMosaicRestrictionTransaction } from './AccountMosaicRestrictionTransaction';
import { AccountOperationRestrictionTransaction } from './AccountOperationRestrictionTransaction';
import { Deadline } from './Deadline';
import { TransactionType } from './TransactionType';
export declare class AccountRestrictionTransaction {
    /**
     * Create an account address restriction transaction object
     * @param deadline - The deadline to include the transaction.
     * @param restrictionFlags - Type of account restriction transaction
     * @param restrictionAdditions - Account restriction additions.
     * @param restrictionDeletions - Account restriction deletions.
     * @param networkType - The network type.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {AccountAddressRestrictionTransaction}
     */
    static createAddressRestrictionModificationTransaction(deadline: Deadline, restrictionFlags: AccountRestrictionFlags, restrictionAdditions: Array<Address | NamespaceId>, restrictionDeletions: Array<Address | NamespaceId>, networkType: NetworkType, maxFee?: UInt64): AccountAddressRestrictionTransaction;
    /**
     * Create an account mosaic restriction transaction object
     * @param deadline - The deadline to include the transaction.
     * @param restrictionFlags - Type of account restriction transaction
     * @param restrictionAdditions - Account restriction additions.
     * @param restrictionDeletions - Account restriction deletions.
     * @param networkType - The network type.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {AccountMosaicRestrictionTransaction}
     */
    static createMosaicRestrictionModificationTransaction(deadline: Deadline, restrictionFlags: AccountRestrictionFlags, restrictionAdditions: Array<MosaicId | NamespaceId>, restrictionDeletions: Array<MosaicId | NamespaceId>, networkType: NetworkType, maxFee?: UInt64): AccountMosaicRestrictionTransaction;
    /**
     * Create an account operation restriction transaction object
     * @param deadline - The deadline to include the transaction.
     * @param restrictionFlags - Type of account restriction transaction
     * @param restrictionAdditions - Account restriction additions.
     * @param restrictionDeletions - Account restriction deletions.
     * @param networkType - The network type.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {AccountOperationRestrictionTransaction}
     */
    static createOperationRestrictionModificationTransaction(deadline: Deadline, restrictionFlags: AccountRestrictionFlags, restrictionAdditions: TransactionType[], restrictionDeletions: TransactionType[], networkType: NetworkType, maxFee?: UInt64): AccountOperationRestrictionTransaction;
}
