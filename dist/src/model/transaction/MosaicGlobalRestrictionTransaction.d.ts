import { PublicAccount } from '../account/PublicAccount';
import { NetworkType } from '../blockchain/NetworkType';
import { MosaicId } from '../mosaic/MosaicId';
import { NamespaceId } from '../namespace/NamespaceId';
import { MosaicRestrictionType } from '../restriction/MosaicRestrictionType';
import { UInt64 } from '../UInt64';
import { Deadline } from './Deadline';
import { InnerTransaction } from './InnerTransaction';
import { Transaction } from './Transaction';
import { TransactionInfo } from './TransactionInfo';
export declare class MosaicGlobalRestrictionTransaction extends Transaction {
    /**
     * The mosaic id.
     */
    readonly mosaicId: MosaicId | NamespaceId;
    /**
     * The refrence mosaic id.
     */
    readonly referenceMosaicId: MosaicId | NamespaceId;
    /**
     * The restriction key.
     */
    readonly restrictionKey: UInt64;
    /**
     * The previous restriction value.
     */
    readonly previousRestrictionValue: UInt64;
    /**
     * The previous restriction type.
     */
    readonly previousRestrictionType: MosaicRestrictionType;
    /**
     * The new restriction value.
     */
    readonly newRestrictionValue: UInt64;
    /**
     * The new restriction type.
     */
    readonly newRestrictionType: MosaicRestrictionType;
    /**
     * Create a mosaic address restriction transaction object
     *
     * The mosaic global restrictions are the network-wide rules that will determine
     * whether an account will be able to transact a given mosaic.
     *
     * Only accounts tagged with the key identifiers and values that meet the conditions
     * will be able to execute transactions involving the mosaic.
     *
     * Additionally, the mosaic creator can define restrictions that depend directly on
     * global restrictions set on another mosaic - known as **reference mosaic**.
     * The referenced mosaic and the restricted mosaic do not necessarily have to be created
     * by the same account, enabling the delegation of mosaic permissions to a third party.
     *
     * @param deadline - The deadline to include the transaction.
     * @param mosaicId - The mosaic id ex: new MosaicId([481110499, 231112638]).
     * @param restrictionKey - The restriction key.
     * @param previousRestrictionValue - The previous restriction value.
     * @param previousRestrictionType - The previous restriction type.
     * @param newRestrictionValue - The new restriction value.
     * @param newRestrictionType - The new restriction tpye.
     * @param networkType - The network type.
     * @param referenceMosaicId - (Optional) The unresolved mosaic identifier providing the restriction key.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {MosaicGlobalRestrictionTransaction}
     */
    static create(deadline: Deadline, mosaicId: MosaicId | NamespaceId, restrictionKey: UInt64, previousRestrictionValue: UInt64, previousRestrictionType: MosaicRestrictionType, newRestrictionValue: UInt64, newRestrictionType: MosaicRestrictionType, networkType: NetworkType, referenceMosaicId?: MosaicId | NamespaceId, maxFee?: UInt64): MosaicGlobalRestrictionTransaction;
    /**
     * @param networkType - The network type
     * @param version - The transaction version
     * @param deadline - The deadline to include the transaction.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @param mosaicId - The unresolved mosaic identifier.
     * @param referenceMosaicId - The mosaic id providing the restriction key.
     * @param restrictionKey - The restriction key.
     * @param previousRestrictionValue - The previous restriction value.
     * @param previousRestrictionType - The previous restriction type.
     * @param newRestrictionValue - The new restriction value.
     * @param previousRestrictionType - The previous restriction tpye.
     * @param signature - The transaction signature
     * @param signer - The signer
     * @param transactionInfo - The transaction info
     */
    constructor(networkType: NetworkType, version: number, deadline: Deadline, maxFee: UInt64, 
        /**
         * The mosaic id.
         */
        mosaicId: MosaicId | NamespaceId, 
        /**
         * The refrence mosaic id.
         */
        referenceMosaicId: MosaicId | NamespaceId, 
        /**
         * The restriction key.
         */
        restrictionKey: UInt64, 
        /**
         * The previous restriction value.
         */
        previousRestrictionValue: UInt64, 
        /**
         * The previous restriction type.
         */
        previousRestrictionType: MosaicRestrictionType, 
        /**
         * The new restriction value.
         */
        newRestrictionValue: UInt64, 
        /**
         * The new restriction type.
         */
        newRestrictionType: MosaicRestrictionType, signature?: string, signer?: PublicAccount, transactionInfo?: TransactionInfo);
    /**
     * Create a transaction object from payload
     * @param {string} payload Binary payload
     * @param {Boolean} isEmbedded Is embedded transaction (Default: false)
     * @returns {Transaction | InnerTransaction}
     */
    static createFromPayload(payload: string, isEmbedded?: boolean): Transaction | InnerTransaction;
    /**
     * @override Transaction.size()
     * @description get the byte size of a MosaicDefinitionTransaction
     * @returns {number}
     * @memberof MosaicGlobalRestrictionTransaction
     */
    readonly size: number;
}
