"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const format_1 = require("../../core/format");
const UnresolvedMapping_1 = require("../../core/utils/UnresolvedMapping");
const AmountDto_1 = require("../../infrastructure/catbuffer/AmountDto");
const EmbeddedMosaicGlobalRestrictionTransactionBuilder_1 = require("../../infrastructure/catbuffer/EmbeddedMosaicGlobalRestrictionTransactionBuilder");
const KeyDto_1 = require("../../infrastructure/catbuffer/KeyDto");
const MosaicGlobalRestrictionTransactionBuilder_1 = require("../../infrastructure/catbuffer/MosaicGlobalRestrictionTransactionBuilder");
const SignatureDto_1 = require("../../infrastructure/catbuffer/SignatureDto");
const TimestampDto_1 = require("../../infrastructure/catbuffer/TimestampDto");
const UnresolvedMosaicIdDto_1 = require("../../infrastructure/catbuffer/UnresolvedMosaicIdDto");
const PublicAccount_1 = require("../account/PublicAccount");
const UInt64_1 = require("../UInt64");
const Deadline_1 = require("./Deadline");
const Transaction_1 = require("./Transaction");
const TransactionType_1 = require("./TransactionType");
const TransactionVersion_1 = require("./TransactionVersion");
class MosaicGlobalRestrictionTransaction extends Transaction_1.Transaction {
    /**
     * @param networkType - The network type
     * @param version - The transaction version
     * @param deadline - The deadline to include the transaction.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @param mosaicId - The unresolved mosaic identifier.
     * @param referenceMosaicId - The mosaic id providing the restriction key.
     * @param restrictionKey - The restriction key.
     * @param previousRestrictionValue - The previous restriction value.
     * @param previousRestrictionType - The previous restriction type.
     * @param newRestrictionValue - The new restriction value.
     * @param previousRestrictionType - The previous restriction tpye.
     * @param signature - The transaction signature
     * @param signer - The signer
     * @param transactionInfo - The transaction info
     */
    constructor(networkType, version, deadline, maxFee, 
    /**
     * The mosaic id.
     */
    mosaicId, 
    /**
     * The refrence mosaic id.
     */
    referenceMosaicId, 
    /**
     * The restriction key.
     */
    restrictionKey, 
    /**
     * The previous restriction value.
     */
    previousRestrictionValue, 
    /**
     * The previous restriction type.
     */
    previousRestrictionType, 
    /**
     * The new restriction value.
     */
    newRestrictionValue, 
    /**
     * The new restriction type.
     */
    newRestrictionType, signature, signer, transactionInfo) {
        super(TransactionType_1.TransactionType.MOSAIC_GLOBAL_RESTRICTION, networkType, version, deadline, maxFee, signature, signer, transactionInfo);
        this.mosaicId = mosaicId;
        this.referenceMosaicId = referenceMosaicId;
        this.restrictionKey = restrictionKey;
        this.previousRestrictionValue = previousRestrictionValue;
        this.previousRestrictionType = previousRestrictionType;
        this.newRestrictionValue = newRestrictionValue;
        this.newRestrictionType = newRestrictionType;
    }
    /**
     * Create a mosaic address restriction transaction object
     *
     * The mosaic global restrictions are the network-wide rules that will determine
     * whether an account will be able to transact a given mosaic.
     *
     * Only accounts tagged with the key identifiers and values that meet the conditions
     * will be able to execute transactions involving the mosaic.
     *
     * Additionally, the mosaic creator can define restrictions that depend directly on
     * global restrictions set on another mosaic - known as **reference mosaic**.
     * The referenced mosaic and the restricted mosaic do not necessarily have to be created
     * by the same account, enabling the delegation of mosaic permissions to a third party.
     *
     * @param deadline - The deadline to include the transaction.
     * @param mosaicId - The mosaic id ex: new MosaicId([481110499, 231112638]).
     * @param restrictionKey - The restriction key.
     * @param previousRestrictionValue - The previous restriction value.
     * @param previousRestrictionType - The previous restriction type.
     * @param newRestrictionValue - The new restriction value.
     * @param newRestrictionType - The new restriction tpye.
     * @param networkType - The network type.
     * @param referenceMosaicId - (Optional) The unresolved mosaic identifier providing the restriction key.
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {MosaicGlobalRestrictionTransaction}
     */
    static create(deadline, mosaicId, restrictionKey, previousRestrictionValue, previousRestrictionType, newRestrictionValue, newRestrictionType, networkType, referenceMosaicId = UnresolvedMapping_1.UnresolvedMapping.toUnresolvedMosaic(UInt64_1.UInt64.fromUint(0).toHex()), maxFee = new UInt64_1.UInt64([0, 0])) {
        return new MosaicGlobalRestrictionTransaction(networkType, TransactionVersion_1.TransactionVersion.MOSAIC_GLOBAL_RESTRICTION, deadline, maxFee, mosaicId, referenceMosaicId, restrictionKey, previousRestrictionValue, previousRestrictionType, newRestrictionValue, newRestrictionType);
    }
    /**
     * Create a transaction object from payload
     * @param {string} payload Binary payload
     * @param {Boolean} isEmbedded Is embedded transaction (Default: false)
     * @returns {Transaction | InnerTransaction}
     */
    static createFromPayload(payload, isEmbedded = false) {
        const builder = isEmbedded ? EmbeddedMosaicGlobalRestrictionTransactionBuilder_1.EmbeddedMosaicGlobalRestrictionTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload)) :
            MosaicGlobalRestrictionTransactionBuilder_1.MosaicGlobalRestrictionTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload));
        const signerPublicKey = format_1.Convert.uint8ToHex(builder.getSignerPublicKey().key);
        const networkType = builder.getNetwork().valueOf();
        const transaction = MosaicGlobalRestrictionTransaction.create(isEmbedded ? Deadline_1.Deadline.create() : Deadline_1.Deadline.createFromDTO(builder.getDeadline().timestamp), UnresolvedMapping_1.UnresolvedMapping.toUnresolvedMosaic(new UInt64_1.UInt64(builder.getMosaicId().unresolvedMosaicId).toHex()), new UInt64_1.UInt64(builder.getRestrictionKey()), new UInt64_1.UInt64(builder.getPreviousRestrictionValue()), builder.getPreviousRestrictionType().valueOf(), new UInt64_1.UInt64(builder.getNewRestrictionValue()), builder.getNewRestrictionType().valueOf(), networkType, UnresolvedMapping_1.UnresolvedMapping.toUnresolvedMosaic(new UInt64_1.UInt64(builder.getReferenceMosaicId().unresolvedMosaicId).toHex()), isEmbedded ? new UInt64_1.UInt64([0, 0]) : new UInt64_1.UInt64(builder.fee.amount));
        return isEmbedded ?
            transaction.toAggregate(PublicAccount_1.PublicAccount.createFromPublicKey(signerPublicKey, networkType)) : transaction;
    }
    /**
     * @override Transaction.size()
     * @description get the byte size of a MosaicDefinitionTransaction
     * @returns {number}
     * @memberof MosaicGlobalRestrictionTransaction
     */
    get size() {
        const byteSize = super.size;
        // set static byte size fields
        const byteMosaicId = 8;
        const byteReferenceMosaicId = 8;
        const byteRestrictionKey = 8;
        const bytePreviousRestrictionValue = 8;
        const byteNewRestrictionValue = 8;
        const bytePreviousRestrictionType = 1;
        const byteNewRestrictionType = 1;
        return byteSize + byteMosaicId + byteRestrictionKey + byteReferenceMosaicId +
            bytePreviousRestrictionValue + byteNewRestrictionValue + byteNewRestrictionType +
            bytePreviousRestrictionType;
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateBytes() {
        const signerBuffer = new Uint8Array(32);
        const signatureBuffer = new Uint8Array(64);
        const transactionBuilder = new MosaicGlobalRestrictionTransactionBuilder_1.MosaicGlobalRestrictionTransactionBuilder(new SignatureDto_1.SignatureDto(signatureBuffer), new KeyDto_1.KeyDto(signerBuffer), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.MOSAIC_GLOBAL_RESTRICTION.valueOf(), new AmountDto_1.AmountDto(this.maxFee.toDTO()), new TimestampDto_1.TimestampDto(this.deadline.toDTO()), new UnresolvedMosaicIdDto_1.UnresolvedMosaicIdDto(this.mosaicId.id.toDTO()), new UnresolvedMosaicIdDto_1.UnresolvedMosaicIdDto(this.referenceMosaicId.id.toDTO()), this.restrictionKey.toDTO(), this.previousRestrictionValue.toDTO(), this.newRestrictionValue.toDTO(), this.previousRestrictionType.valueOf(), this.newRestrictionType.valueOf());
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateEmbeddedBytes() {
        const transactionBuilder = new EmbeddedMosaicGlobalRestrictionTransactionBuilder_1.EmbeddedMosaicGlobalRestrictionTransactionBuilder(new KeyDto_1.KeyDto(format_1.Convert.hexToUint8(this.signer.publicKey)), this.versionToDTO(), this.networkType.valueOf(), TransactionType_1.TransactionType.MOSAIC_GLOBAL_RESTRICTION.valueOf(), new UnresolvedMosaicIdDto_1.UnresolvedMosaicIdDto(this.mosaicId.id.toDTO()), new UnresolvedMosaicIdDto_1.UnresolvedMosaicIdDto(this.referenceMosaicId.id.toDTO()), this.restrictionKey.toDTO(), this.previousRestrictionValue.toDTO(), this.newRestrictionValue.toDTO(), this.previousRestrictionType.valueOf(), this.newRestrictionType.valueOf());
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @param statement Block receipt statement
     * @param aggregateTransactionIndex Transaction index for aggregated transaction
     * @returns {MosaicGlobalRestrictionTransaction}
     */
    resolveAliases(statement, aggregateTransactionIndex = 0) {
        const transactionInfo = this.checkTransactionHeightAndIndex();
        return Object.assign({}, Object.getPrototypeOf(this), { mosaicId: statement.resolveMosaicId(this.mosaicId, transactionInfo.height.toString(), transactionInfo.index, aggregateTransactionIndex), referenceMosaicId: statement.resolveMosaicId(this.referenceMosaicId, transactionInfo.height.toString(), transactionInfo.index, aggregateTransactionIndex) });
    }
}
exports.MosaicGlobalRestrictionTransaction = MosaicGlobalRestrictionTransaction;
//# sourceMappingURL=MosaicGlobalRestrictionTransaction.js.map