"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const crypto_1 = require("../../core/crypto");
const format_1 = require("../../core/format");
const AggregateBondedTransactionBuilder_1 = require("../../infrastructure/catbuffer/AggregateBondedTransactionBuilder");
const AggregateCompleteTransactionBuilder_1 = require("../../infrastructure/catbuffer/AggregateCompleteTransactionBuilder");
const AmountDto_1 = require("../../infrastructure/catbuffer/AmountDto");
const CosignatureBuilder_1 = require("../../infrastructure/catbuffer/CosignatureBuilder");
const GeneratorUtils_1 = require("../../infrastructure/catbuffer/GeneratorUtils");
const Hash256Dto_1 = require("../../infrastructure/catbuffer/Hash256Dto");
const KeyDto_1 = require("../../infrastructure/catbuffer/KeyDto");
const SignatureDto_1 = require("../../infrastructure/catbuffer/SignatureDto");
const TimestampDto_1 = require("../../infrastructure/catbuffer/TimestampDto");
const CreateTransactionFromPayload_1 = require("../../infrastructure/transaction/CreateTransactionFromPayload");
const PublicAccount_1 = require("../account/PublicAccount");
const UInt64_1 = require("../UInt64");
const AggregateTransactionCosignature_1 = require("./AggregateTransactionCosignature");
const Deadline_1 = require("./Deadline");
const SignedTransaction_1 = require("./SignedTransaction");
const Transaction_1 = require("./Transaction");
const TransactionType_1 = require("./TransactionType");
const TransactionVersion_1 = require("./TransactionVersion");
/**
 * Aggregate innerTransactions contain multiple innerTransactions that can be initiated by different accounts.
 */
class AggregateTransaction extends Transaction_1.Transaction {
    /**
     * @param networkType
     * @param type
     * @param version
     * @param deadline
     * @param maxFee
     * @param innerTransactions
     * @param cosignatures
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    constructor(networkType, type, version, deadline, maxFee, 
    /**
     * The array of innerTransactions included in the aggregate transaction.
     */
    innerTransactions, 
    /**
     * The array of transaction cosigners signatures.
     */
    cosignatures, signature, signer, transactionInfo) {
        super(type, networkType, version, deadline, maxFee, signature, signer, transactionInfo);
        this.innerTransactions = innerTransactions;
        this.cosignatures = cosignatures;
    }
    /**
     * Create an aggregate complete transaction object
     * @param deadline - The deadline to include the transaction.
     * @param innerTransactions - The array of inner innerTransactions.
     * @param networkType - The network type.
     * @param cosignatures
     * @param maxFee - (Optional) Max fee defined by the sender
     * @returns {AggregateTransaction}
     */
    static createComplete(deadline, innerTransactions, networkType, cosignatures, maxFee = new UInt64_1.UInt64([0, 0])) {
        return new AggregateTransaction(networkType, TransactionType_1.TransactionType.AGGREGATE_COMPLETE, TransactionVersion_1.TransactionVersion.AGGREGATE_COMPLETE, deadline, maxFee, innerTransactions, cosignatures);
    }
    /**
     * Create an aggregate bonded transaction object
     * @param {Deadline} deadline
     * @param {InnerTransaction[]} innerTransactions
     * @param {NetworkType} networkType
     * @param {AggregateTransactionCosignature[]} cosignatures
     * @param {UInt64} maxFee - (Optional) Max fee defined by the sender
     * @return {AggregateTransaction}
     */
    static createBonded(deadline, innerTransactions, networkType, cosignatures = [], maxFee = new UInt64_1.UInt64([0, 0])) {
        return new AggregateTransaction(networkType, TransactionType_1.TransactionType.AGGREGATE_BONDED, TransactionVersion_1.TransactionVersion.AGGREGATE_BONDED, deadline, maxFee, innerTransactions, cosignatures);
    }
    /**
     * Create a transaction object from payload
     * @param {string} payload Binary payload
     * @returns {AggregateTransaction}
     */
    static createFromPayload(payload) {
        /**
         * Get transaction type from the payload hex
         * As buffer uses separate builder class for Complete and bonded
         */
        const type = parseInt(format_1.Convert.uint8ToHex(format_1.Convert.hexToUint8(payload.substring(220, 224)).reverse()), 16);
        const builder = type === TransactionType_1.TransactionType.AGGREGATE_COMPLETE ?
            AggregateCompleteTransactionBuilder_1.AggregateCompleteTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload)) :
            AggregateBondedTransactionBuilder_1.AggregateBondedTransactionBuilder.loadFromBinary(format_1.Convert.hexToUint8(payload));
        const innerTransactionHex = format_1.Convert.uint8ToHex(builder.getTransactions());
        const networkType = builder.getNetwork().valueOf();
        const consignaturesHex = format_1.Convert.uint8ToHex(builder.getCosignatures());
        /**
         * Get inner transactions array
         */
        const embeddedTransactionArray = [];
        let innerBinary = innerTransactionHex;
        while (innerBinary.length) {
            const payloadSize = parseInt(format_1.Convert.uint8ToHex(format_1.Convert.hexToUint8(innerBinary.substring(0, 8)).reverse()), 16) * 2;
            const innerTransaction = innerBinary.substring(0, payloadSize);
            embeddedTransactionArray.push(innerTransaction);
            innerBinary = innerBinary.substring(payloadSize).replace(/\b0+/g, '');
        }
        /**
         * Get cosignatures
         */
        const consignatureArray = consignaturesHex.match(/.{1,192}/g);
        const consignatures = consignatureArray ? consignatureArray.map((cosignature) => new AggregateTransactionCosignature_1.AggregateTransactionCosignature(cosignature.substring(64, 192), PublicAccount_1.PublicAccount.createFromPublicKey(cosignature.substring(0, 64), networkType))) : [];
        return type === TransactionType_1.TransactionType.AGGREGATE_COMPLETE ?
            AggregateTransaction.createComplete(Deadline_1.Deadline.createFromDTO(builder.deadline.timestamp), embeddedTransactionArray.map((transactionRaw) => {
                return CreateTransactionFromPayload_1.CreateTransactionFromPayload(transactionRaw, true);
            }), networkType, consignatures, new UInt64_1.UInt64(builder.fee.amount)) : AggregateTransaction.createBonded(Deadline_1.Deadline.createFromDTO(builder.deadline.timestamp), embeddedTransactionArray.map((transactionRaw) => {
            return CreateTransactionFromPayload_1.CreateTransactionFromPayload(transactionRaw, true);
        }), networkType, consignatures, new UInt64_1.UInt64(builder.fee.amount));
    }
    /**
     * @description add inner transactions to current list
     * @param {InnerTransaction[]} transaction
     * @returns {AggregateTransaction}
     * @memberof AggregateTransaction
     */
    addTransactions(transactions) {
        const innerTransactions = this.innerTransactions.concat(transactions);
        return Object.assign({ __proto__: Object.getPrototypeOf(this) }, this, { innerTransactions });
    }
    /**
     * @description add cosignatures to current list
     * @param {AggregateTransactionCosignature[]} transaction
     * @returns {AggregateTransaction}
     * @memberof AggregateTransaction
     */
    addCosignatures(cosigs) {
        const cosignatures = this.cosignatures.concat(cosigs);
        return Object.assign({ __proto__: Object.getPrototypeOf(this) }, this, { cosignatures });
    }
    /**
     * @internal
     * Sign transaction with cosignatories creating a new SignedTransaction
     * @param initiatorAccount - Initiator account
     * @param cosignatories - The array of accounts that will cosign the transaction
     * @param generationHash - Network generation hash hex
     * @returns {SignedTransaction}
     */
    signTransactionWithCosignatories(initiatorAccount, cosignatories, generationHash) {
        const signedTransaction = this.signWith(initiatorAccount, generationHash);
        const transactionHashBytes = format_1.Convert.hexToUint8(signedTransaction.hash);
        let signedPayload = signedTransaction.payload;
        cosignatories.forEach((cosigner) => {
            const signSchema = crypto_1.SHA3Hasher.resolveSignSchema(cosigner.networkType);
            const signature = crypto_1.KeyPair.sign(cosigner, transactionHashBytes, signSchema);
            signedPayload += cosigner.publicKey + format_1.Convert.uint8ToHex(signature);
        });
        // Calculate new size
        const size = `00000000${(signedPayload.length / 2).toString(16)}`;
        const formatedSize = size.substr(size.length - 8, size.length);
        const littleEndianSize = formatedSize.substr(6, 2) + formatedSize.substr(4, 2) +
            formatedSize.substr(2, 2) + formatedSize.substr(0, 2);
        signedPayload = littleEndianSize + signedPayload.substr(8, signedPayload.length - 8);
        return new SignedTransaction_1.SignedTransaction(signedPayload, signedTransaction.hash, initiatorAccount.publicKey, this.type, this.networkType);
    }
    /**
     * @internal
     * Sign transaction with cosignatories collected from cosigned transactions and creating a new SignedTransaction
     * For off chain Aggregated Complete Transaction co-signing.
     * @param initiatorAccount - Initiator account
     * @param {CosignatureSignedTransaction[]} cosignatureSignedTransactions - Array of cosigned transaction
     * @param generationHash - Network generation hash hex
     * @return {SignedTransaction}
     */
    signTransactionGivenSignatures(initiatorAccount, cosignatureSignedTransactions, generationHash) {
        const signedTransaction = this.signWith(initiatorAccount, generationHash);
        let signedPayload = signedTransaction.payload;
        cosignatureSignedTransactions.forEach((cosignedTransaction) => {
            signedPayload += cosignedTransaction.signerPublicKey + cosignedTransaction.signature;
        });
        // Calculate new size
        const size = `00000000${(signedPayload.length / 2).toString(16)}`;
        const formatedSize = size.substr(size.length - 8, size.length);
        const littleEndianSize = formatedSize.substr(6, 2) + formatedSize.substr(4, 2) +
            formatedSize.substr(2, 2) + formatedSize.substr(0, 2);
        signedPayload = littleEndianSize + signedPayload.substr(8, signedPayload.length - 8);
        return new SignedTransaction_1.SignedTransaction(signedPayload, signedTransaction.hash, initiatorAccount.publicKey, this.type, this.networkType);
    }
    /**
     * Check if account has signed transaction
     * @param publicAccount - Signer public account
     * @returns {boolean}
     */
    signedByAccount(publicAccount) {
        return this.cosignatures.find((cosignature) => cosignature.signer.equals(publicAccount)) !== undefined
            || (this.signer !== undefined && this.signer.equals(publicAccount));
    }
    /**
     * @override Transaction.size()
     * @description get the byte size of a AggregateTransaction
     * @returns {number}
     * @memberof AggregateTransaction
     */
    get size() {
        const byteSize = super.size;
        const byteTransactionHash = 32;
        // set static byte size fields
        const bytePayloadSize = 4;
        const byteHeader_Reserved1 = 4;
        // calculate each inner transaction's size
        let byteTransactions = 0;
        this.innerTransactions.forEach((transaction) => {
            const transactionByte = transaction.toAggregateTransactionBytes();
            const innerTransactionPadding = new Uint8Array(this.getInnerTransactionPaddingSize(transactionByte.length, 8));
            const paddedTransactionByte = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(transactionByte, innerTransactionPadding);
            byteTransactions += paddedTransactionByte.length;
        });
        const byteCosignatures = this.cosignatures.length * 96;
        return byteSize + byteTransactionHash + bytePayloadSize + byteHeader_Reserved1 +
            byteTransactions + byteCosignatures;
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateBytes() {
        const signerBuffer = new Uint8Array(32);
        const signatureBuffer = new Uint8Array(64);
        let transactions = Uint8Array.from([]);
        this.innerTransactions.forEach((transaction) => {
            const transactionByte = transaction.toAggregateTransactionBytes();
            const innerTransactionPadding = new Uint8Array(this.getInnerTransactionPaddingSize(transactionByte.length, 8));
            const paddedTransactionByte = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(transactionByte, innerTransactionPadding);
            transactions = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(transactions, paddedTransactionByte);
        });
        let cosignatures = Uint8Array.from([]);
        this.cosignatures.forEach((cosignature) => {
            const signerBytes = format_1.Convert.hexToUint8(cosignature.signer.publicKey);
            const signatureBytes = format_1.Convert.hexToUint8(cosignature.signature);
            const cosignatureBytes = new CosignatureBuilder_1.CosignatureBuilder(new KeyDto_1.KeyDto(signerBytes), new SignatureDto_1.SignatureDto(signatureBytes)).serialize();
            cosignatures = GeneratorUtils_1.GeneratorUtils.concatTypedArrays(cosignatures, cosignatureBytes);
        });
        const transactionBuilder = this.type === TransactionType_1.TransactionType.AGGREGATE_COMPLETE ?
            new AggregateCompleteTransactionBuilder_1.AggregateCompleteTransactionBuilder(new SignatureDto_1.SignatureDto(signatureBuffer), new KeyDto_1.KeyDto(signerBuffer), this.versionToDTO(), this.networkType.valueOf(), this.type.valueOf(), new AmountDto_1.AmountDto(this.maxFee.toDTO()), new TimestampDto_1.TimestampDto(this.deadline.toDTO()), new Hash256Dto_1.Hash256Dto(this.calculateInnerTransactionHash()), transactions, cosignatures) :
            new AggregateBondedTransactionBuilder_1.AggregateBondedTransactionBuilder(new SignatureDto_1.SignatureDto(signatureBuffer), new KeyDto_1.KeyDto(signerBuffer), this.versionToDTO(), this.networkType.valueOf(), this.type.valueOf(), new AmountDto_1.AmountDto(this.maxFee.toDTO()), new TimestampDto_1.TimestampDto(this.deadline.toDTO()), new Hash256Dto_1.Hash256Dto(this.calculateInnerTransactionHash()), transactions, cosignatures);
        return transactionBuilder.serialize();
    }
    /**
     * @internal
     * @returns {Uint8Array}
     */
    generateEmbeddedBytes() {
        throw new Error('Method not implemented');
    }
    /**
     * @internal
     * Generate inner transaction root hash (merkle tree)
     * @returns {Uint8Array}
     */
    calculateInnerTransactionHash() {
        // Note: Transaction hashing *always* uses SHA3
        const hasher = crypto_1.SHA3Hasher.createHasher(32, crypto_1.SignSchema.SHA3);
        const builder = new crypto_1.MerkleHashBuilder(32, crypto_1.SignSchema.SHA3);
        this.innerTransactions.forEach((transaction) => {
            const entityHash = new Uint8Array(32);
            // for each embedded transaction hash their body
            hasher.reset();
            hasher.update(transaction.toAggregateTransactionBytes());
            hasher.finalize(entityHash);
            // update merkle tree (add transaction hash)
            builder.update(entityHash);
        });
        // calculate root hash with all transactions
        return builder.getRootHash();
    }
    /**
     * Gets the padding size that rounds up \a size to the next multiple of \a alignment.
     * @param size Inner transaction size
     * @param alignment Next multiple alignment
     */
    getInnerTransactionPaddingSize(size, alignment) {
        return 0 === size % alignment ? 0 : alignment - (size % alignment);
    }
    /**
     * @internal
     * @returns {AggregateTransaction}
     */
    resolveAliases(statement) {
        const transactionInfo = this.checkTransactionHeightAndIndex();
        return Object.assign({ __proto__: Object.getPrototypeOf(this) }, this, { innerTransactions: this.innerTransactions.map((tx) => tx.resolveAliases(statement, transactionInfo.index))
                .sort((a, b) => a.transactionInfo.index - b.transactionInfo.index) });
    }
}
exports.AggregateTransaction = AggregateTransaction;
//# sourceMappingURL=AggregateTransaction.js.map