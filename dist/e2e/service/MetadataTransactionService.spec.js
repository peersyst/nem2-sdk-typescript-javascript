"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const Listener_1 = require("../../src/infrastructure/Listener");
const MetadataHttp_1 = require("../../src/infrastructure/MetadataHttp");
const TransactionHttp_1 = require("../../src/infrastructure/TransactionHttp");
const Account_1 = require("../../src/model/account/Account");
const NetworkType_1 = require("../../src/model/blockchain/NetworkType");
const MetadataType_1 = require("../../src/model/metadata/MetadataType");
const MosaicFlags_1 = require("../../src/model/mosaic/MosaicFlags");
const MosaicId_1 = require("../../src/model/mosaic/MosaicId");
const MosaicNonce_1 = require("../../src/model/mosaic/MosaicNonce");
const NamespaceId_1 = require("../../src/model/namespace/NamespaceId");
const AggregateTransaction_1 = require("../../src/model/transaction/AggregateTransaction");
const Deadline_1 = require("../../src/model/transaction/Deadline");
const MosaicDefinitionTransaction_1 = require("../../src/model/transaction/MosaicDefinitionTransaction");
const MosaicMetadataTransaction_1 = require("../../src/model/transaction/MosaicMetadataTransaction");
const NamespaceMetadataTransaction_1 = require("../../src/model/transaction/NamespaceMetadataTransaction");
const NamespaceRegistrationTransaction_1 = require("../../src/model/transaction/NamespaceRegistrationTransaction");
const TransactionType_1 = require("../../src/model/transaction/TransactionType");
const UInt64_1 = require("../../src/model/UInt64");
const MetadataTransactionService_1 = require("../../src/service/MetadataTransactionService");
describe('MetadataTransactionService', () => {
    const deadline = Deadline_1.Deadline.create();
    const key = UInt64_1.UInt64.fromUint(123);
    const newValue = 'new test value';
    let targetAccount;
    let metadataHttp;
    let transactionHttp;
    let mosaicId;
    let namespaceId;
    let config;
    let generationHash;
    before((done) => {
        const path = require('path');
        require('fs').readFile(path.resolve(__dirname, '../conf/network.conf'), (err, data) => {
            if (err) {
                throw err;
            }
            const json = JSON.parse(data);
            config = json;
            targetAccount = Account_1.Account.createFromPrivateKey(json.testAccount.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            generationHash = json.generationHash;
            metadataHttp = new MetadataHttp_1.MetadataHttp(json.apiUrl);
            transactionHttp = new TransactionHttp_1.TransactionHttp(json.apiUrl);
            done();
        });
    });
    /**
     * =========================
     * Setup test data
     * =========================
     */
    describe('MosaicDefinitionTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const nonce = MosaicNonce_1.MosaicNonce.createRandom();
            mosaicId = MosaicId_1.MosaicId.createFromNonce(nonce, targetAccount.publicAccount);
            const mosaicDefinitionTransaction = MosaicDefinitionTransaction_1.MosaicDefinitionTransaction.create(Deadline_1.Deadline.create(), nonce, mosaicId, MosaicFlags_1.MosaicFlags.create(true, true, true), 3, UInt64_1.UInt64.fromUint(1000), NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = mosaicDefinitionTransaction.signWith(targetAccount, generationHash);
            listener.confirmed(targetAccount.address).subscribe(() => {
                done();
            });
            listener.status(targetAccount.address).subscribe((error) => {
                console.log('Error:', error);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('Setup test NamespaceId', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('Announce NamespaceRegistrationTransaction', (done) => {
            const namespaceName = 'root-test-namespace-' + Math.floor(Math.random() * 10000);
            const registerNamespaceTransaction = NamespaceRegistrationTransaction_1.NamespaceRegistrationTransaction.createRootNamespace(Deadline_1.Deadline.create(), namespaceName, UInt64_1.UInt64.fromUint(9), NetworkType_1.NetworkType.MIJIN_TEST);
            namespaceId = new NamespaceId_1.NamespaceId(namespaceName);
            const signedTransaction = registerNamespaceTransaction.signWith(targetAccount, generationHash);
            listener.confirmed(targetAccount.address).subscribe(() => {
                done();
            });
            listener.status(targetAccount.address).subscribe((error) => {
                console.log('Error:', error);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('MosaicMetadataTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const mosaicMetadataTransaction = MosaicMetadataTransaction_1.MosaicMetadataTransaction.create(Deadline_1.Deadline.create(), targetAccount.publicKey, key, mosaicId, newValue.length, newValue, NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [mosaicMetadataTransaction.toAggregate(targetAccount.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction.signWith(targetAccount, generationHash);
            listener.confirmed(targetAccount.address).subscribe(() => {
                done();
            });
            listener.status(targetAccount.address).subscribe((error) => {
                console.log('Error:', error);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('NamespaceMetadataTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const namespaceMetadataTransaction = NamespaceMetadataTransaction_1.NamespaceMetadataTransaction.create(Deadline_1.Deadline.create(), targetAccount.publicKey, key, namespaceId, newValue.length, newValue, NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [namespaceMetadataTransaction.toAggregate(targetAccount.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction.signWith(targetAccount, generationHash);
            listener.confirmed(targetAccount.address).subscribe(() => {
                done();
            });
            listener.status(targetAccount.address).subscribe((error) => {
                console.log('Error:', error);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    /**
     * =========================
     * Test
     * =========================
     */
    describe('Test new services', () => {
        it('should create AccountMetadataTransaction - no current metadata', (done) => {
            const metaDataService = new MetadataTransactionService_1.MetadataTransactionService(metadataHttp);
            return metaDataService.createMetadataTransaction(deadline, NetworkType_1.NetworkType.MIJIN_TEST, MetadataType_1.MetadataType.Account, targetAccount.publicAccount, key, newValue, targetAccount.publicAccount).subscribe((transaction) => {
                chai_1.expect(transaction.type).to.be.equal(TransactionType_1.TransactionType.ACCOUNT_METADATA_TRANSACTION);
                chai_1.expect(transaction.scopedMetadataKey.toHex()).to.be.equal(key.toHex());
                chai_1.expect(transaction.value).to.be.equal(newValue);
                chai_1.expect(transaction.targetPublicKey).to.be.equal(targetAccount.publicKey);
                done();
            });
        });
        it('should create MosaicMetadataTransaction', (done) => {
            const metaDataService = new MetadataTransactionService_1.MetadataTransactionService(metadataHttp);
            return metaDataService.createMetadataTransaction(deadline, NetworkType_1.NetworkType.MIJIN_TEST, MetadataType_1.MetadataType.Mosaic, targetAccount.publicAccount, key, newValue + 'delta', targetAccount.publicAccount, mosaicId).subscribe((transaction) => {
                chai_1.expect(transaction.type).to.be.equal(TransactionType_1.TransactionType.MOSAIC_METADATA_TRANSACTION);
                chai_1.expect(transaction.scopedMetadataKey.toHex()).to.be.equal(key.toHex());
                chai_1.expect(transaction.valueSizeDelta).to.be.equal(5);
                chai_1.expect(transaction.value).to.be.equal(newValue + 'delta');
                chai_1.expect(transaction.targetPublicKey).to.be.equal(targetAccount.publicKey);
                chai_1.expect(transaction.targetMosaicId.toHex()).to.be.equal(mosaicId.toHex());
                done();
            });
        });
        it('should create NamespaceMetadataTransaction', (done) => {
            const metaDataService = new MetadataTransactionService_1.MetadataTransactionService(metadataHttp);
            return metaDataService.createMetadataTransaction(deadline, NetworkType_1.NetworkType.MIJIN_TEST, MetadataType_1.MetadataType.Namespace, targetAccount.publicAccount, key, newValue + 'delta', targetAccount.publicAccount, namespaceId).subscribe((transaction) => {
                chai_1.expect(transaction.type).to.be.equal(TransactionType_1.TransactionType.NAMESPACE_METADATA_TRANSACTION);
                chai_1.expect(transaction.scopedMetadataKey.toHex()).to.be.equal(key.toHex());
                chai_1.expect(transaction.valueSizeDelta).to.be.equal(5);
                chai_1.expect(transaction.value).to.be.equal(newValue + 'delta');
                chai_1.expect(transaction.targetPublicKey).to.be.equal(targetAccount.publicKey);
                chai_1.expect(transaction.targetNamespaceId.toHex()).to.be.equal(namespaceId.toHex());
                done();
            });
        });
    });
    describe('Announce transaction through service', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('should create MosaicMetadataTransaction and announce', (done) => {
            const metaDataService = new MetadataTransactionService_1.MetadataTransactionService(metadataHttp);
            return metaDataService.createMetadataTransaction(deadline, NetworkType_1.NetworkType.MIJIN_TEST, MetadataType_1.MetadataType.Mosaic, targetAccount.publicAccount, key, newValue + 'delta', targetAccount.publicAccount, mosaicId).subscribe((transaction) => {
                const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [transaction.toAggregate(targetAccount.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
                const signedTransaction = aggregateTransaction.signWith(targetAccount, generationHash);
                listener.confirmed(targetAccount.address).subscribe(() => {
                    done();
                });
                listener.status(targetAccount.address).subscribe((error) => {
                    console.log('Error:', error);
                    chai_1.assert(false);
                    done();
                });
                transactionHttp.announce(signedTransaction);
            });
        });
    });
    describe('Announce transaction through service with delta size increase', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('should create MosaicMetadataTransaction and announce', (done) => {
            const metaDataService = new MetadataTransactionService_1.MetadataTransactionService(metadataHttp);
            return metaDataService.createMetadataTransaction(deadline, NetworkType_1.NetworkType.MIJIN_TEST, MetadataType_1.MetadataType.Mosaic, targetAccount.publicAccount, key, newValue + 'delta' + 'extra delta', targetAccount.publicAccount, mosaicId).subscribe((transaction) => {
                const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [transaction.toAggregate(targetAccount.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
                const signedTransaction = aggregateTransaction.signWith(targetAccount, generationHash);
                listener.confirmed(targetAccount.address).subscribe(() => {
                    done();
                });
                listener.status(targetAccount.address).subscribe((error) => {
                    console.log('Error:', error);
                    chai_1.assert(false);
                    done();
                });
                transactionHttp.announce(signedTransaction);
            });
        });
    });
    describe('Announce transaction through service with delta size decrease', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('should create MosaicMetadataTransaction and announce', (done) => {
            const metaDataService = new MetadataTransactionService_1.MetadataTransactionService(metadataHttp);
            return metaDataService.createMetadataTransaction(deadline, NetworkType_1.NetworkType.MIJIN_TEST, MetadataType_1.MetadataType.Mosaic, targetAccount.publicAccount, key, newValue, targetAccount.publicAccount, mosaicId).subscribe((transaction) => {
                const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [transaction.toAggregate(targetAccount.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
                const signedTransaction = aggregateTransaction.signWith(targetAccount, generationHash);
                listener.confirmed(targetAccount.address).subscribe(() => {
                    done();
                });
                listener.status(targetAccount.address).subscribe((error) => {
                    console.log('Error:', error);
                    chai_1.assert(false);
                    done();
                });
                transactionHttp.announce(signedTransaction);
            });
        });
    });
});
//# sourceMappingURL=MetadataTransactionService.spec.js.map