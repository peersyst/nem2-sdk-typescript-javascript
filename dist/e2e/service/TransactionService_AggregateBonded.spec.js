"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const Listener_1 = require("../../src/infrastructure/Listener");
const NamespaceHttp_1 = require("../../src/infrastructure/NamespaceHttp");
const TransactionHttp_1 = require("../../src/infrastructure/TransactionHttp");
const Account_1 = require("../../src/model/account/Account");
const NetworkType_1 = require("../../src/model/blockchain/NetworkType");
const PlainMessage_1 = require("../../src/model/message/PlainMessage");
const Mosaic_1 = require("../../src/model/mosaic/Mosaic");
const NetworkCurrencyMosaic_1 = require("../../src/model/mosaic/NetworkCurrencyMosaic");
const NamespaceId_1 = require("../../src/model/namespace/NamespaceId");
const AggregateTransaction_1 = require("../../src/model/transaction/AggregateTransaction");
const Deadline_1 = require("../../src/model/transaction/Deadline");
const LockFundsTransaction_1 = require("../../src/model/transaction/LockFundsTransaction");
const MultisigAccountModificationTransaction_1 = require("../../src/model/transaction/MultisigAccountModificationTransaction");
const TransactionType_1 = require("../../src/model/transaction/TransactionType");
const TransferTransaction_1 = require("../../src/model/transaction/TransferTransaction");
const UInt64_1 = require("../../src/model/UInt64");
const TransactionService_1 = require("../../src/service/TransactionService");
const TransactionUtils_1 = require("../infrastructure/TransactionUtils");
describe('TransactionService', () => {
    let account;
    let account2;
    let multisigAccount;
    let cosignAccount1;
    let cosignAccount2;
    let cosignAccount3;
    let networkCurrencyMosaicId;
    let url;
    let generationHash;
    let transactionHttp;
    let transactionService;
    let namespaceHttp;
    let config;
    before((done) => {
        const path = require('path');
        require('fs').readFile(path.resolve(__dirname, '../conf/network.conf'), (err, data) => {
            if (err) {
                throw err;
            }
            const json = JSON.parse(data);
            config = json;
            account = Account_1.Account.createFromPrivateKey(json.testAccount.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            account2 = Account_1.Account.createFromPrivateKey(json.testAccount2.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            multisigAccount = Account_1.Account.createFromPrivateKey(json.multisigAccount.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            cosignAccount1 = Account_1.Account.createFromPrivateKey(json.cosignatoryAccount.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            cosignAccount2 = Account_1.Account.createFromPrivateKey(json.cosignatory2Account.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            cosignAccount3 = Account_1.Account.createFromPrivateKey(json.cosignatory3Account.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            url = json.apiUrl;
            generationHash = json.generationHash;
            transactionHttp = new TransactionHttp_1.TransactionHttp(url);
            namespaceHttp = new NamespaceHttp_1.NamespaceHttp(url);
            transactionService = new TransactionService_1.TransactionService(url);
            done();
        });
    });
    /**
     * =========================
     * Setup test data
     * =========================
     */
    describe('Get network currency mosaic id', () => {
        it('get mosaicId', (done) => {
            namespaceHttp.getLinkedMosaicId(new NamespaceId_1.NamespaceId('cat.currency')).subscribe((networkMosaicId) => {
                networkCurrencyMosaicId = networkMosaicId;
                done();
            });
        });
    });
    describe('Setup test multisig account', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('Announce MultisigAccountModificationTransaction', (done) => {
            const modifyMultisigAccountTransaction = MultisigAccountModificationTransaction_1.MultisigAccountModificationTransaction.create(Deadline_1.Deadline.create(), 2, 1, [
                cosignAccount1.publicAccount,
                cosignAccount2.publicAccount,
                cosignAccount3.publicAccount,
            ], [], NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [modifyMultisigAccountTransaction.toAggregate(multisigAccount.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction
                .signTransactionWithCosignatories(multisigAccount, [cosignAccount1, cosignAccount2, cosignAccount3], generationHash);
            listener.confirmed(multisigAccount.address).subscribe(() => {
                done();
            });
            listener.status(multisigAccount.address).subscribe((error) => {
                console.log('Error:', error);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    /**
     * =========================
     * Test
     * =========================
     */
    describe('should announce transaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('announce', (done) => {
            const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), account2.address, [
                NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(1),
            ], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = transferTransaction.signWith(account, generationHash);
            transactionService.announce(signedTransaction, listener).subscribe((tx) => {
                chai_1.expect(tx.signer.publicKey).to.be.equal(account.publicKey);
                chai_1.expect(tx.recipientAddress.equals(account2.address)).to.be.true;
                chai_1.expect(tx.message.payload).to.be.equal('test-message');
                done();
            });
        });
    });
    describe('should announce aggregate bonded with hashlock', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('announce', (done) => {
            const signedAggregatedTransaction = TransactionUtils_1.TransactionUtils.createSignedAggregatedBondTransaction(multisigAccount, account, account2.address, generationHash);
            const lockFundsTransaction = LockFundsTransaction_1.LockFundsTransaction.create(Deadline_1.Deadline.create(), new Mosaic_1.Mosaic(networkCurrencyMosaicId, UInt64_1.UInt64.fromUint(10 * Math.pow(10, NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.DIVISIBILITY))), UInt64_1.UInt64.fromUint(1000), signedAggregatedTransaction, NetworkType_1.NetworkType.MIJIN_TEST);
            const signedLockFundsTransaction = lockFundsTransaction.signWith(account, generationHash);
            transactionService
                .announceHashLockAggregateBonded(signedLockFundsTransaction, signedAggregatedTransaction, listener).subscribe((tx) => {
                chai_1.expect(tx.signer.publicKey).to.be.equal(account.publicKey);
                chai_1.expect(tx.type).to.be.equal(TransactionType_1.TransactionType.AGGREGATE_BONDED);
                done();
            });
        });
    });
    describe('should announce aggregate bonded transaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('announce', (done) => {
            const signedAggregatedTransaction = TransactionUtils_1.TransactionUtils.createSignedAggregatedBondTransaction(multisigAccount, account, account2.address, generationHash);
            const lockFundsTransaction = LockFundsTransaction_1.LockFundsTransaction.create(Deadline_1.Deadline.create(), new Mosaic_1.Mosaic(networkCurrencyMosaicId, UInt64_1.UInt64.fromUint(10 * Math.pow(10, NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.DIVISIBILITY))), UInt64_1.UInt64.fromUint(1000), signedAggregatedTransaction, NetworkType_1.NetworkType.MIJIN_TEST);
            const signedLockFundsTransaction = lockFundsTransaction.signWith(account, generationHash);
            transactionService.announce(signedLockFundsTransaction, listener).subscribe(() => {
                transactionService.announceAggregateBonded(signedAggregatedTransaction, listener).subscribe((tx) => {
                    chai_1.expect(tx.signer.publicKey).to.be.equal(account.publicKey);
                    chai_1.expect(tx.type).to.be.equal(TransactionType_1.TransactionType.AGGREGATE_BONDED);
                    done();
                });
            });
        });
    });
    /**
     * =========================
     * House Keeping
     * =========================
     */
    describe('Restore test multisig Accounts', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('Announce MultisigAccountModificationTransaction', (done) => {
            const removeCosigner1 = MultisigAccountModificationTransaction_1.MultisigAccountModificationTransaction.create(Deadline_1.Deadline.create(), -1, 0, [], [cosignAccount1.publicAccount,
            ], NetworkType_1.NetworkType.MIJIN_TEST);
            const removeCosigner2 = MultisigAccountModificationTransaction_1.MultisigAccountModificationTransaction.create(Deadline_1.Deadline.create(), 0, 0, [], [
                cosignAccount2.publicAccount,
            ], NetworkType_1.NetworkType.MIJIN_TEST);
            const removeCosigner3 = MultisigAccountModificationTransaction_1.MultisigAccountModificationTransaction.create(Deadline_1.Deadline.create(), -1, -1, [], [
                cosignAccount3.publicAccount,
            ], NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [removeCosigner1.toAggregate(multisigAccount.publicAccount),
                removeCosigner2.toAggregate(multisigAccount.publicAccount),
                removeCosigner3.toAggregate(multisigAccount.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction
                .signTransactionWithCosignatories(cosignAccount1, [cosignAccount2, cosignAccount3], generationHash);
            listener.confirmed(cosignAccount1.address).subscribe(() => {
                done();
            });
            listener.status(cosignAccount1.address).subscribe((error) => {
                console.log('Error:', error);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
});
//# sourceMappingURL=TransactionService_AggregateBonded.spec.js.map