"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const KeyGenerator_1 = require("../../src/core/format/KeyGenerator");
const Listener_1 = require("../../src/infrastructure/Listener");
const RestrictionMosaicHttp_1 = require("../../src/infrastructure/RestrictionMosaicHttp");
const TransactionHttp_1 = require("../../src/infrastructure/TransactionHttp");
const Account_1 = require("../../src/model/account/Account");
const NetworkType_1 = require("../../src/model/blockchain/NetworkType");
const MosaicFlags_1 = require("../../src/model/mosaic/MosaicFlags");
const MosaicId_1 = require("../../src/model/mosaic/MosaicId");
const MosaicNonce_1 = require("../../src/model/mosaic/MosaicNonce");
const MosaicRestrictionType_1 = require("../../src/model/restriction/MosaicRestrictionType");
const AggregateTransaction_1 = require("../../src/model/transaction/AggregateTransaction");
const Deadline_1 = require("../../src/model/transaction/Deadline");
const MosaicAddressRestrictionTransaction_1 = require("../../src/model/transaction/MosaicAddressRestrictionTransaction");
const MosaicDefinitionTransaction_1 = require("../../src/model/transaction/MosaicDefinitionTransaction");
const MosaicGlobalRestrictionTransaction_1 = require("../../src/model/transaction/MosaicGlobalRestrictionTransaction");
const TransactionType_1 = require("../../src/model/transaction/TransactionType");
const UInt64_1 = require("../../src/model/UInt64");
const MosaicRestrictionTransactionService_1 = require("../../src/service/MosaicRestrictionTransactionService");
describe('MosaicRestrictionTransactionService', () => {
    const deadline = Deadline_1.Deadline.create();
    const key = KeyGenerator_1.KeyGenerator.generateUInt64Key('TestKey');
    let targetAccount;
    let account;
    let restrictionHttp;
    let transactionHttp;
    let mosaicId;
    let config;
    let generationHash;
    before((done) => {
        const path = require('path');
        require('fs').readFile(path.resolve(__dirname, '../conf/network.conf'), (err, data) => {
            if (err) {
                throw err;
            }
            const json = JSON.parse(data);
            config = json;
            account = Account_1.Account.createFromPrivateKey(json.testAccount.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            targetAccount = Account_1.Account.createFromPrivateKey(json.testAccount3.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            generationHash = json.generationHash;
            restrictionHttp = new RestrictionMosaicHttp_1.RestrictionMosaicHttp(json.apiUrl);
            transactionHttp = new TransactionHttp_1.TransactionHttp(json.apiUrl);
            done();
        });
    });
    /**
     * =========================
     * Setup test data
     * =========================
     */
    describe('MosaicDefinitionTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const nonce = MosaicNonce_1.MosaicNonce.createRandom();
            mosaicId = MosaicId_1.MosaicId.createFromNonce(nonce, account.publicAccount);
            const mosaicDefinitionTransaction = MosaicDefinitionTransaction_1.MosaicDefinitionTransaction.create(Deadline_1.Deadline.create(), nonce, mosaicId, MosaicFlags_1.MosaicFlags.create(true, true, true), 3, UInt64_1.UInt64.fromUint(1000), NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = mosaicDefinitionTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('MosaicGlobalRestrictionTransaction - with referenceMosaicId', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const mosaicGlobalRestrictionTransaction = MosaicGlobalRestrictionTransaction_1.MosaicGlobalRestrictionTransaction.create(Deadline_1.Deadline.create(), mosaicId, key, UInt64_1.UInt64.fromUint(0), MosaicRestrictionType_1.MosaicRestrictionType.NONE, UInt64_1.UInt64.fromUint(0), MosaicRestrictionType_1.MosaicRestrictionType.GE, NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = mosaicGlobalRestrictionTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('MosaicAddressRestrictionTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const mosaicAddressRestrictionTransaction = MosaicAddressRestrictionTransaction_1.MosaicAddressRestrictionTransaction.create(Deadline_1.Deadline.create(), mosaicId, key, targetAccount.address, UInt64_1.UInt64.fromUint(2), NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [mosaicAddressRestrictionTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    /**
     * =========================
     * Test
     * =========================
     */
    describe('Test new services', () => {
        it('should create MosaicGlobalRestrictionTransaction', (done) => {
            const service = new MosaicRestrictionTransactionService_1.MosaicRestrictionTransactionService(restrictionHttp);
            return service.createMosaicGlobalRestrictionTransaction(deadline, NetworkType_1.NetworkType.MIJIN_TEST, mosaicId, key, '1', MosaicRestrictionType_1.MosaicRestrictionType.GE).subscribe((transaction) => {
                chai_1.expect(transaction.type).to.be.equal(TransactionType_1.TransactionType.MOSAIC_GLOBAL_RESTRICTION);
                chai_1.expect(transaction.previousRestrictionValue.toString()).to.be.equal('0');
                chai_1.expect(transaction.previousRestrictionType).to.be.equal(MosaicRestrictionType_1.MosaicRestrictionType.GE);
                chai_1.expect(transaction.newRestrictionValue.toString()).to.be.equal('1');
                chai_1.expect(transaction.newRestrictionType).to.be.equal(MosaicRestrictionType_1.MosaicRestrictionType.GE);
                chai_1.expect(transaction.restrictionKey.toHex()).to.be.equal(key.toHex());
                done();
            });
        });
        it('should create MosaicAddressRestrictionTransaction', (done) => {
            const service = new MosaicRestrictionTransactionService_1.MosaicRestrictionTransactionService(restrictionHttp);
            return service.createMosaicAddressRestrictionTransaction(deadline, NetworkType_1.NetworkType.MIJIN_TEST, mosaicId, key, targetAccount.address, '3').subscribe((transaction) => {
                chai_1.expect(transaction.type).to.be.equal(TransactionType_1.TransactionType.MOSAIC_ADDRESS_RESTRICTION);
                chai_1.expect(transaction.previousRestrictionValue.toString()).to.be.equal('2');
                chai_1.expect(transaction.newRestrictionValue.toString()).to.be.equal('3');
                chai_1.expect(transaction.targetAddressToString()).to.be.equal(targetAccount.address.plain());
                chai_1.expect(transaction.restrictionKey.toHex()).to.be.equal(key.toHex());
                done();
            });
        });
    });
    describe('Announce MosaicGlobalRestriction through service', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('should create MosaicGlobalRestriction and announce', (done) => {
            const service = new MosaicRestrictionTransactionService_1.MosaicRestrictionTransactionService(restrictionHttp);
            return service.createMosaicGlobalRestrictionTransaction(deadline, NetworkType_1.NetworkType.MIJIN_TEST, mosaicId, key, '1', MosaicRestrictionType_1.MosaicRestrictionType.GE).subscribe((transaction) => {
                const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [transaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
                const signedTransaction = aggregateTransaction.signWith(account, generationHash);
                listener.confirmed(account.address).subscribe(() => {
                    done();
                });
                listener.status(account.address).subscribe((error) => {
                    console.log('Error:', error);
                    chai_1.assert(false);
                    done();
                });
                transactionHttp.announce(signedTransaction);
            });
        });
    });
    describe('Announce MosaicAddressRestriction through service', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('should create MosaicAddressRestriction and announce', (done) => {
            const service = new MosaicRestrictionTransactionService_1.MosaicRestrictionTransactionService(restrictionHttp);
            return service.createMosaicAddressRestrictionTransaction(deadline, NetworkType_1.NetworkType.MIJIN_TEST, mosaicId, key, targetAccount.address, '3').subscribe((transaction) => {
                const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [transaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
                const signedTransaction = aggregateTransaction.signWith(account, generationHash);
                listener.confirmed(account.address).subscribe(() => {
                    done();
                });
                listener.status(account.address).subscribe((error) => {
                    console.log('Error:', error);
                    chai_1.assert(false);
                    done();
                });
                transactionHttp.announce(signedTransaction);
            });
        });
    });
});
//# sourceMappingURL=MosaicRestrictionTransactionService.spec.js.map