"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const chai_2 = require("chai");
const Convert_1 = require("../../src/core/format/Convert");
const Listener_1 = require("../../src/infrastructure/Listener");
const NamespaceHttp_1 = require("../../src/infrastructure/NamespaceHttp");
const TransactionHttp_1 = require("../../src/infrastructure/TransactionHttp");
const Account_1 = require("../../src/model/account/Account");
const NetworkType_1 = require("../../src/model/blockchain/NetworkType");
const PlainMessage_1 = require("../../src/model/message/PlainMessage");
const Mosaic_1 = require("../../src/model/mosaic/Mosaic");
const MosaicFlags_1 = require("../../src/model/mosaic/MosaicFlags");
const MosaicId_1 = require("../../src/model/mosaic/MosaicId");
const MosaicNonce_1 = require("../../src/model/mosaic/MosaicNonce");
const MosaicSupplyChangeAction_1 = require("../../src/model/mosaic/MosaicSupplyChangeAction");
const NetworkCurrencyMosaic_1 = require("../../src/model/mosaic/NetworkCurrencyMosaic");
const AliasAction_1 = require("../../src/model/namespace/AliasAction");
const NamespaceId_1 = require("../../src/model/namespace/NamespaceId");
const AddressAliasTransaction_1 = require("../../src/model/transaction/AddressAliasTransaction");
const AggregateTransaction_1 = require("../../src/model/transaction/AggregateTransaction");
const Deadline_1 = require("../../src/model/transaction/Deadline");
const MosaicAliasTransaction_1 = require("../../src/model/transaction/MosaicAliasTransaction");
const MosaicDefinitionTransaction_1 = require("../../src/model/transaction/MosaicDefinitionTransaction");
const MosaicMetadataTransaction_1 = require("../../src/model/transaction/MosaicMetadataTransaction");
const MosaicSupplyChangeTransaction_1 = require("../../src/model/transaction/MosaicSupplyChangeTransaction");
const NamespaceRegistrationTransaction_1 = require("../../src/model/transaction/NamespaceRegistrationTransaction");
const TransferTransaction_1 = require("../../src/model/transaction/TransferTransaction");
const UInt64_1 = require("../../src/model/UInt64");
const TransactionService_1 = require("../../src/service/TransactionService");
describe('TransactionService', () => {
    let account;
    let account2;
    let account3;
    let account4;
    let url;
    let generationHash;
    let namespaceHttp;
    let addressAlias;
    let mosaicAlias;
    let mosaicId;
    let newMosaicId;
    let transactionHttp;
    let config;
    let transactionHashes;
    let transactionHashesMultiple;
    before((done) => {
        const path = require('path');
        require('fs').readFile(path.resolve(__dirname, '../conf/network.conf'), (err, data) => {
            if (err) {
                throw err;
            }
            const json = JSON.parse(data);
            config = json;
            account = Account_1.Account.createFromPrivateKey(json.testAccount.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            account2 = Account_1.Account.createFromPrivateKey(json.testAccount2.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            account3 = Account_1.Account.createFromPrivateKey(json.testAccount3.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            account4 = Account_1.Account.createFromPrivateKey(json.cosignatory4Account.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            url = json.apiUrl;
            generationHash = json.generationHash;
            namespaceHttp = new NamespaceHttp_1.NamespaceHttp(json.apiUrl);
            transactionHttp = new TransactionHttp_1.TransactionHttp(json.apiUrl);
            transactionHashes = [];
            transactionHashesMultiple = [];
            done();
        });
    });
    /**
     * =========================
     * Setup test data
     * =========================
     */
    describe('Create address alias NamespaceId', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('Announce NamespaceRegistrationTransaction', (done) => {
            const namespaceName = 'root-test-namespace-' + Math.floor(Math.random() * 10000);
            const registerNamespaceTransaction = NamespaceRegistrationTransaction_1.NamespaceRegistrationTransaction.createRootNamespace(Deadline_1.Deadline.create(), namespaceName, UInt64_1.UInt64.fromUint(20), NetworkType_1.NetworkType.MIJIN_TEST);
            addressAlias = new NamespaceId_1.NamespaceId(namespaceName);
            const signedTransaction = registerNamespaceTransaction.signWith(account, generationHash);
            transactionHashes.push(signedTransaction.hash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('Create mosaic alias NamespaceId', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('Announce NamespaceRegistrationTransaction', (done) => {
            const namespaceName = 'root-test-namespace-' + Math.floor(Math.random() * 10000);
            const registerNamespaceTransaction = NamespaceRegistrationTransaction_1.NamespaceRegistrationTransaction.createRootNamespace(Deadline_1.Deadline.create(), namespaceName, UInt64_1.UInt64.fromUint(50), NetworkType_1.NetworkType.MIJIN_TEST);
            mosaicAlias = new NamespaceId_1.NamespaceId(namespaceName);
            const signedTransaction = registerNamespaceTransaction.signWith(account, generationHash);
            transactionHashes.push(signedTransaction.hash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('Setup test AddressAlias', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('Announce addressAliasTransaction', (done) => {
            const addressAliasTransaction = AddressAliasTransaction_1.AddressAliasTransaction.create(Deadline_1.Deadline.create(), AliasAction_1.AliasAction.Link, addressAlias, account.address, NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = addressAliasTransaction.signWith(account, generationHash);
            transactionHashes.push(signedTransaction.hash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('Setup test MosaicId', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('Announce MosaicDefinitionTransaction', (done) => {
            const nonce = MosaicNonce_1.MosaicNonce.createRandom();
            mosaicId = MosaicId_1.MosaicId.createFromNonce(nonce, account.publicAccount);
            const mosaicDefinitionTransaction = MosaicDefinitionTransaction_1.MosaicDefinitionTransaction.create(Deadline_1.Deadline.create(), nonce, mosaicId, MosaicFlags_1.MosaicFlags.create(true, true, false), 3, UInt64_1.UInt64.fromUint(50), NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = mosaicDefinitionTransaction.signWith(account, generationHash);
            transactionHashes.push(signedTransaction.hash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('MosaicSupplyChangeTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const mosaicSupplyChangeTransaction = MosaicSupplyChangeTransaction_1.MosaicSupplyChangeTransaction.create(Deadline_1.Deadline.create(), mosaicId, MosaicSupplyChangeAction_1.MosaicSupplyChangeAction.Increase, UInt64_1.UInt64.fromUint(200000), NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = mosaicSupplyChangeTransaction.signWith(account, generationHash);
            transactionHashes.push(signedTransaction.hash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('Setup MosaicAlias', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('Announce MosaicAliasTransaction', (done) => {
            const mosaicAliasTransaction = MosaicAliasTransaction_1.MosaicAliasTransaction.create(Deadline_1.Deadline.create(), AliasAction_1.AliasAction.Link, mosaicAlias, mosaicId, NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = mosaicAliasTransaction.signWith(account, generationHash);
            transactionHashes.push(signedTransaction.hash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('Create Transfer with alias', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('Announce TransferTransaction', (done) => {
            const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), addressAlias, [
                NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(1),
                new Mosaic_1.Mosaic(mosaicAlias, UInt64_1.UInt64.fromUint(1)),
            ], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = transferTransaction.signWith(account, generationHash);
            transactionHashes.push(signedTransaction.hash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    /**
     * =====================================
     * Setup test aggregate transaction data
     * =====================================
     */
    describe('Create Aggreate TransferTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const signedTransaction = buildAggregateTransaction().signWith(account, generationHash);
            transactionHashes.push(signedTransaction.hash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    /**
     * =====================================
     * Setup test Multiple transaction on same block
     * =====================================
     */
    describe('Transfer mosaic to account 3', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('Announce TransferTransaction', (done) => {
            const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), account3.address, [
                new Mosaic_1.Mosaic(mosaicAlias, UInt64_1.UInt64.fromUint(200)),
            ], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = transferTransaction.signWith(account, generationHash);
            transactionHashes.push(signedTransaction.hash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('Create multiple transfers with alias', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('Announce TransferTransaction', (done) => {
            const transactions = [];
            // 1. Transfer A -> B
            const transaction1 = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), account2.address, [
                new Mosaic_1.Mosaic(mosaicAlias, UInt64_1.UInt64.fromUint(1)),
            ], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
            transactions.push(transaction1.signWith(account, generationHash));
            // 2. Transfer C -> D
            const transaction2 = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), account4.address, [
                new Mosaic_1.Mosaic(mosaicAlias, UInt64_1.UInt64.fromUint(1)),
            ], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
            transactions.push(transaction2.signWith(account3, generationHash));
            // 3. Aggregate
            const lastSignedTx = buildAggregateTransaction().signWith(account, generationHash);
            transactions.push(lastSignedTx);
            transactions.forEach((tx) => {
                transactionHashesMultiple.push(tx.hash);
                transactionHttp.announce(tx);
            });
            listener.confirmed(account.address, lastSignedTx.hash).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            listener.status(account3.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
        });
    });
    /**
     * =========================
     * Test
     * =========================
     */
    describe('should return resolved transaction', () => {
        it('call transaction service', (done) => {
            const transactionService = new TransactionService_1.TransactionService(url);
            transactionService.resolveAliases(transactionHashes).subscribe((transactions) => {
                chai_2.expect(transactions.length).to.be.equal(8);
                transactions.map((tx) => {
                    if (tx instanceof TransferTransaction_1.TransferTransaction) {
                        chai_2.expect(tx.recipientAddress.plain()).to.be.equal(account.address.plain());
                        chai_2.expect(tx.mosaics.find((m) => m.id.toHex() === mosaicId.toHex())).not.to.equal(undefined);
                    }
                    else if (tx instanceof AggregateTransaction_1.AggregateTransaction) {
                        chai_2.expect(tx.innerTransactions.length).to.be.equal(5);
                        // Assert Transfer
                        chai_2.expect(tx.innerTransactions[0].recipientAddress
                            .plain()).to.be.equal(account.address.plain());
                        chai_2.expect(tx.innerTransactions[0].mosaics
                            .find((m) => m.id.toHex() === mosaicId.toHex())).not.to.equal(undefined);
                        // Assert MosaicMeta
                        chai_2.expect(tx.innerTransactions[4]
                            .targetMosaicId.toHex() === newMosaicId.toHex()).to.be.true;
                    }
                });
            }, done());
        });
    });
    describe('Test resolve alias with multiple transaction in single block', () => {
        it('call transaction service', (done) => {
            const transactionService = new TransactionService_1.TransactionService(url);
            transactionService.resolveAliases(transactionHashesMultiple).subscribe((tx) => {
                chai_2.expect(tx.length).to.be.equal(3);
                chai_2.expect(tx[0].mosaics[0].id.toHex()).to.be.equal(mosaicId.toHex());
                chai_2.expect(tx[1].mosaics[0].id.toHex()).to.be.equal(mosaicId.toHex());
                chai_2.expect(tx[2].innerTransactions[4]
                    .targetMosaicId.toHex()).to.be.equal(newMosaicId.toHex());
            }, done());
        });
    });
    function buildAggregateTransaction() {
        const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), addressAlias, [
            NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(1),
            new Mosaic_1.Mosaic(mosaicAlias, UInt64_1.UInt64.fromUint(1)),
        ], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
        // Unlink MosaicAlias
        const mosaicAliasTransactionUnlink = MosaicAliasTransaction_1.MosaicAliasTransaction.create(Deadline_1.Deadline.create(), AliasAction_1.AliasAction.Unlink, mosaicAlias, mosaicId, NetworkType_1.NetworkType.MIJIN_TEST);
        // Create a new Mosaic
        const nonce = MosaicNonce_1.MosaicNonce.createRandom();
        newMosaicId = MosaicId_1.MosaicId.createFromNonce(nonce, account.publicAccount);
        const mosaicDefinitionTransaction = MosaicDefinitionTransaction_1.MosaicDefinitionTransaction.create(Deadline_1.Deadline.create(), nonce, newMosaicId, MosaicFlags_1.MosaicFlags.create(true, true, false), 3, UInt64_1.UInt64.fromUint(0), NetworkType_1.NetworkType.MIJIN_TEST);
        // Link namespace with new MosaicId
        const mosaicAliasTransactionRelink = MosaicAliasTransaction_1.MosaicAliasTransaction.create(Deadline_1.Deadline.create(), AliasAction_1.AliasAction.Link, mosaicAlias, newMosaicId, NetworkType_1.NetworkType.MIJIN_TEST);
        // Use new mosaicAlias in metadata
        const mosaicMetadataTransaction = MosaicMetadataTransaction_1.MosaicMetadataTransaction.create(Deadline_1.Deadline.create(), account.publicKey, UInt64_1.UInt64.fromUint(5), mosaicAlias, 10, Convert_1.Convert.uint8ToUtf8(new Uint8Array(10)), NetworkType_1.NetworkType.MIJIN_TEST);
        return AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [
            transferTransaction.toAggregate(account.publicAccount),
            mosaicAliasTransactionUnlink.toAggregate(account.publicAccount),
            mosaicDefinitionTransaction.toAggregate(account.publicAccount),
            mosaicAliasTransactionRelink.toAggregate(account.publicAccount),
            mosaicMetadataTransaction.toAggregate(account.publicAccount),
        ], NetworkType_1.NetworkType.MIJIN_TEST, []);
    }
});
//# sourceMappingURL=TransactionService.spec.js.map