"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const operators_1 = require("rxjs/operators");
const BlockHttp_1 = require("../../src/infrastructure/BlockHttp");
const infrastructure_1 = require("../../src/infrastructure/infrastructure");
const QueryParams_1 = require("../../src/infrastructure/QueryParams");
const Account_1 = require("../../src/model/account/Account");
const NetworkType_1 = require("../../src/model/blockchain/NetworkType");
const PlainMessage_1 = require("../../src/model/message/PlainMessage");
const NetworkCurrencyMosaic_1 = require("../../src/model/mosaic/NetworkCurrencyMosaic");
const Deadline_1 = require("../../src/model/transaction/Deadline");
const TransferTransaction_1 = require("../../src/model/transaction/TransferTransaction");
describe('BlockHttp', () => {
    let account;
    let account2;
    let blockHttp;
    let receiptHttp;
    let transactionHttp;
    let blockReceiptHash = '';
    let blockTransactionHash = '';
    let config;
    let chainHeight;
    let generationHash;
    before((done) => {
        const path = require('path');
        require('fs').readFile(path.resolve(__dirname, '../conf/network.conf'), (err, data) => {
            if (err) {
                throw err;
            }
            const json = JSON.parse(data);
            config = json;
            account = Account_1.Account.createFromPrivateKey(json.testAccount.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            account2 = Account_1.Account.createFromPrivateKey(json.testAccount2.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            blockHttp = new BlockHttp_1.BlockHttp(json.apiUrl);
            transactionHttp = new infrastructure_1.TransactionHttp(json.apiUrl);
            receiptHttp = new infrastructure_1.ReceiptHttp(json.apiUrl);
            generationHash = json.generationHash;
            done();
        });
    });
    /**
     * =========================
     * Setup Test Data
     * =========================
     */
    describe('Setup Test Data', () => {
        let listener;
        before(() => {
            listener = new infrastructure_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('Announce TransferTransaction', (done) => {
            const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), account2.address, [NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(1)], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = transferTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                chainHeight = transaction.transactionInfo.height.toString();
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('getBlockByHeight', () => {
        it('should return block info given height', (done) => {
            blockHttp.getBlockByHeight('1')
                .subscribe((blockInfo) => {
                blockReceiptHash = blockInfo.blockReceiptsHash;
                blockTransactionHash = blockInfo.blockTransactionsHash;
                chai_1.expect(blockInfo.height.lower).to.be.equal(1);
                chai_1.expect(blockInfo.height.higher).to.be.equal(0);
                chai_1.expect(blockInfo.timestamp.lower).to.be.equal(0);
                chai_1.expect(blockInfo.timestamp.higher).to.be.equal(0);
                done();
            });
        });
    });
    describe('getBlockTransactions', () => {
        let nextId;
        let firstId;
        it('should return block transactions data given height', (done) => {
            blockHttp.getBlockTransactions('1')
                .subscribe((transactions) => {
                nextId = transactions[0].transactionInfo.id;
                firstId = transactions[1].transactionInfo.id;
                chai_1.expect(transactions.length).to.be.greaterThan(0);
                done();
            });
        });
        it('should return block transactions data given height with paginated transactionId', (done) => {
            blockHttp.getBlockTransactions('1', new QueryParams_1.QueryParams(10, nextId))
                .subscribe((transactions) => {
                chai_1.expect(transactions[0].transactionInfo.id).to.be.equal(firstId);
                chai_1.expect(transactions.length).to.be.greaterThan(0);
                done();
            });
        });
    });
    describe('getBlocksByHeightWithLimit', () => {
        it('should return block info given height and limit', (done) => {
            blockHttp.getBlocksByHeightWithLimit(chainHeight, 50)
                .subscribe((blocksInfo) => {
                chai_1.expect(blocksInfo.length).to.be.greaterThan(0);
                done();
            });
        });
    });
    describe('getMerkleReceipts', () => {
        it('should return Merkle Receipts', (done) => {
            receiptHttp.getBlockReceipts(chainHeight).pipe(operators_1.mergeMap((_) => {
                return receiptHttp.getMerkleReceipts(chainHeight, _.transactionStatements[0].generateHash());
            }))
                .subscribe((merkleReceipts) => {
                chai_1.expect(merkleReceipts.merklePath).not.to.be.null;
                done();
            });
        });
    });
    describe('getMerkleTransaction', () => {
        it('should return Merkle Transaction', (done) => {
            blockHttp.getBlockTransactions(chainHeight).pipe(operators_1.mergeMap((_) => {
                const hash = _[0].transactionInfo.hash;
                if (hash) {
                    return blockHttp.getMerkleTransaction(chainHeight, hash);
                }
                // If reaching this line, something is not right
                throw new Error('Tansacation hash is undefined');
            }))
                .subscribe((merkleTransactionss) => {
                chai_1.expect(merkleTransactionss.merklePath).not.to.be.null;
                done();
            });
        });
    });
    describe('getBlockReceipts', () => {
        it('should return block receipts', (done) => {
            receiptHttp.getBlockReceipts(chainHeight)
                .subscribe((statement) => {
                chai_1.expect(statement.transactionStatements).not.to.be.null;
                chai_1.expect(statement.transactionStatements.length).to.be.greaterThan(0);
                done();
            });
        });
    });
});
//# sourceMappingURL=BlockHttp.spec.js.map