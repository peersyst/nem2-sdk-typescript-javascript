"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const assert_1 = require("assert");
const chai_1 = require("chai");
const Address_1 = require("../../../src/model/account/Address");
const MosaicId_1 = require("../../../src/model/mosaic/MosaicId");
const NamespaceId_1 = require("../../../src/model/namespace/NamespaceId");
const TransactionType_1 = require("../../../src/model/transaction/TransactionType");
const UInt64_1 = require("../../../src/model/UInt64");
const ValidateTransaction = {
    validateStandaloneTx: (transaction, transactionDTO) => {
        assert_1.deepEqual(transaction.transactionInfo.height, UInt64_1.UInt64.fromNumericString(transactionDTO.meta.height));
        chai_1.expect(transaction.transactionInfo.hash)
            .to.be.equal(transactionDTO.meta.hash);
        chai_1.expect(transaction.transactionInfo.merkleComponentHash)
            .to.be.equal(transactionDTO.meta.merkleComponentHash);
        chai_1.expect(transaction.transactionInfo.index)
            .to.be.equal(transactionDTO.meta.index);
        chai_1.expect(transaction.transactionInfo.id)
            .to.be.equal(transactionDTO.meta.id);
        chai_1.expect(transaction.signature)
            .to.be.equal(transactionDTO.transaction.signature);
        chai_1.expect(transaction.signer.publicKey)
            .to.be.equal(transactionDTO.transaction.signerPublicKey);
        chai_1.expect(transaction.networkType)
            .to.be.equal(transactionDTO.transaction.network);
        chai_1.expect(transaction.version)
            .to.be.equal(transactionDTO.transaction.version);
        chai_1.expect(transaction.type)
            .to.be.equal(transactionDTO.transaction.type);
        assert_1.deepEqual(transaction.maxFee, UInt64_1.UInt64.fromNumericString(transactionDTO.transaction.maxFee));
        assert_1.deepEqual(transaction.deadline.toString(), transactionDTO.transaction.deadline);
        if (transaction.type === TransactionType_1.TransactionType.TRANSFER) {
            ValidateTransaction.validateTransferTx(transaction, transactionDTO);
        }
        else if (transaction.type === TransactionType_1.TransactionType.REGISTER_NAMESPACE) {
            ValidateTransaction.validateNamespaceCreationTx(transaction, transactionDTO);
        }
        else if (transaction.type === TransactionType_1.TransactionType.MOSAIC_DEFINITION) {
            ValidateTransaction.validateMosaicCreationTx(transaction, transactionDTO);
        }
        else if (transaction.type === TransactionType_1.TransactionType.MOSAIC_SUPPLY_CHANGE) {
            ValidateTransaction.validateMosaicSupplyChangeTx(transaction, transactionDTO);
        }
        else if (transaction.type === TransactionType_1.TransactionType.MODIFY_MULTISIG_ACCOUNT) {
            ValidateTransaction.validateMultisigModificationTx(transaction, transactionDTO);
        }
    },
    validateAggregateTx: (aggregateTransaction, aggregateTransactionDTO) => {
        assert_1.deepEqual(aggregateTransaction.transactionInfo.height, UInt64_1.UInt64.fromNumericString(aggregateTransactionDTO.meta.height));
        chai_1.expect(aggregateTransaction.transactionInfo.hash)
            .to.be.equal(aggregateTransactionDTO.meta.hash);
        chai_1.expect(aggregateTransaction.transactionInfo.merkleComponentHash)
            .to.be.equal(aggregateTransactionDTO.meta.merkleComponentHash);
        chai_1.expect(aggregateTransaction.transactionInfo.index)
            .to.be.equal(aggregateTransactionDTO.meta.index);
        chai_1.expect(aggregateTransaction.transactionInfo.id)
            .to.be.equal(aggregateTransactionDTO.meta.id);
        chai_1.expect(aggregateTransaction.signature)
            .to.be.equal(aggregateTransactionDTO.transaction.signature);
        chai_1.expect(aggregateTransaction.signer.publicKey)
            .to.be.equal(aggregateTransactionDTO.transaction.signerPublicKey);
        chai_1.expect(aggregateTransaction.networkType)
            .to.be.equal(aggregateTransactionDTO.transaction.network);
        chai_1.expect(aggregateTransaction.version)
            .to.be.equal(aggregateTransactionDTO.transaction.version);
        chai_1.expect(aggregateTransaction.type)
            .to.be.equal(aggregateTransactionDTO.transaction.type);
        assert_1.deepEqual(aggregateTransaction.maxFee, UInt64_1.UInt64.fromNumericString(aggregateTransactionDTO.transaction.maxFee));
        assert_1.deepEqual(aggregateTransaction.deadline.toString(), aggregateTransactionDTO.transaction.deadline);
        ValidateTransaction.validateStandaloneTx(aggregateTransaction.innerTransactions[0], aggregateTransactionDTO.transaction.transactions[0]);
    },
    validateMosaicCreationTx: (mosaicDefinitionTransaction, mosaicDefinitionTransactionDTO) => {
        assert_1.deepEqual(mosaicDefinitionTransaction.mosaicId, new MosaicId_1.MosaicId(mosaicDefinitionTransactionDTO.transaction.id));
        chai_1.expect(mosaicDefinitionTransaction.divisibility)
            .to.be.equal(mosaicDefinitionTransactionDTO.transaction.divisibility);
        assert_1.deepEqual(mosaicDefinitionTransaction.duration, UInt64_1.UInt64.fromNumericString(mosaicDefinitionTransactionDTO.transaction.duration));
        chai_1.expect(mosaicDefinitionTransaction.flags.supplyMutable)
            .to.be.equal(true);
        chai_1.expect(mosaicDefinitionTransaction.flags.transferable)
            .to.be.equal(true);
    },
    validateMosaicSupplyChangeTx: (mosaicSupplyChangeTransaction, mosaicSupplyChangeTransactionDTO) => {
        assert_1.deepEqual(mosaicSupplyChangeTransaction.mosaicId, new MosaicId_1.MosaicId(mosaicSupplyChangeTransactionDTO.transaction.mosaicId));
        chai_1.expect(mosaicSupplyChangeTransaction.action)
            .to.be.equal(mosaicSupplyChangeTransactionDTO.transaction.action);
        assert_1.deepEqual(mosaicSupplyChangeTransaction.delta, UInt64_1.UInt64.fromNumericString(mosaicSupplyChangeTransactionDTO.transaction.delta));
    },
    validateMultisigModificationTx: (modifyMultisigAccountTransaction, modifyMultisigAccountTransactionDTO) => {
        chai_1.expect(modifyMultisigAccountTransaction.minApprovalDelta)
            .to.be.equal(modifyMultisigAccountTransactionDTO.transaction.minApprovalDelta);
        chai_1.expect(modifyMultisigAccountTransaction.minRemovalDelta)
            .to.be.equal(modifyMultisigAccountTransactionDTO.transaction.minRemovalDelta);
        chai_1.expect(modifyMultisigAccountTransaction.publicKeyAdditions.length).to.be.equal(1);
        chai_1.expect(modifyMultisigAccountTransaction.publicKeyDeletions.length).to.be.equal(0);
    },
    validateNamespaceCreationTx: (registerNamespaceTransaction, registerNamespaceTransactionDTO) => {
        chai_1.expect(registerNamespaceTransaction.registrationType)
            .to.be.equal(registerNamespaceTransactionDTO.transaction.registrationType);
        chai_1.expect(registerNamespaceTransaction.namespaceName)
            .to.be.equal(registerNamespaceTransactionDTO.transaction.name);
        assert_1.deepEqual(registerNamespaceTransaction.namespaceId, NamespaceId_1.NamespaceId.createFromEncoded(registerNamespaceTransactionDTO.transaction.id));
        if (registerNamespaceTransaction.registrationType === 0) {
            assert_1.deepEqual(registerNamespaceTransaction.duration, UInt64_1.UInt64.fromNumericString(registerNamespaceTransactionDTO.transaction.duration));
        }
        else {
            assert_1.deepEqual(registerNamespaceTransaction.parentId, NamespaceId_1.NamespaceId.createFromEncoded(registerNamespaceTransactionDTO.transaction.parentId));
        }
    },
    validateTransferTx: (transferTransaction, transferTransactionDTO) => {
        assert_1.deepEqual(transferTransaction.recipientAddress, Address_1.Address.createFromEncoded(transferTransactionDTO.transaction.recipientAddress));
        chai_1.expect(transferTransaction.message.payload)
            .to.be.equal('test-message');
    },
};
exports.default = ValidateTransaction;
//# sourceMappingURL=ValidateTransaction.js.map