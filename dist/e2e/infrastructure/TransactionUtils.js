"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const js_joda_1 = require("js-joda");
const PublicAccount_1 = require("../../src/model/account/PublicAccount");
const NetworkType_1 = require("../../src/model/blockchain/NetworkType");
const PlainMessage_1 = require("../../src/model/message/PlainMessage");
const Mosaic_1 = require("../../src/model/mosaic/Mosaic");
const NetworkCurrencyMosaic_1 = require("../../src/model/mosaic/NetworkCurrencyMosaic");
const AggregateTransaction_1 = require("../../src/model/transaction/AggregateTransaction");
const CosignatureTransaction_1 = require("../../src/model/transaction/CosignatureTransaction");
const Deadline_1 = require("../../src/model/transaction/Deadline");
const LockFundsTransaction_1 = require("../../src/model/transaction/LockFundsTransaction");
const MultisigAccountModificationTransaction_1 = require("../../src/model/transaction/MultisigAccountModificationTransaction");
const TransferTransaction_1 = require("../../src/model/transaction/TransferTransaction");
const UInt64_1 = require("../../src/model/UInt64");
class TransactionUtils {
    static createAndAnnounce(signer, recipient, transactionHttp, mosaic = [], generationHash) {
        const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), recipient, mosaic, PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
        const signedTransaction = signer.sign(transferTransaction, generationHash);
        transactionHttp.announce(signedTransaction);
    }
    static announceAggregateBoundedTransaction(signedTransaction, transactionHttp) {
        transactionHttp.announceAggregateBonded(signedTransaction);
    }
    static createSignedAggregatedBondTransaction(aggregatedTo, signer, recipient, generationHash) {
        const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), recipient, [], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
        const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createBonded(Deadline_1.Deadline.create(2, js_joda_1.ChronoUnit.MINUTES), [transferTransaction.toAggregate(aggregatedTo.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
        return signer.sign(aggregateTransaction, generationHash);
    }
    static createHashLockTransactionAndAnnounce(signedAggregatedTransaction, signer, mosaicId, transactionHttp, generationHash) {
        const lockFundsTransaction = LockFundsTransaction_1.LockFundsTransaction.create(Deadline_1.Deadline.create(), new Mosaic_1.Mosaic(mosaicId, UInt64_1.UInt64.fromUint(10 * Math.pow(10, NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.DIVISIBILITY))), UInt64_1.UInt64.fromUint(1000), signedAggregatedTransaction, NetworkType_1.NetworkType.MIJIN_TEST);
        const signedLockFundsTransaction = signer.sign(lockFundsTransaction, generationHash);
        transactionHttp.announce(signedLockFundsTransaction);
    }
    static cosignTransaction(transaction, account, transactionHttp) {
        const cosignatureTransaction = CosignatureTransaction_1.CosignatureTransaction.create(transaction);
        const cosignatureSignedTransaction = account.signCosignatureTransaction(cosignatureTransaction);
        transactionHttp.announceAggregateBondedCosignature(cosignatureSignedTransaction);
    }
    static createMultisigAccountModificationTransaction(account, transactionHttp, generationHash) {
        const modifyMultisig = MultisigAccountModificationTransaction_1.MultisigAccountModificationTransaction.create(Deadline_1.Deadline.create(), 2, 1, [PublicAccount_1.PublicAccount.createFromPublicKey(account.publicKey, NetworkType_1.NetworkType.MIJIN_TEST)], [], NetworkType_1.NetworkType.MIJIN_TEST);
        const signedTransaction = account.sign(modifyMultisig, generationHash);
        transactionHttp.announce(signedTransaction);
    }
}
exports.TransactionUtils = TransactionUtils;
//# sourceMappingURL=TransactionUtils.js.map