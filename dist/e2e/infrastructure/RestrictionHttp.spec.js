"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const assert_1 = require("assert");
const chai_1 = require("chai");
const AccountHttp_1 = require("../../src/infrastructure/AccountHttp");
const infrastructure_1 = require("../../src/infrastructure/infrastructure");
const RestrictionAccountHttp_1 = require("../../src/infrastructure/RestrictionAccountHttp");
const RestrictionMosaicHttp_1 = require("../../src/infrastructure/RestrictionMosaicHttp");
const TransactionHttp_1 = require("../../src/infrastructure/TransactionHttp");
const Account_1 = require("../../src/model/account/Account");
const Address_1 = require("../../src/model/account/Address");
const PublicAccount_1 = require("../../src/model/account/PublicAccount");
const NetworkType_1 = require("../../src/model/blockchain/NetworkType");
const MosaicFlags_1 = require("../../src/model/mosaic/MosaicFlags");
const MosaicId_1 = require("../../src/model/mosaic/MosaicId");
const MosaicNonce_1 = require("../../src/model/mosaic/MosaicNonce");
const AccountRestrictionType_1 = require("../../src/model/restriction/AccountRestrictionType");
const MosaicRestrictionEntryType_1 = require("../../src/model/restriction/MosaicRestrictionEntryType");
const MosaicRestrictionType_1 = require("../../src/model/restriction/MosaicRestrictionType");
const AccountRestrictionTransaction_1 = require("../../src/model/transaction/AccountRestrictionTransaction");
const AggregateTransaction_1 = require("../../src/model/transaction/AggregateTransaction");
const Deadline_1 = require("../../src/model/transaction/Deadline");
const MosaicAddressRestrictionTransaction_1 = require("../../src/model/transaction/MosaicAddressRestrictionTransaction");
const MosaicDefinitionTransaction_1 = require("../../src/model/transaction/MosaicDefinitionTransaction");
const MosaicGlobalRestrictionTransaction_1 = require("../../src/model/transaction/MosaicGlobalRestrictionTransaction");
const UInt64_1 = require("../../src/model/UInt64");
describe('RestrictionHttp', () => {
    let account;
    let account2;
    let account3;
    let multisigAccount;
    let cosignAccount1;
    let cosignAccount2;
    let cosignAccount3;
    let accountAddress;
    let accountPublicKey;
    let publicAccount;
    let accountHttp;
    let restrictionMosaicHttp;
    let restrictionAccountHttp;
    let transactionHttp;
    let mosaicId;
    let referenceMosaicId;
    let generationHash;
    let config;
    before((done) => {
        const path = require('path');
        require('fs').readFile(path.resolve(__dirname, '../conf/network.conf'), (err, data) => {
            if (err) {
                throw err;
            }
            const json = JSON.parse(data);
            config = json;
            account = Account_1.Account.createFromPrivateKey(json.testAccount.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            account2 = Account_1.Account.createFromPrivateKey(json.testAccount2.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            account3 = Account_1.Account.createFromPrivateKey(json.testAccount3.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            multisigAccount = Account_1.Account.createFromPrivateKey(json.multisigAccount.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            cosignAccount1 = Account_1.Account.createFromPrivateKey(json.cosignatoryAccount.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            cosignAccount2 = Account_1.Account.createFromPrivateKey(json.cosignatory2Account.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            cosignAccount3 = Account_1.Account.createFromPrivateKey(json.cosignatory3Account.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            accountAddress = Address_1.Address.createFromRawAddress(json.testAccount.address);
            accountPublicKey = json.testAccount.publicKey;
            publicAccount = PublicAccount_1.PublicAccount.createFromPublicKey(json.testAccount.publicKey, NetworkType_1.NetworkType.MIJIN_TEST);
            generationHash = json.generationHash;
            accountHttp = new AccountHttp_1.AccountHttp(json.apiUrl);
            transactionHttp = new TransactionHttp_1.TransactionHttp(json.apiUrl);
            restrictionMosaicHttp = new RestrictionMosaicHttp_1.RestrictionMosaicHttp(json.apiUrl);
            restrictionAccountHttp = new RestrictionAccountHttp_1.RestrictionAccountHttp(json.apiUrl);
            done();
        });
    });
    /**
     * =========================
     * Setup test data
     * =========================
     */
    describe('MosaicDefinitionTransaction', () => {
        let listener;
        before(() => {
            listener = new infrastructure_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const nonce = MosaicNonce_1.MosaicNonce.createRandom();
            mosaicId = MosaicId_1.MosaicId.createFromNonce(nonce, account.publicAccount);
            const mosaicDefinitionTransaction = MosaicDefinitionTransaction_1.MosaicDefinitionTransaction.create(Deadline_1.Deadline.create(), nonce, mosaicId, MosaicFlags_1.MosaicFlags.create(true, true, true), 3, UInt64_1.UInt64.fromUint(1000), NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = mosaicDefinitionTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('MosaicDefinitionTransaction', () => {
        let listener;
        before(() => {
            listener = new infrastructure_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const nonce = MosaicNonce_1.MosaicNonce.createRandom();
            referenceMosaicId = MosaicId_1.MosaicId.createFromNonce(nonce, account.publicAccount);
            const mosaicDefinitionTransaction = MosaicDefinitionTransaction_1.MosaicDefinitionTransaction.create(Deadline_1.Deadline.create(), nonce, referenceMosaicId, MosaicFlags_1.MosaicFlags.create(true, true, true), 3, UInt64_1.UInt64.fromUint(1000), NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = mosaicDefinitionTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('Setup Test AccountAddressRestriction', () => {
        let listener;
        before(() => {
            listener = new infrastructure_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('Announce AccountRestrictionTransaction', (done) => {
            const addressModification = AccountRestrictionTransaction_1.AccountRestrictionTransaction.createAddressRestrictionModificationTransaction(Deadline_1.Deadline.create(), AccountRestrictionType_1.AccountRestrictionFlags.AllowIncomingAddress, [account3.address], [], NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = addressModification.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('MosaicGlobalRestrictionTransaction - Reference', () => {
        let listener;
        before(() => {
            listener = new infrastructure_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const mosaicGlobalRestrictionTransaction = MosaicGlobalRestrictionTransaction_1.MosaicGlobalRestrictionTransaction.create(Deadline_1.Deadline.create(), referenceMosaicId, UInt64_1.UInt64.fromUint(60641), UInt64_1.UInt64.fromUint(0), MosaicRestrictionType_1.MosaicRestrictionType.NONE, UInt64_1.UInt64.fromUint(0), MosaicRestrictionType_1.MosaicRestrictionType.GE, NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = mosaicGlobalRestrictionTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('MosaicGlobalRestrictionTransaction - with referenceMosaicId', () => {
        let listener;
        before(() => {
            listener = new infrastructure_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const mosaicGlobalRestrictionTransaction = MosaicGlobalRestrictionTransaction_1.MosaicGlobalRestrictionTransaction.create(Deadline_1.Deadline.create(), mosaicId, UInt64_1.UInt64.fromUint(60641), UInt64_1.UInt64.fromUint(0), MosaicRestrictionType_1.MosaicRestrictionType.NONE, UInt64_1.UInt64.fromUint(0), MosaicRestrictionType_1.MosaicRestrictionType.GE, NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = mosaicGlobalRestrictionTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('MosaicAddressRestrictionTransaction', () => {
        let listener;
        before(() => {
            listener = new infrastructure_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const mosaicAddressRestrictionTransaction = MosaicAddressRestrictionTransaction_1.MosaicAddressRestrictionTransaction.create(Deadline_1.Deadline.create(), mosaicId, UInt64_1.UInt64.fromUint(60641), account3.address, UInt64_1.UInt64.fromUint(2), NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [mosaicAddressRestrictionTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    /**
     * =========================
     * Tests
     * =========================
     */
    describe('getAccountRestrictions', () => {
        it('should call getAccountRestrictions successfully', (done) => {
            setTimeout(() => {
                restrictionAccountHttp.getAccountRestrictions(accountAddress).subscribe((accountRestrictions) => {
                    chai_1.expect(accountRestrictions.length).to.be.greaterThan(0);
                    done();
                });
            }, 1000);
        });
    });
    describe('getAccountRestrictionsFromAccounts', () => {
        it('should call getAccountRestrictionsFromAccounts successfully', (done) => {
            setTimeout(() => {
                restrictionAccountHttp.getAccountRestrictionsFromAccounts([accountAddress]).subscribe((accountRestrictions) => {
                    assert_1.deepEqual(accountRestrictions[0].address, accountAddress);
                    done();
                });
            }, 1000);
        });
    });
    describe('getMosaicAddressRestriction', () => {
        it('should call getMosaicAddressRestriction successfully', (done) => {
            setTimeout(() => {
                restrictionMosaicHttp.getMosaicAddressRestriction(mosaicId, account3.address).subscribe((mosaicRestriction) => {
                    assert_1.deepEqual(mosaicRestriction.mosaicId.toHex(), mosaicId.toHex());
                    assert_1.deepEqual(mosaicRestriction.entryType, MosaicRestrictionEntryType_1.MosaicRestrictionEntryType.ADDRESS);
                    assert_1.deepEqual(mosaicRestriction.targetAddress.plain(), account3.address.plain());
                    assert_1.deepEqual(mosaicRestriction.restrictions.get(UInt64_1.UInt64.fromUint(60641).toString()), UInt64_1.UInt64.fromUint(2).toString());
                    done();
                });
            }, 1000);
        });
    });
    describe('getMosaicAddressRestrictions', () => {
        it('should call getMosaicAddressRestrictions successfully', (done) => {
            setTimeout(() => {
                restrictionMosaicHttp.getMosaicAddressRestrictions(mosaicId, [account3.address]).subscribe((mosaicRestriction) => {
                    assert_1.deepEqual(mosaicRestriction[0].mosaicId.toHex(), mosaicId.toHex());
                    assert_1.deepEqual(mosaicRestriction[0].entryType, MosaicRestrictionEntryType_1.MosaicRestrictionEntryType.ADDRESS);
                    assert_1.deepEqual(mosaicRestriction[0].targetAddress.plain(), account3.address.plain());
                    assert_1.deepEqual(mosaicRestriction[0].restrictions.get(UInt64_1.UInt64.fromUint(60641).toString()), UInt64_1.UInt64.fromUint(2).toString());
                    done();
                });
            }, 1000);
        });
    });
    describe('getMosaicGlobalRestriction', () => {
        it('should call getMosaicGlobalRestriction successfully', (done) => {
            setTimeout(() => {
                restrictionMosaicHttp.getMosaicGlobalRestriction(mosaicId).subscribe((mosaicRestriction) => {
                    assert_1.deepEqual(mosaicRestriction.mosaicId.toHex(), mosaicId.toHex());
                    assert_1.deepEqual(mosaicRestriction.entryType, MosaicRestrictionEntryType_1.MosaicRestrictionEntryType.GLOBAL);
                    assert_1.deepEqual(mosaicRestriction.restrictions.get(UInt64_1.UInt64.fromUint(60641).toString()).referenceMosaicId.toHex(), new MosaicId_1.MosaicId(UInt64_1.UInt64.fromUint(0).toHex()).toHex());
                    assert_1.deepEqual(mosaicRestriction.restrictions.get(UInt64_1.UInt64.fromUint(60641).toString()).restrictionType, MosaicRestrictionType_1.MosaicRestrictionType.GE);
                    assert_1.deepEqual(mosaicRestriction.restrictions.get(UInt64_1.UInt64.fromUint(60641).toString()).restrictionValue.toString(), UInt64_1.UInt64.fromUint(0).toString());
                    done();
                });
            }, 1000);
        });
    });
    describe('getMosaicGlobalRestrictions', () => {
        it('should call getMosaicGlobalRestrictions successfully', (done) => {
            setTimeout(() => {
                restrictionMosaicHttp.getMosaicGlobalRestrictions([mosaicId]).subscribe((mosaicRestriction) => {
                    assert_1.deepEqual(mosaicRestriction[0].mosaicId.toHex(), mosaicId.toHex());
                    assert_1.deepEqual(mosaicRestriction[0].entryType, MosaicRestrictionEntryType_1.MosaicRestrictionEntryType.GLOBAL);
                    assert_1.deepEqual(mosaicRestriction[0].restrictions.get(UInt64_1.UInt64.fromUint(60641).toString()).referenceMosaicId.toHex(), new MosaicId_1.MosaicId(UInt64_1.UInt64.fromUint(0).toHex()).toHex());
                    assert_1.deepEqual(mosaicRestriction[0].restrictions.get(UInt64_1.UInt64.fromUint(60641).toString()).restrictionType, MosaicRestrictionType_1.MosaicRestrictionType.GE);
                    assert_1.deepEqual(mosaicRestriction[0].restrictions.get(UInt64_1.UInt64.fromUint(60641).toString()).restrictionValue.toString(), UInt64_1.UInt64.fromUint(0).toString());
                    done();
                });
            }, 1000);
        });
    });
    /**
     * =========================
     * House Keeping
     * =========================
     */
    describe('Remove test AccountRestriction - Address', () => {
        let listener;
        before(() => {
            listener = new infrastructure_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('Announce AccountRestrictionTransaction', (done) => {
            const addressModification = AccountRestrictionTransaction_1.AccountRestrictionTransaction.createAddressRestrictionModificationTransaction(Deadline_1.Deadline.create(), AccountRestrictionType_1.AccountRestrictionFlags.AllowIncomingAddress, [], [account3.address], NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = addressModification.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
});
//# sourceMappingURL=RestrictionHttp.spec.js.map