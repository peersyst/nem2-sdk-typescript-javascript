"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const assert_1 = require("assert");
const chai_1 = require("chai");
const infrastructure_1 = require("../../src/infrastructure/infrastructure");
const NamespaceHttp_1 = require("../../src/infrastructure/NamespaceHttp");
const TransactionHttp_1 = require("../../src/infrastructure/TransactionHttp");
const Account_1 = require("../../src/model/account/Account");
const NetworkType_1 = require("../../src/model/blockchain/NetworkType");
const NetworkCurrencyMosaic_1 = require("../../src/model/mosaic/NetworkCurrencyMosaic");
const AliasAction_1 = require("../../src/model/namespace/AliasAction");
const NamespaceId_1 = require("../../src/model/namespace/NamespaceId");
const AddressAliasTransaction_1 = require("../../src/model/transaction/AddressAliasTransaction");
const Deadline_1 = require("../../src/model/transaction/Deadline");
const NamespaceRegistrationTransaction_1 = require("../../src/model/transaction/NamespaceRegistrationTransaction");
const UInt64_1 = require("../../src/model/UInt64");
describe('NamespaceHttp', () => {
    const defaultNamespaceId = NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.NAMESPACE_ID;
    let namespaceId;
    let namespaceHttp;
    let account;
    let config;
    let transactionHttp;
    let generationHash;
    before((done) => {
        const path = require('path');
        require('fs').readFile(path.resolve(__dirname, '../conf/network.conf'), (err, data) => {
            if (err) {
                throw err;
            }
            const json = JSON.parse(data);
            config = json;
            account = Account_1.Account.createFromPrivateKey(json.testAccount.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            namespaceHttp = new NamespaceHttp_1.NamespaceHttp(json.apiUrl, NetworkType_1.NetworkType.MIJIN_TEST);
            transactionHttp = new TransactionHttp_1.TransactionHttp(json.apiUrl);
            generationHash = json.generationHash;
            done();
        });
    });
    describe('NamespaceRegistrationTransaction', () => {
        let listener;
        before(() => {
            listener = new infrastructure_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const namespaceName = 'root-test-namespace-' + Math.floor(Math.random() * 10000);
            const registerNamespaceTransaction = NamespaceRegistrationTransaction_1.NamespaceRegistrationTransaction.createRootNamespace(Deadline_1.Deadline.create(), namespaceName, UInt64_1.UInt64.fromUint(1000), NetworkType_1.NetworkType.MIJIN_TEST);
            namespaceId = new NamespaceId_1.NamespaceId(namespaceName);
            const signedTransaction = registerNamespaceTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('AddressAliasTransaction', () => {
        let listener;
        before(() => {
            listener = new infrastructure_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const addressAliasTransaction = AddressAliasTransaction_1.AddressAliasTransaction.create(Deadline_1.Deadline.create(), AliasAction_1.AliasAction.Link, namespaceId, account.address, NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = addressAliasTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('getNamespace', () => {
        it('should return namespace data given namepsaceId', (done) => {
            namespaceHttp.getNamespace(defaultNamespaceId)
                .subscribe((namespace) => {
                chai_1.expect(namespace.startHeight.lower).to.be.equal(1);
                chai_1.expect(namespace.startHeight.higher).to.be.equal(0);
                done();
            });
        });
    });
    describe('getNamespacesFromAccount', () => {
        it('should return namespace data given publicKeyNemesis', (done) => {
            namespaceHttp.getNamespacesFromAccount(account.address)
                .subscribe((namespaces) => {
                assert_1.deepEqual(namespaces[0].owner, account.publicAccount);
                done();
            });
        });
    });
    describe('getNamespacesFromAccounts', () => {
        it('should return namespaces data given publicKeyNemesis', (done) => {
            namespaceHttp.getNamespacesFromAccounts([account.address])
                .subscribe((namespaces) => {
                assert_1.deepEqual(namespaces[0].owner, account.publicAccount);
                done();
            });
        });
    });
    describe('getNamespacesName', () => {
        it('should return namespace name given array of namespaceIds', (done) => {
            namespaceHttp.getNamespacesName([defaultNamespaceId])
                .subscribe((namespaceNames) => {
                chai_1.expect(namespaceNames[0].name).to.be.equal('currency');
                done();
            });
        });
    });
    describe('getLinkedMosaicId', () => {
        it('should return mosaicId given currency namespaceId', (done) => {
            namespaceHttp.getLinkedMosaicId(defaultNamespaceId)
                .subscribe((mosaicId) => {
                chai_1.expect(mosaicId).to.not.be.null;
                done();
            });
        });
    });
    describe('getLinkedAddress', () => {
        it('should return address given namespaceId', (done) => {
            namespaceHttp.getLinkedAddress(namespaceId)
                .subscribe((address) => {
                chai_1.expect(address.plain()).to.be.equal(account.address.plain());
                done();
            });
        });
    });
});
//# sourceMappingURL=NamespaceHttp.spec.js.map