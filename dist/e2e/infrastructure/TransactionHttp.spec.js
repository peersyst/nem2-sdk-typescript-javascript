"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const chai_1 = require("chai");
const CryptoJS = require("crypto-js");
const js_joda_1 = require("js-joda");
const js_sha3_1 = require("js-sha3");
const crypto_1 = require("../../src/core/crypto");
const format_1 = require("../../src/core/format");
const TransactionMapping_1 = require("../../src/core/utils/TransactionMapping");
const AccountHttp_1 = require("../../src/infrastructure/AccountHttp");
const infrastructure_1 = require("../../src/infrastructure/infrastructure");
const Listener_1 = require("../../src/infrastructure/Listener");
const TransactionHttp_1 = require("../../src/infrastructure/TransactionHttp");
const Account_1 = require("../../src/model/account/Account");
const NetworkType_1 = require("../../src/model/blockchain/NetworkType");
const PlainMessage_1 = require("../../src/model/message/PlainMessage");
const Mosaic_1 = require("../../src/model/mosaic/Mosaic");
const MosaicFlags_1 = require("../../src/model/mosaic/MosaicFlags");
const MosaicId_1 = require("../../src/model/mosaic/MosaicId");
const MosaicNonce_1 = require("../../src/model/mosaic/MosaicNonce");
const MosaicSupplyChangeAction_1 = require("../../src/model/mosaic/MosaicSupplyChangeAction");
const NetworkCurrencyMosaic_1 = require("../../src/model/mosaic/NetworkCurrencyMosaic");
const AliasAction_1 = require("../../src/model/namespace/AliasAction");
const NamespaceId_1 = require("../../src/model/namespace/NamespaceId");
const AccountRestrictionModificationAction_1 = require("../../src/model/restriction/AccountRestrictionModificationAction");
const AccountRestrictionType_1 = require("../../src/model/restriction/AccountRestrictionType");
const MosaicRestrictionType_1 = require("../../src/model/restriction/MosaicRestrictionType");
const AccountLinkTransaction_1 = require("../../src/model/transaction/AccountLinkTransaction");
const AccountMetadataTransaction_1 = require("../../src/model/transaction/AccountMetadataTransaction");
const AccountRestrictionModification_1 = require("../../src/model/transaction/AccountRestrictionModification");
const AccountRestrictionTransaction_1 = require("../../src/model/transaction/AccountRestrictionTransaction");
const AddressAliasTransaction_1 = require("../../src/model/transaction/AddressAliasTransaction");
const AggregateTransaction_1 = require("../../src/model/transaction/AggregateTransaction");
const CosignatureSignedTransaction_1 = require("../../src/model/transaction/CosignatureSignedTransaction");
const CosignatureTransaction_1 = require("../../src/model/transaction/CosignatureTransaction");
const Deadline_1 = require("../../src/model/transaction/Deadline");
const HashLockTransaction_1 = require("../../src/model/transaction/HashLockTransaction");
const HashType_1 = require("../../src/model/transaction/HashType");
const LinkAction_1 = require("../../src/model/transaction/LinkAction");
const LockFundsTransaction_1 = require("../../src/model/transaction/LockFundsTransaction");
const MosaicAddressRestrictionTransaction_1 = require("../../src/model/transaction/MosaicAddressRestrictionTransaction");
const MosaicAliasTransaction_1 = require("../../src/model/transaction/MosaicAliasTransaction");
const MosaicDefinitionTransaction_1 = require("../../src/model/transaction/MosaicDefinitionTransaction");
const MosaicGlobalRestrictionTransaction_1 = require("../../src/model/transaction/MosaicGlobalRestrictionTransaction");
const MosaicMetadataTransaction_1 = require("../../src/model/transaction/MosaicMetadataTransaction");
const MosaicSupplyChangeTransaction_1 = require("../../src/model/transaction/MosaicSupplyChangeTransaction");
const NamespaceMetadataTransaction_1 = require("../../src/model/transaction/NamespaceMetadataTransaction");
const NamespaceRegistrationTransaction_1 = require("../../src/model/transaction/NamespaceRegistrationTransaction");
const SecretLockTransaction_1 = require("../../src/model/transaction/SecretLockTransaction");
const SecretProofTransaction_1 = require("../../src/model/transaction/SecretProofTransaction");
const TransactionType_1 = require("../../src/model/transaction/TransactionType");
const TransferTransaction_1 = require("../../src/model/transaction/TransferTransaction");
const UInt64_1 = require("../../src/model/UInt64");
describe('TransactionHttp', () => {
    let transactionHash;
    let transactionId;
    let account;
    let account2;
    let account3;
    let testAccountNoBalance;
    let harvestingAccount;
    let transactionHttp;
    let multisigAccount;
    let cosignAccount1;
    let cosignAccount2;
    let cosignAccount3;
    let mosaicId;
    let namespaceId;
    let networkCurrencyMosaicId;
    let accountHttp;
    let namespaceHttp;
    let config;
    const secureRandom = require('secure-random');
    const sha256 = require('js-sha256');
    const ripemd160 = require('ripemd160');
    let generationHash;
    before((done) => {
        const path = require('path');
        require('fs').readFile(path.resolve(__dirname, '../conf/network.conf'), (err, data) => {
            if (err) {
                throw err;
            }
            const json = JSON.parse(data);
            config = json;
            account = Account_1.Account.createFromPrivateKey(json.testAccount.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            account2 = Account_1.Account.createFromPrivateKey(json.testAccount2.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            account3 = Account_1.Account.createFromPrivateKey(json.testAccount3.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            testAccountNoBalance = Account_1.Account.createFromPrivateKey(json.testAccountNoBalance.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            harvestingAccount = Account_1.Account.createFromPrivateKey(json.harvestingAccount.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            multisigAccount = Account_1.Account.createFromPrivateKey(json.multisigAccount.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            cosignAccount1 = Account_1.Account.createFromPrivateKey(json.cosignatoryAccount.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            cosignAccount2 = Account_1.Account.createFromPrivateKey(json.cosignatory2Account.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            cosignAccount3 = Account_1.Account.createFromPrivateKey(json.cosignatory3Account.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            accountHttp = new AccountHttp_1.AccountHttp(json.apiUrl);
            transactionHttp = new TransactionHttp_1.TransactionHttp(json.apiUrl);
            namespaceHttp = new infrastructure_1.NamespaceHttp(json.apiUrl);
            generationHash = json.generationHash;
            done();
        });
    });
    describe('Get network currency mosaic id', () => {
        it('get mosaicId', (done) => {
            namespaceHttp.getLinkedMosaicId(new NamespaceId_1.NamespaceId('cat.currency')).subscribe((networkMosaicId) => {
                networkCurrencyMosaicId = networkMosaicId;
                done();
            });
        });
    });
    describe('MosaicDefinitionTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const nonce = MosaicNonce_1.MosaicNonce.createRandom();
            mosaicId = MosaicId_1.MosaicId.createFromNonce(nonce, account.publicAccount);
            const mosaicDefinitionTransaction = MosaicDefinitionTransaction_1.MosaicDefinitionTransaction.create(Deadline_1.Deadline.create(), nonce, mosaicId, MosaicFlags_1.MosaicFlags.create(true, true, true), 3, UInt64_1.UInt64.fromUint(1000), NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = mosaicDefinitionTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                chai_1.expect(transaction.mosaicId, 'MosaicId').not.to.be.undefined;
                chai_1.expect(transaction.nonce, 'Nonce').not.to.be.undefined;
                chai_1.expect(transaction.divisibility, 'Divisibility').not.to.be.undefined;
                chai_1.expect(transaction.duration, 'Duration').not.to.be.undefined;
                chai_1.expect(transaction.flags.supplyMutable, 'SupplyMutable').not.to.be.undefined;
                chai_1.expect(transaction.flags.transferable, 'Transferable').not.to.be.undefined;
                chai_1.expect(transaction.flags.restrictable, 'Restrictable').not.to.be.undefined;
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('MosaicDefinitionTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const nonce = MosaicNonce_1.MosaicNonce.createRandom();
            const mosaicDefinitionTransaction = MosaicDefinitionTransaction_1.MosaicDefinitionTransaction.create(Deadline_1.Deadline.create(), nonce, MosaicId_1.MosaicId.createFromNonce(nonce, account.publicAccount), MosaicFlags_1.MosaicFlags.create(true, true, true), 3, UInt64_1.UInt64.fromUint(0), NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [mosaicDefinitionTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('AccountMetadataTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const accountMetadataTransaction = AccountMetadataTransaction_1.AccountMetadataTransaction.create(Deadline_1.Deadline.create(), account.publicKey, UInt64_1.UInt64.fromUint(5), 10, format_1.Convert.uint8ToUtf8(new Uint8Array(10)), NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [accountMetadataTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                transaction.innerTransactions.forEach((innerTx) => {
                    chai_1.expect(innerTx.targetPublicKey, 'TargetPublicKey').not.to.be.undefined;
                    chai_1.expect(innerTx.scopedMetadataKey, 'ScopedMetadataKey').not.to.be.undefined;
                    chai_1.expect(innerTx.valueSizeDelta, 'ValueSizeDelta').not.to.be.undefined;
                    chai_1.expect(innerTx.value, 'Value').not.to.be.undefined;
                });
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('MosaicMetadataTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const mosaicMetadataTransaction = MosaicMetadataTransaction_1.MosaicMetadataTransaction.create(Deadline_1.Deadline.create(), account.publicKey, UInt64_1.UInt64.fromUint(5), mosaicId, 10, format_1.Convert.uint8ToUtf8(new Uint8Array(10)), NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [mosaicMetadataTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                transaction.innerTransactions.forEach((innerTx) => {
                    chai_1.expect(innerTx.targetPublicKey, 'TargetPublicKey').not.to.be.undefined;
                    chai_1.expect(innerTx.scopedMetadataKey, 'ScopedMetadataKey').not.to.be.undefined;
                    chai_1.expect(innerTx.valueSizeDelta, 'ValueSizeDelta').not.to.be.undefined;
                    chai_1.expect(innerTx.value, 'Value').not.to.be.undefined;
                    chai_1.expect(innerTx.targetMosaicId, 'TargetMosaicId').not.to.be.undefined;
                });
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('NamespaceRegistrationTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const namespaceName = 'root-test-namespace-' + Math.floor(Math.random() * 10000);
            const registerNamespaceTransaction = NamespaceRegistrationTransaction_1.NamespaceRegistrationTransaction.createRootNamespace(Deadline_1.Deadline.create(), namespaceName, UInt64_1.UInt64.fromUint(10), NetworkType_1.NetworkType.MIJIN_TEST);
            namespaceId = new NamespaceId_1.NamespaceId(namespaceName);
            const signedTransaction = registerNamespaceTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                chai_1.expect(transaction.namespaceId, 'NamespaceId').not.to.be.undefined;
                chai_1.expect(transaction.namespaceName, 'NamespaceName').not.to.be.undefined;
                chai_1.expect(transaction.registrationType, 'RegistrationType').not.to.be.undefined;
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('NamespaceRegistrationTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const registerNamespaceTransaction = NamespaceRegistrationTransaction_1.NamespaceRegistrationTransaction.createRootNamespace(Deadline_1.Deadline.create(), 'root-test-namespace-' + Math.floor(Math.random() * 10000), UInt64_1.UInt64.fromUint(5), NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [registerNamespaceTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('NamespaceMetadataTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const namespaceMetadataTransaction = NamespaceMetadataTransaction_1.NamespaceMetadataTransaction.create(Deadline_1.Deadline.create(), account.publicKey, UInt64_1.UInt64.fromUint(5), namespaceId, 10, format_1.Convert.uint8ToUtf8(new Uint8Array(10)), NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [namespaceMetadataTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                transaction.innerTransactions.forEach((innerTx) => {
                    chai_1.expect(innerTx.targetPublicKey, 'TargetPublicKey').not.to.be.undefined;
                    chai_1.expect(innerTx.scopedMetadataKey, 'ScopedMetadataKey').not.to.be.undefined;
                    chai_1.expect(innerTx.valueSizeDelta, 'ValueSizeDelta').not.to.be.undefined;
                    chai_1.expect(innerTx.value, 'Value').not.to.be.undefined;
                    chai_1.expect(innerTx.targetNamespaceId, 'TargetNamespaceId').not.to.be.undefined;
                });
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('MosaicGlobalRestrictionTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const mosaicGlobalRestrictionTransaction = MosaicGlobalRestrictionTransaction_1.MosaicGlobalRestrictionTransaction.create(Deadline_1.Deadline.create(), mosaicId, UInt64_1.UInt64.fromUint(60641), UInt64_1.UInt64.fromUint(0), MosaicRestrictionType_1.MosaicRestrictionType.NONE, UInt64_1.UInt64.fromUint(0), MosaicRestrictionType_1.MosaicRestrictionType.GE, NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = mosaicGlobalRestrictionTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('MosaicGlobalRestrictionTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const mosaicGlobalRestrictionTransaction = MosaicGlobalRestrictionTransaction_1.MosaicGlobalRestrictionTransaction.create(Deadline_1.Deadline.create(), mosaicId, UInt64_1.UInt64.fromUint(60641), UInt64_1.UInt64.fromUint(0), MosaicRestrictionType_1.MosaicRestrictionType.GE, UInt64_1.UInt64.fromUint(1), MosaicRestrictionType_1.MosaicRestrictionType.GE, NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [mosaicGlobalRestrictionTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('MosaicAddressRestrictionTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const mosaicAddressRestrictionTransaction = MosaicAddressRestrictionTransaction_1.MosaicAddressRestrictionTransaction.create(Deadline_1.Deadline.create(), mosaicId, UInt64_1.UInt64.fromUint(60641), account3.address, UInt64_1.UInt64.fromUint(2), NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [mosaicAddressRestrictionTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('TransferTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), account2.address, [NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(1)], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = transferTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                chai_1.expect(transaction.message, 'Message').not.to.be.undefined;
                chai_1.expect(transaction.mosaics, 'Mosaic').not.to.be.undefined;
                chai_1.expect(transaction.recipientAddress, 'RecipientAddress').not.to.be.undefined;
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('TransferTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), account2.address, [NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(1)], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [transferTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('AccountRestrictionTransaction - Outgoing Address', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const addressModification = AccountRestrictionTransaction_1.AccountRestrictionTransaction.createAddressRestrictionModificationTransaction(Deadline_1.Deadline.create(), AccountRestrictionType_1.AccountRestrictionFlags.BlockOutgoingAddress, [account3.address], [], NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = addressModification.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                chai_1.expect(transaction.restrictionAdditions, 'RestrictionAdditions').not.to.be.undefined;
                chai_1.expect(transaction.restrictionFlags, 'RestrictionFlags').not.to.be.undefined;
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('AccountRestrictionTransaction - Outgoing Address', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const addressModification = AccountRestrictionTransaction_1.AccountRestrictionTransaction.createAddressRestrictionModificationTransaction(Deadline_1.Deadline.create(), AccountRestrictionType_1.AccountRestrictionFlags.BlockOutgoingAddress, [], [account3.address], NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [addressModification.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('AccountRestrictionTransaction - Incoming Address', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const addressModification = AccountRestrictionTransaction_1.AccountRestrictionTransaction.createAddressRestrictionModificationTransaction(Deadline_1.Deadline.create(), AccountRestrictionType_1.AccountRestrictionFlags.BlockIncomingAddress, [account3.address], [], NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = addressModification.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                chai_1.expect(transaction.restrictionAdditions, 'RestrictionAdditions').not.to.be.undefined;
                chai_1.expect(transaction.restrictionDeletions, 'RestrictionDeletions').not.to.be.undefined;
                chai_1.expect(transaction.restrictionFlags, 'RestrictionFlags').not.to.be.undefined;
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('AccountRestrictionTransaction - Incoming Address', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const addressModification = AccountRestrictionTransaction_1.AccountRestrictionTransaction.createAddressRestrictionModificationTransaction(Deadline_1.Deadline.create(), AccountRestrictionType_1.AccountRestrictionFlags.BlockIncomingAddress, [], [account3.address], NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [addressModification.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('AccountRestrictionTransaction - Mosaic', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const mosaicRestrictionFilter = AccountRestrictionModification_1.AccountRestrictionModification.createForMosaic(AccountRestrictionModificationAction_1.AccountRestrictionModificationAction.Add, mosaicId);
            const addressModification = AccountRestrictionTransaction_1.AccountRestrictionTransaction.createMosaicRestrictionModificationTransaction(Deadline_1.Deadline.create(), AccountRestrictionType_1.AccountRestrictionFlags.BlockMosaic, [mosaicId], [], NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = addressModification.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                chai_1.expect(transaction.restrictionAdditions, 'RestrictionAdditions').not.to.be.undefined;
                chai_1.expect(transaction.restrictionDeletions, 'RestrictionDeletions').not.to.be.undefined;
                chai_1.expect(transaction.restrictionFlags, 'RestrictionFlags').not.to.be.undefined;
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('AccountRestrictionTransaction - Mosaic', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const addressModification = AccountRestrictionTransaction_1.AccountRestrictionTransaction.createMosaicRestrictionModificationTransaction(Deadline_1.Deadline.create(), AccountRestrictionType_1.AccountRestrictionFlags.BlockMosaic, [], [mosaicId], NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [addressModification.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('AccountRestrictionTransaction - Incoming Operation', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const addressModification = AccountRestrictionTransaction_1.AccountRestrictionTransaction.createOperationRestrictionModificationTransaction(Deadline_1.Deadline.create(), AccountRestrictionType_1.AccountRestrictionFlags.BlockIncomingTransactionType, [TransactionType_1.TransactionType.LINK_ACCOUNT], [], NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = addressModification.signWith(account3, generationHash);
            listener.confirmed(account3.address).subscribe((transaction) => {
                chai_1.expect(transaction.restrictionAdditions, 'RestrictionAdditions').not.to.be.undefined;
                chai_1.expect(transaction.restrictionDeletions, 'RestrictionDeletions').not.to.be.undefined;
                chai_1.expect(transaction.restrictionFlags, 'RestrictionFlags').not.to.be.undefined;
                done();
            });
            listener.status(account3.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('AccountRestrictionTransaction - Incoming Operation', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const addressModification = AccountRestrictionTransaction_1.AccountRestrictionTransaction.createOperationRestrictionModificationTransaction(Deadline_1.Deadline.create(), AccountRestrictionType_1.AccountRestrictionFlags.BlockIncomingTransactionType, [], [TransactionType_1.TransactionType.LINK_ACCOUNT], NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [addressModification.toAggregate(account3.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction.signWith(account3, generationHash);
            listener.confirmed(account3.address).subscribe((transaction) => {
                done();
            });
            listener.status(account3.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('AccountRestrictionTransaction - Outgoing Operation', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const operationRestrictionFilter = AccountRestrictionModification_1.AccountRestrictionModification.createForOperation(AccountRestrictionModificationAction_1.AccountRestrictionModificationAction.Add, TransactionType_1.TransactionType.LINK_ACCOUNT);
            const addressModification = AccountRestrictionTransaction_1.AccountRestrictionTransaction.createOperationRestrictionModificationTransaction(Deadline_1.Deadline.create(), AccountRestrictionType_1.AccountRestrictionFlags.BlockOutgoingTransactionType, [TransactionType_1.TransactionType.LINK_ACCOUNT], [], NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = addressModification.signWith(account3, generationHash);
            listener.confirmed(account3.address).subscribe((transaction) => {
                chai_1.expect(transaction.restrictionAdditions, 'RestrictionAdditions').not.to.be.undefined;
                chai_1.expect(transaction.restrictionDeletions, 'RestrictionDeletions').not.to.be.undefined;
                chai_1.expect(transaction.restrictionFlags, 'RestrictionFlags').not.to.be.undefined;
                done();
            });
            listener.status(account3.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('AccountRestrictionTransaction - Outgoing Operation', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const addressModification = AccountRestrictionTransaction_1.AccountRestrictionTransaction.createOperationRestrictionModificationTransaction(Deadline_1.Deadline.create(), AccountRestrictionType_1.AccountRestrictionFlags.BlockOutgoingTransactionType, [], [TransactionType_1.TransactionType.LINK_ACCOUNT], NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [addressModification.toAggregate(account3.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction.signWith(account3, generationHash);
            listener.confirmed(account3.address).subscribe((transaction) => {
                done();
            });
            listener.status(account3.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('AccountLinkTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const accountLinkTransaction = AccountLinkTransaction_1.AccountLinkTransaction.create(Deadline_1.Deadline.create(), harvestingAccount.publicKey, LinkAction_1.LinkAction.Link, NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = accountLinkTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                chai_1.expect(transaction.remotePublicKey, 'RemotePublicKey').not.to.be.undefined;
                chai_1.expect(transaction.linkAction, 'LinkAction').not.to.be.undefined;
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('AccountLinkTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const accountLinkTransaction = AccountLinkTransaction_1.AccountLinkTransaction.create(Deadline_1.Deadline.create(), harvestingAccount.publicKey, LinkAction_1.LinkAction.Unlink, NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [accountLinkTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('AddressAliasTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const addressAliasTransaction = AddressAliasTransaction_1.AddressAliasTransaction.create(Deadline_1.Deadline.create(), AliasAction_1.AliasAction.Link, namespaceId, account.address, NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = addressAliasTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                chai_1.expect(transaction.namespaceId, 'NamespaceId').not.to.be.undefined;
                chai_1.expect(transaction.aliasAction, 'AliasAction').not.to.be.undefined;
                chai_1.expect(transaction.address, 'Address').not.to.be.undefined;
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('Transfer Transaction using address alias', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('Announce TransferTransaction', (done) => {
            const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), namespaceId, [NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(1)], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = transferTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                chai_1.expect(transaction.recipientAddress, 'AecipientAddress').not.to.be.undefined;
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('AddressAliasTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const addressAliasTransaction = AddressAliasTransaction_1.AddressAliasTransaction.create(Deadline_1.Deadline.create(), AliasAction_1.AliasAction.Unlink, namespaceId, account.address, NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [addressAliasTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('MosaicSupplyChangeTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const mosaicSupplyChangeTransaction = MosaicSupplyChangeTransaction_1.MosaicSupplyChangeTransaction.create(Deadline_1.Deadline.create(), mosaicId, MosaicSupplyChangeAction_1.MosaicSupplyChangeAction.Increase, UInt64_1.UInt64.fromUint(10), NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = mosaicSupplyChangeTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                chai_1.expect(transaction.delta, 'Delta').not.to.be.undefined;
                chai_1.expect(transaction.action, 'Action').not.to.be.undefined;
                chai_1.expect(transaction.mosaicId, 'MosaicId').not.to.be.undefined;
                done();
            });
            listener.status(account3.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('MosaicSupplyChangeTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const mosaicSupplyChangeTransaction = MosaicSupplyChangeTransaction_1.MosaicSupplyChangeTransaction.create(Deadline_1.Deadline.create(), mosaicId, MosaicSupplyChangeAction_1.MosaicSupplyChangeAction.Increase, UInt64_1.UInt64.fromUint(10), NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [mosaicSupplyChangeTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account3.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('MosaicAliasTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const mosaicAliasTransaction = MosaicAliasTransaction_1.MosaicAliasTransaction.create(Deadline_1.Deadline.create(), AliasAction_1.AliasAction.Link, namespaceId, mosaicId, NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = mosaicAliasTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                chai_1.expect(transaction.namespaceId, 'NamespaceId').not.to.be.undefined;
                chai_1.expect(transaction.aliasAction, 'AliasAction').not.to.be.undefined;
                chai_1.expect(transaction.mosaicId, 'MosaicId').not.to.be.undefined;
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('HashLockTransaction - MosaicAlias', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createBonded(Deadline_1.Deadline.create(), [], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = account.sign(aggregateTransaction, generationHash);
            const hashLockTransaction = HashLockTransaction_1.HashLockTransaction.create(Deadline_1.Deadline.create(), new Mosaic_1.Mosaic(new NamespaceId_1.NamespaceId('cat.currency'), UInt64_1.UInt64.fromUint(10 * Math.pow(10, NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.DIVISIBILITY))), UInt64_1.UInt64.fromUint(10000), signedTransaction, NetworkType_1.NetworkType.MIJIN_TEST);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(hashLockTransaction.signWith(account, generationHash));
        });
    });
    describe('MosaicAliasTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const mosaicAliasTransaction = MosaicAliasTransaction_1.MosaicAliasTransaction.create(Deadline_1.Deadline.create(), AliasAction_1.AliasAction.Unlink, namespaceId, mosaicId, NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [mosaicAliasTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('LockFundsTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createBonded(Deadline_1.Deadline.create(), [], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = account.sign(aggregateTransaction, generationHash);
            const lockFundsTransaction = LockFundsTransaction_1.LockFundsTransaction.create(Deadline_1.Deadline.create(), new Mosaic_1.Mosaic(networkCurrencyMosaicId, UInt64_1.UInt64.fromUint(10 * Math.pow(10, NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.DIVISIBILITY))), UInt64_1.UInt64.fromUint(10000), signedTransaction, NetworkType_1.NetworkType.MIJIN_TEST);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(lockFundsTransaction.signWith(account, generationHash));
        });
    });
    describe('LockFundsTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createBonded(Deadline_1.Deadline.create(), [], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = account.sign(aggregateTransaction, generationHash);
            const lockFundsTransaction = LockFundsTransaction_1.LockFundsTransaction.create(Deadline_1.Deadline.create(), new Mosaic_1.Mosaic(networkCurrencyMosaicId, UInt64_1.UInt64.fromUint(10 * Math.pow(10, NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.DIVISIBILITY))), UInt64_1.UInt64.fromUint(10), signedTransaction, NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateLockFundsTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [lockFundsTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(aggregateLockFundsTransaction.signWith(account, generationHash));
        });
    });
    describe('Aggregate Complete Transaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('should announce aggregated complete transaction', (done) => {
            const tx = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), account2.address, [], PlainMessage_1.PlainMessage.create('Hi'), NetworkType_1.NetworkType.MIJIN_TEST);
            const aggTx = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [
                tx.toAggregate(account.publicAccount),
            ], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTx = account.sign(aggTx, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTx);
        });
    });
    describe('SecretLockTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Sha3_256, js_sha3_1.sha3_256.create().update(crypto_1.Crypto.randomBytes(20)).hex(), account2.address, NetworkType_1.NetworkType.MIJIN_TEST);
            listener.confirmed(account.address).subscribe((transaction) => {
                chai_1.expect(transaction.mosaic, 'Mosaic').not.to.be.undefined;
                chai_1.expect(transaction.duration, 'Duration').not.to.be.undefined;
                chai_1.expect(transaction.hashType, 'HashType').not.to.be.undefined;
                chai_1.expect(transaction.secret, 'Secret').not.to.be.undefined;
                chai_1.expect(transaction.recipientAddress, 'RecipientAddress').not.to.be.undefined;
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(secretLockTransaction.signWith(account, generationHash));
        });
    });
    describe('HashType: Op_Sha3_256', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Sha3_256, js_sha3_1.sha3_256.create().update(crypto_1.Crypto.randomBytes(20)).hex(), account2.address, NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateSecretLockTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [secretLockTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(aggregateSecretLockTransaction.signWith(account, generationHash));
        });
    });
    describe('HashType: Keccak_256', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Keccak_256, js_sha3_1.sha3_256.create().update(crypto_1.Crypto.randomBytes(20)).hex(), account2.address, NetworkType_1.NetworkType.MIJIN_TEST);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(secretLockTransaction.signWith(account, generationHash));
        });
    });
    describe('HashType: Keccak_256', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Keccak_256, js_sha3_1.sha3_256.create().update(crypto_1.Crypto.randomBytes(20)).hex(), account2.address, NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateSecretLockTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [secretLockTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(aggregateSecretLockTransaction.signWith(account, generationHash));
        });
    });
    describe('HashType: Op_Hash_160', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const secretSeed = String.fromCharCode.apply(null, crypto_1.Crypto.randomBytes(20));
            const secret = CryptoJS.RIPEMD160(CryptoJS.SHA256(secretSeed).toString(CryptoJS.enc.Hex)).toString(CryptoJS.enc.Hex);
            const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Hash_160, secret, account2.address, NetworkType_1.NetworkType.MIJIN_TEST);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(secretLockTransaction.signWith(account, generationHash));
        });
    });
    describe('HashType: Op_Hash_160', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const secretSeed = String.fromCharCode.apply(null, crypto_1.Crypto.randomBytes(20));
            const secret = CryptoJS.RIPEMD160(CryptoJS.SHA256(secretSeed).toString(CryptoJS.enc.Hex)).toString(CryptoJS.enc.Hex);
            const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Hash_160, secret, account2.address, NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateSecretLockTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [secretLockTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(aggregateSecretLockTransaction.signWith(account, generationHash));
        });
    });
    describe('HashType: Op_Hash_256', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Hash_256, js_sha3_1.sha3_256.create().update(crypto_1.Crypto.randomBytes(20)).hex(), account2.address, NetworkType_1.NetworkType.MIJIN_TEST);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(secretLockTransaction.signWith(account, generationHash));
        });
    });
    describe('HashType: Op_Hash_256', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Hash_256, js_sha3_1.sha3_256.create().update(crypto_1.Crypto.randomBytes(20)).hex(), account2.address, NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateSecretLockTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [secretLockTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(aggregateSecretLockTransaction.signWith(account, generationHash));
        });
    });
    describe('SecretProofTransaction - HashType: Op_Sha3_256', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const secretSeed = crypto_1.Crypto.randomBytes(20);
            const secret = js_sha3_1.sha3_256.create().update(secretSeed).hex();
            const proof = format_1.Convert.uint8ToHex(secretSeed);
            const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(1, js_joda_1.ChronoUnit.HOURS), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(11), HashType_1.HashType.Op_Sha3_256, secret, account2.address, NetworkType_1.NetworkType.MIJIN_TEST);
            listener.confirmed(account.address).subscribe(() => {
                listener.confirmed(account2.address).subscribe((transaction) => {
                    chai_1.expect(transaction.secret, 'Secret').not.to.be.undefined;
                    chai_1.expect(transaction.recipientAddress, 'RecipientAddress').not.to.be.undefined;
                    chai_1.expect(transaction.hashType, 'HashType').not.to.be.undefined;
                    chai_1.expect(transaction.proof, 'Proof').not.to.be.undefined;
                    done();
                });
                const secretProofTransaction = SecretProofTransaction_1.SecretProofTransaction.create(Deadline_1.Deadline.create(), HashType_1.HashType.Op_Sha3_256, secret, account2.address, proof, NetworkType_1.NetworkType.MIJIN_TEST);
                const signedTx = secretProofTransaction.signWith(account2, generationHash);
                transactionHttp.announce(signedTx);
            });
            listener.status(account2.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            const signedSecretLockTx = secretLockTransaction.signWith(account, generationHash);
            transactionHttp.announce(signedSecretLockTx);
        });
    });
    describe('SecretProofTransaction - HashType: Op_Sha3_256', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const secretSeed = crypto_1.Crypto.randomBytes(20);
            const secret = js_sha3_1.sha3_256.create().update(secretSeed).hex();
            const proof = format_1.Convert.uint8ToHex(secretSeed);
            const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Sha3_256, secret, account2.address, NetworkType_1.NetworkType.MIJIN_TEST);
            listener.confirmed(account.address).subscribe(() => {
                listener.confirmed(account2.address).subscribe(() => {
                    done();
                });
                const secretProofTransaction = SecretProofTransaction_1.SecretProofTransaction.create(Deadline_1.Deadline.create(), HashType_1.HashType.Op_Sha3_256, secret, account2.address, proof, NetworkType_1.NetworkType.MIJIN_TEST);
                const aggregateSecretProofTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [secretProofTransaction.toAggregate(account2.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
                transactionHttp.announce(aggregateSecretProofTransaction.signWith(account2, generationHash));
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(secretLockTransaction.signWith(account, generationHash));
        });
    });
    describe('SecretProofTransaction - HashType: Op_Keccak_256', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const secretSeed = crypto_1.Crypto.randomBytes(20);
            const secret = js_sha3_1.keccak_256.create().update(secretSeed).hex();
            const proof = format_1.Convert.uint8ToHex(secretSeed);
            const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Keccak_256, secret, account2.address, NetworkType_1.NetworkType.MIJIN_TEST);
            listener.confirmed(account.address).subscribe(() => {
                listener.confirmed(account2.address).subscribe(() => {
                    done();
                });
                const secretProofTransaction = SecretProofTransaction_1.SecretProofTransaction.create(Deadline_1.Deadline.create(), HashType_1.HashType.Op_Keccak_256, secret, account2.address, proof, NetworkType_1.NetworkType.MIJIN_TEST);
                transactionHttp.announce(secretProofTransaction.signWith(account2, generationHash));
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(secretLockTransaction.signWith(account, generationHash));
        });
    });
    describe('SecretProofTransaction - HashType: Op_Keccak_256', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const secretSeed = crypto_1.Crypto.randomBytes(20);
            const secret = js_sha3_1.keccak_256.create().update(secretSeed).hex();
            const proof = format_1.Convert.uint8ToHex(secretSeed);
            const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Keccak_256, secret, account2.address, NetworkType_1.NetworkType.MIJIN_TEST);
            listener.confirmed(account.address).subscribe(() => {
                listener.confirmed(account2.address).subscribe(() => {
                    done();
                });
                const secretProofTransaction = SecretProofTransaction_1.SecretProofTransaction.create(Deadline_1.Deadline.create(), HashType_1.HashType.Op_Keccak_256, secret, account2.address, proof, NetworkType_1.NetworkType.MIJIN_TEST);
                const aggregateSecretProofTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [secretProofTransaction.toAggregate(account2.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
                transactionHttp.announce(aggregateSecretProofTransaction.signWith(account2, generationHash));
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(secretLockTransaction.signWith(account, generationHash));
        });
    });
    describe('SecretProofTransaction - HashType: Op_Hash_160', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const randomBytes = secureRandom.randomBuffer(32);
            const secretSeed = randomBytes.toString('hex');
            const hash = sha256(Buffer.from(secretSeed, 'hex'));
            const secret = new ripemd160().update(Buffer.from(hash, 'hex')).digest('hex');
            const proof = secretSeed;
            const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Hash_160, secret, account2.address, NetworkType_1.NetworkType.MIJIN_TEST);
            listener.confirmed(account.address).subscribe(() => {
                listener.confirmed(account2.address).subscribe(() => {
                    done();
                });
                listener.status(account2.address).subscribe((error) => {
                    console.log('Error:', error);
                    chai_1.assert(false);
                    done();
                });
                const secretProofTransaction = SecretProofTransaction_1.SecretProofTransaction.create(Deadline_1.Deadline.create(), HashType_1.HashType.Op_Hash_160, secret, account2.address, proof, NetworkType_1.NetworkType.MIJIN_TEST);
                const signedTx = secretProofTransaction.signWith(account2, generationHash);
                transactionHttp.announce(signedTx);
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(secretLockTransaction.signWith(account, generationHash));
        });
    });
    describe('SecretProofTransaction - HashType: Op_Hash_160', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const randomBytes = secureRandom.randomBuffer(32);
            const secretSeed = randomBytes.toString('hex');
            const hash = sha256(Buffer.from(secretSeed, 'hex'));
            const secret = new ripemd160().update(Buffer.from(hash, 'hex')).digest('hex');
            const proof = secretSeed;
            const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Hash_160, secret, account2.address, NetworkType_1.NetworkType.MIJIN_TEST);
            listener.confirmed(account.address).subscribe(() => {
                listener.confirmed(account2.address).subscribe(() => {
                    done();
                });
                listener.status(account2.address).subscribe((error) => {
                    console.log('Error:', error);
                    chai_1.assert(false);
                    done();
                });
                const secretProofTransaction = SecretProofTransaction_1.SecretProofTransaction.create(Deadline_1.Deadline.create(), HashType_1.HashType.Op_Hash_160, secret, account2.address, proof, NetworkType_1.NetworkType.MIJIN_TEST);
                const aggregateSecretProofTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [secretProofTransaction.toAggregate(account2.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
                transactionHttp.announce(aggregateSecretProofTransaction.signWith(account2, generationHash));
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(secretLockTransaction.signWith(account, generationHash));
        });
    });
    describe('SecretProofTransaction - HashType: Op_Hash_256', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const randomBytes = secureRandom.randomBuffer(32);
            const secretSeed = randomBytes.toString('hex');
            const hash = sha256(Buffer.from(secretSeed, 'hex'));
            const secret = sha256(Buffer.from(hash, 'hex'));
            const proof = secretSeed;
            const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Hash_256, secret, account2.address, NetworkType_1.NetworkType.MIJIN_TEST);
            listener.confirmed(account.address).subscribe(() => {
                listener.confirmed(account2.address).subscribe(() => {
                    done();
                });
                listener.status(account2.address).subscribe((error) => {
                    console.log('Error:', error);
                    chai_1.assert(false);
                    done();
                });
                const secretProofTransaction = SecretProofTransaction_1.SecretProofTransaction.create(Deadline_1.Deadline.create(), HashType_1.HashType.Op_Hash_256, secret, account2.address, proof, NetworkType_1.NetworkType.MIJIN_TEST);
                transactionHttp.announce(secretProofTransaction.signWith(account2, generationHash));
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(secretLockTransaction.signWith(account, generationHash));
        });
    });
    describe('SecretProofTransaction - HashType: Op_Hash_256', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const randomBytes = secureRandom.randomBuffer(32);
            const secretSeed = randomBytes.toString('hex');
            const hash = sha256(Buffer.from(secretSeed, 'hex'));
            const secret = sha256(Buffer.from(hash, 'hex'));
            const proof = secretSeed;
            const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Hash_256, secret, account2.address, NetworkType_1.NetworkType.MIJIN_TEST);
            listener.confirmed(account.address).subscribe(() => {
                listener.confirmed(account2.address).subscribe(() => {
                    done();
                });
                listener.status(account2.address).subscribe((error) => {
                    console.log('Error:', error);
                    chai_1.assert(false);
                    done();
                });
                const secretProofTransaction = SecretProofTransaction_1.SecretProofTransaction.create(Deadline_1.Deadline.create(), HashType_1.HashType.Op_Hash_256, secret, account2.address, proof, NetworkType_1.NetworkType.MIJIN_TEST);
                const aggregateSecretProofTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [secretProofTransaction.toAggregate(account2.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
                transactionHttp.announce(aggregateSecretProofTransaction.signWith(account2, generationHash));
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(secretLockTransaction.signWith(account, generationHash));
        });
    });
    describe('SignTransactionGivenSignatures', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('Announce cosign signatures given', (done) => {
            /**
             * @see https://github.com/nemtech/nem2-sdk-typescript-javascript/issues/112
             */
            // AliceAccount: account
            // BobAccount: account
            const sendAmount = NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createRelative(1000);
            const backAmount = NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createRelative(1);
            const aliceTransferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), account2.address, [sendAmount], PlainMessage_1.PlainMessage.create('payout'), NetworkType_1.NetworkType.MIJIN_TEST);
            const bobTransferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), account.address, [backAmount], PlainMessage_1.PlainMessage.create('payout'), NetworkType_1.NetworkType.MIJIN_TEST);
            // 01. Alice creates the aggregated tx and sign it. Then payload send to Bob
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [
                aliceTransferTransaction.toAggregate(account.publicAccount),
                bobTransferTransaction.toAggregate(account2.publicAccount),
            ], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const aliceSignedTransaction = aggregateTransaction.signWith(account, generationHash);
            // 02 Bob cosigns the tx and sends it back to Alice
            const signedTxBob = CosignatureTransaction_1.CosignatureTransaction.signTransactionPayload(account2, aliceSignedTransaction.payload, generationHash);
            // 03. Alice collects the cosignatures, recreate, sign, and announces the transaction
            const cosignatureSignedTransactions = [
                new CosignatureSignedTransaction_1.CosignatureSignedTransaction(signedTxBob.parentHash, signedTxBob.signature, signedTxBob.signerPublicKey),
            ];
            const recreatedTx = TransactionMapping_1.TransactionMapping.createFromPayload(aliceSignedTransaction.payload);
            const signedTransaction = recreatedTx.signTransactionGivenSignatures(account, cosignatureSignedTransactions, generationHash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('transactions', () => {
        it('should call transactions successfully', (done) => {
            accountHttp.getAccountTransactions(account.publicAccount.address).subscribe((transactions) => {
                const transaction = transactions[0];
                transactionId = transaction.transactionInfo.id;
                transactionHash = transaction.transactionInfo.hash;
                done();
            });
        });
    });
    describe('getTransaction', () => {
        it('should return transaction info given transactionHash', (done) => {
            transactionHttp.getTransaction(transactionHash)
                .subscribe((transaction) => {
                chai_1.expect(transaction.transactionInfo.hash).to.be.equal(transactionHash);
                chai_1.expect(transaction.transactionInfo.id).to.be.equal(transactionId);
                done();
            });
        });
        it('should return transaction info given transactionId', (done) => {
            transactionHttp.getTransaction(transactionId)
                .subscribe((transaction) => {
                chai_1.expect(transaction.transactionInfo.hash).to.be.equal(transactionHash);
                chai_1.expect(transaction.transactionInfo.id).to.be.equal(transactionId);
                done();
            });
        });
    });
    describe('getTransactions', () => {
        it('should return transaction info given array of transactionHash', (done) => {
            transactionHttp.getTransactions([transactionHash])
                .subscribe((transactions) => {
                chai_1.expect(transactions[0].transactionInfo.hash).to.be.equal(transactionHash);
                chai_1.expect(transactions[0].transactionInfo.id).to.be.equal(transactionId);
                done();
            });
        });
        it('should return transaction info given array of transactionId', (done) => {
            transactionHttp.getTransactions([transactionId])
                .subscribe((transactions) => {
                chai_1.expect(transactions[0].transactionInfo.hash).to.be.equal(transactionHash);
                chai_1.expect(transactions[0].transactionInfo.id).to.be.equal(transactionId);
                done();
            });
        });
    });
    describe('getTransactionStatus', () => {
        it('should return transaction status given transactionHash', (done) => {
            transactionHttp.getTransactionStatus(transactionHash)
                .subscribe((transactionStatus) => {
                chai_1.expect(transactionStatus.group).to.be.equal('confirmed');
                chai_1.expect(transactionStatus.height.lower).to.be.greaterThan(0);
                chai_1.expect(transactionStatus.height.higher).to.be.equal(0);
                done();
            });
        });
    });
    describe('getTransactionsStatuses', () => {
        it('should return transaction status given array of transactionHash', (done) => {
            transactionHttp.getTransactionsStatuses([transactionHash])
                .subscribe((transactionStatuses) => {
                chai_1.expect(transactionStatuses[0].group).to.be.equal('confirmed');
                chai_1.expect(transactionStatuses[0].height.lower).to.be.greaterThan(0);
                chai_1.expect(transactionStatuses[0].height.higher).to.be.equal(0);
                done();
            });
        });
    });
    describe('announce', () => {
        it('should return success when announce', (done) => {
            const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), account2.address, [NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(1)], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = transferTransaction.signWith(account, generationHash);
            transactionHttp.announce(signedTransaction)
                .subscribe((transactionAnnounceResponse) => {
                chai_1.expect(transactionAnnounceResponse.message)
                    .to.be.equal('packet 9 was pushed to the network via /transaction');
                done();
            });
        });
    });
    describe('announceAggregateBonded', () => {
        it('should return success when announceAggregateBonded', (done) => {
            const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), account2.address, [NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createRelative(1)], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createBonded(Deadline_1.Deadline.create(2, js_joda_1.ChronoUnit.MINUTES), [transferTransaction.toAggregate(multisigAccount.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction.signWith(cosignAccount1, generationHash);
            transactionHttp.announceAggregateBonded(signedTransaction)
                .subscribe((transactionAnnounceResponse) => {
                chai_1.expect(transactionAnnounceResponse.message)
                    .to.be.equal('packet 500 was pushed to the network via /transaction/partial');
                done();
            });
        });
    });
    describe('announceAggregateBondedCosignature', () => {
        it('should return success when announceAggregateBondedCosignature', (done) => {
            const payload = new CosignatureSignedTransaction_1.CosignatureSignedTransaction('', '', '');
            transactionHttp.announceAggregateBondedCosignature(payload)
                .subscribe((transactionAnnounceResponse) => {
                chai_1.expect(transactionAnnounceResponse.message)
                    .to.be.equal('packet 501 was pushed to the network via /transaction/cosignature');
                done();
            });
        });
    });
    describe('getTransactionEffectiveFee', () => {
        it('should return effective paid fee given transactionHash', (done) => {
            transactionHttp.getTransactionEffectiveFee(transactionHash)
                .subscribe((effectiveFee) => {
                chai_1.expect(effectiveFee).to.not.be.undefined;
                chai_1.expect(effectiveFee).to.be.equal(0);
                done();
            });
        });
    });
});
//# sourceMappingURL=TransactionHttp.spec.js.map