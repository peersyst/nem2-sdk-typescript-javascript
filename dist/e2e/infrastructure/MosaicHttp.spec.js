"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const chai_1 = require("chai");
const infrastructure_1 = require("../../src/infrastructure/infrastructure");
const MosaicHttp_1 = require("../../src/infrastructure/MosaicHttp");
const Account_1 = require("../../src/model/account/Account");
const NetworkType_1 = require("../../src/model/blockchain/NetworkType");
const MosaicFlags_1 = require("../../src/model/mosaic/MosaicFlags");
const MosaicId_1 = require("../../src/model/mosaic/MosaicId");
const MosaicNonce_1 = require("../../src/model/mosaic/MosaicNonce");
const AliasAction_1 = require("../../src/model/namespace/AliasAction");
const NamespaceId_1 = require("../../src/model/namespace/NamespaceId");
const Deadline_1 = require("../../src/model/transaction/Deadline");
const MosaicAliasTransaction_1 = require("../../src/model/transaction/MosaicAliasTransaction");
const MosaicDefinitionTransaction_1 = require("../../src/model/transaction/MosaicDefinitionTransaction");
const NamespaceRegistrationTransaction_1 = require("../../src/model/transaction/NamespaceRegistrationTransaction");
const UInt64_1 = require("../../src/model/UInt64");
describe('MosaicHttp', () => {
    let mosaicId;
    let mosaicHttp;
    let account;
    let config;
    let namespaceId;
    let transactionHttp;
    let namespaceHttp;
    let generationHash;
    before((done) => {
        const path = require('path');
        require('fs').readFile(path.resolve(__dirname, '../conf/network.conf'), (err, data) => {
            if (err) {
                throw err;
            }
            const json = JSON.parse(data);
            config = json;
            account = Account_1.Account.createFromPrivateKey(json.testAccount.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            mosaicHttp = new MosaicHttp_1.MosaicHttp(json.apiUrl);
            transactionHttp = new infrastructure_1.TransactionHttp(json.apiUrl);
            namespaceHttp = new infrastructure_1.NamespaceHttp(json.apiUrl);
            generationHash = json.generationHash;
            done();
        });
    });
    /**
     * =========================
     * Setup Test Data
     * =========================
     */
    describe('Setup test MosaicId', () => {
        let listener;
        before(() => {
            listener = new infrastructure_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('Announce MosaicDefinitionTransaction', (done) => {
            const nonce = MosaicNonce_1.MosaicNonce.createRandom();
            mosaicId = MosaicId_1.MosaicId.createFromNonce(nonce, account.publicAccount);
            const mosaicDefinitionTransaction = MosaicDefinitionTransaction_1.MosaicDefinitionTransaction.create(Deadline_1.Deadline.create(), nonce, mosaicId, MosaicFlags_1.MosaicFlags.create(true, true, false), 3, UInt64_1.UInt64.fromUint(0), NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = mosaicDefinitionTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('Setup test NamespaceId', () => {
        let listener;
        before(() => {
            listener = new infrastructure_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('Announce NamespaceRegistrationTransaction', (done) => {
            const namespaceName = 'root-test-namespace-' + Math.floor(Math.random() * 10000);
            const registerNamespaceTransaction = NamespaceRegistrationTransaction_1.NamespaceRegistrationTransaction.createRootNamespace(Deadline_1.Deadline.create(), namespaceName, UInt64_1.UInt64.fromUint(1000), NetworkType_1.NetworkType.MIJIN_TEST);
            namespaceId = new NamespaceId_1.NamespaceId(namespaceName);
            const signedTransaction = registerNamespaceTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('Setup test MosaicAlias', () => {
        let listener;
        before(() => {
            listener = new infrastructure_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('Announce MosaicAliasTransaction', (done) => {
            const mosaicAliasTransaction = MosaicAliasTransaction_1.MosaicAliasTransaction.create(Deadline_1.Deadline.create(), AliasAction_1.AliasAction.Link, namespaceId, mosaicId, NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = mosaicAliasTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    /**
     * =========================
     * Test
     * =========================
     */
    describe('getMosaic', () => {
        it('should return mosaic given mosaicId', (done) => {
            mosaicHttp.getMosaic(mosaicId)
                .subscribe((mosaicInfo) => {
                chai_1.expect(mosaicInfo.height.lower).not.to.be.null;
                chai_1.expect(mosaicInfo.divisibility).to.be.equal(3);
                chai_1.expect(mosaicInfo.isSupplyMutable()).to.be.equal(true);
                chai_1.expect(mosaicInfo.isTransferable()).to.be.equal(true);
                done();
            });
        });
    });
    describe('getMosaics', () => {
        it('should return mosaics given array of mosaicIds', (done) => {
            mosaicHttp.getMosaics([mosaicId])
                .subscribe((mosaicInfos) => {
                chai_1.expect(mosaicInfos[0].height.lower).not.to.be.null;
                chai_1.expect(mosaicInfos[0].divisibility).to.be.equal(3);
                chai_1.expect(mosaicInfos[0].isSupplyMutable()).to.be.equal(true);
                chai_1.expect(mosaicInfos[0].isTransferable()).to.be.equal(true);
                done();
            });
        });
    });
    describe('getMosaicsNames', () => {
        it('should call getMosaicsNames successfully', (done) => {
            namespaceHttp.getMosaicsNames([mosaicId]).subscribe((mosaicNames) => {
                chai_1.expect(mosaicNames.length).to.be.greaterThan(0);
                done();
            });
        });
    });
    describe('getMosaicsFromAccount', () => {
        it('should call getMosaicsFromAccount successfully', (done) => {
            mosaicHttp.getMosaicsFromAccount(account.address).subscribe((mosaics) => {
                chai_1.expect(mosaics.length).to.be.greaterThan(0);
                chai_1.expect(mosaics.find((m) => m.id.toHex() === mosaicId.toHex()) !== undefined).to.be.true;
                done();
            });
        });
    });
    describe('getMosaicsFromAccounts', () => {
        it('should call getMosaicsFromAccounts successfully', (done) => {
            mosaicHttp.getMosaicsFromAccounts([account.address]).subscribe((mosaics) => {
                chai_1.expect(mosaics.length).to.be.greaterThan(0);
                chai_1.expect(mosaics.find((m) => m.id.toHex() === mosaicId.toHex()) !== undefined).to.be.true;
                done();
            });
        });
    });
    /**
     * =========================
     * House Keeping
     * =========================
     */
    describe('Remove test MosaicAlias', () => {
        let listener;
        before(() => {
            listener = new infrastructure_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('Announce MosaicAliasTransaction', (done) => {
            const mosaicAliasTransaction = MosaicAliasTransaction_1.MosaicAliasTransaction.create(Deadline_1.Deadline.create(), AliasAction_1.AliasAction.Unlink, namespaceId, mosaicId, NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = mosaicAliasTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
});
//# sourceMappingURL=MosaicHttp.spec.js.map