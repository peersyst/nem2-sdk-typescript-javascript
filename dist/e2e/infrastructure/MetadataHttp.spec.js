"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const infrastructure_1 = require("../../src/infrastructure/infrastructure");
const MetadataHttp_1 = require("../../src/infrastructure/MetadataHttp");
const Account_1 = require("../../src/model/account/Account");
const Address_1 = require("../../src/model/account/Address");
const PublicAccount_1 = require("../../src/model/account/PublicAccount");
const NetworkType_1 = require("../../src/model/blockchain/NetworkType");
const MosaicFlags_1 = require("../../src/model/mosaic/MosaicFlags");
const MosaicId_1 = require("../../src/model/mosaic/MosaicId");
const MosaicNonce_1 = require("../../src/model/mosaic/MosaicNonce");
const NamespaceId_1 = require("../../src/model/namespace/NamespaceId");
const AccountMetadataTransaction_1 = require("../../src/model/transaction/AccountMetadataTransaction");
const AggregateTransaction_1 = require("../../src/model/transaction/AggregateTransaction");
const Deadline_1 = require("../../src/model/transaction/Deadline");
const MosaicDefinitionTransaction_1 = require("../../src/model/transaction/MosaicDefinitionTransaction");
const MosaicMetadataTransaction_1 = require("../../src/model/transaction/MosaicMetadataTransaction");
const NamespaceMetadataTransaction_1 = require("../../src/model/transaction/NamespaceMetadataTransaction");
const NamespaceRegistrationTransaction_1 = require("../../src/model/transaction/NamespaceRegistrationTransaction");
const UInt64_1 = require("../../src/model/UInt64");
describe('MetadataHttp', () => {
    let account;
    let account2;
    let account3;
    let multisigAccount;
    let cosignAccount1;
    let cosignAccount2;
    let cosignAccount3;
    let accountAddress;
    let accountPublicKey;
    let publicAccount;
    let metadataHttp;
    let transactionHttp;
    let mosaicId;
    let namespaceId;
    let generationHash;
    let config;
    before((done) => {
        const path = require('path');
        require('fs').readFile(path.resolve(__dirname, '../conf/network.conf'), (err, data) => {
            if (err) {
                throw err;
            }
            const json = JSON.parse(data);
            config = json;
            account = Account_1.Account.createFromPrivateKey(json.testAccount.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            account2 = Account_1.Account.createFromPrivateKey(json.testAccount2.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            account3 = Account_1.Account.createFromPrivateKey(json.testAccount3.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            multisigAccount = Account_1.Account.createFromPrivateKey(json.multisigAccount.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            cosignAccount1 = Account_1.Account.createFromPrivateKey(json.cosignatoryAccount.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            cosignAccount2 = Account_1.Account.createFromPrivateKey(json.cosignatory2Account.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            cosignAccount3 = Account_1.Account.createFromPrivateKey(json.cosignatory3Account.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            accountAddress = Address_1.Address.createFromRawAddress(json.testAccount.address);
            accountPublicKey = json.testAccount.publicKey;
            publicAccount = PublicAccount_1.PublicAccount.createFromPublicKey(json.testAccount.publicKey, NetworkType_1.NetworkType.MIJIN_TEST);
            generationHash = json.generationHash;
            metadataHttp = new MetadataHttp_1.MetadataHttp(json.apiUrl);
            transactionHttp = new infrastructure_1.TransactionHttp(json.apiUrl);
            done();
        });
    });
    /**
     * =========================
     * Setup test data
     * =========================
     */
    describe('MosaicDefinitionTransaction', () => {
        let listener;
        before(() => {
            listener = new infrastructure_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const nonce = MosaicNonce_1.MosaicNonce.createRandom();
            mosaicId = MosaicId_1.MosaicId.createFromNonce(nonce, account.publicAccount);
            const mosaicDefinitionTransaction = MosaicDefinitionTransaction_1.MosaicDefinitionTransaction.create(Deadline_1.Deadline.create(), nonce, mosaicId, MosaicFlags_1.MosaicFlags.create(true, true, true), 3, UInt64_1.UInt64.fromUint(1000), NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = mosaicDefinitionTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('Setup test NamespaceId', () => {
        let listener;
        before(() => {
            listener = new infrastructure_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('Announce NamespaceRegistrationTransaction', (done) => {
            const namespaceName = 'root-test-namespace-' + Math.floor(Math.random() * 10000);
            const registerNamespaceTransaction = NamespaceRegistrationTransaction_1.NamespaceRegistrationTransaction.createRootNamespace(Deadline_1.Deadline.create(), namespaceName, UInt64_1.UInt64.fromUint(9), NetworkType_1.NetworkType.MIJIN_TEST);
            namespaceId = new NamespaceId_1.NamespaceId(namespaceName);
            const signedTransaction = registerNamespaceTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('AccountMetadataTransaction', () => {
        let listener;
        before(() => {
            listener = new infrastructure_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const accountMetadataTransaction = AccountMetadataTransaction_1.AccountMetadataTransaction.create(Deadline_1.Deadline.create(), account.publicKey, UInt64_1.UInt64.fromUint(5), 23, `Test account meta value`, NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [accountMetadataTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('MosaicMetadataTransaction', () => {
        let listener;
        before(() => {
            listener = new infrastructure_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const mosaicMetadataTransaction = MosaicMetadataTransaction_1.MosaicMetadataTransaction.create(Deadline_1.Deadline.create(), account.publicKey, UInt64_1.UInt64.fromUint(5), mosaicId, 22, `Test mosaic meta value`, NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [mosaicMetadataTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('NamespaceMetadataTransaction', () => {
        let listener;
        before(() => {
            listener = new infrastructure_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const namespaceMetadataTransaction = NamespaceMetadataTransaction_1.NamespaceMetadataTransaction.create(Deadline_1.Deadline.create(), account.publicKey, UInt64_1.UInt64.fromUint(5), namespaceId, 25, `Test namespace meta value`, NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [namespaceMetadataTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    /**
     * =========================
     * Tests
     * =========================
     */
    describe('getAccountMetadata', () => {
        it('should return metadata given a NEM Address', (done) => {
            metadataHttp.getAccountMetadata(accountAddress)
                .subscribe((metadata) => {
                chai_1.expect(metadata.length).to.be.greaterThan(0);
                chai_1.expect(metadata[0].metadataEntry.scopedMetadataKey.toString()).to.be.equal('5');
                chai_1.expect(metadata[0].metadataEntry.senderPublicKey).to.be.equal(account.publicKey);
                chai_1.expect(metadata[0].metadataEntry.targetPublicKey).to.be.equal(account.publicKey);
                chai_1.expect(metadata[0].metadataEntry.targetId).to.be.undefined;
                chai_1.expect(metadata[0].metadataEntry.value).to.be.equal('Test account meta value');
                chai_1.expect(metadata[0].metadataEntry.value.length).to.be.equal(23);
                done();
            });
        });
    });
    describe('getAccountMetadataByKey', () => {
        it('should return metadata given a NEM Address and metadata key', (done) => {
            metadataHttp.getAccountMetadataByKey(accountAddress, UInt64_1.UInt64.fromUint(5).toHex())
                .subscribe((metadata) => {
                chai_1.expect(metadata.length).to.be.greaterThan(0);
                chai_1.expect(metadata[0].metadataEntry.scopedMetadataKey.toString()).to.be.equal('5');
                chai_1.expect(metadata[0].metadataEntry.senderPublicKey).to.be.equal(account.publicKey);
                chai_1.expect(metadata[0].metadataEntry.targetPublicKey).to.be.equal(account.publicKey);
                chai_1.expect(metadata[0].metadataEntry.targetId).to.be.undefined;
                chai_1.expect(metadata[0].metadataEntry.value).to.be.equal('Test account meta value');
                chai_1.expect(metadata[0].metadataEntry.value.length).to.be.equal(23);
                done();
            });
        });
    });
    describe('getAccountMetadataByKeyAndSender', () => {
        it('should return metadata given a NEM Address and metadata key and sender public key', (done) => {
            metadataHttp.getAccountMetadataByKeyAndSender(accountAddress, UInt64_1.UInt64.fromUint(5).toHex(), account.publicKey)
                .subscribe((metadata) => {
                chai_1.expect(metadata.metadataEntry.scopedMetadataKey.toString()).to.be.equal('5');
                chai_1.expect(metadata.metadataEntry.senderPublicKey).to.be.equal(account.publicKey);
                chai_1.expect(metadata.metadataEntry.targetPublicKey).to.be.equal(account.publicKey);
                chai_1.expect(metadata.metadataEntry.targetId).to.be.undefined;
                chai_1.expect(metadata.metadataEntry.value).to.be.equal('Test account meta value');
                chai_1.expect(metadata.metadataEntry.value.length).to.be.equal(23);
                done();
            });
        });
    });
    describe('getMosaicMetadata', () => {
        it('should return metadata given a mosaicId', (done) => {
            metadataHttp.getMosaicMetadata(mosaicId)
                .subscribe((metadata) => {
                chai_1.expect(metadata.length).to.be.greaterThan(0);
                chai_1.expect(metadata[0].metadataEntry.scopedMetadataKey.toString()).to.be.equal('5');
                chai_1.expect(metadata[0].metadataEntry.senderPublicKey).to.be.equal(account.publicKey);
                chai_1.expect(metadata[0].metadataEntry.targetPublicKey).to.be.equal(account.publicKey);
                chai_1.expect(metadata[0].metadataEntry.targetId.toHex()).to.be.equal(mosaicId.toHex());
                chai_1.expect(metadata[0].metadataEntry.value).to.be.equal('Test mosaic meta value');
                chai_1.expect(metadata[0].metadataEntry.value.length).to.be.equal(22);
                done();
            });
        });
    });
    describe('getMosaicMetadataByKey', () => {
        it('should return metadata given a mosaicId and metadata key', (done) => {
            metadataHttp.getMosaicMetadataByKey(mosaicId, UInt64_1.UInt64.fromUint(5).toHex())
                .subscribe((metadata) => {
                chai_1.expect(metadata.length).to.be.greaterThan(0);
                chai_1.expect(metadata[0].metadataEntry.scopedMetadataKey.toString()).to.be.equal('5');
                chai_1.expect(metadata[0].metadataEntry.senderPublicKey).to.be.equal(account.publicKey);
                chai_1.expect(metadata[0].metadataEntry.targetPublicKey).to.be.equal(account.publicKey);
                chai_1.expect(metadata[0].metadataEntry.targetId.toHex()).to.be.equal(mosaicId.toHex());
                chai_1.expect(metadata[0].metadataEntry.value).to.be.equal('Test mosaic meta value');
                chai_1.expect(metadata[0].metadataEntry.value.length).to.be.equal(22);
                done();
            });
        });
    });
    describe('getMosaicMetadataByKeyAndSender', () => {
        it('should return metadata given a mosaicId and metadata key and sender public key', (done) => {
            metadataHttp.getMosaicMetadataByKeyAndSender(mosaicId, UInt64_1.UInt64.fromUint(5).toHex(), account.publicKey)
                .subscribe((metadata) => {
                chai_1.expect(metadata.metadataEntry.scopedMetadataKey.toString()).to.be.equal('5');
                chai_1.expect(metadata.metadataEntry.senderPublicKey).to.be.equal(account.publicKey);
                chai_1.expect(metadata.metadataEntry.targetPublicKey).to.be.equal(account.publicKey);
                chai_1.expect(metadata.metadataEntry.targetId.toHex()).to.be.equal(mosaicId.toHex());
                chai_1.expect(metadata.metadataEntry.value).to.be.equal('Test mosaic meta value');
                chai_1.expect(metadata.metadataEntry.value.length).to.be.equal(22);
                done();
            });
        });
    });
    describe('getNamespaceMetadata', () => {
        it('should return metadata given a namespaceId', (done) => {
            metadataHttp.getNamespaceMetadata(namespaceId)
                .subscribe((metadata) => {
                chai_1.expect(metadata.length).to.be.greaterThan(0);
                chai_1.expect(metadata[0].metadataEntry.scopedMetadataKey.toString()).to.be.equal('5');
                chai_1.expect(metadata[0].metadataEntry.senderPublicKey).to.be.equal(account.publicKey);
                chai_1.expect(metadata[0].metadataEntry.targetPublicKey).to.be.equal(account.publicKey);
                chai_1.expect(metadata[0].metadataEntry.targetId.toHex()).to.be.equal(namespaceId.toHex());
                chai_1.expect(metadata[0].metadataEntry.value).to.be.equal('Test namespace meta value');
                chai_1.expect(metadata[0].metadataEntry.value.length).to.be.equal(25);
                done();
            });
        });
    });
    describe('getNamespaceMetadataByKey', () => {
        it('should return metadata given a namespaceId and metadata key', (done) => {
            metadataHttp.getNamespaceMetadataByKey(namespaceId, UInt64_1.UInt64.fromUint(5).toHex())
                .subscribe((metadata) => {
                chai_1.expect(metadata.length).to.be.greaterThan(0);
                chai_1.expect(metadata[0].metadataEntry.scopedMetadataKey.toString()).to.be.equal('5');
                chai_1.expect(metadata[0].metadataEntry.senderPublicKey).to.be.equal(account.publicKey);
                chai_1.expect(metadata[0].metadataEntry.targetPublicKey).to.be.equal(account.publicKey);
                chai_1.expect(metadata[0].metadataEntry.targetId.toHex()).to.be.equal(namespaceId.toHex());
                chai_1.expect(metadata[0].metadataEntry.value).to.be.equal('Test namespace meta value');
                chai_1.expect(metadata[0].metadataEntry.value.length).to.be.equal(25);
                done();
            });
        });
    });
    describe('getNamespaceMetadataByKeyAndSender', () => {
        it('should return metadata given a namespaceId and metadata key and sender public key', (done) => {
            metadataHttp.getNamespaceMetadataByKeyAndSender(namespaceId, UInt64_1.UInt64.fromUint(5).toHex(), account.publicKey)
                .subscribe((metadata) => {
                chai_1.expect(metadata.metadataEntry.scopedMetadataKey.toString()).to.be.equal('5');
                chai_1.expect(metadata.metadataEntry.senderPublicKey).to.be.equal(account.publicKey);
                chai_1.expect(metadata.metadataEntry.targetPublicKey).to.be.equal(account.publicKey);
                chai_1.expect(metadata.metadataEntry.targetId.toHex()).to.be.equal(namespaceId.toHex());
                chai_1.expect(metadata.metadataEntry.value).to.be.equal('Test namespace meta value');
                chai_1.expect(metadata.metadataEntry.value.length).to.be.equal(25);
                done();
            });
        });
    });
});
//# sourceMappingURL=MetadataHttp.spec.js.map