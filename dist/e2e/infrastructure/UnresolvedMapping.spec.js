"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const chai_1 = require("chai");
const format_1 = require("../../src/core/format");
const AccountHttp_1 = require("../../src/infrastructure/AccountHttp");
const infrastructure_1 = require("../../src/infrastructure/infrastructure");
const Listener_1 = require("../../src/infrastructure/Listener");
const TransactionHttp_1 = require("../../src/infrastructure/TransactionHttp");
const Account_1 = require("../../src/model/account/Account");
const NetworkType_1 = require("../../src/model/blockchain/NetworkType");
const PlainMessage_1 = require("../../src/model/message/PlainMessage");
const MosaicFlags_1 = require("../../src/model/mosaic/MosaicFlags");
const MosaicId_1 = require("../../src/model/mosaic/MosaicId");
const MosaicNonce_1 = require("../../src/model/mosaic/MosaicNonce");
const NetworkCurrencyMosaic_1 = require("../../src/model/mosaic/NetworkCurrencyMosaic");
const AliasAction_1 = require("../../src/model/namespace/AliasAction");
const NamespaceId_1 = require("../../src/model/namespace/NamespaceId");
const MosaicRestrictionType_1 = require("../../src/model/restriction/MosaicRestrictionType");
const AddressAliasTransaction_1 = require("../../src/model/transaction/AddressAliasTransaction");
const AggregateTransaction_1 = require("../../src/model/transaction/AggregateTransaction");
const Deadline_1 = require("../../src/model/transaction/Deadline");
const MosaicAddressRestrictionTransaction_1 = require("../../src/model/transaction/MosaicAddressRestrictionTransaction");
const MosaicAliasTransaction_1 = require("../../src/model/transaction/MosaicAliasTransaction");
const MosaicDefinitionTransaction_1 = require("../../src/model/transaction/MosaicDefinitionTransaction");
const MosaicGlobalRestrictionTransaction_1 = require("../../src/model/transaction/MosaicGlobalRestrictionTransaction");
const MosaicMetadataTransaction_1 = require("../../src/model/transaction/MosaicMetadataTransaction");
const NamespaceRegistrationTransaction_1 = require("../../src/model/transaction/NamespaceRegistrationTransaction");
const TransferTransaction_1 = require("../../src/model/transaction/TransferTransaction");
const UInt64_1 = require("../../src/model/UInt64");
describe('TransactionHttp', () => {
    let account;
    let account2;
    let account3;
    let testAccountNoBalance;
    let harvestingAccount;
    let transactionHttp;
    let multisigAccount;
    let cosignAccount1;
    let cosignAccount2;
    let cosignAccount3;
    let mosaicId;
    let namespaceIdAddress;
    let namespaceIdMosaic;
    let networkCurrencyMosaicId;
    let accountHttp;
    let namespaceHttp;
    let config;
    let generationHash;
    before((done) => {
        const path = require('path');
        require('fs').readFile(path.resolve(__dirname, '../conf/network.conf'), (err, data) => {
            if (err) {
                throw err;
            }
            const json = JSON.parse(data);
            config = json;
            account = Account_1.Account.createFromPrivateKey(json.testAccount.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            account2 = Account_1.Account.createFromPrivateKey(json.testAccount2.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            account3 = Account_1.Account.createFromPrivateKey(json.testAccount3.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            testAccountNoBalance = Account_1.Account.createFromPrivateKey(json.testAccountNoBalance.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            harvestingAccount = Account_1.Account.createFromPrivateKey(json.harvestingAccount.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            multisigAccount = Account_1.Account.createFromPrivateKey(json.multisigAccount.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            cosignAccount1 = Account_1.Account.createFromPrivateKey(json.cosignatoryAccount.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            cosignAccount2 = Account_1.Account.createFromPrivateKey(json.cosignatory2Account.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            cosignAccount3 = Account_1.Account.createFromPrivateKey(json.cosignatory3Account.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            accountHttp = new AccountHttp_1.AccountHttp(json.apiUrl);
            transactionHttp = new TransactionHttp_1.TransactionHttp(json.apiUrl);
            namespaceHttp = new infrastructure_1.NamespaceHttp(json.apiUrl);
            generationHash = json.generationHash;
            done();
        });
    });
    /**
     * =========================
     * Setup test data
     * =========================
     */
    describe('Get network currency mosaic id', () => {
        it('get mosaicId', (done) => {
            namespaceHttp.getLinkedMosaicId(new NamespaceId_1.NamespaceId('cat.currency')).subscribe((networkMosaicId) => {
                networkCurrencyMosaicId = networkMosaicId;
                done();
            });
        });
    });
    describe('MosaicDefinitionTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const nonce = MosaicNonce_1.MosaicNonce.createRandom();
            mosaicId = MosaicId_1.MosaicId.createFromNonce(nonce, account.publicAccount);
            const mosaicDefinitionTransaction = MosaicDefinitionTransaction_1.MosaicDefinitionTransaction.create(Deadline_1.Deadline.create(), nonce, mosaicId, MosaicFlags_1.MosaicFlags.create(true, true, true), 3, UInt64_1.UInt64.fromUint(1000), NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = mosaicDefinitionTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('NamespaceRegistrationTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const namespaceName = 'root-test-namespace-' + Math.floor(Math.random() * 10000);
            const registerNamespaceTransaction = NamespaceRegistrationTransaction_1.NamespaceRegistrationTransaction.createRootNamespace(Deadline_1.Deadline.create(), namespaceName, UInt64_1.UInt64.fromUint(50), NetworkType_1.NetworkType.MIJIN_TEST);
            namespaceIdMosaic = new NamespaceId_1.NamespaceId(namespaceName);
            const signedTransaction = registerNamespaceTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('NamespaceRegistrationTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const namespaceName = 'root-test-namespace-' + Math.floor(Math.random() * 10000);
            const registerNamespaceTransaction = NamespaceRegistrationTransaction_1.NamespaceRegistrationTransaction.createRootNamespace(Deadline_1.Deadline.create(), namespaceName, UInt64_1.UInt64.fromUint(50), NetworkType_1.NetworkType.MIJIN_TEST);
            namespaceIdAddress = new NamespaceId_1.NamespaceId(namespaceName);
            const signedTransaction = registerNamespaceTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('AddressAliasTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const addressAliasTransaction = AddressAliasTransaction_1.AddressAliasTransaction.create(Deadline_1.Deadline.create(), AliasAction_1.AliasAction.Link, namespaceIdAddress, account.address, NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = addressAliasTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('MosaicAliasTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const mosaicAliasTransaction = MosaicAliasTransaction_1.MosaicAliasTransaction.create(Deadline_1.Deadline.create(), AliasAction_1.AliasAction.Link, namespaceIdMosaic, mosaicId, NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = mosaicAliasTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe(() => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    /**
     * =========================
     * Test unresolved inputs
     * =========================
     */
    describe('MosaicMetadataTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const mosaicMetadataTransaction = MosaicMetadataTransaction_1.MosaicMetadataTransaction.create(Deadline_1.Deadline.create(), account.publicKey, UInt64_1.UInt64.fromUint(5), namespaceIdMosaic, 10, format_1.Convert.uint8ToUtf8(new Uint8Array(10)), NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [mosaicMetadataTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                transaction.innerTransactions.forEach((innerTx) => {
                    chai_1.expect(innerTx.targetMosaicId instanceof NamespaceId_1.NamespaceId).to.be.true;
                });
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('MosaicGlobalRestrictionTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const mosaicGlobalRestrictionTransaction = MosaicGlobalRestrictionTransaction_1.MosaicGlobalRestrictionTransaction.create(Deadline_1.Deadline.create(), namespaceIdMosaic, UInt64_1.UInt64.fromUint(60641), UInt64_1.UInt64.fromUint(0), MosaicRestrictionType_1.MosaicRestrictionType.NONE, UInt64_1.UInt64.fromUint(0), MosaicRestrictionType_1.MosaicRestrictionType.GE, NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = mosaicGlobalRestrictionTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                chai_1.expect(transaction.mosaicId instanceof NamespaceId_1.NamespaceId).to.be.true;
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('MosaicAddressRestrictionTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregate', (done) => {
            const mosaicAddressRestrictionTransaction = MosaicAddressRestrictionTransaction_1.MosaicAddressRestrictionTransaction.create(Deadline_1.Deadline.create(), namespaceIdMosaic, UInt64_1.UInt64.fromUint(60641), namespaceIdAddress, UInt64_1.UInt64.fromUint(2), NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [mosaicAddressRestrictionTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                transaction.innerTransactions.forEach((innerTx) => {
                    chai_1.expect(innerTx.mosaicId instanceof NamespaceId_1.NamespaceId).to.be.true;
                    chai_1.expect(innerTx.targetAddress instanceof NamespaceId_1.NamespaceId).to.be.true;
                });
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('TransferTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), account2.address, [NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(1)], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = transferTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                chai_1.expect(transaction.mosaics[0].id instanceof NamespaceId_1.NamespaceId).to.be.true;
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    /**
     * =========================
     * House Keeping
     * =========================
     */
    describe('AddressAliasTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const addressAliasTransaction = AddressAliasTransaction_1.AddressAliasTransaction.create(Deadline_1.Deadline.create(), AliasAction_1.AliasAction.Unlink, namespaceIdAddress, account.address, NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = addressAliasTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                chai_1.expect(transaction.namespaceId, 'NamespaceId').not.to.be.undefined;
                chai_1.expect(transaction.aliasAction, 'AliasAction').not.to.be.undefined;
                chai_1.expect(transaction.address, 'Address').not.to.be.undefined;
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('MosaicAliasTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const mosaicAliasTransaction = MosaicAliasTransaction_1.MosaicAliasTransaction.create(Deadline_1.Deadline.create(), AliasAction_1.AliasAction.Unlink, namespaceIdMosaic, mosaicId, NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = mosaicAliasTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                chai_1.expect(transaction.namespaceId, 'NamespaceId').not.to.be.undefined;
                chai_1.expect(transaction.aliasAction, 'AliasAction').not.to.be.undefined;
                chai_1.expect(transaction.mosaicId, 'MosaicId').not.to.be.undefined;
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
});
//# sourceMappingURL=UnresolvedMapping.spec.js.map