import { TransactionHttp } from '../../src/infrastructure/TransactionHttp';
import { Account } from '../../src/model/account/Account';
import { Address } from '../../src/model/account/Address';
import { Mosaic } from '../../src/model/mosaic/Mosaic';
import { MosaicId } from '../../src/model/mosaic/MosaicId';
import { AggregateTransaction } from '../../src/model/transaction/AggregateTransaction';
import { SignedTransaction } from '../../src/model/transaction/SignedTransaction';
export declare class TransactionUtils {
    static createAndAnnounce(signer: Account, recipient: Address, transactionHttp: TransactionHttp, mosaic: Mosaic[] | undefined, generationHash: string): void;
    static announceAggregateBoundedTransaction(signedTransaction: SignedTransaction, transactionHttp: TransactionHttp): void;
    static createSignedAggregatedBondTransaction(aggregatedTo: Account, signer: Account, recipient: Address, generationHash: string): SignedTransaction;
    static createHashLockTransactionAndAnnounce(signedAggregatedTransaction: SignedTransaction, signer: Account, mosaicId: MosaicId, transactionHttp: TransactionHttp, generationHash: string): void;
    static cosignTransaction(transaction: AggregateTransaction, account: Account, transactionHttp: TransactionHttp): void;
    static createMultisigAccountModificationTransaction(account: Account, transactionHttp: TransactionHttp, generationHash: string): void;
}
