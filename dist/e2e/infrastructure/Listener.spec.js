"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const chai_1 = require("chai");
const AccountHttp_1 = require("../../src/infrastructure/AccountHttp");
const infrastructure_1 = require("../../src/infrastructure/infrastructure");
const Listener_1 = require("../../src/infrastructure/Listener");
const TransactionHttp_1 = require("../../src/infrastructure/TransactionHttp");
const Account_1 = require("../../src/model/account/Account");
const NetworkType_1 = require("../../src/model/blockchain/NetworkType");
const PlainMessage_1 = require("../../src/model/message/PlainMessage");
const model_1 = require("../../src/model/model");
const NetworkCurrencyMosaic_1 = require("../../src/model/mosaic/NetworkCurrencyMosaic");
const NamespaceId_1 = require("../../src/model/namespace/NamespaceId");
const AggregateTransaction_1 = require("../../src/model/transaction/AggregateTransaction");
const Deadline_1 = require("../../src/model/transaction/Deadline");
const MultisigAccountModificationTransaction_1 = require("../../src/model/transaction/MultisigAccountModificationTransaction");
const TransferTransaction_1 = require("../../src/model/transaction/TransferTransaction");
const TransactionUtils_1 = require("./TransactionUtils");
describe('Listener', () => {
    let accountHttp;
    let apiUrl;
    let transactionHttp;
    let account;
    let account2;
    let cosignAccount1;
    let cosignAccount2;
    let cosignAccount3;
    let cosignAccount4;
    let multisigAccount;
    let networkCurrencyMosaicId;
    let namespaceHttp;
    let generationHash;
    let config;
    before((done) => {
        const path = require('path');
        require('fs').readFile(path.resolve(__dirname, '../conf/network.conf'), (err, data) => {
            if (err) {
                throw err;
            }
            const json = JSON.parse(data);
            config = json;
            apiUrl = json.apiUrl;
            account = Account_1.Account.createFromPrivateKey(json.testAccount.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            account2 = Account_1.Account.createFromPrivateKey(json.testAccount2.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            multisigAccount = Account_1.Account.createFromPrivateKey(json.multisigAccount.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            cosignAccount1 = Account_1.Account.createFromPrivateKey(json.cosignatoryAccount.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            cosignAccount2 = Account_1.Account.createFromPrivateKey(json.cosignatory2Account.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            cosignAccount3 = Account_1.Account.createFromPrivateKey(json.cosignatory3Account.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            cosignAccount4 = Account_1.Account.createFromPrivateKey(json.cosignatory4Account.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
            transactionHttp = new TransactionHttp_1.TransactionHttp(json.apiUrl);
            accountHttp = new AccountHttp_1.AccountHttp(json.apiUrl);
            namespaceHttp = new infrastructure_1.NamespaceHttp(json.apiUrl);
            generationHash = json.generationHash;
            done();
        });
    });
    describe('Confirmed', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('confirmedTransactionsGiven address signer', (done) => {
            listener.confirmed(account.address).subscribe((res) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            TransactionUtils_1.TransactionUtils.createAndAnnounce(account, account.address, transactionHttp, undefined, generationHash);
        });
    });
    describe('Confirmed', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('confirmedTransactionsGiven address recipient', (done) => {
            const recipientAddress = account2.address;
            listener.confirmed(recipientAddress).subscribe((res) => {
                done();
            });
            listener.status(recipientAddress).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            TransactionUtils_1.TransactionUtils.createAndAnnounce(account, recipientAddress, transactionHttp, undefined, generationHash);
        });
    });
    describe('UnConfirmed', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('unconfirmedTransactionsAdded', (done) => {
            listener.unconfirmedAdded(account.address).subscribe((res) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            TransactionUtils_1.TransactionUtils.createAndAnnounce(account, account.address, transactionHttp, undefined, generationHash);
        });
    });
    describe('UnConfirmed', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('unconfirmedTransactionsRemoved', (done) => {
            listener.unconfirmedAdded(account.address).subscribe((res) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            TransactionUtils_1.TransactionUtils.createAndAnnounce(account, account.address, transactionHttp, undefined, generationHash);
        });
    });
    describe('Get network currency mosaic id', () => {
        it('get mosaicId', (done) => {
            namespaceHttp.getLinkedMosaicId(new NamespaceId_1.NamespaceId('cat.currency')).subscribe((networkMosaicId) => {
                networkCurrencyMosaicId = networkMosaicId;
                done();
            });
        });
    });
    describe('TransferTransaction', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('standalone', (done) => {
            const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), cosignAccount1.address, [new model_1.Mosaic(networkCurrencyMosaicId, model_1.UInt64.fromUint(10 * Math.pow(10, NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.DIVISIBILITY)))], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
            const signedTransaction = transferTransaction.signWith(account, generationHash);
            listener.confirmed(account.address).subscribe((transaction) => {
                done();
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('MultisigAccountModificationTransaction - Create multisig account', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('MultisigAccountModificationTransaction', (done) => {
            const modifyMultisigAccountTransaction = MultisigAccountModificationTransaction_1.MultisigAccountModificationTransaction.create(Deadline_1.Deadline.create(), 2, 1, [cosignAccount1.publicAccount,
                cosignAccount2.publicAccount,
                cosignAccount3.publicAccount,
            ], [], NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [modifyMultisigAccountTransaction.toAggregate(multisigAccount.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction
                .signTransactionWithCosignatories(multisigAccount, [cosignAccount1, cosignAccount2, cosignAccount3], generationHash);
            listener.confirmed(multisigAccount.address).subscribe((transaction) => {
                done();
            });
            listener.status(multisigAccount.address).subscribe((error) => {
                console.log('Error:', error);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('Aggregate Bonded Transactions', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregateBondedTransactionsAdded', (done) => {
            listener.aggregateBondedAdded(account.address).subscribe((res) => {
                done();
            });
            listener.confirmed(account.address).subscribe((res) => {
                TransactionUtils_1.TransactionUtils.announceAggregateBoundedTransaction(signedAggregatedTx, transactionHttp);
            });
            listener.status(account.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            const signedAggregatedTx = TransactionUtils_1.TransactionUtils.createSignedAggregatedBondTransaction(multisigAccount, account, account2.address, generationHash);
            TransactionUtils_1.TransactionUtils.createHashLockTransactionAndAnnounce(signedAggregatedTx, account, networkCurrencyMosaicId, transactionHttp, generationHash);
        });
    });
    describe('Aggregate Bonded Transactions', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('aggregateBondedTransactionsRemoved', (done) => {
            listener.confirmed(cosignAccount1.address).subscribe((res) => {
                listener.aggregateBondedRemoved(cosignAccount1.address).subscribe(() => {
                    done();
                });
                listener.aggregateBondedAdded(cosignAccount1.address).subscribe(() => {
                    accountHttp.getAccountPartialTransactions(cosignAccount1.publicAccount.address).subscribe((transactions) => {
                        const transactionToCosign = transactions[0];
                        TransactionUtils_1.TransactionUtils.cosignTransaction(transactionToCosign, cosignAccount2, transactionHttp);
                    });
                });
                listener.status(cosignAccount1.address).subscribe((error) => {
                    console.log('Error:', error);
                    chai_1.assert(false);
                    done();
                });
                TransactionUtils_1.TransactionUtils.announceAggregateBoundedTransaction(signedAggregatedTx, transactionHttp);
            });
            listener.status(cosignAccount1.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            const signedAggregatedTx = TransactionUtils_1.TransactionUtils.createSignedAggregatedBondTransaction(multisigAccount, cosignAccount1, account2.address, generationHash);
            TransactionUtils_1.TransactionUtils.
                createHashLockTransactionAndAnnounce(signedAggregatedTx, cosignAccount1, networkCurrencyMosaicId, transactionHttp, generationHash);
        });
    });
    describe('Aggregate Bonded Transactions', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('cosignatureAdded', (done) => {
            listener.cosignatureAdded(cosignAccount1.address).subscribe(() => {
                done();
            });
            listener.aggregateBondedAdded(cosignAccount1.address).subscribe(() => {
                accountHttp.getAccountPartialTransactions(cosignAccount1.publicAccount.address).subscribe((transactions) => {
                    const transactionToCosign = transactions[0];
                    TransactionUtils_1.TransactionUtils.cosignTransaction(transactionToCosign, cosignAccount2, transactionHttp);
                });
            });
            listener.confirmed(cosignAccount1.address).subscribe((res) => {
                TransactionUtils_1.TransactionUtils.announceAggregateBoundedTransaction(signedAggregatedTx, transactionHttp);
            });
            listener.status(cosignAccount1.address).subscribe((error) => {
                console.log('Error:', error);
                chai_1.assert(false);
                done();
            });
            const signedAggregatedTx = TransactionUtils_1.TransactionUtils.createSignedAggregatedBondTransaction(multisigAccount, cosignAccount1, account2.address, generationHash);
            TransactionUtils_1.TransactionUtils.
                createHashLockTransactionAndAnnounce(signedAggregatedTx, cosignAccount1, networkCurrencyMosaicId, transactionHttp, generationHash);
        });
    });
    describe('MultisigAccountModificationTransaction - Restore multisig Accounts', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(config.apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('Restore Multisig Account', (done) => {
            const removeCosigner1 = MultisigAccountModificationTransaction_1.MultisigAccountModificationTransaction.create(Deadline_1.Deadline.create(), -1, 0, [], [cosignAccount1.publicAccount], NetworkType_1.NetworkType.MIJIN_TEST);
            const removeCosigner2 = MultisigAccountModificationTransaction_1.MultisigAccountModificationTransaction.create(Deadline_1.Deadline.create(), 0, 0, [], [cosignAccount2.publicAccount], NetworkType_1.NetworkType.MIJIN_TEST);
            const removeCosigner3 = MultisigAccountModificationTransaction_1.MultisigAccountModificationTransaction.create(Deadline_1.Deadline.create(), -1, -1, [], [cosignAccount3.publicAccount], NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [removeCosigner1.toAggregate(multisigAccount.publicAccount),
                removeCosigner2.toAggregate(multisigAccount.publicAccount),
                removeCosigner3.toAggregate(multisigAccount.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = aggregateTransaction
                .signTransactionWithCosignatories(cosignAccount1, [cosignAccount2, cosignAccount3], generationHash);
            listener.confirmed(cosignAccount1.address).subscribe((transaction) => {
                done();
            });
            listener.status(cosignAccount1.address).subscribe((error) => {
                console.log('Error:', error);
                done();
            });
            transactionHttp.announce(signedTransaction);
        });
    });
    describe('Transactions Status', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('transactionStatusGiven', (done) => {
            listener.status(account.address).subscribe((error) => {
                chai_1.expect(error.status).to.be.equal('Failure_Core_Insufficient_Balance');
                done();
            });
            const mosaics = [NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createRelative(1000000000000)];
            TransactionUtils_1.TransactionUtils.createAndAnnounce(account, account2.address, transactionHttp, mosaics, generationHash);
        });
    });
    describe('New Block', () => {
        let listener;
        before(() => {
            listener = new Listener_1.Listener(apiUrl);
            return listener.open();
        });
        after(() => {
            return listener.close();
        });
        it('newBlock', (done) => {
            listener.newBlock().subscribe((res) => {
                done();
            });
            TransactionUtils_1.TransactionUtils.createAndAnnounce(account, account.address, transactionHttp, undefined, generationHash);
        });
    });
});
//# sourceMappingURL=Listener.spec.js.map