import { Account } from '../../src/model/account/Account';
export declare const TestingAccount: Account;
export declare const MultisigAccount: Account;
export declare const CosignatoryAccount: Account;
export declare const Cosignatory2Account: Account;
export declare const Cosignatory3Account: Account;
export declare const NIS2_URL = "http://localhost:3000";
