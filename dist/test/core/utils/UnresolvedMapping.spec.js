"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const chai_1 = require("chai");
const format_1 = require("../../../src/core/format");
const UnresolvedMapping_1 = require("../../../src/core/utils/UnresolvedMapping");
const Address_1 = require("../../../src/model/account/Address");
const NetworkType_1 = require("../../../src/model/blockchain/NetworkType");
const MosaicId_1 = require("../../../src/model/mosaic/MosaicId");
const NamespaceId_1 = require("../../../src/model/namespace/NamespaceId");
describe('UnresolvedMapping', () => {
    let mosaicId;
    let namespacId;
    let address;
    before(() => {
        mosaicId = new MosaicId_1.MosaicId('11F4B1B3AC033DB5');
        namespacId = NamespaceId_1.NamespaceId.createFromEncoded('9550CA3FC9B41FC5');
        address = Address_1.Address.createFromRawAddress('MCTVW23D2MN5VE4AQ4TZIDZENGNOZXPRPR72DYSX');
    });
    describe('toUnresolvedMosaic', () => {
        it('can map hex string to MosaicId', () => {
            const unresolved = UnresolvedMapping_1.UnresolvedMapping.toUnresolvedMosaic(mosaicId.toHex());
            chai_1.expect(unresolved instanceof MosaicId_1.MosaicId).to.be.true;
            chai_1.expect(unresolved instanceof NamespaceId_1.NamespaceId).to.be.false;
        });
        it('can map hex string to NamespaceId', () => {
            const unresolved = UnresolvedMapping_1.UnresolvedMapping.toUnresolvedMosaic(namespacId.toHex());
            chai_1.expect(unresolved instanceof MosaicId_1.MosaicId).to.be.false;
            chai_1.expect(unresolved instanceof NamespaceId_1.NamespaceId).to.be.true;
        });
        it('should throw error if id not in hex', () => {
            chai_1.expect(() => {
                UnresolvedMapping_1.UnresolvedMapping.toUnresolvedMosaic('test');
            }).to.throw(Error, 'Input string is not in valid hexadecimal notation.');
        });
    });
    describe('toUnresolvedAddress', () => {
        it('can map hex string to Address', () => {
            const unresolved = UnresolvedMapping_1.UnresolvedMapping.toUnresolvedAddress(format_1.Convert.uint8ToHex(format_1.RawAddress.stringToAddress(address.plain())));
            chai_1.expect(unresolved instanceof Address_1.Address).to.be.true;
            chai_1.expect(unresolved instanceof NamespaceId_1.NamespaceId).to.be.false;
        });
        it('can map hex string to NamespaceId', () => {
            const unresolved = UnresolvedMapping_1.UnresolvedMapping.toUnresolvedMosaic(namespacId.toHex());
            chai_1.expect(unresolved instanceof Address_1.Address).to.be.false;
            chai_1.expect(unresolved instanceof NamespaceId_1.NamespaceId).to.be.true;
        });
        it('should throw error if id not in hex', () => {
            chai_1.expect(() => {
                UnresolvedMapping_1.UnresolvedMapping.toUnresolvedAddress('test');
            }).to.throw(Error, 'Input string is not in valid hexadecimal notation.');
        });
    });
    describe('toUnresolvedAddressBytes', () => {
        it('can map Address to buffer', () => {
            const buffer = UnresolvedMapping_1.UnresolvedMapping.toUnresolvedAddressBytes(address, NetworkType_1.NetworkType.MIJIN_TEST);
            chai_1.expect(buffer instanceof Uint8Array).to.be.true;
            chai_1.expect(format_1.Convert.uint8ToHex(buffer)).to.be.equal(format_1.Convert.uint8ToHex(format_1.RawAddress.stringToAddress(address.plain())));
        });
        it('can map hex string to NamespaceId using MIJIN_TEST', () => {
            const buffer = UnresolvedMapping_1.UnresolvedMapping.toUnresolvedAddressBytes(namespacId, NetworkType_1.NetworkType.MIJIN_TEST);
            chai_1.expect(buffer instanceof Uint8Array).to.be.true;
            chai_1.expect(buffer[0]).to.be.equal(NetworkType_1.NetworkType.MIJIN_TEST | 1);
            chai_1.expect(format_1.Convert.uint8ToHex(buffer)).to.be.equal('91C51FB4C93FCA509500000000000000000000000000000000');
        });
        it('can map hex string to NamespaceId using MAIN_NET', () => {
            const buffer = UnresolvedMapping_1.UnresolvedMapping.toUnresolvedAddressBytes(namespacId, NetworkType_1.NetworkType.MAIN_NET);
            chai_1.expect(buffer instanceof Uint8Array).to.be.true;
            chai_1.expect(buffer[0]).to.be.equal(NetworkType_1.NetworkType.MAIN_NET | 1);
            chai_1.expect(format_1.Convert.uint8ToHex(buffer)).to.be.equal('69C51FB4C93FCA509500000000000000000000000000000000');
        });
    });
});
//# sourceMappingURL=UnresolvedMapping.spec.js.map