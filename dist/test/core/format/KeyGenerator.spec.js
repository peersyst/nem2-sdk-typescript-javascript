"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const chai_1 = require("chai");
const UInt64_1 = require("../../../src/model/UInt64");
const KeyGenerator_1 = require("../../../src/core/format/KeyGenerator");
describe('key generator', () => {
    describe('generate key from string', () => {
        it('throws if input is empty', () => {
            chai_1.expect(() => KeyGenerator_1.KeyGenerator.generateUInt64Key('')).to.throw(Error, 'Input must not be empty');
        });
        it('returns UInt64', () => {
            chai_1.expect(KeyGenerator_1.KeyGenerator.generateUInt64Key('a')).to.be.instanceOf(UInt64_1.UInt64);
        });
        it('generates correct keys', () => {
            chai_1.expect(KeyGenerator_1.KeyGenerator.generateUInt64Key('a').toHex()).to.equal('F524A0FBF24B0880');
        });
        it('generates keys deterministically', () => {
            chai_1.expect(KeyGenerator_1.KeyGenerator.generateUInt64Key('abc').toHex()).to.equal('B225E24FA75D983A');
            chai_1.expect(KeyGenerator_1.KeyGenerator.generateUInt64Key('abc').toHex()).to.equal('B225E24FA75D983A');
            chai_1.expect(KeyGenerator_1.KeyGenerator.generateUInt64Key('def').toHex()).to.equal('B0AC5222678F0D8E');
            chai_1.expect(KeyGenerator_1.KeyGenerator.generateUInt64Key('def').toHex()).to.equal('B0AC5222678F0D8E');
            chai_1.expect(KeyGenerator_1.KeyGenerator.generateUInt64Key('abc').toHex()).to.equal('B225E24FA75D983A');
        });
    });
});
//# sourceMappingURL=KeyGenerator.spec.js.map