"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const assert_1 = require("assert");
const MetadataEntry_1 = require("../../../src/model/metadata/MetadataEntry");
const MetadataType_1 = require("../../../src/model/metadata/MetadataType");
const model_1 = require("../../../src/model/model");
const UInt64_1 = require("../../../src/model/UInt64");
const conf_spec_1 = require("../../conf/conf.spec");
describe('MetadataEntry', () => {
    let account;
    const hash = '57F7DA205008026C776CB6AED843393F04CD458E0AA2D9F1D5F31A402072B2D6';
    before(() => {
        account = conf_spec_1.TestingAccount;
    });
    it('should createComplete an Account Metadata object', () => {
        const metadataEntryDTO = {
            compositeHash: hash,
            senderPublicKey: account.publicKey,
            targetPublicKey: account.publicKey,
            scopedMetadataKey: '85BBEA6CC462B244',
            targetId: undefined,
            metadataType: 0,
            value: '12345',
        };
        const metadata = new MetadataEntry_1.MetadataEntry(metadataEntryDTO.compositeHash, metadataEntryDTO.senderPublicKey, metadataEntryDTO.targetPublicKey, UInt64_1.UInt64.fromHex(metadataEntryDTO.scopedMetadataKey), metadataEntryDTO.metadataType, metadataEntryDTO.value);
        assert_1.deepEqual(metadata.senderPublicKey, account.publicKey);
        assert_1.deepEqual(metadata.compositeHash, hash);
        assert_1.deepEqual(metadata.targetPublicKey, account.publicKey);
        assert_1.deepEqual(metadata.scopedMetadataKey, UInt64_1.UInt64.fromHex('85BBEA6CC462B244'));
        assert_1.deepEqual(metadata.targetId, undefined);
        assert_1.deepEqual(metadata.metadataType, MetadataType_1.MetadataType.Account);
        assert_1.deepEqual(metadata.value, '12345');
    });
    it('should createComplete an Mosaic Metadata object', () => {
        const metadataEntryDTO = {
            compositeHash: hash,
            senderPublicKey: account.publicKey,
            targetPublicKey: account.publicKey,
            scopedMetadataKey: '85BBEA6CC462B244',
            targetId: '85BBEA6CC462B244',
            metadataType: 1,
            valueSize: 5,
            value: '12345',
        };
        const metadata = new MetadataEntry_1.MetadataEntry(metadataEntryDTO.compositeHash, metadataEntryDTO.senderPublicKey, metadataEntryDTO.targetPublicKey, UInt64_1.UInt64.fromHex(metadataEntryDTO.scopedMetadataKey), metadataEntryDTO.metadataType, metadataEntryDTO.value, new model_1.MosaicId(metadataEntryDTO.targetId));
        assert_1.deepEqual(metadata.senderPublicKey, account.publicKey);
        assert_1.deepEqual(metadata.compositeHash, hash);
        assert_1.deepEqual(metadata.targetPublicKey, account.publicKey);
        assert_1.deepEqual(metadata.scopedMetadataKey, UInt64_1.UInt64.fromHex('85BBEA6CC462B244'));
        assert_1.deepEqual(metadata.targetId.toHex(), '85BBEA6CC462B244');
        assert_1.deepEqual(metadata.metadataType, MetadataType_1.MetadataType.Mosaic);
        assert_1.deepEqual(metadata.value, '12345');
    });
    it('should createComplete an Namespace Metadata object', () => {
        const metadataEntryDTO = {
            compositeHash: hash,
            senderPublicKey: account.publicKey,
            targetPublicKey: account.publicKey,
            scopedMetadataKey: '85BBEA6CC462B244',
            targetId: '85BBEA6CC462B244',
            metadataType: 2,
            value: '12345',
        };
        const metadata = new MetadataEntry_1.MetadataEntry(metadataEntryDTO.compositeHash, metadataEntryDTO.senderPublicKey, metadataEntryDTO.targetPublicKey, UInt64_1.UInt64.fromHex(metadataEntryDTO.scopedMetadataKey), metadataEntryDTO.metadataType, metadataEntryDTO.value, model_1.NamespaceId.createFromEncoded(metadataEntryDTO.targetId));
        assert_1.deepEqual(metadata.senderPublicKey, account.publicKey);
        assert_1.deepEqual(metadata.compositeHash, hash);
        assert_1.deepEqual(metadata.targetPublicKey, account.publicKey);
        assert_1.deepEqual(metadata.scopedMetadataKey, UInt64_1.UInt64.fromHex('85BBEA6CC462B244'));
        assert_1.deepEqual(metadata.targetId.toHex(), '85BBEA6CC462B244');
        assert_1.deepEqual(metadata.metadataType, MetadataType_1.MetadataType.Namespace);
        assert_1.deepEqual(metadata.value, '12345');
    });
});
//# sourceMappingURL=MetadataEntry.spec.js.map