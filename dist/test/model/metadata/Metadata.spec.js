"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const assert_1 = require("assert");
const Metadata_1 = require("../../../src/model/metadata/Metadata");
const MetadataEntry_1 = require("../../../src/model/metadata/MetadataEntry");
const MetadataType_1 = require("../../../src/model/metadata/MetadataType");
const UInt64_1 = require("../../../src/model/UInt64");
const conf_spec_1 = require("../../conf/conf.spec");
describe('Metadata', () => {
    let account;
    const hash = '57F7DA205008026C776CB6AED843393F04CD458E0AA2D9F1D5F31A402072B2D6';
    before(() => {
        account = conf_spec_1.TestingAccount;
    });
    it('should createComplete an Metadata object', () => {
        const metadataEntryDTO = {
            compositeHash: hash,
            senderPublicKey: account.publicKey,
            targetPublicKey: account.publicKey,
            scopedMetadataKey: '85BBEA6CC462B244',
            targetId: undefined,
            metadataType: 0,
            value: '12345',
        };
        const metadataDTO = {
            meta: {
                id: '9999',
            },
            metadataEntry: metadataEntryDTO,
        };
        const metadata = new Metadata_1.Metadata(metadataDTO.meta.id, new MetadataEntry_1.MetadataEntry(metadataDTO.metadataEntry.compositeHash, metadataDTO.metadataEntry.senderPublicKey, metadataDTO.metadataEntry.targetPublicKey, UInt64_1.UInt64.fromHex(metadataDTO.metadataEntry.scopedMetadataKey), metadataDTO.metadataEntry.metadataType, metadataDTO.metadataEntry.value));
        assert_1.deepEqual(metadata.id, '9999');
        assert_1.deepEqual(metadata.metadataEntry.senderPublicKey, account.publicKey);
        assert_1.deepEqual(metadata.metadataEntry.compositeHash, hash);
        assert_1.deepEqual(metadata.metadataEntry.targetPublicKey, account.publicKey);
        assert_1.deepEqual(metadata.metadataEntry.scopedMetadataKey, UInt64_1.UInt64.fromHex('85BBEA6CC462B244'));
        assert_1.deepEqual(metadata.metadataEntry.targetId, undefined);
        assert_1.deepEqual(metadata.metadataEntry.metadataType, MetadataType_1.MetadataType.Account);
        assert_1.deepEqual(metadata.metadataEntry.value, '12345');
    });
});
//# sourceMappingURL=Metadata.spec.js.map