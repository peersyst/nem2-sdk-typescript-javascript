"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const format_1 = require("../../../src/core/format");
const Address_1 = require("../../../src/model/account/Address");
const NetworkType_1 = require("../../../src/model/blockchain/NetworkType");
const MosaicId_1 = require("../../../src/model/mosaic/MosaicId");
const AccountRestrictionModificationAction_1 = require("../../../src/model/restriction/AccountRestrictionModificationAction");
const AccountRestrictionType_1 = require("../../../src/model/restriction/AccountRestrictionType");
const AccountRestrictionModification_1 = require("../../../src/model/transaction/AccountRestrictionModification");
const AccountRestrictionTransaction_1 = require("../../../src/model/transaction/AccountRestrictionTransaction");
const Deadline_1 = require("../../../src/model/transaction/Deadline");
const TransactionType_1 = require("../../../src/model/transaction/TransactionType");
const UInt64_1 = require("../../../src/model/UInt64");
const conf_spec_1 = require("../../conf/conf.spec");
describe('AccountRestrictionTransaction', () => {
    let account;
    const generationHash = '57F7DA205008026C776CB6AED843393F04CD458E0AA2D9F1D5F31A402072B2D6';
    before(() => {
        account = conf_spec_1.TestingAccount;
    });
    it('should create address restriction filter', () => {
        const address = Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC');
        const addressRestrictionFilter = AccountRestrictionModification_1.AccountRestrictionModification.createForAddress(AccountRestrictionModificationAction_1.AccountRestrictionModificationAction.Add, address);
        chai_1.expect(addressRestrictionFilter.modificationAction).to.be.equal(AccountRestrictionModificationAction_1.AccountRestrictionModificationAction.Add);
        chai_1.expect(addressRestrictionFilter.value).to.be.equal(address.plain());
    });
    it('should create mosaic restriction filter', () => {
        const mosaicId = new MosaicId_1.MosaicId([2262289484, 3405110546]);
        const mosaicRestrictionFilter = AccountRestrictionModification_1.AccountRestrictionModification.createForMosaic(AccountRestrictionModificationAction_1.AccountRestrictionModificationAction.Add, mosaicId);
        chai_1.expect(mosaicRestrictionFilter.modificationAction).to.be.equal(AccountRestrictionModificationAction_1.AccountRestrictionModificationAction.Add);
        chai_1.expect(mosaicRestrictionFilter.value[0]).to.be.equal(mosaicId.id.lower);
        chai_1.expect(mosaicRestrictionFilter.value[1]).to.be.equal(mosaicId.id.higher);
    });
    it('should create operation restriction filter', () => {
        const operation = TransactionType_1.TransactionType.ADDRESS_ALIAS;
        const operationRestrictionFilter = AccountRestrictionModification_1.AccountRestrictionModification.createForOperation(AccountRestrictionModificationAction_1.AccountRestrictionModificationAction.Add, operation);
        chai_1.expect(operationRestrictionFilter.modificationAction).to.be.equal(AccountRestrictionModificationAction_1.AccountRestrictionModificationAction.Add);
        chai_1.expect(operationRestrictionFilter.value).to.be.equal(operation);
    });
    describe('size', () => {
        it('should return 161 for AccountAddressRestrictionTransaction transaction byte size with 1 modification', () => {
            const address = Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC');
            const addressRestrictionTransaction = AccountRestrictionTransaction_1.AccountRestrictionTransaction.createAddressRestrictionModificationTransaction(Deadline_1.Deadline.create(), AccountRestrictionType_1.AccountRestrictionFlags.AllowIncomingAddress, [address], [], NetworkType_1.NetworkType.MIJIN_TEST);
            chai_1.expect(format_1.Convert.hexToUint8(addressRestrictionTransaction.serialize()).length).to.be.equal(addressRestrictionTransaction.size);
            chai_1.expect(addressRestrictionTransaction.size).to.be.equal(161);
        });
        it('should return 144 for AccountMosaicRestrictionTransaction transaction byte size with 1 modification', () => {
            const mosaicId = new MosaicId_1.MosaicId([2262289484, 3405110546]);
            const mosaicRestrictionTransaction = AccountRestrictionTransaction_1.AccountRestrictionTransaction.createMosaicRestrictionModificationTransaction(Deadline_1.Deadline.create(), AccountRestrictionType_1.AccountRestrictionFlags.AllowMosaic, [mosaicId], [], NetworkType_1.NetworkType.MIJIN_TEST);
            chai_1.expect(mosaicRestrictionTransaction.size).to.be.equal(144);
        });
        it('should return 138 for AccountOperationRestrictionTransaction transaction byte size with 1 modification', () => {
            const operation = TransactionType_1.TransactionType.ADDRESS_ALIAS;
            const operationRestrictionTransaction = AccountRestrictionTransaction_1.AccountRestrictionTransaction.createOperationRestrictionModificationTransaction(Deadline_1.Deadline.create(), AccountRestrictionType_1.AccountRestrictionFlags.AllowIncomingTransactionType, [operation], [], NetworkType_1.NetworkType.MIJIN_TEST);
            chai_1.expect(operationRestrictionTransaction.size).to.be.equal(138);
        });
    });
    it('should default maxFee field be set to 0', () => {
        const address = Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC');
        const addressRestrictionTransaction = AccountRestrictionTransaction_1.AccountRestrictionTransaction.createAddressRestrictionModificationTransaction(Deadline_1.Deadline.create(), AccountRestrictionType_1.AccountRestrictionFlags.AllowIncomingAddress, [address], [], NetworkType_1.NetworkType.MIJIN_TEST);
        chai_1.expect(addressRestrictionTransaction.maxFee.higher).to.be.equal(0);
        chai_1.expect(addressRestrictionTransaction.maxFee.lower).to.be.equal(0);
    });
    it('should filled maxFee override transaction maxFee', () => {
        const address = Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC');
        const addressRestrictionTransaction = AccountRestrictionTransaction_1.AccountRestrictionTransaction.createAddressRestrictionModificationTransaction(Deadline_1.Deadline.create(), AccountRestrictionType_1.AccountRestrictionFlags.AllowIncomingAddress, [address], [], NetworkType_1.NetworkType.MIJIN_TEST, new UInt64_1.UInt64([1, 0]));
        chai_1.expect(addressRestrictionTransaction.maxFee.higher).to.be.equal(0);
        chai_1.expect(addressRestrictionTransaction.maxFee.lower).to.be.equal(1);
    });
    it('should create allow incmoing address restriction transaction', () => {
        const address = Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC');
        const addressRestrictionTransaction = AccountRestrictionTransaction_1.AccountRestrictionTransaction.createAddressRestrictionModificationTransaction(Deadline_1.Deadline.create(), AccountRestrictionType_1.AccountRestrictionFlags.AllowIncomingAddress, [address], [], NetworkType_1.NetworkType.MIJIN_TEST);
        const signedTransaction = addressRestrictionTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(256, signedTransaction.payload.length)).to.be.equal('01000100000000009050B9837EFAB4BBE8A4B9BB32D812F9885C00D8FC1650E142');
    });
    it('should throw exception when create address restriction transaction with wrong type', () => {
        const address = Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC');
        const invalidType = [AccountRestrictionType_1.AccountRestrictionFlags.AllowIncomingTransactionType,
            AccountRestrictionType_1.AccountRestrictionFlags.AllowMosaic,
            AccountRestrictionType_1.AccountRestrictionFlags.AllowOutgoingTransactionType,
            AccountRestrictionType_1.AccountRestrictionFlags.BlockIncomingTransactionType,
            AccountRestrictionType_1.AccountRestrictionFlags.BlockMosaic,
            AccountRestrictionType_1.AccountRestrictionFlags.BlockOutgoingTransactionType,
        ];
        invalidType.forEach((type) => {
            chai_1.expect(() => {
                AccountRestrictionTransaction_1.AccountRestrictionTransaction.createAddressRestrictionModificationTransaction(Deadline_1.Deadline.create(), type, [address], [], NetworkType_1.NetworkType.MIJIN_TEST);
            }).to.throw(Error, 'Restriction type is not allowed.');
        });
    });
    it('should create mosaic restriction transaction', () => {
        const mosaicId = new MosaicId_1.MosaicId([2262289484, 3405110546]);
        const mosaicRestrictionTransaction = AccountRestrictionTransaction_1.AccountRestrictionTransaction.createMosaicRestrictionModificationTransaction(Deadline_1.Deadline.create(), AccountRestrictionType_1.AccountRestrictionFlags.AllowMosaic, [mosaicId], [], NetworkType_1.NetworkType.MIJIN_TEST);
        const signedTransaction = mosaicRestrictionTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(256, signedTransaction.payload.length)).to.be.equal('02000100000000004CCCD78612DDF5CA');
    });
    it('should throw exception when create account mosaic restriction transaction with wrong type', () => {
        const mosaicId = new MosaicId_1.MosaicId([2262289484, 3405110546]);
        const invalidType = [AccountRestrictionType_1.AccountRestrictionFlags.AllowIncomingTransactionType,
            AccountRestrictionType_1.AccountRestrictionFlags.AllowIncomingAddress,
            AccountRestrictionType_1.AccountRestrictionFlags.AllowOutgoingTransactionType,
            AccountRestrictionType_1.AccountRestrictionFlags.BlockIncomingTransactionType,
            AccountRestrictionType_1.AccountRestrictionFlags.AllowOutgoingAddress,
            AccountRestrictionType_1.AccountRestrictionFlags.BlockOutgoingTransactionType,
            AccountRestrictionType_1.AccountRestrictionFlags.BlockIncomingAddress,
            AccountRestrictionType_1.AccountRestrictionFlags.BlockOutgoingAddress,
        ];
        invalidType.forEach((type) => {
            chai_1.expect(() => {
                AccountRestrictionTransaction_1.AccountRestrictionTransaction.createMosaicRestrictionModificationTransaction(Deadline_1.Deadline.create(), type, [mosaicId], [], NetworkType_1.NetworkType.MIJIN_TEST);
            }).to.throw(Error, 'Restriction type is not allowed.');
        });
    });
    it('should create operation restriction transaction', () => {
        const operation = TransactionType_1.TransactionType.ADDRESS_ALIAS;
        const operationRestrictionTransaction = AccountRestrictionTransaction_1.AccountRestrictionTransaction.createOperationRestrictionModificationTransaction(Deadline_1.Deadline.create(), AccountRestrictionType_1.AccountRestrictionFlags.AllowIncomingTransactionType, [operation], [], NetworkType_1.NetworkType.MIJIN_TEST);
        const signedTransaction = operationRestrictionTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(256, signedTransaction.payload.length)).to.be.equal('04000100000000004E42');
    });
    it('should throw exception when create account operation restriction transaction with wrong type', () => {
        const operation = TransactionType_1.TransactionType.ADDRESS_ALIAS;
        const invalidType = [AccountRestrictionType_1.AccountRestrictionFlags.AllowIncomingAddress,
            AccountRestrictionType_1.AccountRestrictionFlags.AllowMosaic,
            AccountRestrictionType_1.AccountRestrictionFlags.BlockMosaic,
            AccountRestrictionType_1.AccountRestrictionFlags.AllowOutgoingAddress,
            AccountRestrictionType_1.AccountRestrictionFlags.BlockIncomingAddress,
            AccountRestrictionType_1.AccountRestrictionFlags.BlockOutgoingAddress,
        ];
        invalidType.forEach((type) => {
            chai_1.expect(() => {
                AccountRestrictionTransaction_1.AccountRestrictionTransaction.createOperationRestrictionModificationTransaction(Deadline_1.Deadline.create(), type, [operation], [], NetworkType_1.NetworkType.MIJIN_TEST);
            }).to.throw(Error, 'Restriction type is not allowed.');
        });
    });
    it('should create outgoing address restriction transaction', () => {
        const address = Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC');
        let addressRestrictionTransaction = AccountRestrictionTransaction_1.AccountRestrictionTransaction.createAddressRestrictionModificationTransaction(Deadline_1.Deadline.create(), AccountRestrictionType_1.AccountRestrictionFlags.AllowOutgoingAddress, [address], [], NetworkType_1.NetworkType.MIJIN_TEST);
        let signedTransaction = addressRestrictionTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(256, signedTransaction.payload.length)).to.be.equal('01400100000000009050B9837EFAB4BBE8A4B9BB32D812F9885C00D8FC1650E142');
        addressRestrictionTransaction = AccountRestrictionTransaction_1.AccountRestrictionTransaction.createAddressRestrictionModificationTransaction(Deadline_1.Deadline.create(), AccountRestrictionType_1.AccountRestrictionFlags.BlockOutgoingAddress, [address], [], NetworkType_1.NetworkType.MIJIN_TEST);
        signedTransaction = addressRestrictionTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(256, signedTransaction.payload.length)).to.be.equal('01C00100000000009050B9837EFAB4BBE8A4B9BB32D812F9885C00D8FC1650E142');
    });
    it('should create outgoing operation restriction transaction', () => {
        const operation = TransactionType_1.TransactionType.ADDRESS_ALIAS;
        let operationRestrictionTransaction = AccountRestrictionTransaction_1.AccountRestrictionTransaction.createOperationRestrictionModificationTransaction(Deadline_1.Deadline.create(), AccountRestrictionType_1.AccountRestrictionFlags.AllowOutgoingTransactionType, [operation], [], NetworkType_1.NetworkType.MIJIN_TEST);
        let signedTransaction = operationRestrictionTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(256, signedTransaction.payload.length)).to.be.equal('04400100000000004E42');
        operationRestrictionTransaction = AccountRestrictionTransaction_1.AccountRestrictionTransaction.createOperationRestrictionModificationTransaction(Deadline_1.Deadline.create(), AccountRestrictionType_1.AccountRestrictionFlags.BlockOutgoingTransactionType, [operation], [], NetworkType_1.NetworkType.MIJIN_TEST);
        signedTransaction = operationRestrictionTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(256, signedTransaction.payload.length)).to.be.equal('04C00100000000004E42');
    });
});
//# sourceMappingURL=AccountRestrictionTransaction.spec.js.map