"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const Convert_1 = require("../../../src/core/format/Convert");
const NetworkType_1 = require("../../../src/model/blockchain/NetworkType");
const MosaicId_1 = require("../../../src/model/mosaic/MosaicId");
const NamespaceId_1 = require("../../../src/model/namespace/NamespaceId");
const ReceiptSource_1 = require("../../../src/model/receipt/ReceiptSource");
const ResolutionEntry_1 = require("../../../src/model/receipt/ResolutionEntry");
const ResolutionStatement_1 = require("../../../src/model/receipt/ResolutionStatement");
const ResolutionType_1 = require("../../../src/model/receipt/ResolutionType");
const Statement_1 = require("../../../src/model/receipt/Statement");
const Deadline_1 = require("../../../src/model/transaction/Deadline");
const MosaicMetadataTransaction_1 = require("../../../src/model/transaction/MosaicMetadataTransaction");
const TransactionInfo_1 = require("../../../src/model/transaction/TransactionInfo");
const UInt64_1 = require("../../../src/model/UInt64");
const conf_spec_1 = require("../../conf/conf.spec");
describe('MosaicMetadataTransaction', () => {
    let account;
    const generationHash = '57F7DA205008026C776CB6AED843393F04CD458E0AA2D9F1D5F31A402072B2D6';
    let statement;
    const unresolvedMosaicId = new NamespaceId_1.NamespaceId('mosaic');
    const resolvedMosaicId = new MosaicId_1.MosaicId('0DC67FBE1CAD29E5');
    before(() => {
        account = conf_spec_1.TestingAccount;
        statement = new Statement_1.Statement([], [], [new ResolutionStatement_1.ResolutionStatement(ResolutionType_1.ResolutionType.Mosaic, UInt64_1.UInt64.fromUint(2), unresolvedMosaicId, [new ResolutionEntry_1.ResolutionEntry(resolvedMosaicId, new ReceiptSource_1.ReceiptSource(1, 0))])]);
    });
    it('should default maxFee field be set to 0', () => {
        const mosaicMetadataTransaction = MosaicMetadataTransaction_1.MosaicMetadataTransaction.create(Deadline_1.Deadline.create(), account.publicKey, UInt64_1.UInt64.fromUint(1000), new MosaicId_1.MosaicId([2262289484, 3405110546]), 1, Convert_1.Convert.uint8ToUtf8(new Uint8Array(10)), NetworkType_1.NetworkType.MIJIN_TEST);
        chai_1.expect(mosaicMetadataTransaction.maxFee.higher).to.be.equal(0);
        chai_1.expect(mosaicMetadataTransaction.maxFee.lower).to.be.equal(0);
    });
    it('should filled maxFee override transaction maxFee', () => {
        const mosaicMetadataTransaction = MosaicMetadataTransaction_1.MosaicMetadataTransaction.create(Deadline_1.Deadline.create(), account.publicKey, UInt64_1.UInt64.fromUint(1000), new MosaicId_1.MosaicId([2262289484, 3405110546]), 1, Convert_1.Convert.uint8ToUtf8(new Uint8Array(10)), NetworkType_1.NetworkType.MIJIN_TEST, new UInt64_1.UInt64([1, 0]));
        chai_1.expect(mosaicMetadataTransaction.maxFee.higher).to.be.equal(0);
        chai_1.expect(mosaicMetadataTransaction.maxFee.lower).to.be.equal(1);
    });
    it('should create and sign an MosaicMetadataTransaction object', () => {
        const mosaicMetadataTransaction = MosaicMetadataTransaction_1.MosaicMetadataTransaction.create(Deadline_1.Deadline.create(), account.publicKey, UInt64_1.UInt64.fromUint(1000), new MosaicId_1.MosaicId([2262289484, 3405110546]), 1, Convert_1.Convert.uint8ToUtf8(new Uint8Array(10)), NetworkType_1.NetworkType.MIJIN_TEST);
        const signedTransaction = mosaicMetadataTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(256, signedTransaction.payload.length)).to.be.equal('C2F93346E27CE6AD1A9F8F5E3066F8326593A406BDF357ACB041E2F9AB402EFEE80' +
            '30000000000004CCCD78612DDF5CA01000A0000000000000000000000');
    });
    it('should throw error if value size is bigger than 1024', () => {
        chai_1.expect(() => {
            MosaicMetadataTransaction_1.MosaicMetadataTransaction.create(Deadline_1.Deadline.create(), account.publicKey, UInt64_1.UInt64.fromUint(1000), new MosaicId_1.MosaicId([2262289484, 3405110546]), 1, Convert_1.Convert.uint8ToUtf8(new Uint8Array(1025)), NetworkType_1.NetworkType.MIJIN_TEST);
        }).to.throw(Error, 'The maximum value size is 1024');
    });
    it('should create and sign an MosaicMetadataTransaction object using alias', () => {
        const namespacId = NamespaceId_1.NamespaceId.createFromEncoded('9550CA3FC9B41FC5');
        const mosaicMetadataTransaction = MosaicMetadataTransaction_1.MosaicMetadataTransaction.create(Deadline_1.Deadline.create(), account.publicKey, UInt64_1.UInt64.fromUint(1000), namespacId, 1, Convert_1.Convert.uint8ToUtf8(new Uint8Array(10)), NetworkType_1.NetworkType.MIJIN_TEST);
        const signedTransaction = mosaicMetadataTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(256, signedTransaction.payload.length)).to.be.equal('C2F93346E27CE6AD1A9F8F5E3066F8326593A406BDF357ACB041E2F9AB402EFEE80' +
            '3000000000000C51FB4C93FCA509501000A0000000000000000000000');
    });
    describe('size', () => {
        it('should return 190 for MosaicMetadataTransaction byte size', () => {
            const mosaicMetadataTransaction = MosaicMetadataTransaction_1.MosaicMetadataTransaction.create(Deadline_1.Deadline.create(), account.publicKey, UInt64_1.UInt64.fromUint(1000), new MosaicId_1.MosaicId([2262289484, 3405110546]), 1, Convert_1.Convert.uint8ToUtf8(new Uint8Array(10)), NetworkType_1.NetworkType.MIJIN_TEST);
            chai_1.expect(mosaicMetadataTransaction.size).to.be.equal(190);
            chai_1.expect(Convert_1.Convert.hexToUint8(mosaicMetadataTransaction.serialize()).length).to.be.equal(mosaicMetadataTransaction.size);
        });
    });
    it('Test set maxFee using multiplier', () => {
        const mosaicMetadataTransaction = MosaicMetadataTransaction_1.MosaicMetadataTransaction.create(Deadline_1.Deadline.create(), account.publicKey, UInt64_1.UInt64.fromUint(1000), new MosaicId_1.MosaicId([2262289484, 3405110546]), 1, Convert_1.Convert.uint8ToUtf8(new Uint8Array(10)), NetworkType_1.NetworkType.MIJIN_TEST).setMaxFee(2);
        chai_1.expect(mosaicMetadataTransaction.maxFee.compact()).to.be.equal(380);
    });
    it('Test resolveAlias can resolve', () => {
        const mosaicMetadataTransaction = new MosaicMetadataTransaction_1.MosaicMetadataTransaction(NetworkType_1.NetworkType.MIJIN_TEST, 1, Deadline_1.Deadline.createFromDTO('1'), UInt64_1.UInt64.fromUint(0), account.publicKey, UInt64_1.UInt64.fromUint(1000), unresolvedMosaicId, 10, Convert_1.Convert.uint8ToUtf8(new Uint8Array(10)), '', account.publicAccount, new TransactionInfo_1.TransactionInfo(UInt64_1.UInt64.fromUint(2), 0, '')).resolveAliases(statement);
        chai_1.expect(mosaicMetadataTransaction.targetMosaicId instanceof MosaicId_1.MosaicId).to.be.true;
        chai_1.expect(mosaicMetadataTransaction.targetMosaicId.equals(resolvedMosaicId)).to.be.true;
    });
});
//# sourceMappingURL=MosaicMetadataTransaction.spec.js.map