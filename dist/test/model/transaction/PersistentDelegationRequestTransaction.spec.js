"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const Address_1 = require("../../../src/model/account/Address");
const NetworkType_1 = require("../../../src/model/blockchain/NetworkType");
const Deadline_1 = require("../../../src/model/transaction/Deadline");
const PersistentDelegationRequestTransaction_1 = require("../../../src/model/transaction/PersistentDelegationRequestTransaction");
const UInt64_1 = require("../../../src/model/UInt64");
const conf_spec_1 = require("../../conf/conf.spec");
describe('PersistentDelegationRequestTransaction', () => {
    let account;
    const delegatedPrivateKey = '8A78C9E9B0E59D0F74C0D47AB29FBD523C706293A3FA9CD9FE0EEB2C10EA924A';
    const recipientPublicKey = '9DBF67474D6E1F8B131B4EB1F5BA0595AFFAE1123607BC1048F342193D7E669F';
    const generationHash = '57F7DA205008026C776CB6AED843393F04CD458E0AA2D9F1D5F31A402072B2D6';
    const messageMarker = 'FECC71C764BFE598';
    before(() => {
        account = conf_spec_1.TestingAccount;
    });
    it('should default maxFee field be set to 0', () => {
        const persistentDelegationRequestTransaction = PersistentDelegationRequestTransaction_1.PersistentDelegationRequestTransaction.createPersistentDelegationRequestTransaction(Deadline_1.Deadline.create(), delegatedPrivateKey, recipientPublicKey, account.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
        chai_1.expect(persistentDelegationRequestTransaction.maxFee.higher).to.be.equal(0);
        chai_1.expect(persistentDelegationRequestTransaction.maxFee.lower).to.be.equal(0);
    });
    it('should filled maxFee override transaction maxFee', () => {
        const persistentDelegationRequestTransaction = PersistentDelegationRequestTransaction_1.PersistentDelegationRequestTransaction.createPersistentDelegationRequestTransaction(Deadline_1.Deadline.create(), delegatedPrivateKey, recipientPublicKey, account.privateKey, NetworkType_1.NetworkType.MIJIN_TEST, new UInt64_1.UInt64([1, 0]));
        chai_1.expect(persistentDelegationRequestTransaction.maxFee.higher).to.be.equal(0);
        chai_1.expect(persistentDelegationRequestTransaction.maxFee.lower).to.be.equal(1);
    });
    it('should createComplete an persistentDelegationRequestTransaction object and sign it', () => {
        const persistentDelegationRequestTransaction = PersistentDelegationRequestTransaction_1.PersistentDelegationRequestTransaction.createPersistentDelegationRequestTransaction(Deadline_1.Deadline.create(), delegatedPrivateKey, recipientPublicKey, account.privateKey, NetworkType_1.NetworkType.MIJIN_TEST);
        chai_1.expect(persistentDelegationRequestTransaction.message.payload.length).to.be.equal(192 + messageMarker.length);
        chai_1.expect(persistentDelegationRequestTransaction.message.payload.includes(messageMarker)).to.be.true;
        chai_1.expect(persistentDelegationRequestTransaction.mosaics.length).to.be.equal(0);
        chai_1.expect(persistentDelegationRequestTransaction.recipientAddress).to.be.instanceof(Address_1.Address);
        chai_1.expect(persistentDelegationRequestTransaction.recipientAddress.plain())
            .to.be.equal('SDBC4JE7GTJAKN2XJCQWWRJMYA35AFOYQBATXOUA');
        const signedTransaction = persistentDelegationRequestTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(256, signedTransaction.payload.length).includes(persistentDelegationRequestTransaction.message.payload)).to.be.true;
    });
    it('should throw exception with invalid harvester publicKey (message)', () => {
        chai_1.expect(() => {
            PersistentDelegationRequestTransaction_1.PersistentDelegationRequestTransaction.createPersistentDelegationRequestTransaction(Deadline_1.Deadline.create(), 'abc', recipientPublicKey, account.privateKey, NetworkType_1.NetworkType.MIJIN_TEST, new UInt64_1.UInt64([1, 0]));
        }).to.throw();
    });
    it('should throw exception with invalid sender private key', () => {
        chai_1.expect(() => {
            PersistentDelegationRequestTransaction_1.PersistentDelegationRequestTransaction.createPersistentDelegationRequestTransaction(Deadline_1.Deadline.create(), delegatedPrivateKey, recipientPublicKey, 'abc', NetworkType_1.NetworkType.MIJIN_TEST, new UInt64_1.UInt64([1, 0]));
        }).to.throw();
    });
});
//# sourceMappingURL=PersistentDelegationRequestTransaction.spec.js.map