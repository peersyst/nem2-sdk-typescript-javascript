"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const format_1 = require("../../../src/core/format");
const CreateTransactionFromPayload_1 = require("../../../src/infrastructure/transaction/CreateTransactionFromPayload");
const Address_1 = require("../../../src/model/account/Address");
const NetworkType_1 = require("../../../src/model/blockchain/NetworkType");
const MessageType_1 = require("../../../src/model/message/MessageType");
const PersistentHarvestingDelegationMessage_1 = require("../../../src/model/message/PersistentHarvestingDelegationMessage");
const PlainMessage_1 = require("../../../src/model/message/PlainMessage");
const model_1 = require("../../../src/model/model");
const Mosaic_1 = require("../../../src/model/mosaic/Mosaic");
const MosaicId_1 = require("../../../src/model/mosaic/MosaicId");
const NetworkCurrencyMosaic_1 = require("../../../src/model/mosaic/NetworkCurrencyMosaic");
const NamespaceId_1 = require("../../../src/model/namespace/NamespaceId");
const ResolutionStatement_1 = require("../../../src/model/receipt/ResolutionStatement");
const Statement_1 = require("../../../src/model/receipt/Statement");
const Deadline_1 = require("../../../src/model/transaction/Deadline");
const TransferTransaction_1 = require("../../../src/model/transaction/TransferTransaction");
const UInt64_1 = require("../../../src/model/UInt64");
const conf_spec_1 = require("../../conf/conf.spec");
describe('TransferTransaction', () => {
    let account;
    const generationHash = '57F7DA205008026C776CB6AED843393F04CD458E0AA2D9F1D5F31A402072B2D6';
    const delegatedPrivateKey = '8A78C9E9B0E59D0F74C0D47AB29FBD523C706293A3FA9CD9FE0EEB2C10EA924A';
    const recipientPublicKey = '9DBF67474D6E1F8B131B4EB1F5BA0595AFFAE1123607BC1048F342193D7E669F';
    const messageMarker = 'FECC71C764BFE598';
    let statement;
    const unresolvedAddress = new NamespaceId_1.NamespaceId('address');
    const unresolvedMosaicId = new NamespaceId_1.NamespaceId('mosaic');
    const mosaicId = new MosaicId_1.MosaicId('0DC67FBE1CAD29E5');
    before(() => {
        account = conf_spec_1.TestingAccount;
    });
    before(() => {
        account = conf_spec_1.TestingAccount;
        statement = new Statement_1.Statement([], [new ResolutionStatement_1.ResolutionStatement(model_1.ResolutionType.Address, UInt64_1.UInt64.fromUint(2), unresolvedAddress, [new model_1.ResolutionEntry(account.address, new model_1.ReceiptSource(1, 0))])], [new ResolutionStatement_1.ResolutionStatement(model_1.ResolutionType.Mosaic, UInt64_1.UInt64.fromUint(2), unresolvedMosaicId, [new model_1.ResolutionEntry(mosaicId, new model_1.ReceiptSource(1, 0))])]);
    });
    it('should default maxFee field be set to 0', () => {
        const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC'), [], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
        chai_1.expect(transferTransaction.maxFee.higher).to.be.equal(0);
        chai_1.expect(transferTransaction.maxFee.lower).to.be.equal(0);
    });
    it('should filled maxFee override transaction maxFee', () => {
        const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC'), [], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST, new UInt64_1.UInt64([1, 0]));
        chai_1.expect(transferTransaction.maxFee.higher).to.be.equal(0);
        chai_1.expect(transferTransaction.maxFee.lower).to.be.equal(1);
    });
    it('should createComplete an TransferTransaction object and sign it without mosaics', () => {
        const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC'), [], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
        chai_1.expect(transferTransaction.message.payload).to.be.equal('test-message');
        chai_1.expect(transferTransaction.mosaics.length).to.be.equal(0);
        chai_1.expect(transferTransaction.recipientAddress).to.be.instanceof(Address_1.Address);
        chai_1.expect(transferTransaction.recipientAddress.plain()).to.be.equal('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC');
        const signedTransaction = transferTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(256, signedTransaction.payload.length)).to.be.equal('9050B9837EFAB4BBE8A4B9BB32D812F9885C00D8FC1650E142000D000000000000746573742D6D657373616765');
    });
    it('should createComplete an TransferTransaction object and sign it with mosaics', () => {
        const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC'), [
            NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createRelative(100),
        ], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
        chai_1.expect(transferTransaction.message.payload).to.be.equal('test-message');
        chai_1.expect(transferTransaction.mosaics.length).to.be.equal(1);
        chai_1.expect(transferTransaction.recipientAddress).to.be.instanceof(Address_1.Address);
        chai_1.expect(transferTransaction.recipientAddress.plain()).to.be.equal('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC');
        const signedTransaction = transferTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(256, signedTransaction.payload.length)).to.be.equal('9050B9837EFAB4BBE8A4B9BB32D812F9885C00D8FC1650E142010D000000000044B262C46CEABB8500E1F' +
            '5050000000000746573742D6D657373616765');
    });
    it('should createComplete an TransferTransaction object with NamespaceId recipientAddress', () => {
        const addressAlias = new NamespaceId_1.NamespaceId('nem.owner');
        const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), addressAlias, [
            NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createRelative(100),
        ], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
        chai_1.expect(transferTransaction.message.payload).to.be.equal('test-message');
        chai_1.expect(transferTransaction.mosaics.length).to.be.equal(1);
        chai_1.expect(transferTransaction.recipientAddress).to.be.instanceof(NamespaceId_1.NamespaceId);
        chai_1.expect(transferTransaction.recipientAddress).to.be.equal(addressAlias);
        chai_1.expect(transferTransaction.recipientAddress.toHex()).to.be.equal(addressAlias.toHex());
        const signedTransaction = transferTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(256, signedTransaction.payload.length)).to.be.equal('9151776168D24257D800000000000000000000000000000000010D000000000044B262C46CEABB8500E1F' +
            '5050000000000746573742D6D657373616765');
    });
    it('should format TransferTransaction payload with 25 bytes binary address', () => {
        const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC'), [
            NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createRelative(100),
        ], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
        // test recipientToString with Address recipient
        chai_1.expect(transferTransaction.recipientToString()).to.be.equal('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC');
        const signedTransaction = transferTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(256, 306)).to.be.equal('9050B9837EFAB4BBE8A4B9BB32D812F9885C00D8FC1650E142');
    });
    it('should format TransferTransaction payload with 8 bytes binary namespaceId', () => {
        const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), new NamespaceId_1.NamespaceId('nem.owner'), [
            NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createRelative(100),
        ], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
        // test recipientToString with NamespaceId recipient
        chai_1.expect(transferTransaction.recipientToString()).to.be.equal('D85742D268617751');
        const signedTransaction = transferTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(256, 306)).to.be.equal('9151776168D24257D800000000000000000000000000000000');
    });
    describe('size', () => {
        it('should return 180 for TransferTransaction with 1 mosaic and message NEM', () => {
            const transaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC'), [
                NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createRelative(100),
            ], PlainMessage_1.PlainMessage.create('NEM'), NetworkType_1.NetworkType.MIJIN_TEST);
            chai_1.expect(format_1.Convert.hexToUint8(transaction.serialize()).length).to.be.equal(transaction.size);
            chai_1.expect(transaction.size).to.be.equal(180);
        });
    });
    it('should create TransferTransaction and sign using catbuffer', () => {
        const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC'), [
            NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createRelative(100),
        ], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
        chai_1.expect(transferTransaction.message.payload).to.be.equal('test-message');
        chai_1.expect(transferTransaction.mosaics.length).to.be.equal(1);
        chai_1.expect(transferTransaction.recipientAddress).to.be.instanceof(Address_1.Address);
        chai_1.expect(transferTransaction.recipientAddress.plain()).to.be.equal('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC');
        const signedTransaction = transferTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(256, signedTransaction.payload.length)).to.be.equal('9050B9837EFAB4BBE8A4B9BB32D812F9885C00D8FC1650E142010D000000000044B262C46CEABB8500E1F' +
            '5050000000000746573742D6D657373616765');
    });
    it('should create Transafer transaction for persistent harvesting delegation request transaction', () => {
        const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC'), [], PersistentHarvestingDelegationMessage_1.PersistentHarvestingDelegationMessage
            .create(delegatedPrivateKey, account.privateKey, recipientPublicKey, NetworkType_1.NetworkType.MIJIN_TEST), NetworkType_1.NetworkType.MIJIN_TEST);
        chai_1.expect(transferTransaction.message.type).to.be.equal(MessageType_1.MessageType.PersistentHarvestingDelegationMessage);
    });
    it('should createComplete an persistentDelegationRequestTransaction object and sign it', () => {
        const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC'), [], PersistentHarvestingDelegationMessage_1.PersistentHarvestingDelegationMessage
            .create(delegatedPrivateKey, account.privateKey, recipientPublicKey, NetworkType_1.NetworkType.MIJIN_TEST), NetworkType_1.NetworkType.MIJIN_TEST);
        chai_1.expect(transferTransaction.message.payload.length).to.be.equal(192 + messageMarker.length);
        chai_1.expect(transferTransaction.message.payload.includes(messageMarker)).to.be.true;
        chai_1.expect(transferTransaction.mosaics.length).to.be.equal(0);
        chai_1.expect(transferTransaction.recipientAddress).to.be.instanceof(Address_1.Address);
        chai_1.expect(transferTransaction.recipientAddress.plain())
            .to.be.equal('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC');
        const signedTransaction = transferTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(256, signedTransaction.payload.length).includes(transferTransaction.message.payload)).to.be.true;
    });
    it('should throw exception with mosaic provided when creating persistentDelegationRequestTransaction', () => {
        chai_1.expect(() => {
            TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC'), [NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createRelative(100)], PersistentHarvestingDelegationMessage_1.PersistentHarvestingDelegationMessage
                .create(delegatedPrivateKey, account.privateKey, recipientPublicKey, NetworkType_1.NetworkType.MIJIN_TEST), NetworkType_1.NetworkType.MIJIN_TEST);
        }).to.throw(Error, 'PersistentDelegationRequestTransaction should be created without Mosaic');
    });
    it('should throw exception with invalid message when creating persistentDelegationRequestTransaction', () => {
        chai_1.expect(() => {
            TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC'), [NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createRelative(100)], PersistentHarvestingDelegationMessage_1.PersistentHarvestingDelegationMessage.create('abc', account.privateKey, recipientPublicKey, NetworkType_1.NetworkType.MIJIN_TEST), NetworkType_1.NetworkType.MIJIN_TEST);
        }).to.throw();
    });
    it('should throw exception with invalid private key when creating persistentDelegationRequestTransaction', () => {
        chai_1.expect(() => {
            TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC'), [NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createRelative(100)], PersistentHarvestingDelegationMessage_1.PersistentHarvestingDelegationMessage.create(delegatedPrivateKey, 'abc', recipientPublicKey, NetworkType_1.NetworkType.MIJIN_TEST), NetworkType_1.NetworkType.MIJIN_TEST);
        }).to.throw();
    });
    it('should sort the Mosaic array', () => {
        const mosaics = [
            new Mosaic_1.Mosaic(new MosaicId_1.MosaicId(UInt64_1.UInt64.fromUint(200).toDTO()), UInt64_1.UInt64.fromUint(0)),
            new Mosaic_1.Mosaic(new MosaicId_1.MosaicId(UInt64_1.UInt64.fromUint(100).toDTO()), UInt64_1.UInt64.fromUint(0)),
        ];
        const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC'), mosaics, PlainMessage_1.PlainMessage.create('NEM'), NetworkType_1.NetworkType.MIJIN_TEST);
        chai_1.expect(transferTransaction.mosaics[0].id.id.compact()).to.be.equal(200);
        chai_1.expect(transferTransaction.mosaics[1].id.id.compact()).to.be.equal(100);
        const signedTransaction = transferTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(320, 384)).to.be.equal('64000000000000000000000000000000C8000000000000000000000000000000');
        const sorted = CreateTransactionFromPayload_1.CreateTransactionFromPayload(signedTransaction.payload);
        chai_1.expect(sorted.mosaics[0].id.id.compact()).to.be.equal(100);
        chai_1.expect(sorted.mosaics[1].id.id.compact()).to.be.equal(200);
    });
    it('should sort the Mosaic array - using Hex MosaicId', () => {
        const mosaics = [
            new Mosaic_1.Mosaic(new MosaicId_1.MosaicId('D525AD41D95FCF29'), UInt64_1.UInt64.fromUint(5)),
            new Mosaic_1.Mosaic(new MosaicId_1.MosaicId('77A1969932D987D7'), UInt64_1.UInt64.fromUint(6)),
            new Mosaic_1.Mosaic(new MosaicId_1.MosaicId('67F2B76F28BD36BA'), UInt64_1.UInt64.fromUint(10)),
        ];
        const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC'), mosaics, PlainMessage_1.PlainMessage.create('NEM'), NetworkType_1.NetworkType.MIJIN_TEST);
        chai_1.expect(transferTransaction.mosaics[0].id.toHex()).to.be.equal('D525AD41D95FCF29');
        chai_1.expect(transferTransaction.mosaics[1].id.toHex()).to.be.equal('77A1969932D987D7');
        chai_1.expect(transferTransaction.mosaics[2].id.toHex()).to.be.equal('67F2B76F28BD36BA');
        const signedTransaction = transferTransaction.signWith(account, generationHash);
        const sorted = CreateTransactionFromPayload_1.CreateTransactionFromPayload(signedTransaction.payload);
        chai_1.expect(sorted.mosaics[0].id.toHex()).to.be.equal('67F2B76F28BD36BA');
        chai_1.expect(sorted.mosaics[1].id.toHex()).to.be.equal('77A1969932D987D7');
        chai_1.expect(sorted.mosaics[2].id.toHex()).to.be.equal('D525AD41D95FCF29');
    });
    it('Test Serialization and Deserialization Using namespaceIds', () => {
        const namespaceId = new NamespaceId_1.NamespaceId('testaccount2');
        const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.createFromDTO('1'), namespaceId, [NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(1)], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
        const payload = transferTransaction.serialize();
        const newTransaction = CreateTransactionFromPayload_1.CreateTransactionFromPayload(payload);
        const newPayload = newTransaction.serialize();
        chai_1.expect(newPayload).to.be.equal(payload);
        chai_1.expect(newTransaction.recipientToString()).to.be.equal(transferTransaction.recipientToString());
    });
    it('Test set maxFee using multiplier', () => {
        const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC'), [NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(1)], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST).setMaxFee(2);
        chai_1.expect(transferTransaction.maxFee.compact()).to.be.equal(378);
    });
    it('Test resolveAlias can resolve', () => {
        const transferTransaction = new TransferTransaction_1.TransferTransaction(NetworkType_1.NetworkType.MIJIN_TEST, 1, Deadline_1.Deadline.createFromDTO('1'), UInt64_1.UInt64.fromUint(0), unresolvedAddress, [new Mosaic_1.Mosaic(unresolvedMosaicId, UInt64_1.UInt64.fromUint(1))], PlainMessage_1.PlainMessage.create('test'), '', account.publicAccount, new model_1.TransactionInfo(UInt64_1.UInt64.fromUint(2), 0, '')).resolveAliases(statement);
        chai_1.expect(transferTransaction.recipientAddress instanceof Address_1.Address).to.be.true;
        chai_1.expect(transferTransaction.mosaics[0].id instanceof MosaicId_1.MosaicId).to.be.true;
        chai_1.expect(transferTransaction.recipientAddress.equals(account.address)).to.be.true;
        chai_1.expect(transferTransaction.mosaics[0].id.equals(mosaicId)).to.be.true;
    });
});
//# sourceMappingURL=TransferTransaction.spec.js.map