"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const assert_1 = require("assert");
const chai_1 = require("chai");
const format_1 = require("../../../src/core/format");
const NetworkType_1 = require("../../../src/model/blockchain/NetworkType");
const Mosaic_1 = require("../../../src/model/mosaic/Mosaic");
const MosaicId_1 = require("../../../src/model/mosaic/MosaicId");
const NetworkCurrencyMosaic_1 = require("../../../src/model/mosaic/NetworkCurrencyMosaic");
const NamespaceId_1 = require("../../../src/model/namespace/NamespaceId");
const ReceiptSource_1 = require("../../../src/model/receipt/ReceiptSource");
const ResolutionEntry_1 = require("../../../src/model/receipt/ResolutionEntry");
const ResolutionStatement_1 = require("../../../src/model/receipt/ResolutionStatement");
const ResolutionType_1 = require("../../../src/model/receipt/ResolutionType");
const Statement_1 = require("../../../src/model/receipt/Statement");
const AggregateTransaction_1 = require("../../../src/model/transaction/AggregateTransaction");
const Deadline_1 = require("../../../src/model/transaction/Deadline");
const LockFundsTransaction_1 = require("../../../src/model/transaction/LockFundsTransaction");
const TransactionInfo_1 = require("../../../src/model/transaction/TransactionInfo");
const UInt64_1 = require("../../../src/model/UInt64");
const conf_spec_1 = require("../../conf/conf.spec");
describe('LockFundsTransaction', () => {
    let account;
    const generationHash = '57F7DA205008026C776CB6AED843393F04CD458E0AA2D9F1D5F31A402072B2D6';
    let statement;
    const unresolvedMosaicId = new NamespaceId_1.NamespaceId('mosaic');
    const resolvedMosaicId = new MosaicId_1.MosaicId('0DC67FBE1CAD29E5');
    before(() => {
        account = conf_spec_1.TestingAccount;
        statement = new Statement_1.Statement([], [], [new ResolutionStatement_1.ResolutionStatement(ResolutionType_1.ResolutionType.Mosaic, UInt64_1.UInt64.fromUint(2), unresolvedMosaicId, [new ResolutionEntry_1.ResolutionEntry(resolvedMosaicId, new ReceiptSource_1.ReceiptSource(1, 0))])]);
    });
    it('should default maxFee field be set to 0', () => {
        const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createBonded(Deadline_1.Deadline.create(), [], NetworkType_1.NetworkType.MIJIN_TEST, []);
        const signedTransaction = account.sign(aggregateTransaction, generationHash);
        const lockFundsTransaction = LockFundsTransaction_1.LockFundsTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createRelative(10), UInt64_1.UInt64.fromUint(10), signedTransaction, NetworkType_1.NetworkType.MIJIN_TEST);
        chai_1.expect(lockFundsTransaction.maxFee.higher).to.be.equal(0);
        chai_1.expect(lockFundsTransaction.maxFee.lower).to.be.equal(0);
        chai_1.expect(format_1.Convert.hexToUint8(lockFundsTransaction.serialize()).length).to.be.equal(lockFundsTransaction.size);
    });
    it('should filled maxFee override transaction maxFee', () => {
        const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createBonded(Deadline_1.Deadline.create(), [], NetworkType_1.NetworkType.MIJIN_TEST, []);
        const signedTransaction = account.sign(aggregateTransaction, generationHash);
        const lockFundsTransaction = LockFundsTransaction_1.LockFundsTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createRelative(10), UInt64_1.UInt64.fromUint(10), signedTransaction, NetworkType_1.NetworkType.MIJIN_TEST, new UInt64_1.UInt64([1, 0]));
        chai_1.expect(lockFundsTransaction.maxFee.higher).to.be.equal(0);
        chai_1.expect(lockFundsTransaction.maxFee.lower).to.be.equal(1);
    });
    it('creation with an aggregate bonded tx', () => {
        const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createBonded(Deadline_1.Deadline.create(), [], NetworkType_1.NetworkType.MIJIN_TEST, []);
        const signedTransaction = account.sign(aggregateTransaction, generationHash);
        const transaction = LockFundsTransaction_1.LockFundsTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createRelative(10), UInt64_1.UInt64.fromUint(10), signedTransaction, NetworkType_1.NetworkType.MIJIN_TEST);
        assert_1.deepEqual(transaction.mosaic.id.id, NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.NAMESPACE_ID.id);
        chai_1.expect(transaction.mosaic.amount.compact()).to.be.equal(10000000);
        chai_1.expect(transaction.hash).to.be.equal(signedTransaction.hash);
    });
    it('should throw exception if it is not a aggregate bonded tx', () => {
        const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [], NetworkType_1.NetworkType.MIJIN_TEST, []);
        const signedTransaction = account.sign(aggregateTransaction, generationHash);
        chai_1.expect(() => {
            LockFundsTransaction_1.LockFundsTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createRelative(10), UInt64_1.UInt64.fromUint(10), signedTransaction, NetworkType_1.NetworkType.MIJIN_TEST);
        }).to.throw(Error);
    });
    it('should create and sign LockFundsTransaction', () => {
        const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createBonded(Deadline_1.Deadline.create(), [], NetworkType_1.NetworkType.MIJIN_TEST, []);
        const signedTransaction = account.sign(aggregateTransaction, generationHash);
        const lockFundsTransaction = LockFundsTransaction_1.LockFundsTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createRelative(10), UInt64_1.UInt64.fromUint(10), signedTransaction, NetworkType_1.NetworkType.MIJIN_TEST);
        const signedTx = lockFundsTransaction.signWith(account, generationHash);
        chai_1.expect(signedTx.payload.substring(144, signedTransaction.payload.length - 104)).to.be.equal('C2F93346E27CE6AD1A9F8F5E3066F8326593A406BDF357ACB041E2F9AB402EFE000000000190484100000000');
    });
    describe('size', () => {
        it('should return 184 for LockFundsTransaction transaction byte size', () => {
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createBonded(Deadline_1.Deadline.create(), [], NetworkType_1.NetworkType.MIJIN_TEST, []);
            const signedTransaction = account.sign(aggregateTransaction, generationHash);
            const lockFundsTransaction = LockFundsTransaction_1.LockFundsTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createRelative(10), UInt64_1.UInt64.fromUint(10), signedTransaction, NetworkType_1.NetworkType.MIJIN_TEST);
            chai_1.expect(lockFundsTransaction.size).to.be.equal(184);
        });
    });
    it('Test set maxFee using multiplier', () => {
        const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createBonded(Deadline_1.Deadline.create(), [], NetworkType_1.NetworkType.MIJIN_TEST, []);
        const signedTransaction = account.sign(aggregateTransaction, generationHash);
        const lockFundsTransaction = LockFundsTransaction_1.LockFundsTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createRelative(10), UInt64_1.UInt64.fromUint(10), signedTransaction, NetworkType_1.NetworkType.MIJIN_TEST).setMaxFee(2);
        chai_1.expect(lockFundsTransaction.maxFee.compact()).to.be.equal(368);
    });
    it('Test resolveAlias can resolve', () => {
        const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createBonded(Deadline_1.Deadline.create(), [], NetworkType_1.NetworkType.MIJIN_TEST, []);
        const signedTransaction = account.sign(aggregateTransaction, generationHash);
        const transaction = new LockFundsTransaction_1.LockFundsTransaction(NetworkType_1.NetworkType.MIJIN_TEST, 1, Deadline_1.Deadline.createFromDTO('1'), UInt64_1.UInt64.fromUint(0), new Mosaic_1.Mosaic(unresolvedMosaicId, UInt64_1.UInt64.fromUint(1)), UInt64_1.UInt64.fromUint(10), signedTransaction, '', account.publicAccount, new TransactionInfo_1.TransactionInfo(UInt64_1.UInt64.fromUint(2), 0, '')).resolveAliases(statement);
        chai_1.expect(transaction.mosaic.id instanceof MosaicId_1.MosaicId).to.be.true;
        chai_1.expect(transaction.mosaic.id.equals(resolvedMosaicId)).to.be.true;
    });
});
//# sourceMappingURL=LockFundsTransaction.spec.js.map