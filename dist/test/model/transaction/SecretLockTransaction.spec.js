"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const assert_1 = require("assert");
const chai_1 = require("chai");
const CryptoJS = require("crypto-js");
const js_sha3_1 = require("js-sha3");
const format_1 = require("../../../src/core/format");
const Address_1 = require("../../../src/model/account/Address");
const NetworkType_1 = require("../../../src/model/blockchain/NetworkType");
const Mosaic_1 = require("../../../src/model/mosaic/Mosaic");
const MosaicId_1 = require("../../../src/model/mosaic/MosaicId");
const NetworkCurrencyMosaic_1 = require("../../../src/model/mosaic/NetworkCurrencyMosaic");
const NamespaceId_1 = require("../../../src/model/namespace/NamespaceId");
const ReceiptSource_1 = require("../../../src/model/receipt/ReceiptSource");
const ResolutionEntry_1 = require("../../../src/model/receipt/ResolutionEntry");
const ResolutionStatement_1 = require("../../../src/model/receipt/ResolutionStatement");
const ResolutionType_1 = require("../../../src/model/receipt/ResolutionType");
const Statement_1 = require("../../../src/model/receipt/Statement");
const Deadline_1 = require("../../../src/model/transaction/Deadline");
const HashType_1 = require("../../../src/model/transaction/HashType");
const SecretLockTransaction_1 = require("../../../src/model/transaction/SecretLockTransaction");
const TransactionInfo_1 = require("../../../src/model/transaction/TransactionInfo");
const UInt64_1 = require("../../../src/model/UInt64");
const conf_spec_1 = require("../../conf/conf.spec");
describe('SecretLockTransaction', () => {
    let account;
    let statement;
    const unresolvedAddress = new NamespaceId_1.NamespaceId('address');
    const unresolvedMosaicId = new NamespaceId_1.NamespaceId('mosaic');
    const mosaicId = new MosaicId_1.MosaicId('0DC67FBE1CAD29E5');
    const generationHash = '57F7DA205008026C776CB6AED843393F04CD458E0AA2D9F1D5F31A402072B2D6';
    before(() => {
        account = conf_spec_1.TestingAccount;
        statement = new Statement_1.Statement([], [new ResolutionStatement_1.ResolutionStatement(ResolutionType_1.ResolutionType.Address, UInt64_1.UInt64.fromUint(2), unresolvedAddress, [new ResolutionEntry_1.ResolutionEntry(account.address, new ReceiptSource_1.ReceiptSource(1, 0))])], [new ResolutionStatement_1.ResolutionStatement(ResolutionType_1.ResolutionType.Mosaic, UInt64_1.UInt64.fromUint(2), unresolvedMosaicId, [new ResolutionEntry_1.ResolutionEntry(mosaicId, new ReceiptSource_1.ReceiptSource(1, 0))])]);
    });
    it('should default maxFee field be set to 0', () => {
        const proof = 'B778A39A3663719DFC5E48C9D78431B1E45C2AF9DF538782BF199C189DABEAC7';
        const recipientAddress = Address_1.Address.createFromRawAddress('SDBDG4IT43MPCW2W4CBBCSJJT42AYALQN7A4VVWL');
        const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Sha3_256, js_sha3_1.sha3_256.create().update(format_1.Convert.hexToUint8(proof)).hex(), recipientAddress, NetworkType_1.NetworkType.MIJIN_TEST);
        chai_1.expect(secretLockTransaction.maxFee.higher).to.be.equal(0);
        chai_1.expect(secretLockTransaction.maxFee.lower).to.be.equal(0);
    });
    it('should filled maxFee override transaction maxFee', () => {
        const proof = 'B778A39A3663719DFC5E48C9D78431B1E45C2AF9DF538782BF199C189DABEAC7';
        const recipientAddress = Address_1.Address.createFromRawAddress('SDBDG4IT43MPCW2W4CBBCSJJT42AYALQN7A4VVWL');
        const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Sha3_256, js_sha3_1.sha3_256.create().update(format_1.Convert.hexToUint8(proof)).hex(), recipientAddress, NetworkType_1.NetworkType.MIJIN_TEST, new UInt64_1.UInt64([1, 0]));
        chai_1.expect(secretLockTransaction.maxFee.higher).to.be.equal(0);
        chai_1.expect(secretLockTransaction.maxFee.lower).to.be.equal(1);
    });
    it('should be created with HashType: Op_Sha3_256 secret', () => {
        const proof = 'B778A39A3663719DFC5E48C9D78431B1E45C2AF9DF538782BF199C189DABEAC7';
        const recipientAddress = Address_1.Address.createFromRawAddress('SDBDG4IT43MPCW2W4CBBCSJJT42AYALQN7A4VVWL');
        const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Sha3_256, js_sha3_1.sha3_256.create().update(format_1.Convert.hexToUint8(proof)).hex(), recipientAddress, NetworkType_1.NetworkType.MIJIN_TEST);
        assert_1.deepEqual(secretLockTransaction.mosaic.id.id, NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.NAMESPACE_ID.id);
        chai_1.expect(secretLockTransaction.mosaic.amount.equals(UInt64_1.UInt64.fromUint(10))).to.be.equal(true);
        chai_1.expect(secretLockTransaction.duration.equals(UInt64_1.UInt64.fromUint(100))).to.be.equal(true);
        chai_1.expect(secretLockTransaction.hashType).to.be.equal(0);
        chai_1.expect(secretLockTransaction.secret).to.be.equal('9b3155b37159da50aa52d5967c509b410f5a36a3b1e31ecb5ac76675d79b4a5e');
        chai_1.expect(secretLockTransaction.recipientAddress).to.be.equal(recipientAddress);
    });
    it('should be created and sign SecretLock Transaction', () => {
        const proof = 'B778A39A3663719DFC5E48C9D78431B1E45C2AF9DF538782BF199C189DABEAC7';
        const recipientAddress = Address_1.Address.createFromRawAddress('SDBDG4IT43MPCW2W4CBBCSJJT42AYALQN7A4VVWL');
        const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Sha3_256, js_sha3_1.sha3_256.create().update(format_1.Convert.hexToUint8(proof)).hex(), recipientAddress, NetworkType_1.NetworkType.MIJIN_TEST);
        const signedTx = secretLockTransaction.signWith(account, generationHash);
        chai_1.expect(signedTx.payload.substring(256, signedTx.payload.length)).to.be.equal('9B3155B37159DA50AA52D5967C509B410F5A36A3B1E31ECB5AC76675D79B4A5E44B262C46CEABB850A' +
            '0000000000000064000000000000000090C2337113E6D8F15B56E0821149299F340C01706FC1CAD6CB');
    });
    it('should throw exception when the input is not related to HashTyp: Op_Sha3_256', () => {
        chai_1.expect(() => {
            const recipientAddress = Address_1.Address.createFromRawAddress('SDBDG4IT43MPCW2W4CBBCSJJT42AYALQN7A4VVWL');
            const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Sha3_256, 'non valid hash', recipientAddress, NetworkType_1.NetworkType.MIJIN_TEST);
        }).to.throw(Error);
    });
    it('should be created with HashType: Op_Keccak_256 secret', () => {
        const proof = 'B778A39A3663719DFC5E48C9D78431B1E45C2AF9DF538782BF199C189DABEAC7';
        const recipientAddress = Address_1.Address.createFromRawAddress('SDBDG4IT43MPCW2W4CBBCSJJT42AYALQN7A4VVWL');
        const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Keccak_256, js_sha3_1.keccak_256.create().update(format_1.Convert.hexToUint8(proof)).hex(), recipientAddress, NetworkType_1.NetworkType.MIJIN_TEST);
        assert_1.deepEqual(secretLockTransaction.mosaic.id.id, NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.NAMESPACE_ID.id);
        chai_1.expect(secretLockTransaction.mosaic.amount.equals(UInt64_1.UInt64.fromUint(10))).to.be.equal(true);
        chai_1.expect(secretLockTransaction.duration.equals(UInt64_1.UInt64.fromUint(100))).to.be.equal(true);
        chai_1.expect(secretLockTransaction.hashType).to.be.equal(1);
        chai_1.expect(secretLockTransaction.secret).to.be.equal('241c1d54c18c8422def03aa16b4b243a8ba491374295a1a6965545e6ac1af314');
        chai_1.expect(secretLockTransaction.recipientAddress).to.be.equal(recipientAddress);
    });
    it('should throw exception when the input is not related to HashTyp: Op_Keccak_256', () => {
        chai_1.expect(() => {
            const recipientAddress = Address_1.Address.createFromRawAddress('SDBDG4IT43MPCW2W4CBBCSJJT42AYALQN7A4VVWL');
            const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Keccak_256, 'non valid hash', recipientAddress, NetworkType_1.NetworkType.MIJIN_TEST);
        }).to.throw(Error);
    });
    it('should be created with HashType: Op_Hash_160 secret', () => {
        const proof = 'B778A39A3663719DFC5E48C9D78431B1E45C2AF9';
        const recipientAddress = Address_1.Address.createFromRawAddress('SDBDG4IT43MPCW2W4CBBCSJJT42AYALQN7A4VVWL');
        const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Hash_160, CryptoJS.RIPEMD160(CryptoJS.SHA256(proof).toString(CryptoJS.enc.Hex)).toString(CryptoJS.enc.Hex), recipientAddress, NetworkType_1.NetworkType.MIJIN_TEST);
        assert_1.deepEqual(secretLockTransaction.mosaic.id.id, NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.NAMESPACE_ID.id);
        chai_1.expect(secretLockTransaction.mosaic.amount.equals(UInt64_1.UInt64.fromUint(10))).to.be.equal(true);
        chai_1.expect(secretLockTransaction.duration.equals(UInt64_1.UInt64.fromUint(100))).to.be.equal(true);
        chai_1.expect(secretLockTransaction.hashType).to.be.equal(2);
        chai_1.expect(secretLockTransaction.secret).to.be.equal('3fc43d717d824302e3821de8129ea2f7786912e5');
        chai_1.expect(secretLockTransaction.recipientAddress).to.be.equal(recipientAddress);
    });
    it('should throw exception when the input is not related to HashTyp: Op_Hash_160', () => {
        chai_1.expect(() => {
            const recipientAddress = Address_1.Address.createFromRawAddress('SDBDG4IT43MPCW2W4CBBCSJJT42AYALQN7A4VVWL');
            const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Hash_160, 'non valid hash', recipientAddress, NetworkType_1.NetworkType.MIJIN_TEST);
        }).to.throw(Error);
    });
    it('should be created with HashType: Op_Hash_256 secret', () => {
        const proof = 'B778A39A3663719DFC5E48C9D78431B1E45C2AF9DF538782BF199C189DABEAC7';
        const recipientAddress = Address_1.Address.createFromRawAddress('SDBDG4IT43MPCW2W4CBBCSJJT42AYALQN7A4VVWL');
        const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Hash_256, CryptoJS.SHA256(CryptoJS.SHA256(proof).toString(CryptoJS.enc.Hex)).toString(CryptoJS.enc.Hex), recipientAddress, NetworkType_1.NetworkType.MIJIN_TEST);
        assert_1.deepEqual(secretLockTransaction.mosaic.id.id, NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.NAMESPACE_ID.id);
        chai_1.expect(secretLockTransaction.mosaic.amount.equals(UInt64_1.UInt64.fromUint(10))).to.be.equal(true);
        chai_1.expect(secretLockTransaction.duration.equals(UInt64_1.UInt64.fromUint(100))).to.be.equal(true);
        chai_1.expect(secretLockTransaction.hashType).to.be.equal(3);
        chai_1.expect(secretLockTransaction.secret).to.be.equal('c346f5ecf5bcfa54ab14fad815c8239bdeb051df8835d212dba2af59f688a00e');
        chai_1.expect(secretLockTransaction.recipientAddress).to.be.equal(recipientAddress);
    });
    it('should throw exception when the input is not related to HashTyp: Op_Hash_256', () => {
        chai_1.expect(() => {
            const recipientAddress = Address_1.Address.createFromRawAddress('SDBDG4IT43MPCW2W4CBBCSJJT42AYALQN7A4VVWL');
            const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Hash_256, 'non valid hash', recipientAddress, NetworkType_1.NetworkType.MIJIN_TEST);
        }).to.throw(Error);
    });
    describe('size', () => {
        it('should return 210 for SecretLockTransaction with proof of 32 bytes', () => {
            const proof = 'B778A39A3663719DFC5E48C9D78431B1E45C2AF9DF538782BF199C189DABEAC7';
            const recipientAddress = Address_1.Address.createFromRawAddress('SDBDG4IT43MPCW2W4CBBCSJJT42AYALQN7A4VVWL');
            const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Hash_256, CryptoJS.SHA256(CryptoJS.SHA256(proof).toString(CryptoJS.enc.Hex)).toString(CryptoJS.enc.Hex), recipientAddress, NetworkType_1.NetworkType.MIJIN_TEST);
            chai_1.expect(secretLockTransaction.size).to.be.equal(210);
            chai_1.expect(format_1.Convert.hexToUint8(secretLockTransaction.serialize()).length).to.be.equal(secretLockTransaction.size);
        });
    });
    it('should be created with alias address', () => {
        const proof = 'B778A39A3663719DFC5E48C9D78431B1E45C2AF9DF538782BF199C189DABEAC7';
        const recipientAddress = new NamespaceId_1.NamespaceId('test');
        const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Sha3_256, js_sha3_1.sha3_256.create().update(format_1.Convert.hexToUint8(proof)).hex(), recipientAddress, NetworkType_1.NetworkType.MIJIN_TEST);
        assert_1.deepEqual(secretLockTransaction.mosaic.id.id, NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.NAMESPACE_ID.id);
        chai_1.expect(secretLockTransaction.mosaic.amount.equals(UInt64_1.UInt64.fromUint(10))).to.be.equal(true);
        chai_1.expect(secretLockTransaction.duration.equals(UInt64_1.UInt64.fromUint(100))).to.be.equal(true);
        chai_1.expect(secretLockTransaction.hashType).to.be.equal(0);
        chai_1.expect(secretLockTransaction.secret).to.be.equal('9b3155b37159da50aa52d5967c509b410f5a36a3b1e31ecb5ac76675d79b4a5e');
        chai_1.expect(secretLockTransaction.recipientAddress).to.be.equal(recipientAddress);
    });
    it('Test set maxFee using multiplier', () => {
        const proof = 'B778A39A3663719DFC5E48C9D78431B1E45C2AF9DF538782BF199C189DABEAC7';
        const recipientAddress = Address_1.Address.createFromRawAddress('SDBDG4IT43MPCW2W4CBBCSJJT42AYALQN7A4VVWL');
        const secretLockTransaction = SecretLockTransaction_1.SecretLockTransaction.create(Deadline_1.Deadline.create(), NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createAbsolute(10), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Sha3_256, js_sha3_1.sha3_256.create().update(format_1.Convert.hexToUint8(proof)).hex(), recipientAddress, NetworkType_1.NetworkType.MIJIN_TEST).setMaxFee(2);
        chai_1.expect(secretLockTransaction.maxFee.compact()).to.be.equal(420);
    });
    it('Test resolveAlias can resolve', () => {
        const proof = 'B778A39A3663719DFC5E48C9D78431B1E45C2AF9DF538782BF199C189DABEAC7';
        const secretLockTransaction = new SecretLockTransaction_1.SecretLockTransaction(NetworkType_1.NetworkType.MIJIN_TEST, 1, Deadline_1.Deadline.createFromDTO('1'), UInt64_1.UInt64.fromUint(0), new Mosaic_1.Mosaic(unresolvedMosaicId, UInt64_1.UInt64.fromUint(1)), UInt64_1.UInt64.fromUint(100), HashType_1.HashType.Op_Sha3_256, js_sha3_1.sha3_256.create().update(format_1.Convert.hexToUint8(proof)).hex(), unresolvedAddress, '', account.publicAccount, new TransactionInfo_1.TransactionInfo(UInt64_1.UInt64.fromUint(2), 0, '')).resolveAliases(statement);
        chai_1.expect(secretLockTransaction.recipientAddress instanceof Address_1.Address).to.be.true;
        chai_1.expect(secretLockTransaction.mosaic.id instanceof MosaicId_1.MosaicId).to.be.true;
        chai_1.expect(secretLockTransaction.recipientAddress.equals(account.address)).to.be.true;
        chai_1.expect(secretLockTransaction.mosaic.id.equals(mosaicId)).to.be.true;
    });
});
//# sourceMappingURL=SecretLockTransaction.spec.js.map