"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const js_joda_1 = require("js-joda");
const format_1 = require("../../../src/core/format");
const TransactionMapping_1 = require("../../../src/core/utils/TransactionMapping");
const CreateTransactionFromDTO_1 = require("../../../src/infrastructure/transaction/CreateTransactionFromDTO");
const Address_1 = require("../../../src/model/account/Address");
const PublicAccount_1 = require("../../../src/model/account/PublicAccount");
const NetworkType_1 = require("../../../src/model/blockchain/NetworkType");
const PlainMessage_1 = require("../../../src/model/message/PlainMessage");
const Mosaic_1 = require("../../../src/model/mosaic/Mosaic");
const MosaicFlags_1 = require("../../../src/model/mosaic/MosaicFlags");
const MosaicId_1 = require("../../../src/model/mosaic/MosaicId");
const MosaicNonce_1 = require("../../../src/model/mosaic/MosaicNonce");
const MosaicSupplyChangeAction_1 = require("../../../src/model/mosaic/MosaicSupplyChangeAction");
const NetworkCurrencyMosaic_1 = require("../../../src/model/mosaic/NetworkCurrencyMosaic");
const NamespaceId_1 = require("../../../src/model/namespace/NamespaceId");
const ReceiptSource_1 = require("../../../src/model/receipt/ReceiptSource");
const ResolutionEntry_1 = require("../../../src/model/receipt/ResolutionEntry");
const ResolutionStatement_1 = require("../../../src/model/receipt/ResolutionStatement");
const ResolutionType_1 = require("../../../src/model/receipt/ResolutionType");
const Statement_1 = require("../../../src/model/receipt/Statement");
const AggregateTransaction_1 = require("../../../src/model/transaction/AggregateTransaction");
const AggregateTransactionCosignature_1 = require("../../../src/model/transaction/AggregateTransactionCosignature");
const CosignatureSignedTransaction_1 = require("../../../src/model/transaction/CosignatureSignedTransaction");
const CosignatureTransaction_1 = require("../../../src/model/transaction/CosignatureTransaction");
const Deadline_1 = require("../../../src/model/transaction/Deadline");
const MosaicDefinitionTransaction_1 = require("../../../src/model/transaction/MosaicDefinitionTransaction");
const MosaicSupplyChangeTransaction_1 = require("../../../src/model/transaction/MosaicSupplyChangeTransaction");
const MultisigAccountModificationTransaction_1 = require("../../../src/model/transaction/MultisigAccountModificationTransaction");
const NamespaceRegistrationTransaction_1 = require("../../../src/model/transaction/NamespaceRegistrationTransaction");
const TransactionInfo_1 = require("../../../src/model/transaction/TransactionInfo");
const TransactionType_1 = require("../../../src/model/transaction/TransactionType");
const TransferTransaction_1 = require("../../../src/model/transaction/TransferTransaction");
const UInt64_1 = require("../../../src/model/UInt64");
const conf_spec_1 = require("../../conf/conf.spec");
describe('AggregateTransaction', () => {
    let account;
    const generationHash = '57F7DA205008026C776CB6AED843393F04CD458E0AA2D9F1D5F31A402072B2D6';
    let statement;
    const unresolvedAddress = new NamespaceId_1.NamespaceId('address');
    const unresolvedMosaicId = new NamespaceId_1.NamespaceId('mosaic');
    const resolvedMosaicId = new MosaicId_1.MosaicId('0DC67FBE1CAD29E5');
    before(() => {
        account = conf_spec_1.TestingAccount;
    });
    before(() => {
        account = conf_spec_1.TestingAccount;
        statement = new Statement_1.Statement([], [new ResolutionStatement_1.ResolutionStatement(ResolutionType_1.ResolutionType.Address, UInt64_1.UInt64.fromUint(2), unresolvedAddress, [new ResolutionEntry_1.ResolutionEntry(account.address, new ReceiptSource_1.ReceiptSource(1, 1))])], [new ResolutionStatement_1.ResolutionStatement(ResolutionType_1.ResolutionType.Mosaic, UInt64_1.UInt64.fromUint(2), unresolvedMosaicId, [new ResolutionEntry_1.ResolutionEntry(resolvedMosaicId, new ReceiptSource_1.ReceiptSource(1, 1))])]);
    });
    it('should default maxFee field be set to 0', () => {
        const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(1, js_joda_1.ChronoUnit.HOURS), Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC'), [], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
        const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [transferTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
        chai_1.expect(aggregateTransaction.maxFee.higher).to.be.equal(0);
        chai_1.expect(aggregateTransaction.maxFee.lower).to.be.equal(0);
    });
    it('should filled maxFee override transaction maxFee', () => {
        const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(1, js_joda_1.ChronoUnit.HOURS), Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC'), [], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
        const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [transferTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, [], new UInt64_1.UInt64([1, 0]));
        chai_1.expect(aggregateTransaction.maxFee.higher).to.be.equal(0);
        chai_1.expect(aggregateTransaction.maxFee.lower).to.be.equal(1);
    });
    it('should createComplete an AggregateTransaction object with TransferTransaction', () => {
        const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(1, js_joda_1.ChronoUnit.HOURS), Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC'), [], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
        const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [transferTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
        const signedTransaction = aggregateTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(0, 8)).to.be.equal('08010000');
        chai_1.expect(signedTransaction.payload.substring(424, signedTransaction.payload.length)).to.be.equal('019054419050B9837EFAB4BBE8A4B9BB32D812F9885C00D8FC1650E142000D000000000000746573742D6D657373616765000000');
    });
    it('should createComplete an AggregateTransaction object with NamespaceRegistrationTransaction', () => {
        const registerNamespaceTransaction = NamespaceRegistrationTransaction_1.NamespaceRegistrationTransaction.createRootNamespace(Deadline_1.Deadline.create(), 'root-test-namespace', UInt64_1.UInt64.fromUint(1000), NetworkType_1.NetworkType.MIJIN_TEST);
        const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [registerNamespaceTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
        const signedTransaction = aggregateTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(0, 8)).to.be.equal('00010000');
        chai_1.expect(signedTransaction.payload.substring(320, 352)).to.be.equal('58000000000000005500000000000000');
        chai_1.expect(signedTransaction.payload.substring(424, signedTransaction.payload.length)).to.be.equal('01904E41E803000000000000CFCBE72D994BE69B0013726F6F742D746573742D6E616D657370616365000000');
    });
    it('should createComplete an AggregateTransaction object with MosaicDefinitionTransaction', () => {
        const mosaicDefinitionTransaction = MosaicDefinitionTransaction_1.MosaicDefinitionTransaction.create(Deadline_1.Deadline.create(), new MosaicNonce_1.MosaicNonce(new Uint8Array([0xE6, 0xDE, 0x84, 0xB8])), // nonce
        new MosaicId_1.MosaicId(UInt64_1.UInt64.fromUint(1).toDTO()), // ID
        MosaicFlags_1.MosaicFlags.create(true, true, true), 3, UInt64_1.UInt64.fromUint(1000), NetworkType_1.NetworkType.MIJIN_TEST);
        const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [mosaicDefinitionTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
        const signedTransaction = aggregateTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(0, 8)).to.be.equal('F0000000');
        chai_1.expect(signedTransaction.payload.substring(320, 352)).to.be.equal('48000000000000004600000000000000');
        chai_1.expect(signedTransaction.payload.substring(424, signedTransaction.payload.length)).to.be.equal('01904D410100000000000000E803000000000000E6DE84B807030000');
    });
    it('should createComplete an AggregateTransaction object with MosaicSupplyChangeTransaction', () => {
        const mosaicId = new MosaicId_1.MosaicId([2262289484, 3405110546]);
        const mosaicSupplyChangeTransaction = MosaicSupplyChangeTransaction_1.MosaicSupplyChangeTransaction.create(Deadline_1.Deadline.create(), mosaicId, MosaicSupplyChangeAction_1.MosaicSupplyChangeAction.Increase, UInt64_1.UInt64.fromUint(10), NetworkType_1.NetworkType.MIJIN_TEST);
        const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [mosaicSupplyChangeTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
        const signedTransaction = aggregateTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(0, 8)).to.be.equal('F0000000');
        chai_1.expect(signedTransaction.payload.substring(320, 352)).to.be.equal('48000000000000004100000000000000');
        chai_1.expect(signedTransaction.payload.substring(424, signedTransaction.payload.length)).to.be.equal('01904D424CCCD78612DDF5CA0A000000000000000100000000000000');
    });
    it('should createComplete an AggregateTransaction object with MultisigAccountModificationTransaction', () => {
        const modifyMultisigAccountTransaction = MultisigAccountModificationTransaction_1.MultisigAccountModificationTransaction.create(Deadline_1.Deadline.create(), 2, 1, [PublicAccount_1.PublicAccount.createFromPublicKey('B0F93CBEE49EEB9953C6F3985B15A4F238E205584D8F924C621CBE4D7AC6EC24', NetworkType_1.NetworkType.MIJIN_TEST),
            PublicAccount_1.PublicAccount.createFromPublicKey('B1B5581FC81A6970DEE418D2C2978F2724228B7B36C5C6DF71B0162BB04778B4', NetworkType_1.NetworkType.MIJIN_TEST),
        ], [], NetworkType_1.NetworkType.MIJIN_TEST);
        const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [modifyMultisigAccountTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
        const signedTransaction = aggregateTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(0, 8)).to.be.equal('20010000');
        chai_1.expect(signedTransaction.payload.substring(320, 352)).to.be.equal('78000000000000007800000000000000');
        chai_1.expect(signedTransaction.payload.substring(424, signedTransaction.payload.length)).to.be.equal('019055410102020000000000B0F93CBEE49EEB9953C6F3985B15A4F238E205584D8F924C621CBE4D7AC6' +
            'EC24B1B5581FC81A6970DEE418D2C2978F2724228B7B36C5C6DF71B0162BB04778B4');
    });
    it('should createComplete an AggregateTransaction object with different cosignatories', () => {
        const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC'), [], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
        const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [transferTransaction.toAggregate(conf_spec_1.MultisigAccount.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
        const signedTransaction = conf_spec_1.CosignatoryAccount.signTransactionWithCosignatories(aggregateTransaction, [conf_spec_1.Cosignatory2Account], generationHash);
        chai_1.expect(signedTransaction.payload.substring(0, 8)).to.be.equal('68010000');
        chai_1.expect(signedTransaction.payload.substring(320, 352)).to.be.equal('60000000000000005D00000000000000');
        chai_1.expect(signedTransaction.payload.substring(424, 424 + 162)).to.be.equal('019054419050B9837EFAB4BBE8A4B9BB32D812F9885C00D8FC1650E142000D0000000' +
            '00000746573742D6D65737361676500000068B3FBB18729C1FDE225C57F8CE080FA828F0067E451A3FD81FA628842');
    });
    it('should createBonded an AggregateTransaction object with TransferTransaction', () => {
        const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC'), [], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
        const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createBonded(Deadline_1.Deadline.create(2, js_joda_1.ChronoUnit.MINUTES), [transferTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
        const signedTransaction = aggregateTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(0, 8)).to.be.equal('08010000');
        chai_1.expect(signedTransaction.payload.substring(320, 352)).to.be.equal('60000000000000005D00000000000000');
        chai_1.expect(signedTransaction.payload.substring(220, 224)).to.be.equal('4142');
        chai_1.expect(signedTransaction.payload.substring(424, signedTransaction.payload.length)).to.be.equal('019054419050B9837EFAB4BBE8A4B9BB32D812F9885C00D8FC1650E142000D000000000000746573742D6D657373616765000000');
    });
    it('should validate if accounts have signed an aggregate transaction', () => {
        const aggregateTransactionDTO = {
            meta: {
                hash: '671653C94E2254F2A23EFEDB15D67C38332AED1FBD24B063C0A8E675582B6A96',
                height: '18160',
                id: '5A0069D83F17CF0001777E55',
                index: 0,
                merkleComponentHash: '81E5E7AE49998802DABC816EC10158D3A7879702FF29084C2C992CD1289877A7',
            },
            transaction: {
                cosignatures: [
                    {
                        signature: '5780C8DF9D46BA2BCF029DCC5D3BF55FE1CB5BE7ABCF30387C4637DD' +
                            'EDFC2152703CA0AD95F21BB9B942F3CC52FCFC2064C7B84CF60D1A9E69195F1943156C07',
                        signerPublicKey: 'A5F82EC8EBB341427B6785C8111906CD0DF18838FB11B51CE0E18B5E79DFF630',
                    },
                ],
                deadline: '1000',
                maxFee: '0',
                signature: '939673209A13FF82397578D22CC96EB8516A6760C894D9B7535E3A1E0680' +
                    '07B9255CFA9A914C97142A7AE18533E381C846B69D2AE0D60D1DC8A55AD120E2B606',
                signerPublicKey: '7681ED5023141D9CDCF184E5A7B60B7D466739918ED5DA30F7E71EA7B86EFF2D',
                transactions: [
                    {
                        meta: {
                            aggregateHash: '3D28C804EDD07D5A728E5C5FFEC01AB07AFA5766AE6997B38526D36015A4D006',
                            aggregateId: '5A0069D83F17CF0001777E55',
                            height: '18160',
                            id: '5A0069D83F17CF0001777E56',
                            index: 0,
                        },
                        transaction: {
                            minApprovalDelta: 1,
                            minRemovalDelta: 1,
                            modifications: [
                                {
                                    cosignatoryPublicKey: '589B73FBC22063E9AE6FBAC67CB9C6EA865EF556E5' +
                                        'FB8B7310D45F77C1250B97',
                                    modificationAction: 0,
                                },
                            ],
                            signerPublicKey: 'B4F12E7C9F6946091E2CB8B6D3A12B50D17CCBBF646386EA27CE2946A7423DCF',
                            type: 16725,
                            version: 1,
                            network: 144,
                        },
                    },
                ],
                type: 16705,
                version: 1,
                network: 144,
            },
        };
        const aggregateTransaction = CreateTransactionFromDTO_1.CreateTransactionFromDTO(aggregateTransactionDTO);
        chai_1.expect(aggregateTransaction.signedByAccount(PublicAccount_1.PublicAccount.createFromPublicKey('A5F82EC8EBB341427B6785C8111906CD0DF18838FB11B51CE0E18B5E79DFF630', NetworkType_1.NetworkType.MIJIN_TEST))).to.be.equal(true);
        chai_1.expect(aggregateTransaction.signedByAccount(PublicAccount_1.PublicAccount.createFromPublicKey('7681ED5023141D9CDCF184E5A7B60B7D466739918ED5DA30F7E71EA7B86EFF2D', NetworkType_1.NetworkType.MIJIN_TEST))).to.be.equal(true);
        chai_1.expect(aggregateTransaction.signedByAccount(PublicAccount_1.PublicAccount.createFromPublicKey('B4F12E7C9F6946091E2CB8B6D3A12B50D17CCBBF646386EA27CE2946A7423DCF', NetworkType_1.NetworkType.MIJIN_TEST))).to.be.equal(false);
        chai_1.expect(aggregateTransaction.innerTransactions[0].signer.publicKey)
            .to.be.equal('B4F12E7C9F6946091E2CB8B6D3A12B50D17CCBBF646386EA27CE2946A7423DCF');
        chai_1.expect(format_1.Convert.hexToUint8(aggregateTransaction.serialize()).length).to.be.equal(aggregateTransaction.size);
    });
    it('should have type 0x4141 when it\'s complete', () => {
        const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [], NetworkType_1.NetworkType.MIJIN_TEST, []);
        chai_1.expect(aggregateTransaction.type).to.be.equal(0x4141);
    });
    it('should have type 0x4241 when it\'s bonded', () => {
        const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createBonded(Deadline_1.Deadline.create(), [], NetworkType_1.NetworkType.MIJIN_TEST);
        chai_1.expect(aggregateTransaction.type).to.be.equal(0x4241);
    });
    it('should throw exception when adding an aggregated transaction as inner transaction', () => {
        const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(1, js_joda_1.ChronoUnit.HOURS), Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC'), [], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
        const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [transferTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
        chai_1.expect(() => {
            AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [aggregateTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
        }).to.throw(Error, 'Inner transaction cannot be an aggregated transaction.');
    });
    it('Should create signed transaction with cosignatories - Aggregated Complete', () => {
        /**
         * @see https://github.com/nemtech/nem2-sdk-typescript-javascript/issues/112
         */
        const accountAlice = conf_spec_1.TestingAccount;
        const accountBob = conf_spec_1.CosignatoryAccount;
        const accountCarol = conf_spec_1.Cosignatory2Account;
        const AtoBTx = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), accountBob.address, [], PlainMessage_1.PlainMessage.create('a to b'), NetworkType_1.NetworkType.MIJIN_TEST);
        const BtoATx = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), accountAlice.address, [], PlainMessage_1.PlainMessage.create('b to a'), NetworkType_1.NetworkType.MIJIN_TEST);
        const CtoATx = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), accountAlice.address, [], PlainMessage_1.PlainMessage.create('c to a'), NetworkType_1.NetworkType.MIJIN_TEST);
        // 01. Alice creates the aggregated tx and sign it, Then payload send to Bob & Carol
        const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [
            AtoBTx.toAggregate(accountAlice.publicAccount),
            BtoATx.toAggregate(accountBob.publicAccount),
            CtoATx.toAggregate(accountCarol.publicAccount)
        ], NetworkType_1.NetworkType.MIJIN_TEST, []);
        const aliceSignedTransaction = aggregateTransaction.signWith(accountAlice, generationHash);
        // 02.1 Bob cosigns the tx and sends it back to Alice
        const signedTxBob = CosignatureTransaction_1.CosignatureTransaction.signTransactionPayload(accountBob, aliceSignedTransaction.payload, generationHash);
        // 02.2 Carol cosigns the tx and sends it back to Alice
        const signedTxCarol = CosignatureTransaction_1.CosignatureTransaction.signTransactionPayload(accountCarol, aliceSignedTransaction.payload, generationHash);
        // 03. Alice collects the cosignatures, recreate, sign, and announces the transaction
        // First Alice need to append cosignatories to current transaction.
        const cosignatureSignedTransactions = [
            new CosignatureSignedTransaction_1.CosignatureSignedTransaction(signedTxBob.parentHash, signedTxBob.signature, signedTxBob.signerPublicKey),
            new CosignatureSignedTransaction_1.CosignatureSignedTransaction(signedTxCarol.parentHash, signedTxCarol.signature, signedTxCarol.signerPublicKey),
        ];
        const recreatedTx = TransactionMapping_1.TransactionMapping.createFromPayload(aliceSignedTransaction.payload);
        const signedTransaction = recreatedTx.signTransactionGivenSignatures(accountAlice, cosignatureSignedTransactions, generationHash);
        chai_1.expect(signedTransaction.type).to.be.equal(TransactionType_1.TransactionType.AGGREGATE_COMPLETE);
        chai_1.expect(signedTransaction.signerPublicKey).to.be.equal(accountAlice.publicKey);
        chai_1.expect(signedTransaction.payload.indexOf(accountBob.publicKey) > -1).to.be.true;
        chai_1.expect(signedTransaction.payload.indexOf(accountCarol.publicKey) > -1).to.be.true;
        // To make sure that the new cosign method returns the same payload & hash as standard cosigning
        const standardCosignedTransaction = aggregateTransaction
            .signTransactionWithCosignatories(accountAlice, [accountBob, accountCarol], generationHash);
        chai_1.expect(standardCosignedTransaction.payload).to.be.equal(signedTransaction.payload);
        chai_1.expect(standardCosignedTransaction.hash).to.be.equal(signedTransaction.hash);
    });
    it('Should be able to add innertransactions to current aggregate tx', () => {
        const transferTx1 = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), account.address, [], PlainMessage_1.PlainMessage.create('a to b'), NetworkType_1.NetworkType.MIJIN_TEST);
        const transferTx2 = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), account.address, [], PlainMessage_1.PlainMessage.create('b to a'), NetworkType_1.NetworkType.MIJIN_TEST);
        let aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [transferTx1.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
        chai_1.expect(aggregateTransaction.type).to.be.equal(TransactionType_1.TransactionType.AGGREGATE_COMPLETE);
        chai_1.expect(aggregateTransaction.innerTransactions.length).to.be.equal(1);
        aggregateTransaction = aggregateTransaction.addTransactions([transferTx2.toAggregate(account.publicAccount)]);
        chai_1.expect(aggregateTransaction.type).to.be.equal(TransactionType_1.TransactionType.AGGREGATE_COMPLETE);
        chai_1.expect(aggregateTransaction.innerTransactions.length).to.be.equal(2);
    });
    it('Should be able to add cosignatures to current aggregate tx', () => {
        const transferTx1 = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), account.address, [], PlainMessage_1.PlainMessage.create('a to b'), NetworkType_1.NetworkType.MIJIN_TEST);
        let aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [transferTx1.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
        chai_1.expect(aggregateTransaction.type).to.be.equal(TransactionType_1.TransactionType.AGGREGATE_COMPLETE);
        chai_1.expect(aggregateTransaction.cosignatures.length).to.be.equal(0);
        // add cosignature after creation
        const signedTransaction = aggregateTransaction.signWith(account, generationHash);
        const cosignature = new AggregateTransactionCosignature_1.AggregateTransactionCosignature(signedTransaction.payload, PublicAccount_1.PublicAccount.createFromPublicKey(signedTransaction.signerPublicKey, NetworkType_1.NetworkType.MIJIN_TEST));
        aggregateTransaction = aggregateTransaction.addCosignatures([cosignature]);
        chai_1.expect(aggregateTransaction.type).to.be.equal(TransactionType_1.TransactionType.AGGREGATE_COMPLETE);
        chai_1.expect(aggregateTransaction.cosignatures.length).to.be.equal(1);
    });
    describe('size', () => {
        it('should return 268 for AggregateTransaction byte size with TransferTransaction with 1 mosaic and message NEM', () => {
            const transaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), Address_1.Address.createFromRawAddress('SBILTA367K2LX2FEXG5TFWAS7GEFYAGY7QLFBYKC'), [
                NetworkCurrencyMosaic_1.NetworkCurrencyMosaic.createRelative(100),
            ], PlainMessage_1.PlainMessage.create('NEM'), NetworkType_1.NetworkType.MIJIN_TEST);
            const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createBonded(Deadline_1.Deadline.create(), [transaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []);
            chai_1.expect(format_1.Convert.hexToUint8(aggregateTransaction.serialize()).length).to.be.equal(aggregateTransaction.size);
            chai_1.expect(aggregateTransaction.size).to.be.equal(272);
        });
    });
    it('Test set maxFee using multiplier', () => {
        const transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(1, js_joda_1.ChronoUnit.HOURS), unresolvedAddress, [new Mosaic_1.Mosaic(unresolvedMosaicId, UInt64_1.UInt64.fromUint(1))], PlainMessage_1.PlainMessage.create('test-message'), NetworkType_1.NetworkType.MIJIN_TEST);
        const aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createComplete(Deadline_1.Deadline.create(), [transferTransaction.toAggregate(account.publicAccount)], NetworkType_1.NetworkType.MIJIN_TEST, []).setMaxFee(2);
        chai_1.expect(aggregateTransaction.maxFee.compact()).to.be.equal(560);
    });
    it('Test resolveAlias can resolve', () => {
        const transferTransaction = new TransferTransaction_1.TransferTransaction(NetworkType_1.NetworkType.MIJIN_TEST, 1, Deadline_1.Deadline.createFromDTO('1'), UInt64_1.UInt64.fromUint(0), unresolvedAddress, [new Mosaic_1.Mosaic(unresolvedMosaicId, UInt64_1.UInt64.fromUint(1))], PlainMessage_1.PlainMessage.create('test'), '', account.publicAccount, new TransactionInfo_1.TransactionInfo(UInt64_1.UInt64.fromUint(2), 0, ''));
        const aggregateTransaction = new AggregateTransaction_1.AggregateTransaction(NetworkType_1.NetworkType.MIJIN_TEST, TransactionType_1.TransactionType.AGGREGATE_COMPLETE, 1, Deadline_1.Deadline.createFromDTO('1'), UInt64_1.UInt64.fromUint(100), [transferTransaction.toAggregate(account.publicAccount)], [], '', account.publicAccount, new TransactionInfo_1.TransactionInfo(UInt64_1.UInt64.fromUint(2), 0, '')).resolveAliases(statement);
        const innerTransaction = aggregateTransaction.innerTransactions[0];
        chai_1.expect(innerTransaction.recipientAddress instanceof Address_1.Address).to.be.true;
        chai_1.expect(innerTransaction.mosaics[0].id instanceof MosaicId_1.MosaicId).to.be.true;
        chai_1.expect(innerTransaction.recipientAddress.equals(account.address)).to.be.true;
        chai_1.expect(innerTransaction.mosaics[0].id.equals(resolvedMosaicId)).to.be.true;
    });
});
//# sourceMappingURL=AggregateTransaction.spec.js.map