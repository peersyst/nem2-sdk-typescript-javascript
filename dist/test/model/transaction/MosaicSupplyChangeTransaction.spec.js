"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const format_1 = require("../../../src/core/format");
const NetworkType_1 = require("../../../src/model/blockchain/NetworkType");
const MosaicId_1 = require("../../../src/model/mosaic/MosaicId");
const MosaicSupplyChangeAction_1 = require("../../../src/model/mosaic/MosaicSupplyChangeAction");
const NamespaceId_1 = require("../../../src/model/namespace/NamespaceId");
const ReceiptSource_1 = require("../../../src/model/receipt/ReceiptSource");
const ResolutionEntry_1 = require("../../../src/model/receipt/ResolutionEntry");
const ResolutionStatement_1 = require("../../../src/model/receipt/ResolutionStatement");
const ResolutionType_1 = require("../../../src/model/receipt/ResolutionType");
const Statement_1 = require("../../../src/model/receipt/Statement");
const Deadline_1 = require("../../../src/model/transaction/Deadline");
const MosaicSupplyChangeTransaction_1 = require("../../../src/model/transaction/MosaicSupplyChangeTransaction");
const TransactionInfo_1 = require("../../../src/model/transaction/TransactionInfo");
const UInt64_1 = require("../../../src/model/UInt64");
const conf_spec_1 = require("../../conf/conf.spec");
describe('MosaicSupplyChangeTransaction', () => {
    let account;
    const generationHash = '57F7DA205008026C776CB6AED843393F04CD458E0AA2D9F1D5F31A402072B2D6';
    let statement;
    const unresolvedMosaicId = new NamespaceId_1.NamespaceId('mosaic');
    const resolvedMosaicId = new MosaicId_1.MosaicId('0DC67FBE1CAD29E5');
    before(() => {
        account = conf_spec_1.TestingAccount;
        statement = new Statement_1.Statement([], [], [new ResolutionStatement_1.ResolutionStatement(ResolutionType_1.ResolutionType.Mosaic, UInt64_1.UInt64.fromUint(2), unresolvedMosaicId, [new ResolutionEntry_1.ResolutionEntry(resolvedMosaicId, new ReceiptSource_1.ReceiptSource(1, 0))])]);
    });
    it('should default maxFee field be set to 0', () => {
        const mosaicId = new MosaicId_1.MosaicId([2262289484, 3405110546]);
        const mosaicSupplyChangeTransaction = MosaicSupplyChangeTransaction_1.MosaicSupplyChangeTransaction.create(Deadline_1.Deadline.create(), mosaicId, MosaicSupplyChangeAction_1.MosaicSupplyChangeAction.Increase, UInt64_1.UInt64.fromUint(10), NetworkType_1.NetworkType.MIJIN_TEST);
        chai_1.expect(mosaicSupplyChangeTransaction.maxFee.higher).to.be.equal(0);
        chai_1.expect(mosaicSupplyChangeTransaction.maxFee.lower).to.be.equal(0);
    });
    it('should filled maxFee override transaction maxFee', () => {
        const mosaicId = new MosaicId_1.MosaicId([2262289484, 3405110546]);
        const mosaicSupplyChangeTransaction = MosaicSupplyChangeTransaction_1.MosaicSupplyChangeTransaction.create(Deadline_1.Deadline.create(), mosaicId, MosaicSupplyChangeAction_1.MosaicSupplyChangeAction.Increase, UInt64_1.UInt64.fromUint(10), NetworkType_1.NetworkType.MIJIN_TEST, new UInt64_1.UInt64([1, 0]));
        chai_1.expect(mosaicSupplyChangeTransaction.maxFee.higher).to.be.equal(0);
        chai_1.expect(mosaicSupplyChangeTransaction.maxFee.lower).to.be.equal(1);
    });
    it('should createComplete an MosaicSupplyChangeTransaction object and sign it', () => {
        const mosaicId = new MosaicId_1.MosaicId([2262289484, 3405110546]);
        const mosaicSupplyChangeTransaction = MosaicSupplyChangeTransaction_1.MosaicSupplyChangeTransaction.create(Deadline_1.Deadline.create(), mosaicId, MosaicSupplyChangeAction_1.MosaicSupplyChangeAction.Increase, UInt64_1.UInt64.fromUint(10), NetworkType_1.NetworkType.MIJIN_TEST);
        chai_1.expect(mosaicSupplyChangeTransaction.action).to.be.equal(MosaicSupplyChangeAction_1.MosaicSupplyChangeAction.Increase);
        chai_1.expect(mosaicSupplyChangeTransaction.delta.lower).to.be.equal(10);
        chai_1.expect(mosaicSupplyChangeTransaction.delta.higher).to.be.equal(0);
        chai_1.expect(mosaicSupplyChangeTransaction.mosaicId.id.lower).to.be.equal(2262289484);
        chai_1.expect(mosaicSupplyChangeTransaction.mosaicId.id.higher).to.be.equal(3405110546);
        const signedTransaction = mosaicSupplyChangeTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(256, signedTransaction.payload.length)).to.be.equal('4CCCD78612DDF5CA0A0000000000000001');
    });
    describe('size', () => {
        it('should return 145 for MosaicSupplyChange transaction byte size', () => {
            const mosaicId = new MosaicId_1.MosaicId([2262289484, 3405110546]);
            const mosaicSupplyChangeTransaction = MosaicSupplyChangeTransaction_1.MosaicSupplyChangeTransaction.create(Deadline_1.Deadline.create(), mosaicId, MosaicSupplyChangeAction_1.MosaicSupplyChangeAction.Increase, UInt64_1.UInt64.fromUint(10), NetworkType_1.NetworkType.MIJIN_TEST);
            chai_1.expect(mosaicSupplyChangeTransaction.size).to.be.equal(145);
            chai_1.expect(format_1.Convert.hexToUint8(mosaicSupplyChangeTransaction.serialize()).length).to.be.equal(mosaicSupplyChangeTransaction.size);
        });
    });
    it('Test set maxFee using multiplier', () => {
        const mosaicId = new MosaicId_1.MosaicId([2262289484, 3405110546]);
        const mosaicSupplyChangeTransaction = MosaicSupplyChangeTransaction_1.MosaicSupplyChangeTransaction.create(Deadline_1.Deadline.create(), mosaicId, MosaicSupplyChangeAction_1.MosaicSupplyChangeAction.Increase, UInt64_1.UInt64.fromUint(10), NetworkType_1.NetworkType.MIJIN_TEST).setMaxFee(2);
        chai_1.expect(mosaicSupplyChangeTransaction.maxFee.compact()).to.be.equal(290);
    });
    it('Test resolveAlias can resolve', () => {
        const mosaicSupplyChangeTransaction = new MosaicSupplyChangeTransaction_1.MosaicSupplyChangeTransaction(NetworkType_1.NetworkType.MIJIN_TEST, 1, Deadline_1.Deadline.createFromDTO('1'), UInt64_1.UInt64.fromUint(0), unresolvedMosaicId, MosaicSupplyChangeAction_1.MosaicSupplyChangeAction.Increase, UInt64_1.UInt64.fromUint(10), '', account.publicAccount, new TransactionInfo_1.TransactionInfo(UInt64_1.UInt64.fromUint(2), 0, '')).resolveAliases(statement);
        chai_1.expect(mosaicSupplyChangeTransaction.mosaicId instanceof MosaicId_1.MosaicId).to.be.true;
        chai_1.expect(mosaicSupplyChangeTransaction.mosaicId.equals(resolvedMosaicId)).to.be.true;
    });
});
//# sourceMappingURL=MosaicSupplyChangeTransaction.spec.js.map