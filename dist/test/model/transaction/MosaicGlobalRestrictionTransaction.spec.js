"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const format_1 = require("../../../src/core/format");
const NetworkType_1 = require("../../../src/model/blockchain/NetworkType");
const MosaicId_1 = require("../../../src/model/mosaic/MosaicId");
const NamespaceId_1 = require("../../../src/model/namespace/NamespaceId");
const ReceiptSource_1 = require("../../../src/model/receipt/ReceiptSource");
const ResolutionEntry_1 = require("../../../src/model/receipt/ResolutionEntry");
const ResolutionStatement_1 = require("../../../src/model/receipt/ResolutionStatement");
const ResolutionType_1 = require("../../../src/model/receipt/ResolutionType");
const Statement_1 = require("../../../src/model/receipt/Statement");
const MosaicRestrictionType_1 = require("../../../src/model/restriction/MosaicRestrictionType");
const Deadline_1 = require("../../../src/model/transaction/Deadline");
const MosaicGlobalRestrictionTransaction_1 = require("../../../src/model/transaction/MosaicGlobalRestrictionTransaction");
const TransactionInfo_1 = require("../../../src/model/transaction/TransactionInfo");
const UInt64_1 = require("../../../src/model/UInt64");
const conf_spec_1 = require("../../conf/conf.spec");
describe('MosaicGlobalRestrictionTransaction', () => {
    let account;
    const generationHash = '57F7DA205008026C776CB6AED843393F04CD458E0AA2D9F1D5F31A402072B2D6';
    let statement;
    const unresolvedMosaicId = new NamespaceId_1.NamespaceId('mosaic');
    const resolvedMosaicId = new MosaicId_1.MosaicId('0DC67FBE1CAD29E5');
    before(() => {
        account = conf_spec_1.TestingAccount;
        statement = new Statement_1.Statement([], [], [new ResolutionStatement_1.ResolutionStatement(ResolutionType_1.ResolutionType.Mosaic, UInt64_1.UInt64.fromUint(2), unresolvedMosaicId, [new ResolutionEntry_1.ResolutionEntry(resolvedMosaicId, new ReceiptSource_1.ReceiptSource(1, 0))])]);
    });
    it('should createComplete an MosaicGlobalRestrictionTransaction object and sign', () => {
        const mosaicId = new MosaicId_1.MosaicId(UInt64_1.UInt64.fromUint(1).toDTO());
        const referenceMosaicId = new MosaicId_1.MosaicId(UInt64_1.UInt64.fromUint(2).toDTO());
        const mosaicGlobalRestrictionTransaction = MosaicGlobalRestrictionTransaction_1.MosaicGlobalRestrictionTransaction.create(Deadline_1.Deadline.create(), mosaicId, UInt64_1.UInt64.fromUint(1), UInt64_1.UInt64.fromUint(9), MosaicRestrictionType_1.MosaicRestrictionType.EQ, UInt64_1.UInt64.fromUint(8), MosaicRestrictionType_1.MosaicRestrictionType.GE, NetworkType_1.NetworkType.MIJIN_TEST, referenceMosaicId);
        chai_1.expect(mosaicGlobalRestrictionTransaction.mosaicId.toHex()).to.be.equal(mosaicId.toHex());
        chai_1.expect(mosaicGlobalRestrictionTransaction.referenceMosaicId.toHex()).to.be.equal(referenceMosaicId.toHex());
        chai_1.expect(mosaicGlobalRestrictionTransaction.restrictionKey.toHex()).to.be.equal(UInt64_1.UInt64.fromUint(1).toHex());
        chai_1.expect(mosaicGlobalRestrictionTransaction.previousRestrictionValue.toHex()).to.be.equal(UInt64_1.UInt64.fromUint(9).toHex());
        chai_1.expect(mosaicGlobalRestrictionTransaction.newRestrictionValue.toHex()).to.be.equal(UInt64_1.UInt64.fromUint(8).toHex());
        chai_1.expect(mosaicGlobalRestrictionTransaction.previousRestrictionType).to.be.equal(MosaicRestrictionType_1.MosaicRestrictionType.EQ);
        chai_1.expect(mosaicGlobalRestrictionTransaction.newRestrictionType).to.be.equal(MosaicRestrictionType_1.MosaicRestrictionType.GE);
        const signedTransaction = mosaicGlobalRestrictionTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(256, signedTransaction.payload.length)).to.be.equal('010000000000000002000000000000000100000000000000090000000000000008000000000000000106');
    });
    it('should createComplete an MosaicGlobalRestrictionTransaction use mosaic alias', () => {
        const namespacId = NamespaceId_1.NamespaceId.createFromEncoded('9550CA3FC9B41FC5');
        const referenceMosaicId = new MosaicId_1.MosaicId(UInt64_1.UInt64.fromUint(2).toDTO());
        const mosaicGlobalRestrictionTransaction = MosaicGlobalRestrictionTransaction_1.MosaicGlobalRestrictionTransaction.create(Deadline_1.Deadline.create(), namespacId, UInt64_1.UInt64.fromUint(1), UInt64_1.UInt64.fromUint(9), MosaicRestrictionType_1.MosaicRestrictionType.EQ, UInt64_1.UInt64.fromUint(8), MosaicRestrictionType_1.MosaicRestrictionType.GE, NetworkType_1.NetworkType.MIJIN_TEST, referenceMosaicId);
        chai_1.expect(mosaicGlobalRestrictionTransaction.mosaicId.toHex()).to.be.equal(namespacId.toHex());
        chai_1.expect(mosaicGlobalRestrictionTransaction.referenceMosaicId.toHex()).to.be.equal(referenceMosaicId.toHex());
        chai_1.expect(mosaicGlobalRestrictionTransaction.restrictionKey.toHex()).to.be.equal(UInt64_1.UInt64.fromUint(1).toHex());
        chai_1.expect(mosaicGlobalRestrictionTransaction.previousRestrictionValue.toHex()).to.be.equal(UInt64_1.UInt64.fromUint(9).toHex());
        chai_1.expect(mosaicGlobalRestrictionTransaction.newRestrictionValue.toHex()).to.be.equal(UInt64_1.UInt64.fromUint(8).toHex());
        chai_1.expect(mosaicGlobalRestrictionTransaction.previousRestrictionType).to.be.equal(MosaicRestrictionType_1.MosaicRestrictionType.EQ);
        chai_1.expect(mosaicGlobalRestrictionTransaction.newRestrictionType).to.be.equal(MosaicRestrictionType_1.MosaicRestrictionType.GE);
        const signedTransaction = mosaicGlobalRestrictionTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(256, signedTransaction.payload.length)).to.be.equal('C51FB4C93FCA509502000000000000000100000000000000090000000000000008000000000000000106');
        chai_1.expect(format_1.Convert.hexToUint8(mosaicGlobalRestrictionTransaction.serialize()).length)
            .to.be.equal(mosaicGlobalRestrictionTransaction.size);
    });
    it('should createComplete an MosaicGlobalRestrictionTransaction use mosaic alias reference', () => {
        const namespacId = NamespaceId_1.NamespaceId.createFromEncoded('9550CA3FC9B41FC5');
        const mosaicId = new MosaicId_1.MosaicId(UInt64_1.UInt64.fromUint(1).toDTO());
        const mosaicGlobalRestrictionTransaction = MosaicGlobalRestrictionTransaction_1.MosaicGlobalRestrictionTransaction.create(Deadline_1.Deadline.create(), mosaicId, UInt64_1.UInt64.fromUint(1), UInt64_1.UInt64.fromUint(9), MosaicRestrictionType_1.MosaicRestrictionType.EQ, UInt64_1.UInt64.fromUint(8), MosaicRestrictionType_1.MosaicRestrictionType.GE, NetworkType_1.NetworkType.MIJIN_TEST, namespacId);
        chai_1.expect(mosaicGlobalRestrictionTransaction.mosaicId.toHex()).to.be.equal(mosaicId.toHex());
        chai_1.expect(mosaicGlobalRestrictionTransaction.referenceMosaicId.toHex()).to.be.equal(namespacId.toHex());
        chai_1.expect(mosaicGlobalRestrictionTransaction.restrictionKey.toHex()).to.be.equal(UInt64_1.UInt64.fromUint(1).toHex());
        chai_1.expect(mosaicGlobalRestrictionTransaction.previousRestrictionValue.toHex()).to.be.equal(UInt64_1.UInt64.fromUint(9).toHex());
        chai_1.expect(mosaicGlobalRestrictionTransaction.newRestrictionValue.toHex()).to.be.equal(UInt64_1.UInt64.fromUint(8).toHex());
        chai_1.expect(mosaicGlobalRestrictionTransaction.previousRestrictionType).to.be.equal(MosaicRestrictionType_1.MosaicRestrictionType.EQ);
        chai_1.expect(mosaicGlobalRestrictionTransaction.newRestrictionType).to.be.equal(MosaicRestrictionType_1.MosaicRestrictionType.GE);
        const signedTransaction = mosaicGlobalRestrictionTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(256, signedTransaction.payload.length)).to.be.equal('0100000000000000C51FB4C93FCA50950100000000000000090000000000000008000000000000000106');
    });
    it('Test set maxFee using multiplier', () => {
        const mosaicId = new MosaicId_1.MosaicId(UInt64_1.UInt64.fromUint(1).toDTO());
        const referenceMosaicId = new MosaicId_1.MosaicId(UInt64_1.UInt64.fromUint(2).toDTO());
        const mosaicGlobalRestrictionTransaction = MosaicGlobalRestrictionTransaction_1.MosaicGlobalRestrictionTransaction.create(Deadline_1.Deadline.create(), mosaicId, UInt64_1.UInt64.fromUint(1), UInt64_1.UInt64.fromUint(9), MosaicRestrictionType_1.MosaicRestrictionType.EQ, UInt64_1.UInt64.fromUint(8), MosaicRestrictionType_1.MosaicRestrictionType.GE, NetworkType_1.NetworkType.MIJIN_TEST, referenceMosaicId).setMaxFee(2);
        chai_1.expect(mosaicGlobalRestrictionTransaction.maxFee.compact()).to.be.equal(340);
    });
    it('Test resolveAlias can resolve', () => {
        const mosaicGlobalRestrictionTransaction = new MosaicGlobalRestrictionTransaction_1.MosaicGlobalRestrictionTransaction(NetworkType_1.NetworkType.MIJIN_TEST, 1, Deadline_1.Deadline.createFromDTO('1'), UInt64_1.UInt64.fromUint(0), unresolvedMosaicId, unresolvedMosaicId, UInt64_1.UInt64.fromUint(1), UInt64_1.UInt64.fromUint(9), MosaicRestrictionType_1.MosaicRestrictionType.EQ, UInt64_1.UInt64.fromUint(8), MosaicRestrictionType_1.MosaicRestrictionType.GE, '', account.publicAccount, new TransactionInfo_1.TransactionInfo(UInt64_1.UInt64.fromUint(2), 0, '')).resolveAliases(statement);
        chai_1.expect(mosaicGlobalRestrictionTransaction.mosaicId instanceof MosaicId_1.MosaicId).to.be.true;
        chai_1.expect(mosaicGlobalRestrictionTransaction.mosaicId.equals(resolvedMosaicId)).to.be.true;
        chai_1.expect(mosaicGlobalRestrictionTransaction.referenceMosaicId instanceof MosaicId_1.MosaicId).to.be.true;
        chai_1.expect(mosaicGlobalRestrictionTransaction.referenceMosaicId.equals(resolvedMosaicId)).to.be.true;
    });
});
//# sourceMappingURL=MosaicGlobalRestrictionTransaction.spec.js.map