"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const format_1 = require("../../../src/core/format");
const Address_1 = require("../../../src/model/account/Address");
const NetworkType_1 = require("../../../src/model/blockchain/NetworkType");
const MosaicId_1 = require("../../../src/model/mosaic/MosaicId");
const NamespaceId_1 = require("../../../src/model/namespace/NamespaceId");
const ReceiptSource_1 = require("../../../src/model/receipt/ReceiptSource");
const ResolutionEntry_1 = require("../../../src/model/receipt/ResolutionEntry");
const ResolutionStatement_1 = require("../../../src/model/receipt/ResolutionStatement");
const ResolutionType_1 = require("../../../src/model/receipt/ResolutionType");
const Statement_1 = require("../../../src/model/receipt/Statement");
const Deadline_1 = require("../../../src/model/transaction/Deadline");
const MosaicAddressRestrictionTransaction_1 = require("../../../src/model/transaction/MosaicAddressRestrictionTransaction");
const TransactionInfo_1 = require("../../../src/model/transaction/TransactionInfo");
const UInt64_1 = require("../../../src/model/UInt64");
const conf_spec_1 = require("../../conf/conf.spec");
describe('MosaicAddressRestrictionTransaction', () => {
    let account;
    const generationHash = '57F7DA205008026C776CB6AED843393F04CD458E0AA2D9F1D5F31A402072B2D6';
    let statement;
    const unresolvedAddress = new NamespaceId_1.NamespaceId('address');
    const unresolvedMosaicId = new NamespaceId_1.NamespaceId('mosaic');
    const resolvedMosaicId = new MosaicId_1.MosaicId('0DC67FBE1CAD29E5');
    before(() => {
        account = conf_spec_1.TestingAccount;
        statement = new Statement_1.Statement([], [new ResolutionStatement_1.ResolutionStatement(ResolutionType_1.ResolutionType.Address, UInt64_1.UInt64.fromUint(2), unresolvedAddress, [new ResolutionEntry_1.ResolutionEntry(account.address, new ReceiptSource_1.ReceiptSource(1, 0))])], [new ResolutionStatement_1.ResolutionStatement(ResolutionType_1.ResolutionType.Mosaic, UInt64_1.UInt64.fromUint(2), unresolvedMosaicId, [new ResolutionEntry_1.ResolutionEntry(resolvedMosaicId, new ReceiptSource_1.ReceiptSource(1, 0))])]);
    });
    it('should createComplete an MosaicAddressRestrictionTransaction object and sign', () => {
        const mosaicId = new MosaicId_1.MosaicId(UInt64_1.UInt64.fromUint(1).toDTO());
        const mosaicAddressRestrictionTransaction = MosaicAddressRestrictionTransaction_1.MosaicAddressRestrictionTransaction.create(Deadline_1.Deadline.create(), mosaicId, UInt64_1.UInt64.fromUint(1), account.address, UInt64_1.UInt64.fromUint(8), NetworkType_1.NetworkType.MIJIN_TEST, UInt64_1.UInt64.fromUint(9));
        chai_1.expect(mosaicAddressRestrictionTransaction.mosaicId.toHex()).to.be.equal(mosaicId.toHex());
        chai_1.expect(mosaicAddressRestrictionTransaction.restrictionKey.toHex()).to.be.equal(UInt64_1.UInt64.fromUint(1).toHex());
        chai_1.expect(mosaicAddressRestrictionTransaction.previousRestrictionValue.toHex()).to.be.equal(UInt64_1.UInt64.fromUint(9).toHex());
        chai_1.expect(mosaicAddressRestrictionTransaction.newRestrictionValue.toHex()).to.be.equal(UInt64_1.UInt64.fromUint(8).toHex());
        chai_1.expect(mosaicAddressRestrictionTransaction.targetAddressToString()).to.be.equal(account.address.plain());
        const signedTransaction = mosaicAddressRestrictionTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(256, signedTransaction.payload.length)).to.be.equal('0100000000000000010000000000000009000000000000000800000000000000' +
            '90A75B6B63D31BDA93808727940F24699AECDDF17C568508BA');
    });
    it('should createComplete an MosaicAddressRestrictionTransaction use mosaic alias', () => {
        const namespacId = NamespaceId_1.NamespaceId.createFromEncoded('9550CA3FC9B41FC5');
        const mosaicAddressRestrictionTransaction = MosaicAddressRestrictionTransaction_1.MosaicAddressRestrictionTransaction.create(Deadline_1.Deadline.create(), namespacId, UInt64_1.UInt64.fromUint(1), account.address, UInt64_1.UInt64.fromUint(8), NetworkType_1.NetworkType.MIJIN_TEST, UInt64_1.UInt64.fromUint(9));
        chai_1.expect(mosaicAddressRestrictionTransaction.mosaicId.toHex()).to.be.equal(namespacId.toHex());
        chai_1.expect(mosaicAddressRestrictionTransaction.restrictionKey.toHex()).to.be.equal(UInt64_1.UInt64.fromUint(1).toHex());
        chai_1.expect(mosaicAddressRestrictionTransaction.previousRestrictionValue.toHex()).to.be.equal(UInt64_1.UInt64.fromUint(9).toHex());
        chai_1.expect(mosaicAddressRestrictionTransaction.newRestrictionValue.toHex()).to.be.equal(UInt64_1.UInt64.fromUint(8).toHex());
        chai_1.expect(mosaicAddressRestrictionTransaction.targetAddressToString()).to.be.equal(account.address.plain());
        const signedTransaction = mosaicAddressRestrictionTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(256, signedTransaction.payload.length)).to.be.equal('C51FB4C93FCA5095010000000000000009000000000000000800000000000000' +
            '90A75B6B63D31BDA93808727940F24699AECDDF17C568508BA');
    });
    it('should createComplete an MosaicAddressRestrictionTransaction use address alias', () => {
        const mosaicId = new MosaicId_1.MosaicId(UInt64_1.UInt64.fromUint(1).toDTO());
        const namespacId = NamespaceId_1.NamespaceId.createFromEncoded('9550CA3FC9B41FC5');
        const mosaicAddressRestrictionTransaction = MosaicAddressRestrictionTransaction_1.MosaicAddressRestrictionTransaction.create(Deadline_1.Deadline.create(), mosaicId, UInt64_1.UInt64.fromUint(1), namespacId, UInt64_1.UInt64.fromUint(8), NetworkType_1.NetworkType.MIJIN_TEST, UInt64_1.UInt64.fromUint(9));
        chai_1.expect(mosaicAddressRestrictionTransaction.mosaicId.toHex()).to.be.equal(mosaicId.toHex());
        chai_1.expect(mosaicAddressRestrictionTransaction.restrictionKey.toHex()).to.be.equal(UInt64_1.UInt64.fromUint(1).toHex());
        chai_1.expect(mosaicAddressRestrictionTransaction.previousRestrictionValue.toHex()).to.be.equal(UInt64_1.UInt64.fromUint(9).toHex());
        chai_1.expect(mosaicAddressRestrictionTransaction.newRestrictionValue.toHex()).to.be.equal(UInt64_1.UInt64.fromUint(8).toHex());
        chai_1.expect(mosaicAddressRestrictionTransaction.targetAddressToString()).to.be.equal(namespacId.toHex());
        const signedTransaction = mosaicAddressRestrictionTransaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(256, signedTransaction.payload.length)).to.be.equal('0100000000000000010000000000000009000000000000000800000000000000' +
            '91C51FB4C93FCA509500000000000000000000000000000000');
    });
    it('should format targetAddress payload with 8 bytes binary namespaceId - targetAddressToString', () => {
        const transaction = MosaicAddressRestrictionTransaction_1.MosaicAddressRestrictionTransaction.create(Deadline_1.Deadline.create(), new MosaicId_1.MosaicId(UInt64_1.UInt64.fromUint(1).toDTO()), UInt64_1.UInt64.fromUint(1), new NamespaceId_1.NamespaceId('nem.owner'), UInt64_1.UInt64.fromUint(8), NetworkType_1.NetworkType.MIJIN_TEST, UInt64_1.UInt64.fromUint(9));
        // test targetAddressToString with NamespaceId recipient
        chai_1.expect(transaction.targetAddressToString()).to.be.equal('D85742D268617751');
        const signedTransaction = transaction.signWith(account, generationHash);
        chai_1.expect(signedTransaction.payload.substring(256, 304)).to.be.equal('010000000000000001000000000000000900000000000000');
        chai_1.expect(format_1.Convert.hexToUint8(transaction.serialize()).length).to.be.equal(transaction.size);
    });
    it('Test set maxFee using multiplier', () => {
        const transaction = MosaicAddressRestrictionTransaction_1.MosaicAddressRestrictionTransaction.create(Deadline_1.Deadline.create(), new MosaicId_1.MosaicId(UInt64_1.UInt64.fromUint(1).toDTO()), UInt64_1.UInt64.fromUint(1), new NamespaceId_1.NamespaceId('nem.owner'), UInt64_1.UInt64.fromUint(8), NetworkType_1.NetworkType.MIJIN_TEST, UInt64_1.UInt64.fromUint(9)).setMaxFee(2);
        chai_1.expect(transaction.maxFee.compact()).to.be.equal(370);
    });
    it('Test resolveAlias can resolve', () => {
        const transaction = new MosaicAddressRestrictionTransaction_1.MosaicAddressRestrictionTransaction(NetworkType_1.NetworkType.MIJIN_TEST, 1, Deadline_1.Deadline.createFromDTO('1'), UInt64_1.UInt64.fromUint(0), unresolvedMosaicId, UInt64_1.UInt64.fromUint(8), unresolvedAddress, UInt64_1.UInt64.fromUint(8), UInt64_1.UInt64.fromUint(9), '', account.publicAccount, new TransactionInfo_1.TransactionInfo(UInt64_1.UInt64.fromUint(2), 0, '')).resolveAliases(statement);
        chai_1.expect(transaction.targetAddress instanceof Address_1.Address).to.be.true;
        chai_1.expect(transaction.mosaicId instanceof MosaicId_1.MosaicId).to.be.true;
        chai_1.expect(transaction.targetAddress.equals(account.address)).to.be.true;
        chai_1.expect(transaction.mosaicId.equals(resolvedMosaicId)).to.be.true;
    });
});
//# sourceMappingURL=MosaicAddressRestrictionTransaction.spec.js.map