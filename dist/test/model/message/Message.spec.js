"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const EncryptedMessage_1 = require("../../../src/model/message/EncryptedMessage");
const Message_1 = require("../../../src/model/message/Message");
const MessageType_1 = require("../../../src/model/message/MessageType");
const PersistentHarvestingDelegationMessage_1 = require("../../../src/model/message/PersistentHarvestingDelegationMessage");
const PlainMessage_1 = require("../../../src/model/message/PlainMessage");
describe('Message', () => {
    it('should create an plain message dto object', () => {
        const message = new PlainMessage_1.PlainMessage('test');
        chai_1.expect(message.toDTO().type).to.be.equal(MessageType_1.MessageType.PlainMessage);
        chai_1.expect(message.toDTO().payload).to.be.equal('test');
    });
    it('should create an encrypted message dto object', () => {
        const message = new EncryptedMessage_1.EncryptedMessage('test');
        chai_1.expect(message.toDTO().type).to.be.equal(MessageType_1.MessageType.EncryptedMessage);
        chai_1.expect(message.toDTO().payload).to.be.equal('test');
    });
    it('should create an PersistentHarvestingDelegationMessage message dto object', () => {
        const message = new PersistentHarvestingDelegationMessage_1.PersistentHarvestingDelegationMessage('746573742D6D657373616765');
        chai_1.expect(message.toDTO().type).to.be.equal(MessageType_1.MessageType.PersistentHarvestingDelegationMessage);
        chai_1.expect(message.toDTO().payload).to.be.equal('746573742D6D657373616765');
    });
    it('should throw exception on creating PersistentHarvestingDelegationMessage', () => {
        chai_1.expect(() => {
            new PersistentHarvestingDelegationMessage_1.PersistentHarvestingDelegationMessage('test');
        }).to.throw(Error, 'Payload format is not valid hexadecimal string');
    });
    it('should decode hex string', () => {
        const hex = '746573742D6D657373616765';
        chai_1.expect(Message_1.Message.decodeHex(hex)).to.be.equal('test-message');
    });
});
//# sourceMappingURL=Message.spec.js.map