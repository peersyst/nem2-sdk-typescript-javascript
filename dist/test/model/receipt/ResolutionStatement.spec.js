"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const CreateReceiptFromDTO_1 = require("../../../src/infrastructure/receipt/CreateReceiptFromDTO");
const Account_1 = require("../../../src/model/account/Account");
const NetworkType_1 = require("../../../src/model/blockchain/NetworkType");
const model_1 = require("../../../src/model/model");
describe('ResolutionStatement', () => {
    let account;
    let transactionStatementsDTO;
    let addressResolutionStatementsDTO;
    let mosaicResolutionStatementsDTO;
    let statementDTO;
    before(() => {
        account = Account_1.Account.createFromPrivateKey('81C18245507F9C15B61BDEDAFA2C10D9DC2C4E401E573A10935D45AA2A461FD5', NetworkType_1.NetworkType.MIJIN_TEST);
        transactionStatementsDTO = [
            {
                statement: {
                    height: '1473',
                    source: {
                        primaryId: 0,
                        secondaryId: 0,
                    },
                    receipts: [
                        {
                            version: 1,
                            type: 8515,
                            targetPublicKey: 'B2708D49C46F8AB5CDBD7A09C959EEA12E4A782592F3D1D3D17D54622E655D7F',
                            mosaicId: '504677C3281108DB',
                            amount: '0',
                        },
                    ],
                },
            },
        ];
        addressResolutionStatementsDTO = [
            {
                statement: {
                    height: '1473',
                    unresolved: '9156258DE356F030A500000000000000000000000000000000',
                    resolutionEntries: [
                        {
                            source: {
                                primaryId: 1,
                                secondaryId: 0,
                            },
                            resolved: '901D8D4741F80299E66BF7FEEB4F30943DA7B68E068B182319',
                        },
                    ],
                },
            },
        ];
        mosaicResolutionStatementsDTO = [
            {
                statement: {
                    height: '1473',
                    unresolved: '85BBEA6CC462B244',
                    resolutionEntries: [
                        {
                            source: {
                                primaryId: 1,
                                secondaryId: 0,
                            },
                            resolved: '504677C3281108DB',
                        },
                        {
                            source: {
                                primaryId: 3,
                                secondaryId: 5,
                            },
                            resolved: '401F622A3111A3E4',
                        },
                    ],
                },
            },
            {
                statement: {
                    height: '1473',
                    unresolved: 'E81F622A5B11A340',
                    resolutionEntries: [
                        {
                            source: {
                                primaryId: 3,
                                secondaryId: 1,
                            },
                            resolved: '756482FB80FD406C',
                        },
                    ],
                },
            },
            {
                statement: {
                    height: '1500',
                    unresolved: '85BBEA6CC462B244',
                    resolutionEntries: [
                        {
                            source: {
                                primaryId: 1,
                                secondaryId: 1,
                            },
                            resolved: '0DC67FBE1CAD29E5',
                        },
                        {
                            source: {
                                primaryId: 1,
                                secondaryId: 4,
                            },
                            resolved: '7CDF3B117A3C40CC',
                        },
                        {
                            source: {
                                primaryId: 1,
                                secondaryId: 7,
                            },
                            resolved: '0DC67FBE1CAD29E5',
                        },
                        {
                            source: {
                                primaryId: 2,
                                secondaryId: 4,
                            },
                            resolved: '7CDF3B117A3C40CC',
                        },
                    ],
                },
            },
        ];
        statementDTO = {
            transactionStatements: transactionStatementsDTO,
            addressResolutionStatements: addressResolutionStatementsDTO,
            mosaicResolutionStatements: mosaicResolutionStatementsDTO,
        };
    });
    it('should get resolve entry when both primaryId and secondaryId matched', () => {
        const statement = CreateReceiptFromDTO_1.CreateStatementFromDTO(statementDTO, NetworkType_1.NetworkType.MIJIN_TEST);
        const entry = statement.addressResolutionStatements[0].getResolutionEntryById(1, 0);
        chai_1.expect(entry.resolved instanceof model_1.Address).to.be.true;
        chai_1.expect(entry.resolved.equals(account.address)).to.be.true;
    });
    it('should get resolved entry when primaryId is greater than max', () => {
        const statement = CreateReceiptFromDTO_1.CreateStatementFromDTO(statementDTO, NetworkType_1.NetworkType.MIJIN_TEST);
        const entry = statement.mosaicResolutionStatements[0].getResolutionEntryById(4, 0);
        chai_1.expect(entry.source.primaryId).to.be.equal(3);
        chai_1.expect(entry.source.secondaryId).to.be.equal(5);
        chai_1.expect(entry.resolved instanceof model_1.MosaicId).to.be.true;
        chai_1.expect(entry.resolved.equals(new model_1.MosaicId('401F622A3111A3E4'))).to.be.true;
    });
    it('should get resolved entry when primaryId is in middle of 2 pirmaryIds', () => {
        const statement = CreateReceiptFromDTO_1.CreateStatementFromDTO(statementDTO, NetworkType_1.NetworkType.MIJIN_TEST);
        const entry = statement.mosaicResolutionStatements[0].getResolutionEntryById(2, 1);
        chai_1.expect(entry.source.primaryId).to.be.equal(1);
        chai_1.expect(entry.source.secondaryId).to.be.equal(0);
        chai_1.expect(entry.resolved instanceof model_1.MosaicId).to.be.true;
        chai_1.expect(entry.resolved.equals(new model_1.MosaicId('504677C3281108DB'))).to.be.true;
    });
    it('should get resolved entry when primaryId matches but not secondaryId', () => {
        const statement = CreateReceiptFromDTO_1.CreateStatementFromDTO(statementDTO, NetworkType_1.NetworkType.MIJIN_TEST);
        const entry = statement.mosaicResolutionStatements[0].getResolutionEntryById(3, 6);
        chai_1.expect(entry.source.primaryId).to.be.equal(3);
        chai_1.expect(entry.source.secondaryId).to.be.equal(5);
        chai_1.expect(entry.resolved instanceof model_1.MosaicId).to.be.true;
        chai_1.expect(entry.resolved.equals(new model_1.MosaicId('401F622A3111A3E4'))).to.be.true;
    });
    it('should get resolved entry when primaryId matches but secondaryId less than minimum', () => {
        const statement = CreateReceiptFromDTO_1.CreateStatementFromDTO(statementDTO, NetworkType_1.NetworkType.MIJIN_TEST);
        const entry = statement.mosaicResolutionStatements[0].getResolutionEntryById(3, 1);
        chai_1.expect(entry.source.primaryId).to.be.equal(1);
        chai_1.expect(entry.source.secondaryId).to.be.equal(0);
        chai_1.expect(entry.resolved instanceof model_1.MosaicId).to.be.true;
        chai_1.expect(entry.resolved.equals(new model_1.MosaicId('504677C3281108DB'))).to.be.true;
    });
    it('should return undefined', () => {
        const statement = CreateReceiptFromDTO_1.CreateStatementFromDTO(statementDTO, NetworkType_1.NetworkType.MIJIN_TEST);
        const entry = statement.addressResolutionStatements[0].getResolutionEntryById(0, 0);
        chai_1.expect(entry).to.be.undefined;
    });
    it('resolution change in the block (more than one AGGREGATE)', () => {
        const statement = CreateReceiptFromDTO_1.CreateStatementFromDTO(statementDTO, NetworkType_1.NetworkType.MIJIN_TEST);
        const resolution = statement.mosaicResolutionStatements[2];
        chai_1.expect(resolution.getResolutionEntryById(1, 1).resolved.toHex()).to.be.equal('0DC67FBE1CAD29E5');
        chai_1.expect(resolution.getResolutionEntryById(1, 4).resolved.toHex()).to.be.equal('7CDF3B117A3C40CC');
        chai_1.expect(resolution.getResolutionEntryById(1, 7).resolved.toHex()).to.be.equal('0DC67FBE1CAD29E5');
        chai_1.expect(resolution.getResolutionEntryById(2, 1).resolved.toHex()).to.be.equal('0DC67FBE1CAD29E5');
        chai_1.expect(resolution.getResolutionEntryById(2, 4).resolved.toHex()).to.be.equal('7CDF3B117A3C40CC');
        chai_1.expect(resolution.getResolutionEntryById(3, 0).resolved.toHex()).to.be.equal('7CDF3B117A3C40CC');
        chai_1.expect(resolution.getResolutionEntryById(2, 2).resolved.toHex()).to.be.equal('0DC67FBE1CAD29E5');
        chai_1.expect(resolution.getResolutionEntryById(1, 0)).to.be.undefined;
        chai_1.expect(resolution.getResolutionEntryById(1, 6).resolved.toHex()).to.be.equal('7CDF3B117A3C40CC');
        chai_1.expect(resolution.getResolutionEntryById(1, 2).resolved.toHex()).to.be.equal('0DC67FBE1CAD29E5');
    });
});
//# sourceMappingURL=ResolutionStatement.spec.js.map