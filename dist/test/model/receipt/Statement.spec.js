"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const UnresolvedMapping_1 = require("../../../src/core/utils/UnresolvedMapping");
const CreateReceiptFromDTO_1 = require("../../../src/infrastructure/receipt/CreateReceiptFromDTO");
const Account_1 = require("../../../src/model/account/Account");
const NetworkType_1 = require("../../../src/model/blockchain/NetworkType");
const model_1 = require("../../../src/model/model");
describe('Statement', () => {
    let account;
    let transactionStatementsDTO;
    let addressResolutionStatementsDTO;
    let mosaicResolutionStatementsDTO;
    let statementDTO;
    before(() => {
        account = Account_1.Account.createFromPrivateKey('81C18245507F9C15B61BDEDAFA2C10D9DC2C4E401E573A10935D45AA2A461FD5', NetworkType_1.NetworkType.MIJIN_TEST);
        transactionStatementsDTO = [
            {
                statement: {
                    height: '1473',
                    source: {
                        primaryId: 0,
                        secondaryId: 0,
                    },
                    receipts: [
                        {
                            version: 1,
                            type: 8515,
                            targetPublicKey: 'B2708D49C46F8AB5CDBD7A09C959EEA12E4A782592F3D1D3D17D54622E655D7F',
                            mosaicId: '504677C3281108DB',
                            amount: '0',
                        },
                    ],
                },
            },
        ];
        addressResolutionStatementsDTO = [
            {
                statement: {
                    height: '1473',
                    unresolved: '9156258DE356F030A500000000000000000000000000000000',
                    resolutionEntries: [
                        {
                            source: {
                                primaryId: 1,
                                secondaryId: 0,
                            },
                            resolved: '901D8D4741F80299E66BF7FEEB4F30943DA7B68E068B182319',
                        },
                    ],
                },
            },
        ];
        mosaicResolutionStatementsDTO = [
            {
                statement: {
                    height: '1473',
                    unresolved: '85BBEA6CC462B244',
                    resolutionEntries: [
                        {
                            source: {
                                primaryId: 1,
                                secondaryId: 0,
                            },
                            resolved: '504677C3281108DB',
                        },
                    ],
                },
            },
            {
                statement: {
                    height: '1473',
                    unresolved: 'E81F622A5B11A340',
                    resolutionEntries: [
                        {
                            source: {
                                primaryId: 1,
                                secondaryId: 0,
                            },
                            resolved: '756482FB80FD406C',
                        },
                    ],
                },
            },
        ];
        statementDTO = {
            transactionStatements: transactionStatementsDTO,
            addressResolutionStatements: addressResolutionStatementsDTO,
            mosaicResolutionStatements: mosaicResolutionStatementsDTO,
        };
    });
    it('should get resolved address from receipt', () => {
        const unresolvedAddress = UnresolvedMapping_1.UnresolvedMapping.toUnresolvedAddress('9156258DE356F030A500000000000000000000000000000000');
        const statement = CreateReceiptFromDTO_1.CreateStatementFromDTO(statementDTO, NetworkType_1.NetworkType.MIJIN_TEST);
        const resolved = statement.resolveAddress(unresolvedAddress, '1473', 0);
        chai_1.expect(resolved instanceof model_1.Address).to.be.true;
        chai_1.expect(resolved.equals(account.address)).to.be.true;
    });
    it('should get resolved address from receipt without Harvesting_Fee', () => {
        const statementWithoutHarvesting = {
            transactionStatements: [],
            addressResolutionStatements: [
                {
                    statement: {
                        height: '1473',
                        unresolved: '9156258DE356F030A500000000000000000000000000000000',
                        resolutionEntries: [
                            {
                                source: {
                                    primaryId: 1,
                                    secondaryId: 0,
                                },
                                resolved: '901D8D4741F80299E66BF7FEEB4F30943DA7B68E068B182319',
                            },
                        ],
                    },
                },
            ],
            mosaicResolutionStatements: [],
        };
        const unresolvedAddress = UnresolvedMapping_1.UnresolvedMapping.toUnresolvedAddress('9156258DE356F030A500000000000000000000000000000000');
        const statement = CreateReceiptFromDTO_1.CreateStatementFromDTO(statementWithoutHarvesting, NetworkType_1.NetworkType.MIJIN_TEST);
        const resolved = statement.resolveAddress(unresolvedAddress, '1473', 0);
        chai_1.expect(resolved instanceof model_1.Address).to.be.true;
        chai_1.expect(resolved.equals(account.address)).to.be.true;
    });
    it('should get resolved mosaic from receipt', () => {
        const unresolvedMosaic = UnresolvedMapping_1.UnresolvedMapping.toUnresolvedMosaic('E81F622A5B11A340');
        const statement = CreateReceiptFromDTO_1.CreateStatementFromDTO(statementDTO, NetworkType_1.NetworkType.MIJIN_TEST);
        const resolved = statement.resolveMosaicId(unresolvedMosaic, '1473', 0);
        chai_1.expect(resolved instanceof model_1.MosaicId).to.be.true;
        chai_1.expect(resolved.equals(new model_1.MosaicId('756482FB80FD406C'))).to.be.true;
    });
    it('should get resolved mosaic from receipt without Harvesting_Fee', () => {
        const statementWithoutHarvesting = {
            transactionStatements: [],
            addressResolutionStatements: [],
            mosaicResolutionStatements: [
                {
                    statement: {
                        height: '1473',
                        unresolved: '85BBEA6CC462B244',
                        resolutionEntries: [
                            {
                                source: {
                                    primaryId: 1,
                                    secondaryId: 0,
                                },
                                resolved: '504677C3281108DB',
                            },
                        ],
                    },
                },
                {
                    statement: {
                        height: '1473',
                        unresolved: 'E81F622A5B11A340',
                        resolutionEntries: [
                            {
                                source: {
                                    primaryId: 1,
                                    secondaryId: 0,
                                },
                                resolved: '756482FB80FD406C',
                            },
                        ],
                    },
                },
            ],
        };
        const unresolvedMosaic = UnresolvedMapping_1.UnresolvedMapping.toUnresolvedMosaic('E81F622A5B11A340');
        const statement = CreateReceiptFromDTO_1.CreateStatementFromDTO(statementWithoutHarvesting, NetworkType_1.NetworkType.MIJIN_TEST);
        const resolved = statement.resolveMosaicId(unresolvedMosaic, '1473', 0);
        chai_1.expect(resolved instanceof model_1.MosaicId).to.be.true;
        chai_1.expect(resolved.equals(new model_1.MosaicId('756482FB80FD406C'))).to.be.true;
    });
});
//# sourceMappingURL=Statement.spec.js.map