"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const assert_1 = require("assert");
const chai_1 = require("chai");
const CreateReceiptFromDTO_1 = require("../../../src/infrastructure/receipt/CreateReceiptFromDTO");
const Account_1 = require("../../../src/model/account/Account");
const Address_1 = require("../../../src/model/account/Address");
const PublicAccount_1 = require("../../../src/model/account/PublicAccount");
const NetworkType_1 = require("../../../src/model/blockchain/NetworkType");
const MosaicId_1 = require("../../../src/model/mosaic/MosaicId");
const NamespaceId_1 = require("../../../src/model/namespace/NamespaceId");
const ArtifactExpiryReceipt_1 = require("../../../src/model/receipt/ArtifactExpiryReceipt");
const BalanceChangeReceipt_1 = require("../../../src/model/receipt/BalanceChangeReceipt");
const BalanceTransferReceipt_1 = require("../../../src/model/receipt/BalanceTransferReceipt");
const InflationReceipt_1 = require("../../../src/model/receipt/InflationReceipt");
const ReceiptSource_1 = require("../../../src/model/receipt/ReceiptSource");
const ReceiptType_1 = require("../../../src/model/receipt/ReceiptType");
const ReceiptVersion_1 = require("../../../src/model/receipt/ReceiptVersion");
const ResolutionEntry_1 = require("../../../src/model/receipt/ResolutionEntry");
const ResolutionStatement_1 = require("../../../src/model/receipt/ResolutionStatement");
const ResolutionType_1 = require("../../../src/model/receipt/ResolutionType");
const TransactionStatement_1 = require("../../../src/model/receipt/TransactionStatement");
const UInt64_1 = require("../../../src/model/UInt64");
describe('Receipt', () => {
    let account;
    let account2;
    let transactionStatementsDTO;
    let addressResolutionStatementsDTO;
    let mosaicResolutionStatementsDTO;
    let statementDTO;
    const netWorkType = NetworkType_1.NetworkType.MIJIN_TEST;
    before(() => {
        account = Account_1.Account.createFromPrivateKey('D242FB34C2C4DD36E995B9C865F93940065E326661BA5A4A247331D211FE3A3D', NetworkType_1.NetworkType.MIJIN_TEST);
        account2 = Account_1.Account.createFromPrivateKey('E5DCCEBDB01A8B03A7DB7BA5888E2E33FD4617B5F6FED48C4C09C0780F422713', NetworkType_1.NetworkType.MIJIN_TEST);
        transactionStatementsDTO = [
            {
                statement: {
                    height: '52',
                    source: {
                        primaryId: 0,
                        secondaryId: 0,
                    },
                    receipts: [
                        {
                            version: 1,
                            type: 8515,
                            targetPublicKey: account.publicKey,
                            mosaicId: '85BBEA6CC462B244',
                            amount: '1000',
                        },
                    ],
                },
            },
        ];
        addressResolutionStatementsDTO = [
            {
                statement: {
                    height: '1488',
                    unresolved: '9103B60AAF2762688300000000000000000000000000000000',
                    resolutionEntries: [
                        {
                            source: {
                                primaryId: 4,
                                secondaryId: 0,
                            },
                            resolved: '917E7E29A01014C2F300000000000000000000000000000000',
                        },
                    ],
                },
            },
            {
                statement: {
                    height: '1488',
                    unresolved: '917E7E29A01014C2F300000000000000000000000000000000',
                    resolutionEntries: [
                        {
                            source: {
                                primaryId: 2,
                                secondaryId: 0,
                            },
                            resolved: '9103B60AAF2762688300000000000000000000000000000000',
                        },
                    ],
                },
            },
        ];
        mosaicResolutionStatementsDTO = [
            {
                statement: {
                    height: '1506',
                    unresolved: '85BBEA6CC462B244',
                    resolutionEntries: [
                        {
                            source: {
                                primaryId: 1,
                                secondaryId: 0,
                            },
                            resolved: '941299B2B7E1291C',
                        },
                    ],
                },
            },
            {
                statement: {
                    height: '1506',
                    unresolved: '85BBEA6CC462B244',
                    resolutionEntries: [
                        {
                            source: {
                                primaryId: 5,
                                secondaryId: 0,
                            },
                            resolved: '941299B2B7E1291C',
                        },
                    ],
                },
            },
        ];
        statementDTO = {
            transactionStatements: transactionStatementsDTO,
            addressResolutionStatements: addressResolutionStatementsDTO,
            mosaicResolutionStatements: mosaicResolutionStatementsDTO,
        };
    });
    it('should createComplete a balance transfer receipt', () => {
        const receiptDTO = {
            version: 1,
            type: 4685,
            senderPublicKey: account.publicKey,
            recipientAddress: '9103B60AAF2762688300000000000000000000000000000000',
            mosaicId: '941299B2B7E1291C',
            amount: '1000',
        };
        const receipt = new BalanceTransferReceipt_1.BalanceTransferReceipt(PublicAccount_1.PublicAccount.createFromPublicKey(receiptDTO.senderPublicKey, netWorkType), Address_1.Address.createFromEncoded(receiptDTO.recipientAddress), new MosaicId_1.MosaicId(receiptDTO.mosaicId), UInt64_1.UInt64.fromNumericString(receiptDTO.amount), receiptDTO.version, receiptDTO.type);
        assert_1.deepEqual(receipt.amount.toString(), receiptDTO.amount);
        assert_1.deepEqual(receipt.mosaicId.toHex(), receiptDTO.mosaicId);
        assert_1.deepEqual(receipt.type, ReceiptType_1.ReceiptType.Mosaic_Levy);
        assert_1.deepEqual(receipt.version, ReceiptVersion_1.ReceiptVersion.BALANCE_TRANSFER);
        assert_1.deepEqual(receipt.recipientAddress, Address_1.Address.createFromEncoded('9103B60AAF2762688300000000000000000000000000000000'));
    });
    it('should createComplete a balance transfer receipt - Mosaic Rental Fee', () => {
        const receiptDTO = {
            version: 1,
            type: 4685,
            senderPublicKey: account.publicKey,
            recipientAddress: '9103B60AAF2762688300000000000000000000000000000000',
            mosaicId: '941299B2B7E1291C',
            amount: '1000',
        };
        const receipt = new BalanceTransferReceipt_1.BalanceTransferReceipt(PublicAccount_1.PublicAccount.createFromPublicKey(receiptDTO.senderPublicKey, netWorkType), Address_1.Address.createFromEncoded(receiptDTO.recipientAddress), new MosaicId_1.MosaicId(receiptDTO.mosaicId), UInt64_1.UInt64.fromNumericString(receiptDTO.amount), receiptDTO.version, receiptDTO.type);
        assert_1.deepEqual(receipt.amount.toString(), receiptDTO.amount);
        assert_1.deepEqual(receipt.recipientAddress, Address_1.Address.createFromEncoded('9103B60AAF2762688300000000000000000000000000000000'));
        assert_1.deepEqual(receipt.mosaicId.toHex(), receiptDTO.mosaicId);
        assert_1.deepEqual(receipt.type, ReceiptType_1.ReceiptType.Mosaic_Rental_Fee);
        assert_1.deepEqual(receipt.version, ReceiptVersion_1.ReceiptVersion.BALANCE_TRANSFER);
    });
    it('should createComplete a balance change receipt - Harvest Fee', () => {
        const receiptDTO = {
            version: 1,
            type: 8515,
            targetPublicKey: account.publicKey,
            mosaicId: '941299B2B7E1291C',
            amount: '1000',
        };
        const receipt = new BalanceChangeReceipt_1.BalanceChangeReceipt(PublicAccount_1.PublicAccount.createFromPublicKey(receiptDTO.targetPublicKey, netWorkType), new MosaicId_1.MosaicId(receiptDTO.mosaicId), UInt64_1.UInt64.fromNumericString(receiptDTO.amount), receiptDTO.version, receiptDTO.type);
        assert_1.deepEqual(receipt.targetPublicAccount.publicKey, receiptDTO.targetPublicKey);
        assert_1.deepEqual(receipt.amount.toString(), receiptDTO.amount);
        assert_1.deepEqual(receipt.mosaicId.toHex(), receiptDTO.mosaicId);
        assert_1.deepEqual(receipt.type, ReceiptType_1.ReceiptType.Harvest_Fee);
        assert_1.deepEqual(receipt.version, ReceiptVersion_1.ReceiptVersion.BALANCE_CHANGE);
    });
    it('should createComplete a balance change receipt - LockHash', () => {
        const receiptDTO = {
            version: 1,
            type: 12616,
            targetPublicKey: account.publicKey,
            mosaicId: '941299B2B7E1291C',
            amount: '1000',
        };
        const receipt = new BalanceChangeReceipt_1.BalanceChangeReceipt(PublicAccount_1.PublicAccount.createFromPublicKey(receiptDTO.targetPublicKey, netWorkType), new MosaicId_1.MosaicId(receiptDTO.mosaicId), UInt64_1.UInt64.fromNumericString(receiptDTO.amount), receiptDTO.version, receiptDTO.type);
        assert_1.deepEqual(receipt.targetPublicAccount.publicKey, receiptDTO.targetPublicKey);
        assert_1.deepEqual(receipt.amount.toString(), receiptDTO.amount);
        assert_1.deepEqual(receipt.mosaicId.toHex().toUpperCase(), receiptDTO.mosaicId);
        assert_1.deepEqual(receipt.type, ReceiptType_1.ReceiptType.LockHash_Created);
        assert_1.deepEqual(receipt.version, ReceiptVersion_1.ReceiptVersion.BALANCE_CHANGE);
    });
    it('should createComplete an artifact expiry receipt - address', () => {
        const receiptDTO = {
            version: 1,
            type: 16718,
            artifactId: 'D525AD41D95FCF29',
        };
        const receipt = new ArtifactExpiryReceipt_1.ArtifactExpiryReceipt(new NamespaceId_1.NamespaceId([3646934825, 3576016193]), receiptDTO.version, receiptDTO.type);
        assert_1.deepEqual(receipt.artifactId.id.toHex().toUpperCase(), receiptDTO.artifactId);
        assert_1.deepEqual(receipt.type, ReceiptType_1.ReceiptType.Namespace_Expired);
        assert_1.deepEqual(receipt.version, ReceiptVersion_1.ReceiptVersion.ARTIFACT_EXPIRY);
    });
    it('should createComplete an artifact expiry receipt - mosaic', () => {
        const receiptDTO = {
            version: 1,
            type: 16717,
            artifactId: '941299B2B7E1291C',
        };
        const receipt = new ArtifactExpiryReceipt_1.ArtifactExpiryReceipt(new MosaicId_1.MosaicId(receiptDTO.artifactId), receiptDTO.version, receiptDTO.type);
        assert_1.deepEqual(receipt.artifactId.toHex().toUpperCase(), receiptDTO.artifactId);
        assert_1.deepEqual(receipt.type, ReceiptType_1.ReceiptType.Mosaic_Expired);
        assert_1.deepEqual(receipt.version, ReceiptVersion_1.ReceiptVersion.ARTIFACT_EXPIRY);
    });
    it('should createComplete a transaction statement', () => {
        const statementDto = transactionStatementsDTO[0];
        const statement = new TransactionStatement_1.TransactionStatement(statementDto.statement.height, new ReceiptSource_1.ReceiptSource(statementDto.statement.source.primaryId, statementDto.statement.source.secondaryId), statementDto.statement.receipts.map((receipt) => CreateReceiptFromDTO_1.CreateReceiptFromDTO(receipt, netWorkType)));
        assert_1.deepEqual(statement.source.primaryId, statementDto.statement.source.primaryId);
        assert_1.deepEqual(statement.source.secondaryId, statementDto.statement.source.secondaryId);
        assert_1.deepEqual(statement.receipts[0].targetPublicAccount.publicKey, account.publicKey);
    });
    it('should createComplete resolution statement - mosaic', () => {
        const statementDto = mosaicResolutionStatementsDTO[0];
        const statement = new ResolutionStatement_1.ResolutionStatement(ResolutionType_1.ResolutionType.Mosaic, statementDto.statement.height, new MosaicId_1.MosaicId(statementDto.statement.unresolved), statementDto.statement.resolutionEntries.map((resolved) => {
            return new ResolutionEntry_1.ResolutionEntry(new MosaicId_1.MosaicId(resolved.resolved), new ReceiptSource_1.ReceiptSource(resolved.source.primaryId, resolved.source.secondaryId));
        }));
        assert_1.deepEqual(statement.unresolved.toHex(), statementDto.statement.unresolved);
        assert_1.deepEqual(statement.resolutionEntries[0].resolved.toHex(), '941299B2B7E1291C');
    });
    it('should createComplete resolution statement - address', () => {
        const statementDto = addressResolutionStatementsDTO[0];
        const statement = new ResolutionStatement_1.ResolutionStatement(ResolutionType_1.ResolutionType.Address, statementDto.statement.height, Address_1.Address.createFromEncoded(statementDto.statement.unresolved), statementDto.statement.resolutionEntries.map((resolved) => {
            return new ResolutionEntry_1.ResolutionEntry(Address_1.Address.createFromEncoded(resolved.resolved), new ReceiptSource_1.ReceiptSource(resolved.source.primaryId, resolved.source.secondaryId));
        }));
        assert_1.deepEqual(statement.unresolved.plain(), Address_1.Address.createFromEncoded('9103B60AAF2762688300000000000000000000000000000000').plain());
        assert_1.deepEqual(statement.resolutionEntries[0].resolved.plain(), Address_1.Address.createFromEncoded('917E7E29A01014C2F300000000000000000000000000000000').plain());
    });
    it('should createComplete a inflation receipt', () => {
        const receiptDTO = {
            version: 1,
            type: 20803,
            mosaicId: '941299B2B7E1291C',
            amount: '1000',
        };
        const receipt = new InflationReceipt_1.InflationReceipt(new MosaicId_1.MosaicId(receiptDTO.mosaicId), UInt64_1.UInt64.fromNumericString(receiptDTO.amount), receiptDTO.version, receiptDTO.type);
        assert_1.deepEqual(receipt.amount.compact().toString(), receiptDTO.amount);
        assert_1.deepEqual(receipt.mosaicId.toHex().toUpperCase(), receiptDTO.mosaicId);
        assert_1.deepEqual(receipt.type, ReceiptType_1.ReceiptType.Inflation);
        assert_1.deepEqual(receipt.version, ReceiptVersion_1.ReceiptVersion.INFLATION_RECEIPT);
    });
    it('should generate hash for MosaicResolutionStatement', () => {
        const statement = CreateReceiptFromDTO_1.CreateStatementFromDTO(statementDTO, netWorkType);
        const receipt = statement.mosaicResolutionStatements[0];
        const hash = receipt.generateHash(NetworkType_1.NetworkType.MAIN_NET);
        chai_1.expect(hash).to.be.equal('99381CE398D3AAE110FC97E984D7D35A710A5C525A4F959EC8916B382DE78A63');
    });
    it('should generate hash for AddressResolutionStatement', () => {
        const statement = CreateReceiptFromDTO_1.CreateStatementFromDTO(statementDTO, netWorkType);
        const receipt = statement.addressResolutionStatements[0];
        const hash = receipt.generateHash(NetworkType_1.NetworkType.MAIN_NET);
        chai_1.expect(hash).to.be.equal('952225717E26295B97F9A35E719CA1319114CCF23C927BCBD14E7A7AA4BAC617');
    });
    it('should generate hash for TransactionStatement', () => {
        const statement = CreateReceiptFromDTO_1.CreateStatementFromDTO(statementDTO, netWorkType);
        const receipt = statement.transactionStatements[0];
        const hash = receipt.generateHash();
        chai_1.expect(hash).to.be.equal('78E5F66EC55D1331646528F9BF7EC247C68F58E651223E7F05CBD4FBF0BF88FA');
    });
});
//# sourceMappingURL=Receipt.spec.js.map