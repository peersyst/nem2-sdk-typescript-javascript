"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const Address_1 = require("../../../src/model/account/Address");
const NetworkType_1 = require("../../../src/model/blockchain/NetworkType");
const Account_1 = require("../../../src/model/account/Account");
const Address_Decoded_Size = 25;
describe('Address', () => {
    const publicKey = 'c2f93346e27ce6ad1a9f8f5e3066f8326593a406bdf357acb041e2f9ab402efe'.toUpperCase();
    const NIS_PublicKey = 'c5f54ba980fcbb657dbaaa42700539b207873e134d2375efeab5f1ab52f87844';
    it('createComplete an address given publicKey + NetworkType.MIJIN_TEST', () => {
        const address = Address_1.Address.createFromPublicKey(publicKey, NetworkType_1.NetworkType.MIJIN_TEST);
        chai_1.expect(address.plain()).to.be.equal('SCTVW23D2MN5VE4AQ4TZIDZENGNOZXPRPRLIKCF2');
        chai_1.expect(address.networkType).to.be.equal(NetworkType_1.NetworkType.MIJIN_TEST);
    });
    it('print the address in pretty format', () => {
        const address = Address_1.Address.createFromPublicKey(publicKey, NetworkType_1.NetworkType.MIJIN_TEST);
        chai_1.expect(address.pretty()).to.be.equal('SCTVW2-3D2MN5-VE4AQ4-TZIDZE-NGNOZX-PRPRLI-KCF2');
    });
    it('createComplete an address given publicKey + NetworkType.MIJIN', () => {
        const address = Address_1.Address.createFromPublicKey(publicKey, NetworkType_1.NetworkType.MIJIN);
        chai_1.expect(address.plain()).to.be.equal('MCTVW23D2MN5VE4AQ4TZIDZENGNOZXPRPR72DYSX');
        chai_1.expect(address.networkType).to.be.equal(NetworkType_1.NetworkType.MIJIN);
    });
    it('createComplete an address given publicKey + NetworkType.MAIN_NET', () => {
        const address = Address_1.Address.createFromPublicKey(publicKey, NetworkType_1.NetworkType.MAIN_NET);
        chai_1.expect(address.plain()).not.to.be.equal('NDD2CT6LQLIYQ56KIXI3ENTM6EK3D44P5JFXJ4R4');
        chai_1.expect(address.networkType).to.be.equal(NetworkType_1.NetworkType.MAIN_NET);
    });
    it('createComplete an address given publicKey + NetworkType.TEST_NET', () => {
        const address = Address_1.Address.createFromPublicKey(publicKey, NetworkType_1.NetworkType.TEST_NET);
        chai_1.expect(address.plain()).not.to.be.equal('TDD2CT6LQLIYQ56KIXI3ENTM6EK3D44P5KZPFMK2');
        chai_1.expect(address.networkType).to.be.equal(NetworkType_1.NetworkType.TEST_NET);
    });
    /**
     * @see https://raw.githubusercontent.com/nemtech/test-vectors/master/1.test-address-nis1.json
     */
    it('createComplete an address given publicKey + NetworkType.MAIN_NET using NIS1 schema', () => {
        const address = Address_1.Address.createFromPublicKey(NIS_PublicKey, NetworkType_1.NetworkType.MAIN_NET);
        chai_1.expect(address.plain()).to.be.equal('NDD2CT6LQLIYQ56KIXI3ENTM6EK3D44P5JFXJ4R4');
        chai_1.expect(address.networkType).to.be.equal(NetworkType_1.NetworkType.MAIN_NET);
    });
    /**
     * @see https://raw.githubusercontent.com/nemtech/test-vectors/master/1.test-address-nis1.json
     */
    it('createComplete an address given publicKey + NetworkType.TEST_NET using NIS1 schema', () => {
        const address = Address_1.Address.createFromPublicKey(NIS_PublicKey, NetworkType_1.NetworkType.TEST_NET);
        chai_1.expect(address.plain()).to.be.equal('TDD2CT6LQLIYQ56KIXI3ENTM6EK3D44P5KZPFMK2');
        chai_1.expect(address.networkType).to.be.equal(NetworkType_1.NetworkType.TEST_NET);
    });
    it('createComplete an address given SCTVW23D2MN5VE4AQ4TZIDZENGNOZXPRPRLIKCF2', () => {
        const address = Address_1.Address.createFromRawAddress('SCTVW23D2MN5VE4AQ4TZIDZENGNOZXPRPRLIKCF2');
        chai_1.expect(address.networkType).to.be.equal(NetworkType_1.NetworkType.MIJIN_TEST);
    });
    it('createComplete an address given MCTVW23D2MN5VE4AQ4TZIDZENGNOZXPRPR72DYSX', () => {
        const address = Address_1.Address.createFromRawAddress('MCTVW23D2MN5VE4AQ4TZIDZENGNOZXPRPR72DYSX');
        chai_1.expect(address.networkType).to.be.equal(NetworkType_1.NetworkType.MIJIN);
    });
    it('createComplete an address given TCTVW23D2MN5VE4AQ4TZIDZENGNOZXPRPSDRSFRF', () => {
        const address = Address_1.Address.createFromRawAddress('NCTVW23D2MN5VE4AQ4TZIDZENGNOZXPRPQUJ2ZML');
        chai_1.expect(address.networkType).to.be.equal(NetworkType_1.NetworkType.MAIN_NET);
    });
    it('createComplete an address given TCTVW23D2MN5VE4AQ4TZIDZENGNOZXPRPSDRSFRF', () => {
        const address = Address_1.Address.createFromRawAddress('TCTVW23D2MN5VE4AQ4TZIDZENGNOZXPRPSDRSFRF');
        chai_1.expect(address.networkType).to.be.equal(NetworkType_1.NetworkType.TEST_NET);
    });
    it('createComplete an address given SDRDGF-TDLLCB-67D4HP-GIMIHP-NSRYRJ-RT7DOB-GWZY', () => {
        const address = Address_1.Address.createFromRawAddress('SDRDGF-TDLLCB-67D4HP-GIMIHP-NSRYRJ-RT7DOB-GWZY');
        chai_1.expect(address.networkType).to.be.equal(NetworkType_1.NetworkType.MIJIN_TEST);
        chai_1.expect(address.pretty()).to.be.equal('SDRDGF-TDLLCB-67D4HP-GIMIHP-NSRYRJ-RT7DOB-GWZY');
    });
    it('should throw Error when the address contain an invalid network identifier', () => {
        chai_1.expect(() => {
            Address_1.Address.createFromRawAddress('ZCTVW23D2MN5VE4AQ4TZIDZENGNOZXPRPSDRSFRF');
        }).to.throw('Address Network unsupported');
    });
    it('should throw Error when the address is not valid in length', () => {
        chai_1.expect(() => {
            Address_1.Address.createFromRawAddress('ZCTVW234AQ4TZIDZENGNOZXPRPSDRSFRF');
        }).to.throw('Address ZCTVW234AQ4TZIDZENGNOZXPRPSDRSFRF has to be 40 characters long');
    });
    it('should turn a lowercase address to uppercase', () => {
        const address = Address_1.Address.createFromRawAddress('tctvw23d2mn5ve4aq4tzidzengnozxprpsdrsfrf');
        chai_1.expect(address.plain()).to.be.equal('TCTVW23D2MN5VE4AQ4TZIDZENGNOZXPRPSDRSFRF');
    });
    it('should equal addresses', () => {
        const address = Address_1.Address.createFromRawAddress('TCTVW23D2MN5VE4AQ4TZIDZENGNOZXPRPSDRSFRF');
        const compareAddress = Address_1.Address.createFromRawAddress('TCTVW23D2MN5VE4AQ4TZIDZENGNOZXPRPSDRSFRF');
        chai_1.expect(address.equals(compareAddress)).to.be.equal(true);
    });
    it('should not equal addresses', () => {
        const address = Address_1.Address.createFromRawAddress('TCTVW23D2MN5VE4AQ4TZIDZENGNOZXPRPSDRSFRF');
        const compareAddress = Address_1.Address.createFromRawAddress('TCTMW23D2MN5VE4AQ4TZIDZENGNOZXPRPSDRSFRF');
        chai_1.expect(address.equals(compareAddress)).to.be.equal(false);
    });
    it('It creates the address from an encoded value', () => {
        let encoded = '917E7E29A01014C2F300000000000000000000000000000000';
        const address = Address_1.Address.createFromEncoded(encoded);
        chai_1.expect(address.encoded()).to.be.equal(encoded);
    });
    describe('isValidRawAddress', () => {
        it('returns true for valid address when generated', () => {
            // Assert:
            chai_1.expect(Address_1.Address.isValidRawAddress(Account_1.Account.generateNewAccount(NetworkType_1.NetworkType.MIJIN_TEST).address.plain(), NetworkType_1.NetworkType.MIJIN_TEST)).to.equal(true);
            chai_1.expect(Address_1.Address.isValidRawAddress(Account_1.Account.generateNewAccount(NetworkType_1.NetworkType.MAIN_NET).address.plain(), NetworkType_1.NetworkType.MAIN_NET)).to.equal(true);
            chai_1.expect(Address_1.Address.isValidRawAddress(Account_1.Account.generateNewAccount(NetworkType_1.NetworkType.MIJIN).address.plain(), NetworkType_1.NetworkType.MIJIN)).to.equal(true);
            chai_1.expect(Address_1.Address.isValidRawAddress(Account_1.Account.generateNewAccount(NetworkType_1.NetworkType.TEST_NET).address.plain(), NetworkType_1.NetworkType.TEST_NET)).to.equal(true);
        });
        it('returns true for valid address', () => {
            // Arrange:
            const rawAddress = 'SCHCZBZ6QVJAHGJTKYVPW5FBSO2IXXJQBPV5XE6P';
            // Assert:
            chai_1.expect(Address_1.Address.isValidRawAddress(rawAddress, NetworkType_1.NetworkType.MIJIN_TEST)).to.equal(true);
        });
        it('returns false for address with invalid checksum', () => {
            // Arrange:
            const rawAddress = 'SCHCZBZ6QVJAHGJTKYAPW5FBSO2IXXJQBPV5XE6P';
            // Assert:
            chai_1.expect(Address_1.Address.isValidRawAddress(rawAddress, NetworkType_1.NetworkType.MIJIN_TEST)).to.equal(false);
        });
        it('returns false for address with invalid hash', () => {
            // Arrange:
            const rawAddress = 'SCHCZBZ6QVJAHGJTKYVPW5FBSO2IXXJQBPV5XE7P';
            // Assert:
            chai_1.expect(Address_1.Address.isValidRawAddress(rawAddress, NetworkType_1.NetworkType.MIJIN_TEST)).to.equal(false);
        });
        it('returns false for address with invalid prefix', () => {
            // Arrange:
            const rawAddress = 'ACHCZBZ6QVJAHGJTKYVPW5FBSO2IXXJQBPV5XE6P';
            // Assert:
            chai_1.expect(Address_1.Address.isValidRawAddress(rawAddress, NetworkType_1.NetworkType.MIJIN_TEST)).to.equal(false);
        });
    });
    describe('isValidEncodedAddress', () => {
        it('returns true for valid address when generated', () => {
            // Assert:
            chai_1.expect(Address_1.Address.isValidEncodedAddress(Account_1.Account.generateNewAccount(NetworkType_1.NetworkType.MIJIN_TEST).address.encoded(), NetworkType_1.NetworkType.MIJIN_TEST)).to.equal(true);
            chai_1.expect(Address_1.Address.isValidEncodedAddress(Account_1.Account.generateNewAccount(NetworkType_1.NetworkType.MAIN_NET).address.encoded(), NetworkType_1.NetworkType.MAIN_NET)).to.equal(true);
            chai_1.expect(Address_1.Address.isValidEncodedAddress(Account_1.Account.generateNewAccount(NetworkType_1.NetworkType.MIJIN).address.encoded(), NetworkType_1.NetworkType.MIJIN)).to.equal(true);
            chai_1.expect(Address_1.Address.isValidEncodedAddress(Account_1.Account.generateNewAccount(NetworkType_1.NetworkType.TEST_NET).address.encoded(), NetworkType_1.NetworkType.TEST_NET)).to.equal(true);
        });
        it('returns true for valid encoded address', () => {
            // Arrange:
            const encoded = '9085215E4620D383C2DF70235B9EF7507F6A28EF6D16FD7B9C';
            // Assert:
            chai_1.expect(Address_1.Address.isValidEncodedAddress(encoded, NetworkType_1.NetworkType.MIJIN_TEST)).to.equal(true);
        });
        it('returns false for invalid hex encoded address', () => {
            // Arrange:
            const encoded = 'Z085215E4620D383C2DF70235B9EF7507F6A28EF6D16FD7B9C';
            // Assert:
            chai_1.expect(Address_1.Address.isValidEncodedAddress(encoded, NetworkType_1.NetworkType.MIJIN_TEST)).to.equal(false);
        });
        it('returns false for invalid encoded address', () => {
            // Arrange: changed last char
            const encoded = '9085215E4620D383C2DF70235B9EF7507F6A28EF6D16FD7B9D';
            // Assert:
            chai_1.expect(Address_1.Address.isValidEncodedAddress(encoded, NetworkType_1.NetworkType.MIJIN_TEST)).to.equal(false);
        });
        it('returns false for encoded address with wrong length', () => {
            // Arrange: added ABC
            const encoded = '9085215E4620D383C2DF70235B9EF7607F6A28EF6D16FD7B9C';
            // Assert:
            chai_1.expect(Address_1.Address.isValidEncodedAddress(encoded, NetworkType_1.NetworkType.MIJIN_TEST)).to.equal(false);
        });
        it('adding leading or trailing white space invalidates encoded address', () => {
            // Arrange:
            const encoded = '9085215E4620D383C2DF70235B9EF7507F6A28EF6D16FD7B9C';
            // Assert:
            chai_1.expect(Address_1.Address.isValidEncodedAddress(`   \t    ${encoded}`, NetworkType_1.NetworkType.MIJIN_TEST)).to.equal(false);
            chai_1.expect(Address_1.Address.isValidEncodedAddress(`${encoded}   \t    `, NetworkType_1.NetworkType.MIJIN_TEST)).to.equal(false);
            chai_1.expect(Address_1.Address.isValidEncodedAddress(`   \t    ${encoded}   \t    `, NetworkType_1.NetworkType.MIJIN_TEST)).to.equal(false);
        });
    });
});
//# sourceMappingURL=Address.spec.js.map