"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const MosaicFlags_1 = require("../../../src/model/mosaic/MosaicFlags");
const UInt64_1 = require("../../../src/model/UInt64");
describe('MosaicFlags', () => {
    it('should createComplete an MosaicFlags object with constructor', () => {
        const mosaicFlags = new MosaicFlags_1.MosaicFlags(7);
        chai_1.expect(mosaicFlags.supplyMutable).to.be.equal(true);
        chai_1.expect(mosaicFlags.transferable).to.be.equal(true);
        chai_1.expect(mosaicFlags.restrictable).to.be.equal(true);
    });
    it('should createComplete an mosaicFlags object with static method', () => {
        const duration = UInt64_1.UInt64.fromUint(1000);
        const mosaicFlags = MosaicFlags_1.MosaicFlags.create(false, false, false);
        chai_1.expect(mosaicFlags.supplyMutable).to.be.equal(false);
        chai_1.expect(mosaicFlags.transferable).to.be.equal(false);
        chai_1.expect(mosaicFlags.restrictable).to.be.equal(false);
    });
    it('should return corredt flags value', () => {
        let mosaicFlags = MosaicFlags_1.MosaicFlags.create(false, false, false);
        chai_1.expect(mosaicFlags.getValue()).to.be.equal(0);
        mosaicFlags = MosaicFlags_1.MosaicFlags.create(true, false, false);
        chai_1.expect(mosaicFlags.getValue()).to.be.equal(1);
        mosaicFlags = MosaicFlags_1.MosaicFlags.create(false, true, false);
        chai_1.expect(mosaicFlags.getValue()).to.be.equal(2);
        mosaicFlags = MosaicFlags_1.MosaicFlags.create(false, false, true);
        chai_1.expect(mosaicFlags.getValue()).to.be.equal(4);
        mosaicFlags = MosaicFlags_1.MosaicFlags.create(true, true, true);
        chai_1.expect(mosaicFlags.getValue()).to.be.equal(7);
        mosaicFlags = MosaicFlags_1.MosaicFlags.create(true, false, true);
        chai_1.expect(mosaicFlags.getValue()).to.be.equal(5);
        mosaicFlags = MosaicFlags_1.MosaicFlags.create(true, true, false);
        chai_1.expect(mosaicFlags.getValue()).to.be.equal(3);
    });
    it('should return corredt flags json object', () => {
        const mosaicFlags = MosaicFlags_1.MosaicFlags.create(true, true, true);
        chai_1.expect(mosaicFlags.toDTO().flags).to.be.equal(7);
    });
});
//# sourceMappingURL=MosaicFlags.spec.js.map