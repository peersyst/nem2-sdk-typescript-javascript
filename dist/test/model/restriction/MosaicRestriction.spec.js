"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const Address_1 = require("../../../src/model/account/Address");
const MosaicId_1 = require("../../../src/model/mosaic/MosaicId");
const MosaicAddressRestriction_1 = require("../../../src/model/restriction/MosaicAddressRestriction");
const MosaicGlobalRestriction_1 = require("../../../src/model/restriction/MosaicGlobalRestriction");
const MosaicGlobalRestrictionItem_1 = require("../../../src/model/restriction/MosaicGlobalRestrictionItem");
const MosaicRestrictionEntryType_1 = require("../../../src/model/restriction/MosaicRestrictionEntryType");
const MosaicRestrictionType_1 = require("../../../src/model/restriction/MosaicRestrictionType");
const conf_spec_1 = require("../../conf/conf.spec");
describe('MosaicRestrictions', () => {
    let account;
    const hash = '57F7DA205008026C776CB6AED843393F04CD458E0AA2D9F1D5F31A402072B2D6';
    before(() => {
        account = conf_spec_1.TestingAccount;
    });
    it('should createComplete an MosaicAddressRestriction object', () => {
        const mosaicAddressRestrictionDTO = {
            compositeHash: hash,
            entryType: 0,
            mosaicId: '85BBEA6CC462B244',
            targetAddress: '9050B9837EFAB4BBE8A4B9BB32D812F9885C00D8FC1650E142',
            restrictions: [{
                    key: 'testKey',
                    value: 'testValue',
                }],
        };
        const mosaicAddressRestriction = new MosaicAddressRestriction_1.MosaicAddressRestriction(mosaicAddressRestrictionDTO.compositeHash, mosaicAddressRestrictionDTO.entryType, new MosaicId_1.MosaicId(mosaicAddressRestrictionDTO.mosaicId), Address_1.Address.createFromEncoded(mosaicAddressRestrictionDTO.targetAddress), new Map().set(mosaicAddressRestrictionDTO.restrictions[0].key, mosaicAddressRestrictionDTO.restrictions[0].value));
        chai_1.expect(mosaicAddressRestriction.compositeHash).to.be.equal(hash);
        chai_1.expect(mosaicAddressRestriction.entryType).to.be.equal(MosaicRestrictionEntryType_1.MosaicRestrictionEntryType.ADDRESS);
        chai_1.expect(mosaicAddressRestriction.targetAddress.plain())
            .to.be.equal(Address_1.Address.createFromEncoded('9050B9837EFAB4BBE8A4B9BB32D812F9885C00D8FC1650E142').plain());
        chai_1.expect(mosaicAddressRestriction.restrictions.size).to.be.equal(1);
        chai_1.expect(mosaicAddressRestriction.restrictions.get('testKey')).to.not.be.equal(undefined);
    });
    it('should createComplete an MosaicGlobalRestrictionItem object', () => {
        const mosaicGlobalRestrictionItemDTO = {
            referenceMosaicId: '85BBEA6CC462B244',
            restrictionValue: '123',
            restrictionType: 1,
        };
        const mosaicGlobalRestrictionItem = new MosaicGlobalRestrictionItem_1.MosaicGlobalRestrictionItem(new MosaicId_1.MosaicId(mosaicGlobalRestrictionItemDTO.referenceMosaicId), mosaicGlobalRestrictionItemDTO.restrictionValue, mosaicGlobalRestrictionItemDTO.restrictionType);
        chai_1.expect(mosaicGlobalRestrictionItem.referenceMosaicId.toHex()).to.be.equal('85BBEA6CC462B244');
        chai_1.expect(mosaicGlobalRestrictionItem.restrictionValue).to.be.equal('123');
        chai_1.expect(mosaicGlobalRestrictionItem.restrictionType).to.be.equal(MosaicRestrictionType_1.MosaicRestrictionType.EQ);
    });
    it('should createComplete an MosaicGlobalRestriction object', () => {
        const mosaicGlobalRestrictionDTO = {
            compositeHash: hash,
            entryType: 0,
            mosaicId: '85BBEA6CC462B244',
            restrictions: [{
                    key: 'testKey',
                    restriction: {
                        referenceMosaicId: '85BBEA6CC462B244',
                        restrictionValue: '123',
                        restrictionType: 1,
                    },
                }],
        };
        const mosaicGlobalRestriction = new MosaicGlobalRestriction_1.MosaicGlobalRestriction(mosaicGlobalRestrictionDTO.compositeHash, mosaicGlobalRestrictionDTO.entryType, new MosaicId_1.MosaicId(mosaicGlobalRestrictionDTO.mosaicId), new Map().set(mosaicGlobalRestrictionDTO.restrictions[0].key, new MosaicGlobalRestrictionItem_1.MosaicGlobalRestrictionItem(new MosaicId_1.MosaicId(mosaicGlobalRestrictionDTO.restrictions[0].restriction.referenceMosaicId), mosaicGlobalRestrictionDTO.restrictions[0].restriction.restrictionValue, mosaicGlobalRestrictionDTO.restrictions[0].restriction.restrictionType)));
        chai_1.expect(mosaicGlobalRestriction.compositeHash).to.be.equal(hash);
        chai_1.expect(mosaicGlobalRestriction.entryType).to.be.equal(MosaicRestrictionEntryType_1.MosaicRestrictionEntryType.ADDRESS);
        chai_1.expect(mosaicGlobalRestriction.mosaicId.toHex()).to.be.equal('85BBEA6CC462B244');
        chai_1.expect(mosaicGlobalRestriction.restrictions.size).to.be.equal(1);
        chai_1.expect(mosaicGlobalRestriction.restrictions.get('testKey')).to.not.be.equal(undefined);
        chai_1.expect(mosaicGlobalRestriction.restrictions.get('testKey').referenceMosaicId.toHex()).to.be.equal('85BBEA6CC462B244');
        chai_1.expect(mosaicGlobalRestriction.restrictions.get('testKey').restrictionValue).to.be.equal('123');
        chai_1.expect(mosaicGlobalRestriction.restrictions.get('testKey').restrictionType).to.be.equal(MosaicRestrictionType_1.MosaicRestrictionType.EQ);
    });
});
//# sourceMappingURL=MosaicRestriction.spec.js.map