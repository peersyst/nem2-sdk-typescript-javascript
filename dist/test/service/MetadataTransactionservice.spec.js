"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const rxjs_1 = require("rxjs");
const ts_mockito_1 = require("ts-mockito");
const Convert_1 = require("../../src/core/format/Convert");
const MetadataHttp_1 = require("../../src/infrastructure/MetadataHttp");
const NetworkType_1 = require("../../src/model/blockchain/NetworkType");
const Metadata_1 = require("../../src/model/metadata/Metadata");
const MetadataEntry_1 = require("../../src/model/metadata/MetadataEntry");
const MetadataType_1 = require("../../src/model/metadata/MetadataType");
const MosaicId_1 = require("../../src/model/mosaic/MosaicId");
const NamespaceId_1 = require("../../src/model/namespace/NamespaceId");
const Deadline_1 = require("../../src/model/transaction/Deadline");
const TransactionType_1 = require("../../src/model/transaction/TransactionType");
const UInt64_1 = require("../../src/model/UInt64");
const MetadataTransactionService_1 = require("../../src/service/MetadataTransactionService");
const conf_spec_1 = require("../conf/conf.spec");
describe('MetadataTransactionService', () => {
    let account;
    let metadataTransactionService;
    const key = UInt64_1.UInt64.fromHex('85BBEA6CC462B244');
    const value = 'TEST';
    const deltaValue = 'dalta';
    const targetIdHex = '941299B2B7E1291C';
    before(() => {
        account = conf_spec_1.TestingAccount;
        const mockMetadataHttp = ts_mockito_1.mock(MetadataHttp_1.MetadataHttp);
        ts_mockito_1.when(mockMetadataHttp
            .getAccountMetadataByKeyAndSender(ts_mockito_1.deepEqual(account.address), key.toHex(), account.publicKey))
            .thenReturn(rxjs_1.of(mockMetadata(MetadataType_1.MetadataType.Account)));
        ts_mockito_1.when(mockMetadataHttp
            .getMosaicMetadataByKeyAndSender(ts_mockito_1.deepEqual(new MosaicId_1.MosaicId(targetIdHex)), key.toHex(), account.publicKey))
            .thenReturn(rxjs_1.of(mockMetadata(MetadataType_1.MetadataType.Mosaic)));
        ts_mockito_1.when(mockMetadataHttp
            .getNamespaceMetadataByKeyAndSender(ts_mockito_1.deepEqual(NamespaceId_1.NamespaceId.createFromEncoded(targetIdHex)), key.toHex(), account.publicKey))
            .thenReturn(rxjs_1.of(mockMetadata(MetadataType_1.MetadataType.Namespace)));
        const metadataHttp = ts_mockito_1.instance(mockMetadataHttp);
        metadataTransactionService = new MetadataTransactionService_1.MetadataTransactionService(metadataHttp);
    });
    it('should create AccountMetadataTransaction', (done) => {
        metadataTransactionService.createMetadataTransaction(Deadline_1.Deadline.create(), NetworkType_1.NetworkType.MIJIN_TEST, MetadataType_1.MetadataType.Account, account.publicAccount, key, value + deltaValue, account.publicAccount)
            .subscribe((transaction) => {
            chai_1.expect(transaction.type).to.be.equal(TransactionType_1.TransactionType.ACCOUNT_METADATA_TRANSACTION);
            chai_1.expect(transaction.scopedMetadataKey.toHex()).to.be.equal(key.toHex());
            chai_1.expect(Convert_1.Convert.utf8ToHex(transaction.value))
                .to.be.equal(Convert_1.Convert.xor(Convert_1.Convert.utf8ToUint8(value), Convert_1.Convert.utf8ToUint8(value + deltaValue)));
            chai_1.expect(transaction.valueSizeDelta).to.be.equal(deltaValue.length);
            chai_1.expect(transaction.targetPublicKey).to.be.equal(account.publicKey);
            done();
        });
    });
    it('should create MosaicMetadataTransaction', (done) => {
        metadataTransactionService.createMetadataTransaction(Deadline_1.Deadline.create(), NetworkType_1.NetworkType.MIJIN_TEST, MetadataType_1.MetadataType.Mosaic, account.publicAccount, key, value + deltaValue, account.publicAccount, new MosaicId_1.MosaicId(targetIdHex))
            .subscribe((transaction) => {
            chai_1.expect(transaction.type).to.be.equal(TransactionType_1.TransactionType.MOSAIC_METADATA_TRANSACTION);
            chai_1.expect(transaction.scopedMetadataKey.toHex()).to.be.equal(key.toHex());
            chai_1.expect(Convert_1.Convert.utf8ToHex(transaction.value))
                .to.be.equal(Convert_1.Convert.xor(Convert_1.Convert.utf8ToUint8(value), Convert_1.Convert.utf8ToUint8(value + deltaValue)));
            chai_1.expect(transaction.targetMosaicId.toHex()).to.be.equal(targetIdHex);
            chai_1.expect(transaction.valueSizeDelta).to.be.equal(deltaValue.length);
            chai_1.expect(transaction.targetPublicKey).to.be.equal(account.publicKey);
            done();
        });
    });
    it('should create NamespaceMetadataTransaction', (done) => {
        metadataTransactionService.createMetadataTransaction(Deadline_1.Deadline.create(), NetworkType_1.NetworkType.MIJIN_TEST, MetadataType_1.MetadataType.Namespace, account.publicAccount, key, value + deltaValue, account.publicAccount, NamespaceId_1.NamespaceId.createFromEncoded(targetIdHex))
            .subscribe((transaction) => {
            chai_1.expect(transaction.type).to.be.equal(TransactionType_1.TransactionType.NAMESPACE_METADATA_TRANSACTION);
            chai_1.expect(transaction.scopedMetadataKey.toHex()).to.be.equal(key.toHex());
            chai_1.expect(Convert_1.Convert.utf8ToHex(transaction.value))
                .to.be.equal(Convert_1.Convert.xor(Convert_1.Convert.utf8ToUint8(value), Convert_1.Convert.utf8ToUint8(value + deltaValue)));
            chai_1.expect(transaction.targetNamespaceId.toHex()).to.be.equal(targetIdHex);
            chai_1.expect(transaction.valueSizeDelta).to.be.equal(deltaValue.length);
            chai_1.expect(transaction.targetPublicKey).to.be.equal(account.publicKey);
            done();
        });
    });
    it('should throw error with invalid metadata type', () => {
        chai_1.expect(() => {
            metadataTransactionService.createMetadataTransaction(Deadline_1.Deadline.create(), NetworkType_1.NetworkType.MIJIN_TEST, 99, account.publicAccount, key, value + deltaValue, account.publicAccount);
        }).to.throw(Error, 'Metadata type invalid');
    });
    it('should throw error with invalid mosaicId', () => {
        chai_1.expect(() => {
            metadataTransactionService.createMetadataTransaction(Deadline_1.Deadline.create(), NetworkType_1.NetworkType.MIJIN_TEST, MetadataType_1.MetadataType.Mosaic, account.publicAccount, key, value + deltaValue, account.publicAccount);
        }).to.throw(Error, 'TargetId for MosaicMetadataTransaction is invalid');
    });
    it('should throw error with invalid NamespaceId', () => {
        chai_1.expect(() => {
            metadataTransactionService.createMetadataTransaction(Deadline_1.Deadline.create(), NetworkType_1.NetworkType.MIJIN_TEST, MetadataType_1.MetadataType.Namespace, account.publicAccount, key, value + deltaValue, account.publicAccount);
        }).to.throw(Error, 'TargetId for NamespaceMetadataTransaction is invalid');
    });
    function mockMetadata(type) {
        let targetId;
        if (type === MetadataType_1.MetadataType.Account) {
            targetId = undefined;
        }
        else if (type === MetadataType_1.MetadataType.Mosaic) {
            targetId = new MosaicId_1.MosaicId(targetIdHex);
        }
        else if (type === MetadataType_1.MetadataType.Namespace) {
            targetId = NamespaceId_1.NamespaceId.createFromEncoded(targetIdHex);
        }
        return new Metadata_1.Metadata('59DFBA84B2E9E7000135E80C', new MetadataEntry_1.MetadataEntry('5E628EA59818D97AA4118780D9A88C5512FCE7A21C195E1574727EFCE5DF7C0D', account.publicKey, account.publicKey, key, MetadataType_1.MetadataType.Account, value, targetId));
    }
});
//# sourceMappingURL=MetadataTransactionservice.spec.js.map