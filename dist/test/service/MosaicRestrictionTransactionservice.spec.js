"use strict";
/*
 * Copyright 2019 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const rxjs_1 = require("rxjs");
const ts_mockito_1 = require("ts-mockito");
const KeyGenerator_1 = require("../../src/core/format/KeyGenerator");
const RestrictionMosaicHttp_1 = require("../../src/infrastructure/RestrictionMosaicHttp");
const NetworkType_1 = require("../../src/model/blockchain/NetworkType");
const MosaicId_1 = require("../../src/model/mosaic/MosaicId");
const MosaicAddressRestriction_1 = require("../../src/model/restriction/MosaicAddressRestriction");
const MosaicGlobalRestriction_1 = require("../../src/model/restriction/MosaicGlobalRestriction");
const MosaicGlobalRestrictionItem_1 = require("../../src/model/restriction/MosaicGlobalRestrictionItem");
const MosaicRestrictionEntryType_1 = require("../../src/model/restriction/MosaicRestrictionEntryType");
const MosaicRestrictionType_1 = require("../../src/model/restriction/MosaicRestrictionType");
const Deadline_1 = require("../../src/model/transaction/Deadline");
const TransactionType_1 = require("../../src/model/transaction/TransactionType");
const UInt64_1 = require("../../src/model/UInt64");
const MosaicRestrictionTransactionService_1 = require("../../src/service/MosaicRestrictionTransactionService");
const conf_spec_1 = require("../conf/conf.spec");
describe('MosaicRestrictionTransactionService', () => {
    let account;
    let mosaicId;
    let referenceMosaicId;
    let mosaicRestrictionTransactionService;
    const key = KeyGenerator_1.KeyGenerator.generateUInt64Key('TestKey');
    const invalidKey = KeyGenerator_1.KeyGenerator.generateUInt64Key('9999');
    let mosaicIdWrongKey;
    const globalRestrictionValue = '1000';
    const globalRestrictionType = MosaicRestrictionType_1.MosaicRestrictionType.LE;
    const addressRestrictionValue = '10';
    before(() => {
        account = conf_spec_1.TestingAccount;
        mosaicId = new MosaicId_1.MosaicId('85BBEA6CC462B244');
        mosaicIdWrongKey = new MosaicId_1.MosaicId('85BBEA6CC462B288');
        referenceMosaicId = new MosaicId_1.MosaicId('1AB129B545561E6A');
        const mockRestrictionHttp = ts_mockito_1.mock(RestrictionMosaicHttp_1.RestrictionMosaicHttp);
        ts_mockito_1.when(mockRestrictionHttp
            .getMosaicGlobalRestriction(ts_mockito_1.deepEqual(mosaicId)))
            .thenReturn(rxjs_1.of(mockGlobalRestriction()));
        ts_mockito_1.when(mockRestrictionHttp
            .getMosaicGlobalRestriction(ts_mockito_1.deepEqual(mosaicIdWrongKey)))
            .thenThrow(new Error());
        ts_mockito_1.when(mockRestrictionHttp
            .getMosaicAddressRestriction(ts_mockito_1.deepEqual(mosaicId), ts_mockito_1.deepEqual(account.address)))
            .thenReturn(rxjs_1.of(mockAddressRestriction()));
        const restrictionHttp = ts_mockito_1.instance(mockRestrictionHttp);
        mosaicRestrictionTransactionService = new MosaicRestrictionTransactionService_1.MosaicRestrictionTransactionService(restrictionHttp);
    });
    it('should create MosaicGlobalRestriction Transaction', (done) => {
        mosaicRestrictionTransactionService.createMosaicGlobalRestrictionTransaction(Deadline_1.Deadline.create(), NetworkType_1.NetworkType.MIJIN_TEST, mosaicId, key, '2000', MosaicRestrictionType_1.MosaicRestrictionType.LE)
            .subscribe((transaction) => {
            chai_1.expect(transaction.type).to.be.equal(TransactionType_1.TransactionType.MOSAIC_GLOBAL_RESTRICTION);
            chai_1.expect(transaction.restrictionKey.toHex()).to.be.equal(key.toHex());
            chai_1.expect(transaction.previousRestrictionType).to.be.equal(globalRestrictionType);
            chai_1.expect(transaction.previousRestrictionValue.toString()).to.be.equal(globalRestrictionValue);
            chai_1.expect(transaction.referenceMosaicId.toHex()).to.be.equal(new MosaicId_1.MosaicId(UInt64_1.UInt64.fromUint(0).toDTO()).toHex());
            done();
        });
    });
    it('should create MosaicGlobalRestriction Transaction - with referenceMosaicId', (done) => {
        mosaicRestrictionTransactionService.createMosaicGlobalRestrictionTransaction(Deadline_1.Deadline.create(), NetworkType_1.NetworkType.MIJIN_TEST, mosaicId, key, '2000', MosaicRestrictionType_1.MosaicRestrictionType.LE, referenceMosaicId)
            .subscribe((transaction) => {
            chai_1.expect(transaction.type).to.be.equal(TransactionType_1.TransactionType.MOSAIC_GLOBAL_RESTRICTION);
            chai_1.expect(transaction.restrictionKey.toHex()).to.be.equal(key.toHex());
            chai_1.expect(transaction.previousRestrictionType).to.be.equal(globalRestrictionType);
            chai_1.expect(transaction.previousRestrictionValue.toString()).to.be.equal(globalRestrictionValue);
            chai_1.expect(transaction.referenceMosaicId.toHex()).to.be.equal(referenceMosaicId.toHex());
            done();
        });
    });
    it('should create MosaicAddressRestriction Transaction', (done) => {
        mosaicRestrictionTransactionService.createMosaicAddressRestrictionTransaction(Deadline_1.Deadline.create(), NetworkType_1.NetworkType.MIJIN_TEST, mosaicId, key, account.address, '2000')
            .subscribe((transaction) => {
            chai_1.expect(transaction.type).to.be.equal(TransactionType_1.TransactionType.MOSAIC_ADDRESS_RESTRICTION);
            chai_1.expect(transaction.restrictionKey.toHex()).to.be.equal(key.toHex());
            chai_1.expect(transaction.targetAddressToString()).to.be.equal(account.address.plain());
            chai_1.expect(transaction.previousRestrictionValue.toString()).to.be.equal(addressRestrictionValue);
            done();
        });
    });
    it('should throw error with invalid value / key', () => {
        chai_1.expect(() => {
            mosaicRestrictionTransactionService.createMosaicGlobalRestrictionTransaction(Deadline_1.Deadline.create(), NetworkType_1.NetworkType.MIJIN_TEST, mosaicId, key, 'wrong value', MosaicRestrictionType_1.MosaicRestrictionType.LE);
        }).to.throw(Error, 'RestrictionValue: wrong value is not a valid numeric string.');
        chai_1.expect(() => {
            mosaicRestrictionTransactionService.createMosaicAddressRestrictionTransaction(Deadline_1.Deadline.create(), NetworkType_1.NetworkType.MIJIN_TEST, mosaicId, key, account.address, 'wrong value');
        }).to.throw(Error, 'RestrictionValue: wrong value is not a valid numeric string.');
    });
    it('should throw error with invalid global restriction key - MosaicAddressRestriction', () => {
        chai_1.expect(() => {
            mosaicRestrictionTransactionService.createMosaicAddressRestrictionTransaction(Deadline_1.Deadline.create(), NetworkType_1.NetworkType.MIJIN_TEST, mosaicIdWrongKey, invalidKey, account.address, '2000');
        }).to.throw();
    });
    function mockGlobalRestriction() {
        return new MosaicGlobalRestriction_1.MosaicGlobalRestriction('59DFBA84B2E9E7000135E80C', MosaicRestrictionEntryType_1.MosaicRestrictionEntryType.GLOBAL, mosaicId, new Map()
            .set(key.toHex(), new MosaicGlobalRestrictionItem_1.MosaicGlobalRestrictionItem(referenceMosaicId, globalRestrictionValue, globalRestrictionType)));
    }
    function mockAddressRestriction() {
        return new MosaicAddressRestriction_1.MosaicAddressRestriction('59DFBA84B2E9E7000135E80C', MosaicRestrictionEntryType_1.MosaicRestrictionEntryType.GLOBAL, mosaicId, account.address, new Map()
            .set(key.toHex(), addressRestrictionValue));
    }
});
//# sourceMappingURL=MosaicRestrictionTransactionservice.spec.js.map